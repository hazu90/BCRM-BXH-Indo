CREATE OR REPLACE FUNCTION public.customercarehistory_getbylstuserid (
  _datenow timestamp,
  _lstuserid integer []
)
RETURNS TABLE (
  id integer,
  adminid integer,
  receivedby varchar,
  receiveddate timestamp,
  phoningstatus integer,
  crawleradminid integer,
  userid integer
) AS
$body$
DECLARE _aftercreateddate TIMESTAMP;
BEGIN
  _aftercreateddate = _datenow - INTERVAL '7' DAY;	
  
  RETURN QUERY
  SELECT cch.id
  		,cch.adminid
        ,cch.receivedby
        ,cch.receiveddate
        ,cch.phoningstatus
        ,cch.crawleradminid
        ,cch.userid
  FROM public.customercarehistory cch
  WHERE ((cch.phoningstatus IS NULL AND cch.receiveddate > _aftercreateddate)
  	 OR (cch.phoningstatus = 1	   AND cch.waitingoverreject > _datenow	 ))
    AND (cch.userid 	   = ANY(_lstuserid) )
    AND (cch.isreturned IS NULL OR NOT cch.isreturned)          ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;

COMMENT ON FUNCTION public.customercarehistory_getbylstuserid(_datenow timestamp, _lstuserid integer [])
IS 'HieuBV
21/02/2018';