CREATE OR REPLACE FUNCTION public.payment_getviewbypaymentid (
  _paymentid integer
)
RETURNS TABLE (
  id integer,
  customerid integer,
  money integer,
  createddate timestamp,
  createdby varchar,
  status smallint,
  feedback varchar
) AS
$body$
BEGIN
  RETURN QUERY
  SELECT p.id
  	  ,	 p.customerid
      ,	 p.money
      ,	 p.createddate	
      ,	 p.createdby
      ,	 p.status
      ,	 p.feedback
  FROM	 public.payment p
  WHERE  p.id = _paymentid ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;