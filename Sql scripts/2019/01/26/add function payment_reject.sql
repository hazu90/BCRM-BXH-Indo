CREATE OR REPLACE FUNCTION public.payment_reject (
  _id integer,
  _feedback varchar,
  _status smallint,
  _changestatustime timestamp,
  _changestatusby varchar
)
RETURNS void AS
$body$
BEGIN
  UPDATE	public.payment
  SET		feedback 		= _feedback
  		,	status			= _status
        ,  changestatustime = _changestatustime
        ,  changestatusby	= _changestatusby
  WHERE 	id				= _id ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;