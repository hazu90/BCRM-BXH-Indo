CREATE OR REPLACE FUNCTION public.payment_approved (
  _id integer,
  _revenueid integer,
  _verifiedpaymentdate timestamp,
  _changestatustime timestamp,
  _changestatusby varchar
)
RETURNS void AS
$body$
BEGIN
  UPDATE public.payment
  SET	 revenueid 				=  _revenueid
  	,	 verifiedpaymentdate	=  _verifiedpaymentdate
    ,	 changestatustime		=  _changestatustime
    ,	 changestatusby			=  _changestatusby
  WHERE  id						=  _id  	;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;