CREATE OR REPLACE FUNCTION public.payment_getlist (
  _creatorid integer,
  _status smallint,
  _createddatefrom timestamp,
  _createddateto timestamp,
  _pageindex integer,
  _pagesize integer
)
RETURNS TABLE (
  id integer,
  customerid integer,
  money integer,
  createddate timestamp,
  createdby varchar,
  status smallint,
  feedback varchar
) AS
$body$
BEGIN
  RETURN QUERY
  SELECT p.id
  	  ,	 p.customerid
      ,	 p.money
      ,	 p.createddate	
      ,	 p.createdby
      ,	 p.status
      ,	 p.feedback
  FROM	 payment p  
  WHERE  (_creatorid = 0 OR p.createduserid = _creatorid)
  	AND (_status 	= 0 OR p.status		   = _status )
    AND (_createddatefrom IS NULL OR p.createddate >= _createddatefrom )
    AND (_createddateto   IS NULL OR p.createddate <= _createddateto )  
  ORDER BY p.createddate DESC
  LIMIT  _pagesize
  OFFSET 	_pagesize * (_pageindex-1);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;