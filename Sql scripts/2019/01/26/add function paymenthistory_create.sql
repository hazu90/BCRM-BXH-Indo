CREATE OR REPLACE FUNCTION public.paymenthistory_create (
  _action integer,
  _actionby varchar,
  _actiontime timestamp,
  _data varchar,
  _paymentid integer
)
RETURNS void AS
$body$
BEGIN
  INSERT INTO paymenthistory( action, actionby, actiontime, data, paymentid)
  					  VALUES(_action,_actionby,_actiontime,_data,_paymentid) ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;