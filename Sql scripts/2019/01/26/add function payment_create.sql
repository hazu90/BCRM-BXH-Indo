CREATE OR REPLACE FUNCTION public.payment_create (
  _customerid integer,
  _money integer,
  _paymentdate timestamp,
  _createddate timestamp,
  _createdby varchar,
  _createduserid integer,
  _status smallint
)
RETURNS integer AS
$body$
DECLARE
	_id		INTEGER;
BEGIN
  INSERT INTO public.payment( customerid, money, paymentdate, createddate, createdby, createduserid, status)
  					  VALUES(_customerid,_money,_paymentdate,_createddate,_createdby,_createduserid,_status )
                 RETURNING id INTO _id  ;
  RETURN _id;               
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;