CREATE OR REPLACE FUNCTION public.payment_getlist_gettotalrecord (
  _creatorid integer,
  _status smallint,
  _createddatefrom timestamp,
  _createddateto timestamp
)
RETURNS integer AS
$body$
DECLARE
  _count INTEGER;
BEGIN
  SELECT COUNT(1) INTO _count
  FROM	 public.payment p
  WHERE (_creatorid = 0 OR p.createduserid = _creatorid)
  	AND (_status 	= 0 OR p.status		   = _status )
    AND (_createddatefrom IS NULL OR p.createddate >= _createddatefrom )
    AND (_createddateto   IS NULL OR p.createddate <= _createddateto ) ;
   RETURN _count; 
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;