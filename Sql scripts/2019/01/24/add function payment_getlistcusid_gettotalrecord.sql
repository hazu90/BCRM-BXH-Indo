CREATE OR REPLACE FUNCTION public.payment_getlistcusid_gettotalrecord (
  _customerid integer
)
RETURNS integer AS
$body$
DECLARE
	_count INTEGER;
BEGIN
  SELECT COUNT(1) INTO _count
  FROM	 payment p  
  WHERE  p.customerid = _customerid;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;