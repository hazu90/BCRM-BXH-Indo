CREATE OR REPLACE FUNCTION public.payment_getlistcusid (
  _customerid integer,
  _pageindex integer,
  _pagesize integer
)
RETURNS TABLE (
  id integer,
  customerid integer,
  money integer,
  createddate timestamp,
  createdby varchar,
  status smallint
) AS
$body$
BEGIN
 SELECT p.id
  	  ,	 p.customerid
      ,	 p.money
      ,	 p.createddate	
      ,	 p.createdby
      ,	 p.status
  FROM	 payment p  
  WHERE  p.customerid = _customerid 
  ORDER BY p.createddate DESC
  LIMIT  _pagesize
  OFFSET 	_pagesize * (_pageindex-1);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;