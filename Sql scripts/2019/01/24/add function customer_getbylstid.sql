CREATE OR REPLACE FUNCTION public.customer_getbylstid (
  _lstcustomerid integer []
)
RETURNS TABLE (
  id integer,
  fullname varchar,
  phonenumber varchar,
  status integer,
  email varchar
) AS
$body$
BEGIN
  RETURN QUERY	
  SELECT cust.id
  	, 	 cust.fullname
    ,	 cust.phonenumber
    ,	 cust.status
    ,	 cust.email	
  FROM	 public.customer cust
  WHERE  cust.id = ANY(_lstcustomerid);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;