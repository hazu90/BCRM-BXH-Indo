CREATE OR REPLACE FUNCTION public.customeractiveinmonth_getbycustomeridandmonth (
  _customerid integer,
  _month integer
)
RETURNS TABLE (
  id integer,
  customerid integer,
  month integer,
  postingcount integer,
  logincount integer,
  isactive boolean
) AS
$body$
BEGIN
  RETURN QUERY
  SELECT cam.id
  	,	 cam.customerid
    ,	 cam.month
    ,	 cam.postingcount
    ,	 cam.logincount
    ,	 cam.isactive
  FROM	 public.customeractiveinmonth cam
  WHERE  cam.customerid = _customerid
  	AND  cam.month	    = _month ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;