CREATE OR REPLACE FUNCTION public.customer_selectphonenumber_v03 (
  _phonenumber varchar []
)
RETURNS TABLE (
  id integer,
  fullname varchar,
  phonenumber varchar,
  status integer,
  assignto varchar,
  lastmodifieddate timestamp,
  cityid integer,
  icaredate integer,
  caredate timestamp,
  startcaredate timestamp,
  isdeleted boolean,
  createddate timestamp,
  postingtype smallint,
  cityname varchar,
  email varchar,
  createdby varchar,
  postingassistant varchar,
  postingassistancestatus smallint,
  isblacklist boolean,
  postingnumber integer,
  address varchar,
  isactive boolean
) AS
$body$
SELECT	cust.id
      ,	cust.fullname
      ,	cust.phonenumber
      ,	cust.status
      ,	cust.assignto
      ,	cust.lastmodifieddate
      ,	cust.cityid
      ,	cust.icaredate
      ,	cust.caredate
      ,	cust.startcaredate
      ,	cust.isdeleted
      ,	cust.createddate
      , cust.postingtype
      ,   ct.name		AS cityname
      ,	cust.email
      ,	cust.createdby
      ,	cust.postingassistant
      ,	cust.postingassistancestatus
      ,	cust.isblacklist
      ,	cls.postingnumber
      ,	cust.address
      , cam.isactive
FROM		customer	cust
  LEFT JOIN city ct  ON cust.cityid  = ct.id
  INNER JOIN	profile		p	ON	cust.id	=	p.customerid
  LEFT JOIN customerlistingstatistic cls  ON cust.id = cls.customerid
  LEFT JOIN customeractiveinmonth cam	  ON cust.id = cam.customerid
WHERE	p.phonenumber	= ANY(_phonenumber)
$body$
LANGUAGE 'sql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;