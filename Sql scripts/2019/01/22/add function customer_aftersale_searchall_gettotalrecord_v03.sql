CREATE OR REPLACE FUNCTION public.customer_aftersale_searchall_gettotalrecord_v03 (
  _filterkeyword varchar,
  _cityid integer,
  _districtid integer,
  _assignto varchar,
  _type integer,
  _startdate timestamp,
  _enddate timestamp,
  _createdby varchar,
  _postingassistant varchar,
  _postingnumberfrom integer,
  _postingnumberto integer,
  _isactive boolean,
  _activeinmonth integer
)
RETURNS integer AS
$body$
DECLARE
    _count		INTEGER;
BEGIN

  SELECT	COUNT(1)	into _count
  FROM		customer cust
	  LEFT JOIN city ct  ON cust.cityid  = ct.id
      LEFT JOIN customerlistingstatistic cls  ON cust.id = cls.customerid
      LEFT JOIN (SELECT customerid,isactive FROM customeractiveinmonth WHERE month = _activeinmonth) cam	ON cust.id = cam.customerid
  WHERE	(_filterkeyword	=	''
  		  OR lower(cust.fullname)	LIKE ('%' || _filterkeyword || '%')
          OR cust.email				LIKE ('%' || _filterkeyword || '%')
          OR lower(cust.address)    LIKE ('%' || _filterkeyword || '%')
      )
      AND	(_cityid		=	-1	OR	cust.cityid		=		_cityid)
      AND	(_districtid	=	-1	OR	cust.districtid	=		_districtid)
      AND 	(_assignto		=	''	OR	cust.assignto 	=		_assignto)
      AND 	(_type			=	-1	OR	cust.postingtype =		_type)
      AND   (cust.createddate	BETWEEN _startdate AND _enddate )
      AND   (_createdby		=   ''  OR  cust.createdby	=   _createdby )
      AND   (cust.postingassistant IS NULL	OR  cust.postingassistant = _postingassistant )
      AND 	(cust.isblacklist 	   IS NULL OR NOT cust.isblacklist )
      AND   (_postingnumberto = -1
      	 OR (_postingnumberfrom = -1 AND cls.postingnumber IS NULL )
      	 OR (cls.postingnumber > _postingnumberfrom AND cls.postingnumber < _postingnumberto ))
      AND   (_isactive IS NULL OR (_isactive AND cam.isactive ) OR (NOT _isactive AND (cam.isactive IS NULL OR NOT cam.isactive ) ) ) ;
  RETURN _count;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;