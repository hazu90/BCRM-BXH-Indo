CREATE OR REPLACE FUNCTION public.customeractiveinmonth_update (
  _customerid integer,
  _month integer,
  _postingcount integer,
  _logincount integer,
  _isactive boolean,
  _id integer
)
RETURNS void AS
$body$
BEGIN
  UPDATE  public.customeractiveinmonth
  SET	  customerid		= _customerid
  		, month				= _month
        , postingcount		= _postingcount
        , logincount		= _logincount
        , isactive			= _isactive
  WHERE   id				= _id      	;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;