CREATE OR REPLACE FUNCTION public.customeractiveinmonth_create (
  _customerid integer,
  _month integer,
  _postingcount integer,
  _logincount integer,
  _isactive boolean
)
RETURNS void AS
$body$
BEGIN
  INSERT INTO public.customeractiveinmonth( customerid, month, postingcount, logincount, isactive)
  									VALUES(_customerid,_month,_postingcount,_logincount,_isactive);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;