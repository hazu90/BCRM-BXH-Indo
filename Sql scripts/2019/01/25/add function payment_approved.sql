CREATE OR REPLACE FUNCTION public.payment_approved (
  _id integer,
  _revenueid integer,
  _verifiedpaymentdate timestamp,
  _approvedtime timestamp,
  _approvedby varchar
)
RETURNS void AS
$body$
BEGIN
  UPDATE public.payment
  SET	 revenueid 				=  _revenueid
  	,	 verifiedpaymentdate	=  _verifiedpaymentdate
    ,	 approvedtime			=  _approvedtime
    ,	 approvedby				=  _approvedby
  WHERE  id						=  _id  	;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;