CREATE OR REPLACE FUNCTION public.revenue_create (
  _customerid integer,
  _receiveduserid integer,
  _money integer,
  _verifieddate timestamp,
  _admintransactionid integer
)
RETURNS integer AS
$body$
DECLARE 
	_id	 INTEGER;
BEGIN
  INSERT INTO public.revenue( customerid, receiveduserid, money, verifieddate, admintransactionid)
  					  VALUES(_customerid,_receiveduserid,_money,_verifieddate,_admintransactionid)
                RETURNING id INTO _id      ;
  RETURN _id;              
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;