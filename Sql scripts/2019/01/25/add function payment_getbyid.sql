CREATE OR REPLACE FUNCTION public.payment_getbyid (
  _id integer
)
RETURNS TABLE (
  id integer,
  customerid integer,
  money integer,
  createddate timestamp,
  createdby varchar,
  status smallint,
  paymentdate timestamp,
  verifiedpaymentdate timestamp,
  createduserid integer,
  revenueid integer
) AS
$body$
BEGIN
  RETURN QUERY
  SELECT p.id
  	,	 p.customerid
    ,	 p.money
    ,	 p.createddate
    ,	 p.createdby
    ,	 p.status
    ,	 p.paymentdate
    ,	 p.verifiedpaymentdate
    ,	 p.createduserid
    ,	 p.revenueid
  FROM	 public.payment p  
  WHERE  p.id = _id ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;