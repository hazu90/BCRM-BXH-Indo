CREATE OR REPLACE FUNCTION public.customer_getbyemail (
  _email varchar
)
RETURNS TABLE (
  id integer,
  fullname varchar,
  phonenumber varchar,
  status integer,
  type integer,
  email varchar,
  description varchar,
  regionid integer,
  cityid integer,
  districtid integer,
  assignto varchar,
  caredate timestamp,
  icaredate integer,
  startcaredate timestamp,
  companyid integer,
  isdeleted boolean,
  createdby varchar,
  createddate timestamp,
  lastmodifiedby varchar,
  lastmodifieddate timestamp,
  address varchar,
  postingtype smallint,
  postingassistant varchar,
  postingassistancestatus smallint,
  isblacklist boolean
) AS
$body$
SELECT 
  id,
  fullname,
  phonenumber,
  status,
  type,
  email,
  description,
  regionid,
  cityid,
  districtid,
  assignto,
  caredate,
  icaredate,
  startcaredate,
  companyid,
  isdeleted,
  createdby,
  createddate,
  lastmodifiedby,
  lastmodifieddate,
  address,
  postingtype,
  postingassistant,
  postingassistancestatus,
  isblacklist
FROM 	customer
WHERE 	email	=	_email
$body$
LANGUAGE 'sql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;