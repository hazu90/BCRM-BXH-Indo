CREATE OR REPLACE FUNCTION public.users_getlist_v03 (
  _groupids varchar
)
RETURNS TABLE (
  id integer,
  username varchar,
  displayname varchar,
  status integer,
  dob timestamp,
  groupid integer,
  lastactivitydate timestamp,
  groupname varchar,
  email varchar,
  ishiddenfromreport boolean
) AS
$body$
BEGIN
  CREATE TEMPORARY TABLE temp1 AS SELECT CAST(gr AS INTEGER) gr FROM  regexp_split_to_table(_groupids,',') AS Gr;

  RETURN QUERY SELECT
            usr.id
    ,		usr.username
    ,  		usr.displayname
    ,		usr.status
    ,		usr.dob
    ,		usr.groupid
    ,		usr.lastactivitydate
    ,		gr.name		AS groupname
    ,		usr.email
    ,		usr.ishiddenfromreport
  FROM   	public.users usr
    LEFT JOIN public.group gr ON usr.groupid = gr.id
  WHERE   _groupids='0' OR usr.groupid IN (SELECT gr FROM temp1);
END
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;