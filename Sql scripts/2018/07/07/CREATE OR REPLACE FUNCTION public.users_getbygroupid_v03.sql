CREATE OR REPLACE FUNCTION public.users_getbygroupid_v03 (
  _groupid integer
)
RETURNS TABLE (
  id integer,
  username varchar,
  email varchar,
  displayname varchar,
  mobile varchar,
  status integer,
  dob timestamp,
  avatar varchar,
  groupid integer,
  ishiddenfromreport boolean
) AS
$body$
BEGIN
 RETURN QUERY
 SELECT us.id
  		,us.username
        ,us.email
        ,us.displayname
        ,us.mobile
        ,us.status
        ,us.dob
        ,us.avatar
        ,us.groupid
        ,us.ishiddenfromreport
   FROM public.users us
   WHERE us.groupid  = _groupid;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
