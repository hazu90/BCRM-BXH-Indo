CREATE OR REPLACE FUNCTION public.users_updateishiddenfromreport_v03 (
  _id integer,
  _ishiddenfromreport boolean
)
RETURNS void AS
$body$
BEGIN
  UPDATE public.users
  SET ishiddenfromreport = _ishiddenfromreport
  WHERE id = _id ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;