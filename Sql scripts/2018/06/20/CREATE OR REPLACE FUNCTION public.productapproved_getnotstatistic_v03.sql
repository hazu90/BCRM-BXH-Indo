CREATE OR REPLACE FUNCTION public.productapproved_getnotstatistic_v03 (
  _limitrecord integer
)
RETURNS TABLE (
  memberid integer,
  productid integer,
  approveddate timestamp,
  iscrawler boolean,
  isdeleted boolean,
  processedcount integer
) AS
$body$
BEGIN
  RETURN QUERY	
  SELECT pa.memberid
  	,	 pa.productid
    ,	 pa.approveddate
    ,	 pa.iscrawler
    ,	 pa.isdeleted
    ,	 pa.processedcount
  FROM  public.productapproved pa
  WHERE (pa.isstatistic IS NULL OR NOT pa.isstatistic)
  	AND (pa.processedcount IS NULL OR pa.processedcount <4) 
  LIMIT _limitrecord ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;