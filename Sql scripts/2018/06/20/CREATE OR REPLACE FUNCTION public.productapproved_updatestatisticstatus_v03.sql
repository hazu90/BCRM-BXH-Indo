CREATE OR REPLACE FUNCTION public.productapproved_updatestatisticstatus_v03 (
  _productid integer,
  _isstatistic boolean,
  _proccesscount integer
)
RETURNS void AS
$body$
BEGIN
  UPDATE public.productapproved
  SET	 isstatistic 		= _isstatistic
  	,	 processedcount		= _proccesscount
  WHERE  productid   = _productid ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;