CREATE OR REPLACE FUNCTION public.productapproved_updateprocesscount_v03 (
  _productid integer,
  _processedcount integer
)
RETURNS void AS
$body$
BEGIN
  UPDATE public.productapproved
  SET	 processedcount = _processedcount
  WHERE  productid		= _productid ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;