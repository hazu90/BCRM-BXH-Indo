CREATE OR REPLACE FUNCTION public.failurelogging_create_v03 (
  _priority integer,
  _source varchar,
  _data varchar,
  _currentuser varchar,
  _message varchar,
  _createddate timestamp,
  _note varchar
)
RETURNS void AS
$body$
BEGIN
  INSERT INTO public.failurelogging(priority ,source ,data ,currentuser ,message ,createddate ,note )
  							VALUES (_priority,_source,_data,_currentuser,_message,_createddate,_note );
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;