CREATE OR REPLACE FUNCTION public.customer_aftersale_searchall_v03 (
  _filterkeyword varchar,
  _cityid integer,
  _districtid integer,
  _assignto varchar,
  _type integer,
  _startdate timestamp,
  _enddate timestamp,
  _pageindex integer,
  _pagesize integer,
  _createdby varchar,
  _postingassistant varchar,
  _postingnumberfrom integer,
  _postingnumberto integer
)
RETURNS TABLE (
  id integer,
  fullname varchar,
  phonenumber varchar,
  status integer,
  assignto varchar,
  lastmodifieddate timestamp,
  cityid integer,
  icaredate integer,
  caredate timestamp,
  startcaredate timestamp,
  isdeleted boolean,
  createddate timestamp,
  postingtype smallint,
  cityname varchar,
  email varchar,
  createdby varchar,
  postingassistant varchar,
  postingassistancestatus smallint,
  isblacklist boolean,
  postingnumber integer,
  address varchar
) AS
$body$
BEGIN
  RETURN QUERY
  SELECT	cust.id
        ,	cust.fullname
        ,	cust.phonenumber
        ,	cust.status
        ,	cust.assignto
        ,	cust.lastmodifieddate
        ,	cust.cityid
        ,	cust.icaredate
        ,	cust.caredate
        ,	cust.startcaredate
        ,	cust.isdeleted
        ,	cust.createddate
        ,   cust.postingtype
        ,   ct.name		AS cityname
        ,	cust.email
        ,	cust.createdby
        ,	cust.postingassistant
        ,	cust.postingassistancestatus
        ,	cust.isblacklist
        ,	cust.postingnumber
        ,	cust.address
  FROM		customer cust
  	   LEFT JOIN city ct  ON cust.cityid  = ct.id
  WHERE	(_filterkeyword	=	''
  		  OR lower(cust.fullname)	LIKE ('%' || _filterkeyword || '%')
          OR cust.email				LIKE ('%' || _filterkeyword || '%')
          OR lower(cust.address)    LIKE ('%' || _filterkeyword || '%')
      	)
      AND	(_cityid		=	-1	OR	cust.cityid		=	_cityid)
      AND	(_districtid	=	-1	OR	cust.districtid	=	_districtid)
      AND 	(_assignto		=	''	OR	cust.assignto 	=	_assignto)
      AND 	(_type			=	-1	OR	cust.postingtype =	_type)
	  AND   (cust.createddate	BETWEEN _startdate AND _enddate )
      AND   (_createdby		=   ''  		OR  cust.createdby	=   _createdby )
      AND   (cust.postingassistant IS NULL	OR  cust.postingassistant = _postingassistant )
      AND 	((cust.isblacklist IS NULL OR NOT cust.isblacklist))
      AND   (_postingnumberto = -1
      	 OR (cust.postingnumber > _postingnumberfrom AND cust.postingnumber < _postingnumberto ))
  ORDER BY cust.createddate DESC
  LIMIT 	_pagesize
  OFFSET 	_pagesize * (_pageindex-1);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;