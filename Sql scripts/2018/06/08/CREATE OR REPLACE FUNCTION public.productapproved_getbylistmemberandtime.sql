CREATE OR REPLACE FUNCTION public.productapproved_getbylistmemberandtime (
  _listadminid integer [],
  _fromdate timestamp,
  _todate timestamp
)
RETURNS TABLE (
  adminid integer,
  productid integer,
  approveddate timestamp
) AS
$body$
BEGIN
	RETURN QUERY
    SELECT a.memberid, a.productid, a.approveddate
    FROM public.productapproved a
    WHERE a.memberid = ANY(_listadminid)
    AND a.approveddate >= _fromdate
    AND a.approveddate < _todate;
END
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;