CREATE OR REPLACE FUNCTION public.productapproved_getbylistmemberid (
  _listadminid integer []
)
RETURNS TABLE (
  adminid integer,
  productcount integer
) AS
$body$
BEGIN
	RETURN QUERY
    SELECT a.memberid, CAST(COUNT(1) AS INTEGER) AS productcount
    FROM public.productapproved a
    WHERE a.memberid = ANY(_listadminid)
    GROUP BY a.memberid;
END
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;