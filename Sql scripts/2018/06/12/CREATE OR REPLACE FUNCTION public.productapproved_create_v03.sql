CREATE OR REPLACE FUNCTION public.productapproved_create_v03 (
  _memberid integer,
  _productid integer,
  _approveddate timestamp,
  _iscrawler boolean
)
RETURNS void AS
$body$
BEGIN
  INSERT INTO public.productapproved( memberid, productid, approveddate, iscrawler )
  							  VALUES(_memberid,_productid,_approveddate,_iscrawler );

END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;