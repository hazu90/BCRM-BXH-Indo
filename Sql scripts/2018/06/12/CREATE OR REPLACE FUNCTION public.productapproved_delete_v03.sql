CREATE OR REPLACE FUNCTION public.productapproved_delete_v03 (
  _productid integer
)
RETURNS void AS
$body$
BEGIN
  DELETE FROM public.productapproved
  WHERE productid = _productid ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;