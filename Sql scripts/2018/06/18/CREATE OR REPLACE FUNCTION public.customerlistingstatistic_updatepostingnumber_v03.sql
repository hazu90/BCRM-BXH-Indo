CREATE OR REPLACE FUNCTION public.customerlistingstatistic_updatepostingnumber_v03 (
  _customerid integer,
  _postingnumber integer
)
RETURNS void AS
$body$
BEGIN
  UPDATE public.customerlistingstatistic
  SET postingnumber = _postingnumber
  WHERE customerid  = _customerid ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;