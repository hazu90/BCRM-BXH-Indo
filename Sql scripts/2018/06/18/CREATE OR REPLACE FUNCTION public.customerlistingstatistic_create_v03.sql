CREATE OR REPLACE FUNCTION public.customerlistingstatistic_create_v03 (
  _customerid integer,
  _postingnumber integer
)
RETURNS void AS
$body$
BEGIN
  INSERT INTO public.customerlistingstatistic (customerid,postingnumber )
  									VALUES (_customerid,_postingnumber );
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;