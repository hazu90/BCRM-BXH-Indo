CREATE OR REPLACE FUNCTION public.productapproved_getnotstatistic_v03 (
  _limitrecord integer
)
RETURNS TABLE (
  memberid integer,
  productid integer,
  approveddate timestamp,
  iscrawler boolean
) AS
$body$
BEGIN
  SELECT memberid
  	,	 productid
    ,	 approveddate
    ,	 iscrawler
  FROM  public.productapproved
  WHERE isstatistic IS NULL OR NOT isstatistic 
  LIMIT _limitrecord ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;