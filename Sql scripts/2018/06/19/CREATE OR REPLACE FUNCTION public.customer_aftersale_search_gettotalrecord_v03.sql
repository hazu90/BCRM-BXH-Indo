CREATE OR REPLACE FUNCTION public.customer_aftersale_search_gettotalrecord_v03 (
  _filterkeyword varchar,
  _cityid integer,
  _districtid integer,
  _assignto varchar,
  _type integer,
  _startdate timestamp,
  _enddate timestamp,
  _createdby varchar,
  _postingassistant varchar,
  _postingnumberfrom integer,
  _postingnumberto integer
)
RETURNS integer AS
$body$
DECLARE
    _count		INTEGER;
BEGIN

  SELECT	COUNT(1)	into _count
  FROM		customer cust
	  LEFT JOIN city ct  ON cust.cityid  = ct.id
      LEFT JOIN customerlistingstatistic cls  ON cust.id = cls.customerid
  WHERE	(_filterkeyword	=	''
         OR lower(cust.fullname) LIKE ('%' || _filterkeyword || '%')
         OR cust.email			 LIKE ('%' || _filterkeyword || '%')
         OR lower(cust.address)  LIKE ('%' || _filterkeyword || '%')
      )
      AND	(_cityid		=	-1	OR	cust.cityid		=		_cityid)
      AND	(_districtid	=	-1	OR	cust.districtid	=		_districtid)
      AND 	(_assignto		=	''	OR	cust.assignto 	=		_assignto)
      AND 	(_type			=	-1	OR	cust.postingtype =		_type)
      AND   (cust.createddate	BETWEEN _startdate AND _enddate )
      AND   (_createdby		=   ''  OR  cust.createdby	=   _createdby )
      AND   (cust.postingassistant = _postingassistant )
      AND 	(cust.isblacklist 	   IS NULL OR NOT cust.isblacklist )
      AND   (_postingnumberto = -1
         OR (_postingnumberfrom = -1 AND cls.postingnumber IS NULL )
      	 OR (cls.postingnumber > _postingnumberfrom AND cls.postingnumber < _postingnumberto ));
  RETURN _count;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;