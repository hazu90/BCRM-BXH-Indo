CREATE OR REPLACE FUNCTION public.productapproved_updatestatisticstatus_v03 (
  _productid integer,
  _isstatistic boolean
)
RETURNS void AS
$body$
BEGIN
  UPDATE public.productapproved
  SET	 isstatistic = _isstatistic
  WHERE  productid   = _productid ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;