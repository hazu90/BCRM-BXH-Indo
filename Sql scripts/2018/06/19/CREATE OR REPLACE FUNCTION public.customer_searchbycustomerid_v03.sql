CREATE OR REPLACE FUNCTION public.customer_searchbycustomerid_v03 (
  _customerid integer
)
RETURNS TABLE (
  id integer,
  fullname varchar,
  phonenumber varchar,
  status integer,
  assignto varchar,
  lastmodifieddate timestamp,
  cityid integer,
  icaredate integer,
  caredate timestamp,
  startcaredate timestamp,
  isdeleted boolean,
  createddate timestamp,
  postingtype smallint,
  cityname varchar,
  email varchar,
  createdby varchar,
  postingassistant varchar,
  postingassistancestatus smallint,
  isblacklist boolean,
  address varchar,
  postingnumber integer
) AS
$body$
BEGIN
  RETURN QUERY
  SELECT	cust.id
        ,	cust.fullname
        ,	cust.phonenumber
        ,	cust.status
        ,	cust.assignto
        ,	cust.lastmodifieddate
        ,	cust.cityid
        ,	cust.icaredate
        ,	cust.caredate
        ,	cust.startcaredate
        ,	cust.isdeleted
        ,	cust.createddate
        ,   cust.postingtype
        ,   ct.name		AS cityname
        ,	cust.email
        ,	cust.createdby
        ,	cust.postingassistant
        ,	cust.postingassistancestatus
        ,	cust.isblacklist
        ,	cust.address
        ,	cls.postingnumber
  FROM		customer cust
        LEFT JOIN city ct ON cust.cityid = ct.id
        LEFT JOIN customerlistingstatistic cls  ON cust.id = cls.customerid
  WHERE	cust.id = _customerid		;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;