CREATE OR REPLACE FUNCTION public.customerlistingstatistic_getbycustomerid_v03 (
  _customerid integer
)
RETURNS TABLE (
  customerid integer,
  postingnumber integer
) AS
$body$
BEGIN
  RETURN QUERY	
  SELECT cls.customerid
  	,	 cls.postingnumber
  FROM   public.customerlistingstatistic cls
  WHERE  cls.customerid = _customerid  ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;