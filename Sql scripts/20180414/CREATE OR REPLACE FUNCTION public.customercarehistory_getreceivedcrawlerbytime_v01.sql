CREATE OR REPLACE FUNCTION public.customercarehistory_getreceivedcrawlerbytime_v01 (
  _lstuserid integer [],
  _startdate timestamp,
  _enddate timestamp
)
RETURNS TABLE (
  id integer,
  adminid integer,
  receivedby varchar,
  receiveddate timestamp,
  phoningstatus integer,
  note varchar,
  crawleradminid integer,
  userid integer,
  successcontactdate timestamp,
  waitingoverreject timestamp,
  customerid integer,
  isreturned boolean
) AS
$body$
BEGIN
  RETURN QUERY
  SELECT cch.id
  		,cch.adminid
        ,cch.receivedby
        ,cch.receiveddate
        ,cch.phoningstatus
        ,cch.note
        ,cch.crawleradminid
        ,cch.userid
        ,cch.successcontactdate
        ,cch.waitingoverreject
        ,cch.customerid
        ,cch.isreturned
  FROM public.customercarehistory cch
  WHERE cch.userid = ANY(_lstuserid)
  	AND (cch.crawleradminid > 0)
  	AND (cch.receiveddate BETWEEN _startdate AND _enddate) ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
