CREATE OR REPLACE FUNCTION public.customer_aftersale_search_gettotalrecord (
  _filterkeyword varchar,
  _cityid integer,
  _districtid integer,
  _assignto varchar,
  _type integer,
  _sort integer,
  _startdate timestamp,
  _enddate timestamp,
  _createdby varchar,
  _postingassistant varchar,
  _postingassistancestatus integer
)
RETURNS integer AS
$body$
DECLARE
	_isdeleted	BOOLEAN;
    _count		INTEGER;
BEGIN
  IF (_sort = 1) THEN
      _isdeleted = TRUE;
  ELSE
      _isdeleted = FALSE;
  END IF;
  SELECT	COUNT(1)	into _count
  FROM		customer cust
	  LEFT JOIN city ct  ON cust.cityid  = ct.id
  WHERE		(_filterkeyword	=	''	OR	cust.fullname	like ('%' || _filterkeyword || '%') OR	cust.email	like ('%' || _filterkeyword || '%'))
      AND	(_cityid		=	-1	OR	cust.cityid		=		_cityid)
      AND	(_districtid	=	-1	OR	cust.districtid	=		_districtid)
      AND 	(_assignto		=	''	OR	cust.assignto 	=		_assignto)
      AND 	(_type			=	-1	OR	cust.postingtype =		_type)
      AND 	(_sort			=	-1	OR	cust.isdeleted	=		_isdeleted)
      AND   (cust.createddate	BETWEEN _startdate AND _enddate )
      AND   (_createdby		=   ''  OR  cust.createdby	=   _createdby )
      AND   (cust.postingassistant = _postingassistant )
      AND   (_postingassistancestatus = -1 OR 	 (cust.postingassistancestatus IS NULL AND _postingassistancestatus =0) OR cust.postingassistancestatus = _postingassistancestatus )
	  AND 	(cust.isblacklist 	   IS NULL OR NOT cust.isblacklist );
  RETURN _count;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;