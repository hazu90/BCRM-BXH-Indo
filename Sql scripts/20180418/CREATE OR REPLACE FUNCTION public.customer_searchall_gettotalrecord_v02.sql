CREATE OR REPLACE FUNCTION public.customer_searchall_gettotalrecord_v02 (
  _filterkeyword varchar,
  _cityid integer,
  _districtid integer,
  _assignto varchar,
  _lstgroupid integer [],
  _type integer,
  _sort integer,
  _startdate timestamp,
  _enddate timestamp,
  _createdby varchar,
  _postingassistant varchar
)
RETURNS integer AS
$body$
DECLARE	_isdeleted	BOOLEAN;
		_count		INTEGER;
BEGIN
  IF (_sort = 1) THEN
      _isdeleted = TRUE;
  ELSE
      _isdeleted = FALSE;
  END IF;
  SELECT	COUNT(1)	into _count
  FROM		customer cust
  	   LEFT JOIN users ur ON cust.assignto = ur.username
  	   LEFT JOIN city ct  ON cust.cityid  = ct.id
  WHERE		(_filterkeyword	=	''	
  		  OR lower(cust.fullname)	LIKE ('%' || _filterkeyword || '%') 
          OR cust.email				LIKE ('%' || _filterkeyword || '%')
          OR lower(cust.address)    LIKE ('%' || _filterkeyword || '%'))
      AND	(_cityid		=	-1	OR	cust.cityid		=	_cityid)
      AND	(_districtid	=	-1	OR	cust.districtid	=	_districtid)
      AND 	(cust.assignto  =   ''  OR  cust.assignto 	=	_assignto OR ur.groupid		= ANY(_lstgroupid))
      AND 	(_type			=	-1	OR	cust.postingtype =	_type)
      AND 	(_sort			=	-1	OR	cust.isdeleted	=	_isdeleted)
      AND   (cust.createddate	BETWEEN _startdate AND _enddate )
      AND   (_createdby		=   ''  OR  cust.createdby	=   _createdby )
      AND   (_postingassistant =''  OR  cust.postingassistant = _postingassistant )
 ;
  RETURN _count;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;