CREATE OR REPLACE FUNCTION public.customer_update_v02 (
  _fullname varchar,
  _postingtype integer,
  _email varchar,
  _description varchar,
  _regionid integer,
  _cityid integer,
  _districtid integer,
  _lastmodifiedby varchar,
  _lastmodifieddate timestamp,
  _id integer,
  _address varchar,
  _phonenumber varchar
)
RETURNS void AS
$body$
UPDATE 
  public.customer 
SET 
  fullname 			= _fullname,
  postingtype 		= _postingtype,
  email 			= _email,
  description 		= _description,
  regionid		 	= _regionid,
  cityid 			= _cityid,
  districtid 		= _districtid,
  lastmodifiedby 	= _lastmodifiedby,
  lastmodifieddate 	= _lastmodifieddate,
  address			= _address,
  phonenumber		= _phonenumber
WHERE 
  id				= _id
;
$body$
LANGUAGE 'sql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;