CREATE OR REPLACE FUNCTION public.customer_aftersale_searchtoexport_v01 (
  _filterkeyword varchar,
  _cityid integer,
  _districtid integer,
  _assignto varchar,
  _type integer,
  _sort integer,
  _startdate timestamp,
  _enddate timestamp,
  _createdby varchar,
  _postingassistant varchar
)
RETURNS TABLE (
  id integer,
  fullname varchar,
  phonenumber varchar,
  status integer,
  assignto varchar,
  lastmodifieddate timestamp,
  cityid integer,
  icaredate integer,
  caredate timestamp,
  startcaredate timestamp,
  isdeleted boolean,
  createddate timestamp,
  postingtype smallint,
  cityname varchar,
  email varchar,
  createdby varchar,
  postingassistant varchar,
  postingassistancestatus smallint,
  isblacklist boolean,
  postingnumber integer
) AS
$body$
DECLARE	_isdeleted	BOOLEAN;
BEGIN
  IF (_sort = 1) THEN
      _isdeleted = TRUE;
  ELSE
      _isdeleted = FALSE;
  END IF;
  RETURN QUERY
  SELECT	cust.id
        ,	cust.fullname
        ,	cust.phonenumber
        ,	cust.status
        ,	cust.assignto
        ,	cust.lastmodifieddate
        ,	cust.cityid
        ,	cust.icaredate
        ,	cust.caredate
        ,	cust.startcaredate
        ,	cust.isdeleted
        ,	cust.createddate
        ,   cust.postingtype
        ,   ct.name		AS cityname
        ,	cust.email
        ,	cust.createdby
        ,	cust.postingassistant
        ,	cust.postingassistancestatus
        ,	cust.isblacklist
        ,	cust.postingnumber
  FROM		customer cust
  	   LEFT JOIN city ct  ON cust.cityid  = ct.id
  WHERE		(_filterkeyword	=	''	OR	cust.fullname	like ('%' || _filterkeyword || '%')	OR	cust.email	like ('%' || _filterkeyword || '%'))
      AND	(_cityid		=	-1	OR	cust.cityid		=	_cityid)
      AND	(_districtid	=	-1	OR	cust.districtid	=	_districtid)
      AND 	(_assignto		=	''	OR	cust.assignto 	=	_assignto)
      AND 	(_type			=	-1	OR	cust.postingtype =	_type)
      AND 	(_sort			=	-1	OR	cust.isdeleted	=	_isdeleted)
	  AND   (cust.createddate	 BETWEEN _startdate AND _enddate )
      AND   (_createdby		=   ''  OR  cust.createdby	=   _createdby )
      AND   (cust.postingassistant = _postingassistant )
      AND 	((cust.isblacklist is null OR NOT cust.isblacklist))
  ORDER BY cust.createddate DESC;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;