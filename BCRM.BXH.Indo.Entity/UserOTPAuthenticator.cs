﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.Indo.Entity
{
    public class UserOTPAuthenticator
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Source { get; set; }
        public string Link { get; set; }
        public DateTime ExpiredTime { get; set; }
    }
}
