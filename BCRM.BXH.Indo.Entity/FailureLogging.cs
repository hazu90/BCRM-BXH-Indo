﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.Indo.Entity
{
    public class FailureLogging
    {
        public int Id { get; set; }
        public int Priority { get; set; }
        public string Source { get; set; }
        public string Data { get; set; }
        public string CurrentUser { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Note { get; set; }
    }
}
