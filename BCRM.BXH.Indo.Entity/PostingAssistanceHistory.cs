﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.Indo.Entity
{
    public class PostingAssistanceHistory
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime ReceivedDate { get; set; }
        public int CustomerId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ReturnDate { get; set; }
    }
}
