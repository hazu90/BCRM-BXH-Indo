﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.Indo.Entity
{
    public class SyncBcrmBxhNAdmin
    {
        public int CustomerId { get; set; }
        public int AccountId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
