﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.Indo.Entity
{
    public class HiddenFromSale
    {
        public int AdminId { get; set; }
        public short ReasonType { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long PhoneNumber { get; set; }
    }
}
