﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.Indo.Entity
{
    public class Revenue
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ReceivedUserId { get; set; }
        public int Money { get; set; }
        public int VerifiedDate { get; set; }
        public int AdminTransactionId { get; set; }
        public int RevenueId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedTime { get; set; }
    }
}
