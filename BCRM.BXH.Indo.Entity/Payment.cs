﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCRM.BXH.Indo.Entity
{
    public class Payment
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int Money { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public short Status { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? VerifiedPaymentDate { get; set; }
        public int CreatedUserId { get; set; }
        public int? RevenueId { get; set; }
        public DateTime? ChangeStatusTime { get; set; }
        public string ChangeStatusBy { get; set; }
        public string Feedback { get; set; }
    }
}
