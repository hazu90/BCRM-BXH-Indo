﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using BCRM.BXH.Indo.Library;
using Newtonsoft.Json;
using System.Web;

namespace BCRMIndoTest
{
    [TestClass]
    public class UnitTest1
    {
        public class TestModel
        {
            public int AdminId { get; set; }
            public int PostingNumber { get; set; }
        }

        [TestMethod]
        public void TestMethod1()
        {
            var lstTest = new List<TestModel>();
            lstTest.Add(new TestModel() 
            {
                AdminId = 1,
                PostingNumber = 2
            });

            lstTest.Add(new TestModel()
            {
                AdminId = 2,
                PostingNumber = 2
            });

            lstTest.Add(new TestModel()
            {
                AdminId = 3,
                PostingNumber = 2
            });

            var testItem = lstTest.Find(o => o.AdminId == 2);
            testItem.PostingNumber = 1;
            foreach (var item in lstTest)
            {
                if(item.AdminId == 2)
                {
                    Assert.Equals(item.PostingNumber, 1);
                }
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            string original = "{\"webId\":7,\"endDate\":20181010,\"startDate\":20181001}";
            var asesEnctypt = AESEncryption.EncryptString(original);
        }

        [TestMethod]
        public void TestMethod3()
        {
            var otp = new GoogleTOTP();
            //var img = otp.GenerateImage();
        }
    }
}
