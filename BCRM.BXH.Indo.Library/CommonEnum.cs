﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCRM.BXH.Indo.Library
{
    public enum UserStatus
    {
        Avaiable = 1,
        NotAvaiable = 0
    }
    public enum CustomerStatus
    {
        /// <summary>
        /// Chưa sale
        /// </summary>
        Waiting = 1,
        /// <summary>
        /// Xem xét
        /// </summary>
        Indecivise = 2,
        /// <summary>
        /// Thành công
        /// </summary>
        Success = 3,
        /// <summary>
        /// Từ chối
        /// </summary>
        Rejected = 4
    }
    public enum OpportunityStatus
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,
        /// <summary>
        /// Đang tiến hành
        /// </summary>
        Running = 1,
        /// <summary>
        /// Hoàn Thành
        /// </summary>
        Success = 2,
        /// <summary>
        /// Từ chối
        /// </summary>
        Rejected = 3,
        /// <summary>
        /// Chờ xác nhận
        /// </summary>
        Waiting = 4
    }
    public enum TypeOfPayment
    {
        /// <summary>
        /// Chuyển khoản
        /// </summary>
        Banking = 1,
        /// <summary>
        /// Thu phí trực tiếp
        /// </summary>
        DirectMoney = 2,
        /// <summary>
        /// Nhắn tin sms
        /// </summary>
        Sms = 3,
        /// <summary>
        /// Thu phí bàn giao
        /// </summary>
        Handed = 4,
        /// <summary>
        /// Thẻ cào
        /// </summary>
        Card = 5,
        /// <summary>
        /// Bảo Kim
        /// </summary>
        BaoKim = 6
    }
    public enum TypeOfCustomer : int
    {
        /// <summary>
        /// Salon
        /// </summary>
        Showroom = 0,
        /// <summary>
        /// Môi giới
        /// </summary>
        Dealer = 1,
        /// <summary>
        /// Chính chủ
        /// </summary>
        Private = 2        
    }
    /// <summary>
    /// Trạng thái gọi điện
    /// </summary>
    public enum PhoningStatus
    {
        /// <summary>
        /// Chưa liên lạc
        /// </summary>
        NoContact = 0,
        /// <summary>
        /// Liên lạc không thành công 
        /// </summary>
        Failure = 1,
        /// <summary>
        /// Liên lạc thành công
        /// </summary>
        Success = 2
    }
    public enum ContactStatus
    {
        NotCalled = 0,
        Accepted = 1,
        Refused = 2
    }
    public enum PostingType
    {
        /// <summary>
        /// Cá nhân (0-1 tin rao)
        /// </summary>
        Personal = 0,
        /// <summary>
        /// A (2-10 tin rao)
        /// </summary>
        A = 1,
        /// <summary>
        /// AA (11 - 25 tin rao)
        /// </summary>
        AA = 2,
        /// <summary>
        /// AAA (26-50 tin rao)
        /// </summary>
        AAA = 3,
        /// <summary>
        /// AAAA (trên 50 tin rao)
        /// </summary>
        AAAA = 4
    }
    public enum RequestRetrievePostingStatus
    {
        /// <summary>
        /// Chưa yêu cầu
        /// </summary>
        None = 0,
        /// <summary>
        /// Đang chờ
        /// </summary>
        Pending = 1,
        /// <summary>
        /// Hoàn thành
        /// </summary>
        Done = 2
    }
    public enum HideFromLeadReason
    {
        /// <summary>
        /// Tài khoản trùng với tài khoản đã tồn tại
        /// </summary>
        ExistedInCustomerList = 1,
        /// <summary>
        /// Chủ SĐT không còn làm việc ở showroom đó
        /// </summary>
        PhoneOwnerNotWorkingThatShowroom = 2,
        /// <summary>
        /// Chủ SĐT không còn kinh doanh xe nữa
        /// </summary>
        PhoneOwnerNotWorkingInCar = 3,
        /// <summary>
        /// SĐT không liên hệ được
        /// </summary>
        PhoneOwnerNotResponse = 4,
        /// <summary>
        /// SĐT không tồn tại
        /// </summary>
        PhoneNumberNotExist = 5,
        /// <summary>
        /// Lí do khác
        /// </summary>
        Other = 6
    }
    public enum PaymentStatus
    {
        /// <summary>
        /// Payment được tạo mới
        /// </summary>
        Pending = 1,
        /// <summary>
        /// Payment được duyệt
        /// </summary>
        Approved = 2,
        /// <summary>
        /// Payment bị reject
        /// </summary>
        Rejected = 3
    }

    public enum PaymentAction
    {
        /// <summary>
        /// Payment được tạo mới
        /// </summary>
        New = 1,
        /// <summary>
        /// Payment được duyệt
        /// </summary>
        Approved = 2,
        /// <summary>
        /// Payment bị reject
        /// </summary>
        Reject = 3
    }

    public enum RevenueAction
    {
        /// <summary>
        /// Tạo mới doanh thu
        /// </summary>
        New =1,
        /// <summary>
        /// Duyệt payment tự động bổ sung doanh thu
        /// </summary>
        ApprovingPaymentAndNew = 2
    }
}
