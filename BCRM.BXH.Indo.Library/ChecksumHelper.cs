﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVS.Algorithm;

namespace BCRM.BXH.Indo.Library
{
    public class ChecksumResult
    {
        public ChecksumResult()
        {
            this.IsValid = false;
            this.ErrorCode = ChecksumErrorCode.Invalid;
            this.Message = Common.GetEnumDescription(this.ErrorCode);
        }

        public ChecksumResult(ChecksumErrorCode errorCode)
        {
            this.IsValid = errorCode == ChecksumErrorCode.Success;
            this.ErrorCode = errorCode;
            this.Message = Common.GetEnumDescription(errorCode);
        }

        public string Message { get; set; }
        public bool IsValid { get; set; }
        public ChecksumErrorCode ErrorCode { get; set; }

        public enum ChecksumErrorCode
        {
            [Description("Valid token")]
            Success = 0,
            [Description("Invalid token")]
            Invalid = 1,
            [Description("Token has been expired")]
            Expired = 2
        }
    }

    [Serializable]
    public class ChecksumModel<T>
    {
        public ChecksumModel(T entity, int expiredInMinute = 15)
        {
            string jsonEntity = JsonConvert.SerializeObject(entity);

            this.HashMD5 = jsonEntity.ToMD5(); //HashHelper.GetMd5Hash(jsonEntity);
            this.ExpiredDate = System.DateTime.Now.AddMinutes(expiredInMinute);
        }

        public string HashMD5 { get; set; }

        public System.DateTime ExpiredDate { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public bool IsExpired()
        {
            return Common.DateDiff(ExpiredDate, System.DateTime.Now, "m") > 0;
        }
    }

    public class ChecksumHelper
    {
        private static string privateKey = ConfigurationManager.AppSettings["API_CheckSum_PrivateKey"].ToString().ToMD5();
        private static int checksumExpiredInMinute = ConfigurationManager.AppSettings["API_CheckSum_ExpiredInMinute"].ToInt(15);
        //private static string sessionChecksum = "";

        public static void SetPrivateKey(string key)
        {
            privateKey = key;
        }

        public static string GenToken<T>(T entity, int? expiredInMinute = null)
        {
            string token = string.Empty;

            if (!expiredInMinute.HasValue) expiredInMinute = checksumExpiredInMinute;

            if (entity != null && !entity.Equals(default(T)))
            {
                ChecksumModel<T> checksumModel = new ChecksumModel<T>(entity, expiredInMinute.Value);

                token = Crypto.EncryptByKey(checksumModel.ToString(), privateKey);
            }

            return token;
        }

        public static ChecksumResult ValidateToken<T>(T entity, string token)
        {
            ChecksumResult result = new ChecksumResult();

            if (!string.IsNullOrWhiteSpace(token))
            {
                try
                {
                    ChecksumModel<T> requestModel = new ChecksumModel<T>(entity);

                    string decryptString = Crypto.DecryptByKey(token, privateKey);

                    ChecksumModel<T> checksumDecrypt = JsonConvert.DeserializeObject<ChecksumModel<T>>(decryptString);

                    if (!checksumDecrypt.IsExpired())
                    {
                        bool valid = requestModel.HashMD5.Equals(checksumDecrypt.HashMD5);
                        if (valid)
                        {
                            result = new ChecksumResult(ChecksumResult.ChecksumErrorCode.Success);
                        }
                    }
                    else
                    {
                        result = new ChecksumResult(ChecksumResult.ChecksumErrorCode.Expired);
                    }
                }
                catch
                {
                    // Logs
                }
            }

            return result;
        }
    }
}
