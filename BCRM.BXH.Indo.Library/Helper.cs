﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCRM.BXH.Indo.Library
{
    public static partial class Helper
    {
        private static readonly List<KeyValuePair<int, string>> CustomerStatusName = new List<KeyValuePair<int, string>> 
        {  
            new KeyValuePair<int, string>(CustomerStatus.Waiting.GetHashCode(), "Waiting"),
            new KeyValuePair<int, string>(CustomerStatus.Indecivise.GetHashCode(), "Indecivise"),
            new KeyValuePair<int, string>(CustomerStatus.Success.GetHashCode(), "Success"),
            new KeyValuePair<int, string>(CustomerStatus.Rejected.GetHashCode(), "Rejected")
        };

        private static readonly List<KeyValuePair<int, string>> TypeOfCustomerName = new List<KeyValuePair<int, string>> 
        {  
            new KeyValuePair<int, string>(TypeOfCustomer.Showroom.GetHashCode(), "Showroom"),
            new KeyValuePair<int, string>(TypeOfCustomer.Dealer.GetHashCode(), "Dealer"),
            new KeyValuePair<int, string>(TypeOfCustomer.Private.GetHashCode(), "Private")            
        };

        private static readonly List<KeyValuePair<int, string>> TypeOfSystemRole = new List<KeyValuePair<int, string>> 
        {
            new KeyValuePair<int, string>(6, "Sale"),
            new KeyValuePair<int, string>(7, "Manager"),
            new KeyValuePair<int, string>(8, "Admin"),
            new KeyValuePair<int, string>(9, "Coordinate"),
            new KeyValuePair<int, string>(10, "TeamLeader"),
            new KeyValuePair<int, string>(11, "After-sale")
        };

        private static readonly List<KeyValuePair<int, string>> ContactStatusName = new List<KeyValuePair<int, string>> 
        {
            new KeyValuePair<int, string>(ContactStatus.NotCalled.GetHashCode(), "Not Contact")
           ,new KeyValuePair<int, string>(ContactStatus.Accepted.GetHashCode() , "Accepted")
           ,new KeyValuePair<int, string>(ContactStatus.Refused.GetHashCode() , "Refused")
        };

        private static readonly List<KeyValuePair<int, string>> PostingTypeName = new List<KeyValuePair<int, string>> 
        {  
            new KeyValuePair<int, string>(PostingType.Personal.GetHashCode(), "Personal"),
            new KeyValuePair<int, string>(PostingType.A.GetHashCode(), "A"),
            new KeyValuePair<int, string>(PostingType.AA.GetHashCode(), "AA"),
            new KeyValuePair<int, string>(PostingType.AAA.GetHashCode(), "AAA"),
            new KeyValuePair<int, string>(PostingType.AAAA.GetHashCode(), "AAAA")
        };

        private static readonly List<KeyValuePair<int, string>> PostingTypeWithPostingName = new List<KeyValuePair<int, string>> 
        {  
            new KeyValuePair<int, string>(PostingType.Personal.GetHashCode(), "Personal (0-1 listings)"),
            new KeyValuePair<int, string>(PostingType.A.GetHashCode(), "A (2-10 listings)"),
            new KeyValuePair<int, string>(PostingType.AA.GetHashCode(), "AA (11-25 listings)"),
            new KeyValuePair<int, string>(PostingType.AAA.GetHashCode(), "AAA (26-50 listings)"),
            new KeyValuePair<int, string>(PostingType.AAAA.GetHashCode(), "AAAA (50+ listings)")
        };

        private static readonly List<KeyValuePair<int, string>> HideFromLeadReasonName = new List<KeyValuePair<int, string>> 
        {  
            new KeyValuePair<int, string>(HideFromLeadReason.ExistedInCustomerList.GetHashCode(), "Account is already existing !"),
            new KeyValuePair<int, string>(HideFromLeadReason.PhoneOwnerNotWorkingThatShowroom.GetHashCode(), "The owner no longer works in the showroom"),
            new KeyValuePair<int, string>(HideFromLeadReason.PhoneOwnerNotWorkingInCar.GetHashCode(), "The owner doesn't sell cars anymore"),
            new KeyValuePair<int, string>(HideFromLeadReason.PhoneOwnerNotResponse.GetHashCode(), "Phone number can not be contacted"),
            new KeyValuePair<int, string>(HideFromLeadReason.PhoneNumberNotExist.GetHashCode(), "Phone does not exist"),
            new KeyValuePair<int, string>(HideFromLeadReason.Other.GetHashCode(), "Other")
        };


        #region CustomerStatusName
        public static string ToCustomerStatusName(this CustomerStatus status)
        {
            var name = (from n in CustomerStatusName where n.Key == status.GetHashCode() select n.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            else
            {
                return string.Empty;
            }
        }

        public static List<KeyValuePair<int, string>> ListCustomerStatusName()
        {
            return CustomerStatusName;
        }
        #endregion

        #region TypeOfCustomerName
        public static string ToTypeOfCustomerName(this TypeOfCustomer type)
        {
            var name = (from n in TypeOfCustomerName where n.Key == type.GetHashCode() select n.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            else
            {
                return string.Empty;
            }
        }

        public static List<KeyValuePair<int, string>> ListTypeOfCustomerName()
        {
            return TypeOfCustomerName;
        }
        #endregion

        #region PermissionName
        public static string ToPermissionName(this int permission)
        {
            var name = (from n in TypeOfSystemRole where n.Key == permission select n.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            else
            {
                return string.Empty;
            }
        }

        public static List<KeyValuePair<int, string>> ListPermissionName()
        {
            return TypeOfSystemRole;
        }
        #endregion

        #region Contact Status
        public static string ToContactStatusName(this int permission)
        {
            var name = (from n in ContactStatusName where n.Key == permission select n.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            else
            {
                return string.Empty;
            }
        }

        public static List<KeyValuePair<int, string>> ListContactStatusName()
        {
            return ContactStatusName;
        }
        #endregion

        #region Posting type
        public static string ToPostingTypeName(this int postingType)
        {
            var name = (from n in PostingTypeName where n.Key == postingType select n.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            else
            {
                return string.Empty;
            }
        }

        public static List<KeyValuePair<int, string>> ListPostingTypeName()
        {
            return PostingTypeName;
        }

        public static List<KeyValuePair<int, string>> ListPostingTypeWithPostingName()
        {
            return PostingTypeWithPostingName;
        }
        #endregion

        public static string ToHideFromLeadReasonName(this int hideFromLeadReason)
        {
            var name = (from n in HideFromLeadReasonName where n.Key == hideFromLeadReason select n.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
