﻿using BCRM.BXH.Indo.Library;
using SyncFirstApprovedPosting.DAL;
using SyncFirstApprovedPosting.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFirstApprovedPosting
{
    public class Processor
    {
        private Processor()
        {
        }

        public static Processor GetInstance()
        {
            return new Processor();
        }

        public void Run()
        {
            Logger.GetInstance().Write("#####################################");
            Logger.GetInstance().Write(string.Format("Start: {0:yyyy/MM/dd HH:mm:ss:FFF}", DateTime.Now));
            try
            {
                // Gọi sang bên admin để lấy 
                var adminProductApprovedLayer = new AdminProductApprovedLayer();
                var lstAdminProductApprovedForCreate = adminProductApprovedLayer.GetNotProcess(ConfigurationManager.AppSettings["TotalListingEachProcess"].ToInt(0) );
                var productApprovedLayer = new ProductApprovedLayer();
                foreach (var item in lstAdminProductApprovedForCreate)
                {
                    if(item.IsDeleted)
                    {
                        var iSuccess = adminProductApprovedLayer.UpdateStatus(item.ProductId);
                        if (iSuccess == 1)
                        {
                            productApprovedLayer.UpdateIsDelete(item.ProductId);
                        }
                    }
                    else
                    {
                        var iSuccess = adminProductApprovedLayer.UpdateStatus(item.ProductId);
                        if (iSuccess == 1)
                        {
                            productApprovedLayer.Created(new ProductApprovedModel()
                            {
                                MemberId = item.MemberId,
                                ProductId = item.ProductId,
                                ApprovedDate = item.ApprovedDate,
                                IsCrawler = item.IsCrawler
                            });
                        }
                    }
                }
                var lstCustomerListingStatistic = new List<CustomerListingStatisticForChangeModel>();
                var lstProductNotStatistic = productApprovedLayer.GetNotStatistic(ConfigurationManager.AppSettings["TotalListingEachProcess"].ToInt(0));

                var lstSync = new List<SyncBcrmBxhNAdminModel>();
                var syncBcrmBxhAdminLayer = new SyncBcrmBxhAdminLayer();
                foreach (var item in lstProductNotStatistic)
                {
                    if (lstSync.Find(o => o.AccountId == item.MemberId) == null)
                    {
                        var syncInfo = syncBcrmBxhAdminLayer.GetByAdminId(item.MemberId);
                        if (syncInfo == null)
                        {
                            item.SetIsNotProcess(true);
                        }
                        else
                        {
                            lstSync.Add(syncInfo);
                            if (item.IsDeleted)
                            {
                                var customerListingStatisticItem = lstCustomerListingStatistic.Find(o => o.AdminId == item.MemberId);
                                if (customerListingStatisticItem == null)
                                {
                                    lstCustomerListingStatistic.Add(new CustomerListingStatisticForChangeModel(item.MemberId, -1));
                                }
                                else
                                {
                                    customerListingStatisticItem.AddedListing += -1;
                                }
                            }
                            else
                            {
                                var customerListingStatisticItem = lstCustomerListingStatistic.Find(o => o.AdminId == item.MemberId);
                                if (customerListingStatisticItem == null)
                                {
                                    lstCustomerListingStatistic.Add(new CustomerListingStatisticForChangeModel(item.MemberId, 1));
                                }
                                else
                                {
                                    customerListingStatisticItem.AddedListing += 1;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (item.IsDeleted)
                        {
                            var customerListingStatisticItem = lstCustomerListingStatistic.Find(o => o.AdminId == item.MemberId);
                            if (customerListingStatisticItem == null)
                            {
                                lstCustomerListingStatistic.Add(new CustomerListingStatisticForChangeModel(item.MemberId, -1));
                            }
                            else
                            {
                                customerListingStatisticItem.AddedListing += -1;
                            }
                        }
                        else
                        {
                            var customerListingStatisticItem = lstCustomerListingStatistic.Find(o => o.AdminId == item.MemberId);
                            if (customerListingStatisticItem == null)
                            {
                                lstCustomerListingStatistic.Add(new CustomerListingStatisticForChangeModel(item.MemberId, 1));
                            }
                            else
                            {
                                customerListingStatisticItem.AddedListing += 1;
                            }
                        }
                    }
                }
                var customerListingStatisticLayer = new CustomerListingStatisticLayer();
                
                foreach (var item in lstCustomerListingStatistic)
                {
                    //var syncInfo = syncBcrmBxhAdminLayer.GetByAdminId(item.AdminId);
                    var syncInfo = lstSync.Find(o => o.AccountId == item.AdminId);
                    if(syncInfo.CustomerId == 0)
                    {
                        Logger.GetInstance().Write(string.Format("AdminId = {0} Customerid =0 ",item.AdminId));
                    }
                    else
                    {
                        var info = customerListingStatisticLayer.GetByCustomerId(syncInfo.CustomerId);
                        if(info == null)
                        {
                            customerListingStatisticLayer.Create(syncInfo.CustomerId, item.AddedListing);
                        }
                        else
                        {
                            customerListingStatisticLayer.UpdatePostingnumber(syncInfo.CustomerId, info.PostingNumber + item.AddedListing);
                        }
                    }
                }

                foreach (var item in lstProductNotStatistic)
                {
                    if(!item.IsNotProcess)
                    {
                        productApprovedLayer.UpdateStatistic(item.ProductId,item.ProcessedCount +1 , true);
                    }
                    else
                    {
                        Logger.GetInstance().Write(string.Format("ProductId = {0} is not processed ", item.ProductId));
                        productApprovedLayer.UpdateProcessedCount(item.ProductId, item.ProcessedCount + 1);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.GetInstance().Write(ex);
            }
            finally
            {
                Logger.GetInstance().Write(string.Format("End: {0:yyyy/MM/dd HH:mm:ss:FFF}", DateTime.Now));
                MemoryManagement.FlushMemory();
            }
        }

    }
}
