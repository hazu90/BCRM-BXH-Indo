﻿using BCRM.BXH.Indo.Library;
using Npgsql;
using SyncFirstApprovedPosting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFirstApprovedPosting.DAL
{
    public class SyncBcrmBxhAdminLayer
    {
        public SyncBcrmBxhNAdminModel GetByAdminId(int adminId)
        {
            var ret = new SyncBcrmBxhNAdminModel();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("service_syncbcrmbxhnadmin_getbyadminid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_adminid", adminId));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);
                            return ret;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
    }
}
