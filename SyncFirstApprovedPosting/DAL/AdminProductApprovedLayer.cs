﻿using BCRM.BXH.Indo.Library;
using Npgsql;
using SyncFirstApprovedPosting.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFirstApprovedPosting.DAL
{
    public class AdminProductApprovedLayer
    {
        public List<AdminProductApprovedForChangeModel> GetNotProcess(int limitRecord)
        {
            var ret = new List<AdminProductApprovedForChangeModel>();
            using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHIndo"].ToString() ))
            {
                using (var command = db.CreateCommand("func_bcrm_productapproved_getnotprocess", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_limitrecord", limitRecord));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new AdminProductApprovedForChangeModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }

        public int UpdateStatus(int productId)
        {
            using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHIndo"].ToString()))
            {
                using (var command = db.CreateCommand("func_bcrm_productapproved_updatestatus", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_productid", productId));
                    command.Parameters.Add(new NpgsqlParameter("_isprocess", true));
                    return (int) command.ExecuteScalar();
                }
            }
        }
    }
}
