﻿using BCRM.BXH.Indo.Library;
using Npgsql;
using SyncFirstApprovedPosting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFirstApprovedPosting.DAL
{
    public class CustomerListingStatisticLayer
    {
        public void Create(int customerId, int postingNumber)
        {
            using (var db = new PostgresSQL())
            {
                //using(var command = db.BeginTransaction)
                using (var command = db.CreateCommand("customerlistingstatistic_create_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumber", postingNumber));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdatePostingnumber(int customerId,int postingNumber)
        {
            using (var db = new PostgresSQL())
            {
                //using(var command = db.BeginTransaction)
                using (var command = db.CreateCommand("customerlistingstatistic_updatepostingnumber_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumber", postingNumber));
                    command.ExecuteNonQuery();
                }
            }
        }

        public CustomerListingStatisticModel GetByCustomerId(int customerId)
        {
            using (var db = new PostgresSQL())
            {
                //using(var command = db.BeginTransaction)
                using (var command = db.CreateCommand("customerlistingstatistic_getbycustomerid_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var cust = new CustomerListingStatisticModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            return cust;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
    }
}
