﻿using BCRM.BXH.Indo.Library;
using Npgsql;
using SyncFirstApprovedPosting.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFirstApprovedPosting.DAL
{
    public class ProductApprovedLayer
    {
        public void Created(ProductApprovedModel model)
        {
            using (var db = new PostgresSQL())
            {

                //using(var command = db.BeginTransaction)
                using (var command = db.CreateCommand("productapproved_create_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_memberid", model.MemberId ));
                    command.Parameters.Add(new NpgsqlParameter("_productid", model.ProductId));
                    command.Parameters.Add(new NpgsqlParameter("_approveddate", model.ApprovedDate));
                    command.Parameters.Add(new NpgsqlParameter("_iscrawler", model.IsCrawler));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateIsDelete(int productId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("productapproved_updateisdelete_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_productid", productId));
                    command.ExecuteNonQuery();
                }
            }
        }
        public ProductApprovedModel GetByProductIdAndMemberId(int productId,int memberId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("productapproved_getbyproductidandmemberid_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_productid", productId));
                    command.Parameters.Add(new NpgsqlParameter("_memberid", memberId));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var cust = new ProductApprovedModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            return cust;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        public List<ProductApprovedModel> GetNotStatistic(int limitedRecord)
        {
            var ret = new List<ProductApprovedModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("productapproved_getnotstatistic_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_limitrecord", limitedRecord));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new ProductApprovedModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public void UpdateStatistic(int productId,int proccessCount ,bool status)
        {
            using (var db = new PostgresSQL())
            {
                //using(var command = db.BeginTransaction)
                using (var command = db.CreateCommand("productapproved_updatestatisticstatus_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_productid", productId));
                    command.Parameters.Add(new NpgsqlParameter("_isstatistic", status));
                    command.Parameters.Add(new NpgsqlParameter("_proccesscount", proccessCount));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateProcessedCount(int productId,int processCount)
        {
            using (var db = new PostgresSQL())
            {
                //using(var command = db.BeginTransaction)
                using (var command = db.CreateCommand("productapproved_updateprocesscount_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_productid", productId));
                    command.Parameters.Add(new NpgsqlParameter("_processedcount", processCount));
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
