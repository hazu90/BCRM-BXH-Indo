﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFirstApprovedPosting.Models
{
    public class CustomerListingStatisticModel
    {
        public int CustomerId { get; set; }
        public int PostingNumber { get; set; }
    }
}
