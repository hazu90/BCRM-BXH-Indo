﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFirstApprovedPosting.Models
{
    public class ProductApprovedModel
    {
        public int MemberId { get; set; }
        public int ProductId { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool IsCrawler { get; set; }
        public bool IsStatistic { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsNotProcess { get; set; }
        public int ProcessedCount { get; set; }
        public void SetIsNotProcess(bool isNotProcess)
        {
            this.IsNotProcess = IsNotProcess;    
        }
    }
}
