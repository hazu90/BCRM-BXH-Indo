﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFirstApprovedPosting.Models
{
    public class AdminProductApprovedForChangeModel
    {
        public int MemberId { get; set; }
        public int ProductId { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool IsCrawler { get; set; }
        public bool IsProcess { get; set; }
        public bool IsDeleted { get; set; }
    }
}
