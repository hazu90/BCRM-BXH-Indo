﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFirstApprovedPosting.Models
{
    public class CustomerListingStatisticForChangeModel
    {
        public int AdminId { get; set; }
        public int AddedListing { get; set; }
        public CustomerListingStatisticForChangeModel(int adminId,int addedListing)
        {
            this.AdminId = adminId;
            this.AddedListing += addedListing;
        }
    }
}
