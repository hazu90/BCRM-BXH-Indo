﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SyncFirstApprovedPosting
{
    public partial class Service1 : ServiceBase
    {
        private static Timer T;
        public Service1()
        {
            InitializeComponent();
            ServiceName = "BCRM.Indo.SyncFirstApprovedPosting";
            T = new Timer(int.Parse(ConfigurationManager.AppSettings["TimeIntervalInMinute"]) * 1000 * 60);
            T.Elapsed += new ElapsedEventHandler(T_Elapsed);
        }

        protected override void OnStart(string[] args)
        {
            T.Start();
        }

        protected override void OnStop()
        {
            T.Stop();
        }

        private void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            Processor obj = Processor.GetInstance();
            if (obj != null) obj.Run();
        }
    }
}
