﻿using BCRM.BXH.Indo.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Cache
{
    public class IISPaymentApprovingCache
    {
        private IISCache cache = new IISCache();
        private const string CachePaymentApprovingInfo = "DVS.BCRM.BXH.Indo.LstPaymentApproving";
        public List<int> Add(int id)
        {
            var lstPaymentApproving = cache.Get<List<int>>(CachePaymentApprovingInfo);
            if(lstPaymentApproving == null)
            {
                lstPaymentApproving = new List<int>();
            }
            if(!lstPaymentApproving.Contains(id))
            {
                lstPaymentApproving.Add(id);
            }
            cache.Set(CachePaymentApprovingInfo, lstPaymentApproving);
            return lstPaymentApproving;
        }
        public List<int> Remove(int id)
        {
            var lstPaymentApproving = cache.Get<List<int>>(CachePaymentApprovingInfo);
            if(lstPaymentApproving == null)
            {
                return new List<int>();
            }
            lstPaymentApproving.Remove(id);
            cache.Set(CachePaymentApprovingInfo, lstPaymentApproving);
            return lstPaymentApproving;
        }
        public bool Contain(int id)
        {
            var lstPaymentApproving = cache.Get<List<int>>(CachePaymentApprovingInfo);
            if (lstPaymentApproving == null)
            {
                return false;
            }
            if(lstPaymentApproving.Contains(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}