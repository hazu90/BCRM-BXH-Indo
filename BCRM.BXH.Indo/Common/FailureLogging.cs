﻿using BCRM.BXH.Indo.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Common
{
    public class FailureLoggingCommon
    {
        private bool IsLogging 
        {
            get
            {
                return ConfigurationManager.AppSettings["IsLoggingDB"].ToString().Equals("1") ? true : false;
            }
        }
        public void WriteLog(int priority,string source,string data,string currentUser,string message,DateTime dateNow,string note)
        {
            if (string.IsNullOrEmpty(data))
            {
                data = "";
            }
            else
            {

                if (data.Length > 500)
                {
                    data = data.Substring(0, 500);
                }
            }

            if (string.IsNullOrEmpty(message))
            {
                message = "";
            }
            else
            {

                if (message.Length > 500)
                {
                    message = message.Substring(0, 500);
                }
            }

            var failureLoggingLayer = new FailureLoggingLayer();
            if (this.IsLogging)
            {
                failureLoggingLayer.Create(priority, source, data, currentUser, message, dateNow, note);
            }
            
        }

    }
}