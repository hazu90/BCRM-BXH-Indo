﻿var sysdialog = {
    init: function () {
        $('#dialog-error').dialog({
            autoOpen: false,
            resizable: false,
            dialogClass: 'ui-dialog-red',
            open: function () {
                $('#dialog-confirm').children().detach().appendTo($('#dialog-confirm'));
            },
            show: {
                effect: "blind",
                duration: 500
            },
            modal: true,
            buttons: [
	      	{
	      	    "text": "Close",
	      	    'class': 'btn',
	      	    click: function () {
	      	        $(this).dialog("close");
	      	    }
	      	}
	      ]
        });
        $('#dialog-info').dialog({
            autoOpen: false,
            resizable: false,
            dialogClass: 'ui-dialog-blue',
            open: function () {
                $('#dialog-confirm').children().detach().appendTo($('#dialog-confirm'));
            },
            show: {
                effect: "blind",
                duration: 500
            },
            modal: true,
            buttons: [
	      	{
	      	    "text": "Close",
	      	    'class': 'btn',
	      	    click: function () {
	      	        $(this).dialog("close");
	      	    }
	      	}
	      ]
        });
        $('#dialog-warning').dialog({
            autoOpen: false,
            resizable: false,
            dialogClass: 'ui-dialog-yellow',
            open: function () {
                $('#dialog-confirm').children().detach().appendTo($('#dialog-confirm'));
            },
            show: {
                effect: "blind",
                duration: 500
            },
            modal: true,
            buttons: [
	      	{
	      	    "text": "Close",
	      	    'class': 'btn',
	      	    click: function () {
	      	        $(this).dialog("close");
	      	    }
	      	}]
        });
        $('#dialog-confirm').dialog({
            autoOpen: false,
            resizable: false,
            dialogClass: 'ui-dialog-green',
            open: function () {
                $('#dialog-confirm').children().detach().appendTo($('#dialog-confirm'));
            },
            show: {
                effect: "blind",
                duration: 500
            },
            modal: true,
            buttons: [
            {
                'class': 'btn red',
                'text': 'Accept',
                click: function () {
                    $(this).dialog("close");
                    sysdialog.process();
                }
            },
	      	{
	      	    'class': 'btn',
	      	    'text': 'Cancel',
	      	    click: function () {
	      	        $(this).dialog("close");
	      	    }
	      	}]
        });
    },
    show: function (options) {
        if ($('.modal.in').length > 0) {
            $('.modal.in').modal('hide');
            setTimeout(function () {
                $('span.dialog-msg', options.element).html(options.message);
                if (typeof options.process === 'function') {
                    sysdialog.process = options.process;
                }
                else {
                    sysdialog.process = function () { };
                }
                options.element.dialog("open");
                $('.ui-dialog button').blur();
            }, 500);
        }
        else {
            $('span.dialog-msg', options.element).html(options.message);
            if (typeof options.process === 'function') {
                sysdialog.process = options.process;
            }
            else {
                sysdialog.process = function () { };
            }
            options.element.dialog("open");
            $('.ui-dialog button').blur();
        }
    },
    process: function () { }
};