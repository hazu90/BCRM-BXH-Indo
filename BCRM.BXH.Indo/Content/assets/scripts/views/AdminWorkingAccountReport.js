﻿var admin_working_account_report_index = {
    language_res: {
        active_customer_line_title: 'Working customer',
        registered_customer_line_title: 'Registered customer'
    },
    init: function (lan_res) {
        $.extend(admin_working_account_report_index.language_res, lan_res);
        $(document).ready(function () {
            app.initPortletTools();
            app.initDateTimePickers();
            admin_working_account_report_index.action();
            $('button[name=search]', $('#wrapAdminWorkingAccountReportSearch')).trigger('click');
        });
    },
    action: function () {
        var wrap_search = $('#wrapAdminWorkingAccountReportSearch');
        $('button[name=search]', wrap_search).off('click').on('click', function () {
            $('#wrapAdminWorkingAccountReportResult').removeClass('hide');
            var txtTimeToCreationFrom = $('input[name=StartDate]', wrap_search);
            var txtTimeToCreationTo = $('input[name=EndDate]', wrap_search);

            if (txtTimeToCreationFrom.val() == null || txtTimeToCreationFrom.val() == '') {
                sysmess.warning('Start date is required !');
                return;
            }

            if (txtTimeToCreationTo.val() == null || txtTimeToCreationTo.val() == '') {
                sysmess.warning('End date is required !');
                return;
            }
            var option = {
                controller: 'AdminWorkingAccountReport',
                action: 'ShowReport',
                data: {
                    StartDate: sysformat.date(txtTimeToCreationFrom.val()),
                    EndDate: sysformat.date(txtTimeToCreationTo.val())
                },
                callback: function (response) {
                    if(response.Code == ResponseCode.Success){
                        //vẽ biểu đồ tài khoản đăng kí
                        var data_registered_customer = new google.visualization.DataTable();
                        data_registered_customer.addColumn('date','Days');
                        data_registered_customer.addColumn('number', admin_working_account_report_index.language_res.registered_customer_line_title);

                        var arr_date_registered_customer=[];
                        var is_show_column_chart = response.Data.length <= 5 ? true : false;
                        var v_registered_customer_max = 0;
                        for(var index = 0;index < response.Data.length;index++){
                            if(response.Data[index].CountRegisteredCustomer > v_registered_customer_max){
                                v_registered_customer_max = response.Data[index].CountRegisteredCustomer;
                            }
                            var yyyy = Math.round(response.Data[index].ReportDate / 10000) ;
                            var mm = Math.round((response.Data[index].ReportDate - yyyy * 10000)/100) ;
                            var dd = Math.round((response.Data[index].ReportDate - yyyy * 10000 - mm * 100)) ;
                            var cvetDT = new Date(yyyy,mm-1,dd);
                            if (is_show_column_chart){
                                arr_date_registered_customer.push(cvetDT);
                            }
                            data_registered_customer.addRow([cvetDT,response.Data[index].CountRegisteredCustomer]);
                        }

                        var max_devide_five = Math.round(v_registered_customer_max / 5);
                        var temp = Math.round(max_devide_five/5) * 5;
                        if(temp < v_registered_customer_max){
                            max_devide_five = (Math.round(max_devide_five/5)+1) * 5;
                        }
                        else{
                            max_devide_five = temp;
                        }
                        var maximum_y_axis = max_devide_five * 5 ;
                        var arr_ticks = [];
                        for(var index = 0;index <6;index++){
                            arr_ticks.push(index * max_devide_five);
                        }

                        if(response.Data.length > 5){
                            var option_line_chart = {
                                title :'',
                                legend: {position: 'bottom'},
                                hAxis: {
                                    format: 'dd/MM/yyyy'
                                },
                                lineWidth: 2,
                                vAxis:{
                                    viewWindow: {
                                        min:0,
                                        max:maximum_y_axis
                                    },
                                    viewWindowMode:'explicit',
                                    gridlines: { count: 6},
                                    ticks:arr_ticks
                                }
                            };
                            var chart_line = new google.visualization.LineChart(document.getElementById('line_chart'));
                            chart_line.draw(data_registered_customer,option_line_chart);
                        }
                        else{
                            var option_line_chart = {
                                title :'',
                                legend: {position: 'bottom'},
                                hAxis: {
                                    viewWindowMode:'explicit',
                                    format: 'dd/MM/yyyy',
                                    gridlines: { count: arr_date_registered_customer.length},
                                    ticks: arr_date_registered_customer
                                },
                                vAxis:{
                                    viewWindow: {
                                        min:0,
                                        max:maximum_y_axis
                                    },
                                    viewWindowMode:'explicit',
                                    gridlines: { count: 6},
                                    ticks:arr_ticks
                                }
                            };
                            var chart_column = new google.visualization.ColumnChart(document.getElementById('line_chart'));
                            chart_column.draw(data_registered_customer, option_line_chart);
                        }

                        var data_active_customer = new google.visualization.DataTable();
                        data_active_customer.addColumn('date', 'Days');
                        data_active_customer.addColumn('number',admin_working_account_report_index.language_res.active_customer_line_title);

                        var arr_date_active_customer = [];
                        var is_show_column_chart_active_customer = response.Data.length <= 5 ? true : false;
                        var v_active_customer_max = 0;
                        for(var index = 0;index < response.Data.length;index++ ){

                            if(response.Data[index].CountActiveCustomer > v_active_customer_max){
                                v_active_customer_max = response.Data[index].CountActiveCustomer;
                            }

                            var yyyy = Math.round(response.Data[index].ReportDate / 10000) ;
                            var mm = Math.round((response.Data[index].ReportDate - yyyy * 10000)/100) ;
                            var dd = Math.round((response.Data[index].ReportDate - yyyy * 10000 - mm * 100)) ;
                            var cvetDT = new Date(yyyy,mm-1,dd);
                            if (is_show_column_chart_active_customer){
                                arr_date_active_customer.push(cvetDT);
                            }
                            data_active_customer.addRow([cvetDT,response.Data[index].CountActiveCustomer]);
                        }

                        var max_devide_five_active_customer = Math.round(v_active_customer_max / 5);
                        var temp = Math.round(max_devide_five_active_customer/5) * 5;
                        if(temp < v_active_customer_max){
                            max_devide_five_active_customer = (Math.round(max_devide_five_active_customer/5)+1) * 5;
                        }
                        else{
                            max_devide_five_active_customer = temp;
                        }
                        var maximum_y_axis_active_customer = max_devide_five_active_customer * 5 ;
                        var arr_ticks_active_customer = [];
                        for(var index = 0;index <6;index++){
                            arr_ticks_active_customer.push(index * max_devide_five_active_customer);
                        }

                        if(response.Data.length > 5){
                            var option_line_chart = {
                                title :'',
                                legend: {position: 'bottom'},
                                hAxis: {
                                    format: 'dd/MM/yyyy'
                                },
                                lineWidth: 2,
                                vAxis:{
                                    viewWindow: {
                                        min:0,
                                        max:maximum_y_axis_active_customer
                                    },
                                    viewWindowMode:'explicit',
                                    gridlines: { count: 6},
                                    ticks:arr_ticks_active_customer
                                },
                                colors:['red']
                            };
                            var chart_line = new google.visualization.LineChart(document.getElementById('line_chart_2'));
                            chart_line.draw(data_active_customer,option_line_chart);
                        }
                        else{
                            var option_line_chart = {
                                title :'',
                                legend: {position: 'bottom'},
                                hAxis: {
                                    viewWindowMode:'explicit',
                                    format: 'dd/MM/yyyy',
                                    gridlines: { count: arr_date_active_customer.length},
                                    ticks:arr_date_active_customer
                                },
                                vAxis:{
                                    viewWindow: {
                                        min:0,
                                        max:maximum_y_axis_active_customer
                                    },
                                    viewWindowMode:'explicit',
                                    gridlines: { count: 6},
                                    ticks:arr_ticks_active_customer
                                },
                                colors:['red']
                            };
                            var chart_column = new google.visualization.ColumnChart(document.getElementById('line_chart_2'));
                            chart_column.draw(data_active_customer, option_line_chart);
                        }


                        var data = new google.visualization.DataTable();
                        data.addColumn('date', 'Days');
                        data.addColumn('number', admin_working_account_report_index.language_res.registered_customer_line_title);
                        data.addColumn('number',admin_working_account_report_index.language_res.active_customer_line_title);

                        var vMax = 0;
                        for(var index = 0;index < response.Data.length;index++ ){
                            var yyyy = Math.round(response.Data[index].ReportDate / 10000) ;
                            var mm = Math.round((response.Data[index].ReportDate - yyyy * 10000)/100) ;
                            var dd = Math.round((response.Data[index].ReportDate - yyyy * 10000 - mm * 100)) ;
                            var cvetDT = new Date(yyyy,mm-1,dd);
                            if (is_show_column_chart_active_customer){
                                arr_date_active_customer.push(cvetDT);
                            }
                            data.addRow([cvetDT,response.Data[index].CountRegisteredCustomer,response.Data[index].CountActiveCustomer]);
                        }

                        if(response.Data.length > 5){
                            var table = new google.visualization.Table(document.getElementById('table_chart'));
                            var formatter_ddMMyyy =  new google.visualization.DateFormat({pattern: 'dd/MM/yyyy'});
                            formatter_ddMMyyy.format(data,0);
                            table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
                        }
                        else{
                            var table = new google.visualization.Table(document.getElementById('table_chart'));
                            var formatter_ddMMyyy =  new google.visualization.DateFormat({pattern: 'dd/MM/yyyy'});
                            formatter_ddMMyyy.format(data,0);
                            table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
                        }
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });

        $('button[name=reset]', wrap_search).off('click').on('click', function () {
            $('input[name=StartDate]', wrap_search).val($('input[name=HiddStartDate]', wrap_search).val());
            $('input[name=EndDate]', wrap_search).val($('input[name=HiddEndDate]', wrap_search).val());
        });
    }
};