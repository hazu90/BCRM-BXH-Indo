﻿var sale_report_index = {
    init: function () {
        $(document).ready(function () {
            app.initPortletTools();
            app.initDateTimePickers();
            var frm_search = $('#frmSearchSaleReport');
            app.initChoosenSelect(frm_search);
            sale_report_index.action();
            sale_report_index.search();
        });
    }
    ,action : function () {
        var frm_search = $('#frmSearchSaleReport');
        $('button#btnSearch',frm_search).off('click').on('click', function () {
            sale_report_index.search();
        });
        $('button#btnReset',frm_search).off('click').on('click', function () {
            sale_report_index.reset();
        });
        $('select[name=GroupId]',frm_search).off('change').on('change', function () {
            sale_report_index.get_user_by_groupid();
        });
    }
    ,get_user_by_groupid: function () {
        var frm_search = $('#frmSearchSaleReport');
        var groupId =$('select[name=GroupId]',frm_search).val();
        var option = {
            controller:'SaleReport',
            action : 'GetUserByGroupId',
            data:{
                groupId : groupId
            },
            callback: function (response) {
                var dllAssignTo = $('select[name=AssignTo]',frm_search);
                var html = '';
                if( response.Code == ResponseCode.Success ){
                    if(response.Data != null && response.Data.length > 0   ){
                        for(var index = 0 ;index < response.Data.length ; index++ ){
                            html += '<option value="'+ response.Data[index].Key +'">' + response.Data[index].Value +'</option>';
                        }
                    }
                    dllAssignTo.html(html);
                }
                else{
                    dllAssignTo.html(html);
                }
                dllAssignTo.trigger('liszt:updated');
            }
        };
        sysrequest.send(option);
    }
    ,search : function () {
        var frm_search = $('#frmSearchSaleReport');
        var input_filterkeyword = $('input#txtFilterKeyword',frm_search);
        var select_groupid = $('select[name=GroupId]',frm_search);
        var input_startdate = $('input#txtStartDate',frm_search);
        var input_enddate = $('input#txtEndDate',frm_search);

        if(input_startdate.val() == null || input_startdate.val() == ''  ){
            sysmess.warning('You must enter start date !');
            return  ;
        }

        if(input_enddate.val() == null || input_enddate.val() == ''  ){
            sysmess.warning('You must enter end date !');
            return  ;
        }

        var option = {
            controller :'SaleReport',
            action :'Search',
            data :{
                FilterKeyword : input_filterkeyword.val()
               ,GroupId : select_groupid.val()
               ,StartDate : sysformat.date(input_startdate.val())
               ,EndDate : sysformat.date(input_enddate.val())
               ,AssignTo : $('select[name=AssignTo]',frm_search).val()
            },
            dataType :'html',
            element : $('#divListSaleReport'),
            callback: function () {

            }
        };
        sysrequest.send(option);
    }
    ,reset : function(){
        var frm_search = $('#frmSearchSaleReport');
        $('input#txtFilterKeyword',frm_search).val('');
        $('input#txtStartDate',frm_search).val($('input[name=FirstInMonth]',frm_search).val());
        $('input#txtEndDate',frm_search).val($('input[name=NowTime]',frm_search).val());
        $('select[name=GroupId]',frm_search).val($('select[name=GroupId] option:first',frm_search).val());
        $('select[name=GroupId]',frm_search).trigger('liszt:updated');
        sale_report_index.get_user_by_groupid();
    }
};