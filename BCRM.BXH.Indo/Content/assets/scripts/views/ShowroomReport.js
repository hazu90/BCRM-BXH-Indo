﻿var showroom_index = {
    init: function () {
        app.initPortletTools($('#divSearchShowroom'));
        app.initDateTimePickers($('#divActionSearchShowroom'));
        app.initChoosenSelect($('#divActionSearchShowroom'));
        showroom_index.action();
        showroom_index.search(1);
    },
    action: function () {
        var div_search = $('#divActionSearchShowroom');
        $('#btnSearch',div_search).off('click').on('click', function () {
            showroom_index.search(1);
        });
        $("#selCity", div_search).off("change").on("change", function () {
            var cityId = $(this).val();
            if (cityId > 0) {
                var option = {
                    controller: "City",
                    action: "GetDistrictByCity",
                    data: {
                        cityId: cityId
                    },
                    callback: function (response) {
                        if (response.Code == ResponseCode.Success) {
                            $("#selDistrict", div_search).empty();
                            if (response.Data.length > 0) {
                                $("#selDistrict", div_search).append('<option value="">--District--</option>');
                            }
                            var html = "";
                            for (var i = 0; i < response.Data.length; i++) {
                                html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                            }
                            $("#selDistrict", div_search).append(html);
                            $("#selDistrict", div_search).trigger("liszt:updated");

                        } else {
                            sysmess.error(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
            else{
                $("#selDistrict", div_search).html('<option value="">--District--</option>').trigger("liszt:updated");;
            }
        });
    },
    search: function (page_index) {
        var div_search = $('#divActionSearchShowroom');

        var txtFilterKeyword = $('#txtFilterKeyword',div_search);
        var selAssignTo = $('#selAssignTo',div_search);
        var selCity = $('#selCity',div_search);
        var selDistrict = $('#selDistrict',div_search);
        var selShowroomType = $('#selShowroomType',div_search);
        var txtStartDate = $('#txtStartDate',div_search);
        if(txtStartDate.val() == null || txtStartDate.val()=='' ){
            sysmess.warning('You must chose a start date');
            return;
        }

        var txtEndDate = $('#txtEndDate',div_search);
        if(txtEndDate.val() == null || txtEndDate.val() ==''){
            sysmess.warning('You must chose a end date');
            return;
        }

        var option = {
            controller:'ShowroomReport',
            action:'Search',
            data:{
                FilterKeyword:txtFilterKeyword.val(),
                AssignTo:selAssignTo.val(),
                CityId:selCity.val(),
                DistrictId:selDistrict.val(),
                ShowroomType :selShowroomType.val(),
                StartDate:sysformat.date(txtStartDate.val()),
                EndDate:sysformat.date(txtEndDate.val()),
                PageIndex:1,
                PageSize:25
            },
            dataType:'html',
            callback: function () {

            },
            element:$('#divListShowroom')
        };
        sysrequest.send(option);
    }
};