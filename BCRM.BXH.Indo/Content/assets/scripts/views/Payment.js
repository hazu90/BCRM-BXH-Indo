﻿var payment_index = {
    init: function () {
        $(document).ready(function () {
            app.initPortletTools();
            app.initChoosenSelect($('#divSearchPayment'));
            payment_index.action();
            payment_index.search(1);
        });
    },
    action: function () {
        var frm_search = $('#divSearchPayment');
        $('#btnSearch', frm_search).off('click').on('click', function () {
            payment_index.search(1);
        });
        var frm_list = $('#divListPayment');
        $('#btnAddNew', frm_list).off('click').on('click', function () {
            var option = {
                controller: 'Payment',
                action: 'Create',
                dataType: 'html',
                element: $('#divAddPayment'),
                callback: function () {
                    payment_index.display('divAddPayment');
                    payment_create.init();
                }
            };
            sysrequest.send(option);
        });
    },
    display : function(divId){
        divId == 'divListPayment' ? $('#divListPayment').removeClass('hide') : $('#divListPayment').addClass('hide');
        divId == 'divAddPayment' ? $('#divAddPayment').removeClass('hide') : $('#divAddPayment').addClass('hide');
    },
    search: function (page_index) {
        var frm_search = $('#divSearchPayment');
        var filterKeyword = $('input[name=FilterKeyword]', frm_search).val();
        var creatorId = $('#selCreator',frm_search).val();
        var status = $('#selStatus',frm_search).val();
        
        var option = {
            controller: 'Payment',
            action: 'Search',
            data: {
                FilterKeyword: filterKeyword,
                CreatorId: creatorId,
                Status: status,
                CreatedFromDate: null,
                CreatedToDate: null,
                PageIndex : page_index
            },
            callback: function () {
                payment_list.action();
            },
            dataType: 'html',
            element: $('#divTablePayment')
        };
        sysrequest.send(option);
    }
};
var payment_create = {
    init: function () {
        app.initPortletTools();
        app.initDateTimePickers();
        payment_create.action();
    },
    action: function () {
        var divCreate = $('#divAddPayment');
        $('button[name=GetInfoCustomer]', divCreate).off('click').on('click', function () {
            var phoneOrEmail = $('input[name=CustomerInfo]', divCreate).val();
            var option = {
                controller: 'Payment',
                action: 'GetByPhoneOrEmail',
                data: {
                    phoneOrEmail: phoneOrEmail
                },
                callback: function (response) {
                    if (response.Code == ResponseCode.Success) {
                        $('input[name=CustomerInfo]', divCreate).val(response.Data.FullName + '(' + phoneOrEmail + ')');
                        $('input[name=CustomerInfo]', divCreate).attr('readonly', 'readonly');
                        $('input[name=CustomerId]', divCreate).val(response.Data.Id);
                        $('button[name=GetInfoCustomer]', divCreate).addClass('hide');
                        $('button[name=ClearInfoCustomer]', divCreate).removeClass('hide');
                        //payment_index.search(1);
                        //payment_index.display('divListPayment');
                    }
                    else if (response.Code == ResponseCode.Error) {
                        sysmess.error(response.Message);
                    }
                    else {
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });

        $('button[name=ClearInfoCustomer]', divCreate).off('click').on('click', function () {
            $('input[name=CustomerInfo]', divCreate).val('');
            $('input[name=CustomerInfo]', divCreate).removeAttr('readonly');
            $('button[name=GetInfoCustomer]', divCreate).removeClass('hide');
            $(this).addClass('hide');
        });


        $('#btnAddNewPayment', divCreate).off('click').on('click', function () {
            payment_create.clear_error(divCreate);

            if (payment_create.validate()) {
                var customerId = $('input[name=CustomerId]', divCreate).val();
                var money = $('input[name=Money]', divCreate).val();
                var paymentDate = $('input[name=PaymentDate]', divCreate).val();
                var option = {
                    controller: 'Payment',
                    action: 'CreateAction',
                    data: {
                        CustomerId: customerId,
                        Money: money,
                        PaymentDate: sysformat.date(paymentDate)
                    },
                    callback: function (response) {
                        if (response.Code == ResponseCode.Success) {
                            sysmess.info(response.Message);
                            payment_index.search(1);
                            payment_index.display('divListPayment');
                        }
                        else if (response.Code == ResponseCode.Error) {
                            sysmess.error(response.Message);
                        }
                        else {
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });

        $('#btnCloseAddNewPayment', divCreate).off('click').on('click', function () {
            payment_index.display('divListPayment');
        });
    },
    validate: function () {
        var div_create = $('#divAddPayment');
        var txt_customerinfo = $('input[name=CustomerInfo]', div_create);
        var hid_customerid = $('input[name=CustomerId]', div_create);

        var isValid = true;
        if (txt_customerinfo.val() == null || txt_customerinfo.val() == '') {
            var ctr_gr = txt_customerinfo.closest('.control-group');
            ctr_gr.addClass('error');
            $('div[name=msg_valid]', ctr_gr).html('<div class="help-inline"> Customer info is required !</div> ');
            isValid = false;
        }

        var txt_money = $('input[name=Money]', div_create);
        if (txt_money.val() == null || txt_money.val() == '') {
            var ctr_gr = txt_money.closest('.control-group');
            ctr_gr.addClass('error');
            $('div[name=msg_valid]', ctr_gr).html('<div class="help-inline">Money is required ! </div>');
            isValid = false;
        }

        var txt_paymentdate = $('input[name=PaymentDate]', div_create);
        if (txt_paymentdate.val() == null || txt_paymentdate.val() == '') {
            var ctr_gr = txt_paymentdate.closest('.control-group');
            ctr_gr.addClass('error');
            $('div[name=msg_valid]', ctr_gr).html('<div class="help-inline">Payment date is required !</div>');
            isValid = false;
        }

        return isValid;
    },
    clear_error: function (frm) {
        $('.control-group', frm).each(function () {
            $(this).removeClass('error');
        });
        $('div[name=msg_valid]', frm).each(function () {
            $(this).html('');
        });
    }
};

var payment_list = {
    tr_element: null,
    index : 0,
    action: function () {
        var frm_search = $('#divListPayment');
        $('a[name=approved]',frm_search).each(function () {
            $(this).off('click').on('click', function () {
                var id = $(this).data('id');
                payment_list.tr_element = $(this).closest('tr');
                payment_list.index = $('td[name=wrap_index]', payment_list.tr_element).text().trim();

                var option = {
                    controller: 'Payment',
                    action: 'GetTokenKeyForGeneralView',
                    data: {
                        id : id
                    },
                    callback: function (response) {
                        var wrapApproved = $('#divApprovePayment');
                        if (response.Code == ResponseCode.Success) {
                            $('input[name=ApproveTokenEncrypt]', wrapApproved).val(response.Data);
                            $('input[name=HiddId]', wrapApproved).val(id);
                            payment_list.approve_clearerror();
                            payment_list.approve_action();
                        }
                    }
                };
                sysrequest.send(option);
            });
        });

        $('a[name=reject]', frm_search).each(function () {
            $(this).off('click').on('click', function () {
                var id = $(this).data('id');
                payment_list.tr_element = $(this).closest('tr');
                payment_list.index = $('td[name=wrap_index]', payment_list.tr_element).text().trim();

                var option = {
                    controller: 'Payment',
                    action: 'GetTokenKeyForGeneralView',
                    data: {
                        id: id
                    },
                    callback: function (response) {
                        var wrapReject = $('#divRejectPayment');
                        if (response.Code == ResponseCode.Success) {
                            $('input[name=RejectTokenEncrypt]', wrapReject).val(response.Data);
                            $('input[name=HiddId]', wrapReject).val(id);
                            payment_list.reject_clearerror();
                            payment_list.reject_action();
                        }
                    }
                };
                sysrequest.send(option);
            });
        });
    },
    approve_action: function () {
        var wrap_approve = $('#divApprovePayment');
        $('button[name=save]', wrap_approve).off('click').on('click', function () {
            payment_list.approve_clearerror();

            var id = $('input[name=HiddId]', wrap_approve).val();
            var token = $('input[name=ApproveTokenEncrypt]', wrap_approve).val();
            var verifiedDate = $('input[name=VerifiedPaymentDate]', wrap_approve).val();
            if (payment_list.approve_validate()) {
                var option = {
                    controller: 'Payment',
                    action: 'Approved',
                    data: {
                        id: id,
                        verifiedPaymentDate: sysformat.date(verifiedDate),
                        token : token
                    },
                    callback: function (response) {
                        $('button[name=close]', wrap_approve).trigger('click');
                        if (response.Code == ResponseCode.Success) {
                            //sysmess.info(response.Message);
                            payment_list.get_view_byid(id);
                        }
                        else if (reponse.Code == ResponseCode.Error) {
                            sysmess.error(response.Message);
                        }
                        else {
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });

    },
    approve_validate: function () {
        var wrap_approve = $('#divApprovePayment');
        var is_valid = true;
        var txt_verifiedpaymentdate = $('input[name=VerifiedPaymentDate]', wrap_approve);
        if (txt_verifiedpaymentdate.val() == null || txt_verifiedpaymentdate.val() == '') {
            var ctr_gr = txt_verifiedpaymentdate.closest('.control-group');
            ctr_gr.addClass('error');
            $('div[name=msg_valid]', ctr_gr).html('<div class="help-inline">Verified payment date is required ! </div>');
            is_valid = false;
        }
        return is_valid;
    },
    approve_clearerror: function () {
        var wrap_approve = $('#divApprovePayment');
        $('.control-group', wrap_approve).each(function () {
            $(this).removeClass('error');
        });

        $('div[name=msg_valid]', wrap_approve).each(function () {
            $(this).html('');
        });
    },
    reject_action: function () {
        var wrap_reject = $('#divRejectPayment');
        $('button[name=reject]', wrap_reject).off('click').on('click', function () {
            payment_list.reject_clearerror();

            if (payment_list.reject_validate()) {
                var id = $('input[name=HiddId]', wrap_reject).val();

                var option = {
                    controller: 'Payment',
                    action: 'Reject',
                    data: {
                        id: id,
                        feedBack: $('textarea[name=Feedback]', wrap_reject).val(),
                        token: $('input[name=RejectTokenEncrypt]', wrap_reject).val()
                    },
                    callback: function (response) {
                        $('button[name=close]', wrap_reject).trigger('click');
                        if (response.Code == ResponseCode.Success) {
                            //sysmess.info(response.Message);
                            payment_list.get_view_byid(id);
                        }
                        else if (response.Code == ResponseCode.Error) {
                            sysmess.error(response.Message);

                        }
                        else {
                            sysmess.warning(response.Message);
                        }
                    }

                };
                sysrequest.send(option);
            }
        });
    },
    reject_validate: function () {
        var wrap_reject = $('#divRejectPayment');
        var isValid = true;
        var txt_feedback = $('textarea[name=Feedback]', wrap_reject);
        if (txt_feedback.val() == null || txt_feedback.val() == '') {
            var ctr_gr = txt_feedback.closest('.control-group');
            ctr_gr.addClass('error');
            $('div[name=msg_valid]', ctr_gr).html('<div class="help-inline">Feedback is required !</div>');
            isValid = false;
        }
        return isValid;
    },
    reject_clearerror: function () {
        var wrap_reject = $('#divRejectPayment');
        $('.control-group', wrap_reject).each(function () {
            $(this).removeClass('error');
        });

        $('div[name=msg_valid]', wrap_reject).each(function () {
            $(this).html('');
        });
    },
    get_view_byid: function (id) {
        var option = {
            controller: 'Payment',
            action: 'GetViewByPaymentId',
            data: {
                index: payment_list.index,
                id: id
            },
            element: payment_list.tr_element,
            dataType: 'html',
            callback: function () {
                payment_list.action();
            }
        };
        sysrequest.send(option);
    }
}