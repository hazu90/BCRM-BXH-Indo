﻿var customer_index = {
    init: function () {
        $(document).ready(function () {
            app.initDateTimePickers();
            app.initPortletTools();
            app.initChoosenSelect($('#frmSearchCustomer'));
            var form = $("#frmSearchCustomer");
            $("#selCity", form).off("change").on("change", function () {
                var cityId = $(this).val();
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#selDistrict", form).empty();
                                if (response.Data.length > 0) {
                                    $("#selDistrict", form).append('<option value="">--Kota/kab--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#selDistrict", form).append(html);
                                $("#selDistrict", form).trigger('liszt:updated');

                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            customer_index.search();
            $("#btnReset", form).off("click").on("click", function () {
                customer_index.reset();
            });
            $("#btnSearch", form).off("click").on("click", function () {
                customer_index.display_functionality('divBodyListCustomer');
                customer_index.search();
            });

            $('#btnExportExcel',form).off('click').on('click', function () {
                var startDate = $('#txtStartDate',form).val();
                if(startDate==null  || startDate ==''){
                    startDate ='';
                }
                $('#hiddStartDate',form).val(sysformat.date(startDate,'mm/dd/yyyy'));
                var endDate = $('#txtEndDate',form).val();
                if(endDate == null || endDate == ''){
                    endDate = '';
                }
                $('#hiddEndDate',form).val(sysformat.date(endDate,'mm/dd/yyyy'));
                form.submit();
            } );

            customer_index.action();
            form.off('keydown').on('keydown', function (e) {
                customer_index.hitting_enter(e);
            });

            // init in assigning form
            if($('#wrapAssignTo').length >0 ){
                var wrapAssignTo = $('#wrapAssignTo');
                var groupId = $('select[name=group]',wrapAssignTo).val();
                var option = {
                    controller:'Customer',
                    action:'GetSaleByGroupId',
                    data:{
                        groupId:groupId
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success ){
                            var html = '<option value="">--Choose a saler--</option>';
                            if(response.Data != null && response.Data.length >0 ){
                                for(var index = 0;index < response.Data.length;index++ ){
                                    html += '<option value="'+ response.Data[index].UserName +'">'+ response.Data[index].UserName +'</option>';
                                }
                            }
                            $('select[name=saler]',wrapAssignTo).html(html);
                        }
                        else{
                            $('button[name=close]',wrapAssignTo).trigger('click');
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }

            // init in assigning after-sale form
            if($('#wrapAssignToASA').length > 0 ){
                var wrapAssignToASA = $('#wrapAssignToASA');
                var groupId = $('select[name=group]',wrapAssignToASA).val();
                var option = {
                    controller:'Customer',
                    action:'GetAfterSaleByGroupId',
                    data:{
                        groupId:groupId
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success ){
                            var html = '<option value="">-- Choose a after-sale --</option>';
                            if(response.Data != null && response.Data.length >0 ){
                                for(var index = 0;index < response.Data.length;index++ ){
                                    html += '<option value="'+ response.Data[index].UserName +'">'+ response.Data[index].UserName +'</option>';
                                }
                            }
                            $('select[name=saler]',wrapAssignToASA).html(html);
                        }
                        else{
                            $('button[name=close]',wrapAssignToASA).trigger('click');
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
    },
    reset: function () {
        try {
            var form = $('#frmSearchCustomer');
            $("#txtFilterKeyword", form).val('');

            $("#selAssignTo", form).val($("#selAssignTo option:first", form).val());
            $("#selAssignTo", form).trigger('liszt:updated');

            $('#selCreator',form).val($("#selCreator option:first", form).val());
            $("#selCreator", form).trigger('liszt:updated');

            $('#selAftersale',form).val($("#selAftersale option:first", form).val());
            $("#selAftersale", form).trigger('liszt:updated');

            $('#selCity', form).val($("#selCity option:first", form).val());
            $('#selCity', form).trigger('liszt:updated');

            $('#selDistrict',form).html('<option value="">--Kota/kab--</option>');
            $('#selDistrict',form).trigger('liszt:updated');

            $('#selSort', form).val($('#selSort option:first', form).val());
            $('#selSort', form).trigger('liszt:updated');

            $('#selCustomerType', form).val($('#selCustomerType option:first', form).val());
            $('#selCustomerType', form).trigger('liszt:updated');

            $('#selAssistanceStatus', form).val($('#selAssistanceStatus option:first', form).val());
            $('#selAssistanceStatus', form).trigger('liszt:updated');

            $('#selPostingNumber', form).val($('#selPostingNumber option:first', form).val());
            $('#selPostingNumber', form).trigger('liszt:updated');

            $('#divStartDate input', form).val("");
            $('#divEndDate input', form).val("");
        }
        catch (e) {
            sysmess.error('Error javascript! <br />' + e);
        }
    },
    search: function (page) {
        var form = $('#frmSearchCustomer');
        var cityId = -1;
        var districtId = -1;
        if ($("#selCity", form).val() > 0) {
            cityId = $("#selCity", form).val();
        }
        if ($("#selDistrict", form).val() > 0) {
            districtId = $("#selDistrict", form).val();
        }
        var selType = $("#selCustomerType", form);
        var type = -1;
        if(selType.val() != null && selType.val() != ''){
            type = selType.val();
        }

        var strActiveMonth = $('#txtActiveStatusInMonth', form).val();
        var activeMonth = strActiveMonth.split('/')[1] + strActiveMonth.split('/')[0];

        var options = {
            controller: 'Customer',
            action: 'GetListCustomer',
            dataType: 'html',
            data: {
                FilterKeyword: $("#txtFilterKeyword", form).val(),
                StartDate: sysformat.date($("#txtStartDate", form).val()),
                EndDate: sysformat.date($("#txtEndDate", form).val()),
                CityId: cityId,
                DistrictId: districtId,
                AssignTo: $("select#selAssignTo", form).val(),
                Type: type,
                Sort: $("select#selSort", form).val(),
                PageIndex: page,
                PageSize: 25,
                Creator : $('select#selCreator',form).val(),
                PostingAssistant : $('select#selAftersale',form).val(),
                PostingAssistanceStatus : $('select#selAssistanceStatus',form).val(),
                PostingNumber: $('select#selPostingNumber', form).val(),
                ActiveStatus: $('select#selActiveStatus', form).val(),
                ActiveStatusInMonth: activeMonth
            },
            element: $('div.form-inline', $('#divListCustomer')),
            blockUI: $('div#main-content'),
            scrollUI: $('#divListCustomer').parent(),
            callback: function () {
                customer_list.init();
            }
        };
        sysrequest.send(options);
    },
    action : function () {
        // action in assigning to saler form
        var wrapAssignTo = $('#wrapAssignTo');
        $('select[name=group]',wrapAssignTo).off('change').on('change', function () {
            var groupId = $(this).val();
            var option = {
                controller:'Customer',
                action:'GetSaleByGroupId',
                data:{
                    groupId:groupId
                },
                callback: function (response) {
                    if(response.Code == ResponseCode.Success ){
                        var html = '<option value="">--Choose a saler--</option>';
                        if(response.Data != null && response.Data.length >0 ){
                            for(var index = 0;index < response.Data.length;index++ ){
                                html += '<option value="'+ response.Data[index].UserName +'">'+ response.Data[index].UserName +'</option>';
                            }
                        }
                        $('select[name=saler]',wrapAssignTo).html(html);
                    }
                    else{
                        $('button[name=close]',wrapAssignTo).trigger('click');
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });
        $('button[name=save]',wrapAssignTo).off('click').on('click', function () {
            var cid = $(this).attr('cid');
            var select_saler = $('select[name=saler]',wrapAssignTo);
            var ctr_group = select_saler.closest('.control-group');
            ctr_group.removeClass('error');
            $('.help-inline',ctr_group).remove();
            if(select_saler.val() == null || select_saler.val() == ''){
                select_saler.parent().parent().addClass('error');
                select_saler.parent().parent().append('<span class=\"help-inline no-left-padding\">You have to choose a saler</span>');
                return;
            }
            var option={
                controller:'Customer',
                action :'AssignToSaler',
                data:{
                    customerId:cid,
                    userName: select_saler.val(),
                    signatureToken : $('input[name=AssignToTokenEncrypt]',wrapAssignTo).val()
                },
                callback: function (response) {
                    $('button[name=close]',wrapAssignTo).trigger('click');
                    if(response.Code == ResponseCode.Success){
                        sysmess.info(response.Message);
                        customer_index.search(customer_list.get_current_page());
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });

        // action in note form
        var wrapNote = $('#wrapNote');
        $('button[name=save]',wrapNote).off('click').on('click', function () {
            var id = $(this).attr('cid');
            var text_note = $('textarea[name=note]',wrapNote);
            if(text_note.val() == null || text_note.val() == ''){
                text_note.parent().parent().addClass('error');
                text_note.parent().parent().append('<span class=\"help-inline no-left-padding\">You have to text a note !</span>');
                return;
            }
            var note = text_note.val();
            var option ={
                controller:'Customer',
                action:'Note',
                data:{
                    id:id,
                    note:note,
                    signatureToken : $('input[name=AssignToTokenEncrypt]',wrapNote).val()
                },
                callback: function (response) {
                    $('button[name=close]',wrapNote).trigger('click');
                    if(response.Code == ResponseCode.Success ){
                        sysmess.info(response.Message);
                    }
                    else if ( response.Code == ResponseCode.Error ){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });

        // action in assign to after-saler form
        var wrapAssignToASA = $('#wrapAssignToASA');
        $('select[name=group]',wrapAssignToASA).off('change').on('change', function () {
            var groupId = $(this).val();
            var option = {
                controller:'Customer',
                action:'GetAfterSaleByGroupId',
                data:{
                    groupId:groupId
                },
                callback: function (response) {
                    if(response.Code == ResponseCode.Success ){
                        var html = '<option value="">--Choose a after-saler--</option>';
                        if(response.Data != null && response.Data.length >0 ){
                            for(var index = 0;index < response.Data.length;index++ ){
                                html += '<option value="'+ response.Data[index].UserName +'">'+ response.Data[index].UserName +'</option>';
                            }
                        }
                        $('select[name=saler]',wrapAssignToASA).html(html);
                    }
                    else{
                        $('button[name=close]',wrapAssignToASA).trigger('click');
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });

        $('button[name=save]',wrapAssignToASA).off('click').on('click', function () {
            var cid = $(this).attr('cid');
            var select_saler = $('select[name=saler]',wrapAssignToASA);
            if(select_saler.val() == null || select_saler.val() == ''){
                select_saler.parent().parent().addClass('error');
                select_saler.parent().parent().append('<span class=\"help-inline no-left-padding\">You have to choose a after-saler</span>');
                return;
            }
            var option={
                controller:'Customer',
                action :'AssignToAfterSaler',
                data:{
                    customerId:cid,
                    userName: select_saler.val(),
                    signatureToken : $('input[name=AssignToTokenEncrypt]',wrapAssignToASA).val()
                },
                callback: function (response) {
                    $('button[name=close]',wrapAssignToASA).trigger('click');
                    if(response.Code == ResponseCode.Success){
                        sysmess.info(response.Message);
                        customer_index.search(customer_list.get_current_page());
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });

        // action in
        var wrapAddRequestRetrievePosting = $('#wrapAddRequestRetrievePosting');
        $('button#btnAddRequest',$('#wrapPostingAssistanceHistory')).off('click').on('click', function () {
            customer_create.clear_error(wrapAddRequestRetrievePosting);
            $('input[name=postingnumber]',wrapAddRequestRetrievePosting).val('');
            $('button[name=save]',$('#wrapAddRequestRetrievePosting')).data('rrpid',0);
        });

        $('button[name=save]',wrapAddRequestRetrievePosting).off('click').on('click', function () {
            var cusId  = $(this).data('cid');
            var postingNews = $('input[name=postingnumber]',wrapAddRequestRetrievePosting).val();

            customer_create.clear_error(wrapAddRequestRetrievePosting);

            if(postingNews == null || postingNews == ''){
                $('input[name=postingnumber]',wrapAddRequestRetrievePosting).parent().parent().addClass('error');
                $('input[name=postingnumber]',wrapAddRequestRetrievePosting).parent().append('<span class=\"help-inline no-left-padding\">The requested postings is required !</span>');
                return false;
            }
            else{
                if(postingNews.indexOf('.') > -1){
                    $('input[name=postingnumber]',wrapAddRequestRetrievePosting).parent().parent().addClass('error');
                    $('input[name=postingnumber]',wrapAddRequestRetrievePosting).parent().append('<span class=\"help-inline no-left-padding\">The requested postings is just integer !</span>');
                    return false;
                }
                else{
                    var  vlue = parseInt(postingNews);
                    if(vlue <=0){
                        $('input[name=postingnumber]',wrapAddRequestRetrievePosting).parent().parent().addClass('error');
                        $('input[name=postingnumber]',wrapAddRequestRetrievePosting).parent().append('<span class=\"help-inline no-left-padding\">The requested postings is just integer and larger than 0 !</span>');
                        return false;
                    }
                }
            }

            var requestId = $(this).data('rrpid');

            var option = {
               controller :'Customer',
               action :'AddRequestRetrievePosting',
               data :{
                   customerId :cusId,
                   requestId : requestId,
                   postingNews : postingNews
               },
               callback: function (response) {
                   $('button[name=close]',wrapAddRequestRetrievePosting).trigger('click');
                   if(response.Code == ResponseCode.Success){
                       sysmess.info(response.Message);
                       customer_index.request_retrieve_posting_get_by_customerid(cusId);
                   }
                   else if(response.Code == ResponseCode.Error){
                       sysmess.error(response.Message);
                   }
                   else{
                       sysmess.warning(response.Message);
                   }
               }
            } ;
            sysrequest.send(option);
        });
    },
    hitting_enter: function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $(this).blur();
            customer_index.search(1);
        }
    },
    display_functionality : function (div_element) {
        if(div_element == 'divBodyListCustomer'){
            $('#divBodyListCustomer').removeClass('hide');
        }
        else{
            $('#divBodyListCustomer').addClass('hide');
        }

        if(div_element == 'divCreateCustomer'){
            $('#divCreateCustomer').removeClass('hide');
        }
        else{
            $('#divCreateCustomer').addClass('hide');
        }

        if(div_element == 'divEditCustomer'){
            $('#divEditCustomer').removeClass('hide');
        }
        else{
            $('#divEditCustomer').addClass('hide');
        }

        if(div_element == 'wrapCustomerDetail'){
            $('#wrapCustomerDetail').removeClass('hide');
        }
        else{
            $('#wrapCustomerDetail').addClass('hide');
        }

        if(div_element == 'wrapPostingAssistanceHistory'){
            $('#wrapPostingAssistanceHistory').removeClass('hide');
        }
        else{
            $('#wrapPostingAssistanceHistory').addClass('hide');
        }
    },
    request_retrieve_posting_get_by_customerid : function(cusId){
        var option = {
            controller :'Customer',
            action :'ViewRequestRetrievePostingHistory',
            data:{
                customerId : cusId
            },
            dataType:'html',
            element : $('.dataTables_wrapper',$('#wrapPostingAssistanceHistory')),
            callback: function () {
                customer_index.request_retrieve_posting_action();
            }
        };
        sysrequest.send(option);
    },
    request_retrieve_posting_action : function () {
        $('a[name=edit]',$('#wrapPostingAssistanceHistory')).each(function () {
            $(this).off('click').on('click', function () {
                customer_create.clear_error($('#wrapAddRequestRetrievePosting'));
                var requestId = $(this).data('rrpid');
                var cusId = $(this).data('cusid');
                var tr_element = $(this).parent().parent();
                var postingnumber = $.trim($('td[name=postingrequestamount]',tr_element).html());
                $('button[name=save]',$('#wrapAddRequestRetrievePosting')).data('cid',cusId);
                $('button[name=save]',$('#wrapAddRequestRetrievePosting')).data('rrpid',requestId);
                $('input[name=postingnumber]',$('#wrapAddRequestRetrievePosting')).val(postingnumber);
            });
        });
        $('a[name=delete]',$('#wrapPostingAssistanceHistory')).each(function () {
            $(this).off('click').on('click', function () {
                var rrId = $(this).data('rrpid');
                var cusId = $(this).data('cusid');
                var option_delete = {
                    controller :'Customer',
                    action :'RemoveRequestRetrievePosting',
                    data:{
                        customerId : cusId
                       ,requestId  :rrId
                    },
                    callback : function(response){
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            customer_index.request_retrieve_posting_get_by_customerid(response.Data);
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysmess.confirm('Do you want to delete this request ?', function () {
                    sysrequest.send(option_delete);
                });
            });
        });
        $('a[name=compeleting]',$('#wrapPostingAssistanceHistory')).each(function () {
            $(this).off('click').on('click', function () {
                var rrId = $(this).data('rrpid');
                var cusId = $(this).data('cusid');
                var option_completed = {
                    controller :'Customer',
                    action :'CompletedPostingAssitance',
                    data :{
                        requestId :rrId
                       ,customerId : cusId
                    },
                    callback : function(response){
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            customer_index.request_retrieve_posting_get_by_customerid(cusId);
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option_completed);
            });
        });
    }
};
var customer_list = {
    init: function () {
        $(document).ready(function () {
            var area = $("#divListCustomer");
            $("#btnAddNew", area).off("click").on("click", function () {
                customer_index.display_functionality('divCreateCustomer');
                var frm_create = $('#frmCreateCustomer');
                customer_create.clear_error(frm_create);

                $('#ddlPostingType',frm_create).attr('style','');
                $('#ddlPostingType',frm_create).removeClass('chzn-done');
                if($('#ddlPostingType_chzn',frm_create).length >0){
                    $('#ddlPostingType_chzn',frm_create).remove();
                }

                $('#ddlRegion',frm_create).attr('style','');
                $('#ddlRegion',frm_create).removeClass('chzn-done');
                if($('#ddlRegion_chzn',frm_create).length >0){
                    $('#ddlRegion_chzn',frm_create).remove();
                }

                $('#ddlCity',frm_create).attr('style','');
                $('#ddlCity',frm_create).removeClass('chzn-done');
                if($('#ddlCity_chzn',frm_create).length >0){
                    $('#ddlCity_chzn',frm_create).remove();
                }

                $('#ddlDistrict',frm_create).attr('style','');
                $('#ddlDistrict',frm_create).removeClass('chzn-done');
                if($('#ddlDistrict_chzn',frm_create).length >0){
                    $('#ddlDistrict_chzn',frm_create).remove();
                }

                app.initChoosenSelect($('#frmCreateCustomer'));
                //customer_create.init();
                profile_create.reset();
            });

            $("a.edit", area).each(function () {
                var curr = $(this);
                curr.off("click").on("click", function () {
                    var custId = $(this).attr("cid");
                    customer_index.display_functionality('divEditCustomer');
                    var frm_edit = $('#frmUpdateCustomer');
                    $('#ddlPostingTypeEdit',frm_edit).attr('style','');
                    $('#ddlPostingTypeEdit',frm_edit).removeClass('chzn-done');
                    if($('#ddlPostingTypeEdit_chzn',frm_edit).length >0){
                        $('#ddlPostingTypeEdit_chzn',frm_edit).remove();
                    }

                    $('#ddlRegionEdit',frm_edit).attr('style','');
                    $('#ddlRegionEdit',frm_edit).removeClass('chzn-done');
                    if($('#ddlRegionEdit_chzn',frm_edit).length >0){
                        $('#ddlRegionEdit_chzn',frm_edit).remove();
                    }

                    $('#ddlCityEdit',frm_edit).attr('style','');
                    $('#ddlCityEdit',frm_edit).removeClass('chzn-done');
                    if($('#ddlCityEdit_chzn',frm_edit).length >0){
                        $('#ddlCityEdit_chzn',frm_edit).remove();
                    }

                    $('#ddlDistrictEdit',frm_edit).attr('style','');
                    $('#ddlDistrictEdit',frm_edit).removeClass('chzn-done');
                    if($('#ddlDistrictEdit_chzn',frm_edit).length >0){
                        $('#ddlDistrictEdit_chzn',frm_edit).remove();
                    }
                    app.initChoosenSelect(frm_edit);
                    //customer_edit.init();
                    customer_edit.getById(custId);
                });
            });

            //$("a.lock", area).each(function () {
            //    var curr = $(this);
            //    curr.off("click").on("click", function () {
            //        var custId = $(this).attr("cid");
            //        var option = {
            //            controller: "Customer",
            //            action: "Block",
            //            data: { customerId: custId },
            //            callback: function (response) {
            //                switch (response.Code) {
            //                    case ResponseCode.Success:
            //                        sysmess.info("Successfully!");
            //                        customer_index.search(1);
            //                        break;
            //                    case ResponseCode.Error:
            //                        sysmess.error("Error: " + response.Message);
            //                        sysmess.log(response.Message);
            //                        break;
            //                }
            //            }
            //        };
            //        sysmess.confirm('Do you want block this file customer?', function () {
            //            sysrequest.send(option);
            //        });
            //    });
            //});
            //
            //$("a.unlock", area).each(function () {
            //    var curr = $(this);
            //    curr.off("click").on("click", function () {
            //        var custId = $(this).attr("cid");
            //        var option = {
            //            controller: "Customer",
            //            action: "UnBlock",
            //            data: { customerId: custId },
            //            callback: function (response) {
            //                switch (response.Code) {
            //                    case ResponseCode.Success:
            //                        sysmess.info("Successfully!");
            //                        customer_index.search(1);
            //                        break;
            //                    case ResponseCode.Error:
            //                        sysmess.error("Error: " + response.Message);
            //                        sysmess.log(response.Message);
            //                        break;
            //                }
            //            }
            //        };
            //        sysmess.confirm('Do you want active this file customer?', function () {
            //            sysrequest.send(option);
            //        });
            //    });
            //});

            $("a.phonenumber", area).each(function () {
                var curr = $(this);
                curr.off("click").on("click", function () {
                    var custId = $(this).attr("cid");
                    var tr_el = $(this).closest('tr');
                    var options = {
                        controller: 'Profile',
                        action: 'GetListByCustomer',
                        dataType: 'html',
                        data: {
                            customerId: custId
                        },
                        element: $('#divListProfile'),
                        blockUI: $('div#main-content'),
                        scrollUI: $('#divListProfile').parent(),
                        callback: function () {
                            profile_list.tr_row_edit_phone = tr_el;
                            profile_list.customerid = custId;
                            profile_list.init();
                        }
                    };
                    sysrequest.send(options);
                });
            });

            $('a.assignto',area).each(function () {
                $(this).off('click').on('click', function () {
                    var cusId = $(this).attr('cid');
                    $('button[name=save]',$('#wrapAssignTo')).attr('cid',cusId);
                    var option = {
                        controller :'Customer',
                        action:'GetTokenKeyForGeneralView',
                        data:{
                            id : cusId
                        },
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                $('input[name=AssignToTokenEncrypt]',$('#wrapAssignTo')).val(response.Data);
                            }
                            else if(response.Code == ResponseCode.Error){
                                $('input[name=AssignToTokenEncrypt]',$('#wrapAssignTo')).val('');
                            }
                            else{
                                $('input[name=AssignToTokenEncrypt]',$('#wrapAssignTo')).val('');
                            }
                        }
                    };
                    sysrequest.send(option);
                });
            });

            $('a.blacklist',area).each(function () {
                $(this).off('click').on('click', function () {
                    var tr_element = $(this).parent().parent().parent().parent().parent();
                    var cusId = $(this).attr('cid');

                    sysmess.confirm( 'Do you want to blacklist this customer ?' , function () {
                        var option = {
                            controller:'Customer',
                            action:'Blacklist',
                            data:{
                                customerId : cusId,
                                signatureToken :''
                            },
                            callback: function (response) {
                                if(response.Code == ResponseCode.Success){
                                    customer_list.change_row_data(tr_element,cusId);
                                    sysmess.info(response.Message);
                                }
                                else if(response.Code == ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        //sysrequest.send(option);
                        var option_request_token = {
                            controller :'Customer',
                            action:'GetTokenKeyForOneRequest',
                            data:{
                                id:cusId
                            },
                            callback : function(response){
                                if(response.Code == ResponseCode.Success){
                                    option.data.signatureToken = response.Data;
                                    sysrequest.send(option);
                                }
                                else if(response.Code == ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        sysrequest.send(option_request_token);

                    });
                });
            });

            $('a.unblacklist',area).each(function () {
                $(this).off('click').on('click', function () {
                    var tr_element = $(this).parent().parent().parent().parent().parent();
                    var cid = $(this).attr('cid');

                    sysmess.confirm('Do you want to unblacklist this customer ?' , function () {
                        var option = {
                            controller:'Customer',
                            action:'UnBlacklist',
                            data:{
                                customerId : cid,
                                signatureToken :''
                            },
                            callback: function (response) {
                                if(response.Code == ResponseCode.Success){
                                    customer_list.change_row_data(tr_element,cid);
                                    sysmess.info(response.Message);
                                }
                                else if(response.Code == ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        //sysrequest.send(option);

                        var option_request_token = {
                            controller :'Customer',
                            action:'GetTokenKeyForOneRequest',
                            data:{
                                id:cid
                            },
                            callback : function(response){
                                if(response.Code == ResponseCode.Success){
                                    option.data.signatureToken = response.Data;
                                    sysrequest.send(option);
                                }
                                else if(response.Code == ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        sysrequest.send(option_request_token);
                    });
                });
            });

            $('a.note',area).each(function () {
                var cid = $(this).attr('cid');
                $(this).off('click').on('click', function () {
                    $('textarea[name=note]',$('#wrapNote')).val('');
                    $('button[name=save]',$('#wrapNote')).attr('cid',cid);

                    var option = {
                        controller :'Customer',
                        action:'GetTokenKeyForGeneralView',
                        data:{
                            id : cid
                        },
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                $('input[name=AssignToTokenEncrypt]',$('#wrapNote')).val(response.Data);
                            }
                            else if(response.Code == ResponseCode.Error){
                                $('input[name=AssignToTokenEncrypt]',$('#wrapNote')).val('');
                            }
                            else{
                                $('input[name=AssignToTokenEncrypt]',$('#wrapNote')).val('');
                            }
                        }
                    };
                    sysrequest.send(option);
                });
            });

            $('a.detail',area).each(function () {
                var cid = $(this).attr('cid');
                $(this).off('click').on('click', function () {
                    var option = {
                        controller : 'Customer',
                        action:'Detail',
                        data:{
                            customerId : cid
                        },
                        dataType:'html',
                        element:$('#wrapCustomerDetail'),
                        callback: function () {
                            var wrapCustomerDetail = $('#wrapCustomerDetail');
                            customer_index.display_functionality('wrapCustomerDetail');
                            app.initPortletTools();
                            // action in detail form
                            $('button[name=close]',wrapCustomerDetail).off('click').on('click', function () {
                                //$('#lnkRemoveCustomerActivityHistory',wrapCustomerDetail).trigger('click');
                                $('#lnkRemoveCustomerDetail',wrapCustomerDetail).trigger('click');
                                //customer_index.display_functionality('divBodyListCustomer');
                            });

                            $('#lnkRemoveCustomerDetail',wrapCustomerDetail).off('click').on('click', function () {
                                $('#lnkRemoveCustomerActivityHistory',wrapCustomerDetail).trigger('click');
                                customer_index.display_functionality('divBodyListCustomer');
                            });
                        }
                    };
                    sysrequest.send(option);
                });
            });

            //$('a.completedposting',area).each(function () {
            //    var cid = $(this).attr('cid');
            //    $(this).off('click').on('click', function () {
            //        var option = {
            //            controller :'Customer',
            //            action :'CompletedPostingAssitance',
            //            data:{
            //                customerId : cid
            //            },
            //            callback: function (response) {
            //                if(response.Code == ResponseCode.Success){
            //                    sysmess.info(response.Message);
            //                }
            //                else if(response.Code == ResponseCode.Error){
            //                    sysmess.error(response.Message);
            //                }
            //                else{
            //                    sysmess.warning(response.Message);
            //                }
            //            }
            //        };
            //
            //        sysmess.confirm('Do you get posting news completed ?', function () {
            //            sysrequest.send(option);
            //        });
            //    });
            //});

            //$('a.requestposting',area).each(function () {
            //    $(this).off('click').on('click', function () {
            //        var cusId = $(this).attr('cid');
            //        $('button[name=save]',$('#wrapAddRequestRetrievePosting')).data('cid',cusId);
            //        $('button[name=save]',$('#wrapAddRequestRetrievePosting')).data('rrpid',0);
            //        customer_index.display_functionality('wrapPostingAssistanceHistory');
            //        customer_index.request_retrieve_posting_get_by_customerid(cusId);
            //    });
            //});

            $('a.assigntoasa',area).each(function () {
                $(this).off('click').on('click', function () {
                    var cusId = $(this).attr('cid');
                    $('button[name=save]',$('#wrapAssignToASA')).attr('cid',cusId);
                    var option = {
                        controller :'Customer',
                        action:'GetTokenKeyForGeneralView',
                        data:{
                            id : cusId
                        },
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                $('input[name=AssignToTokenEncrypt]',$('#wrapAssignToASA')).val(response.Data);
                            }
                            else if(response.Code == ResponseCode.Error){
                                $('input[name=AssignToTokenEncrypt]',$('#wrapAssignToASA')).val('');
                            }
                            else{
                                $('input[name=AssignToTokenEncrypt]',$('#wrapAssignToASA')).val('');
                            }
                        }
                    };
                    sysrequest.send(option);
                });
            });

            $('a.asareceivegetposting',area).each(function () {
                $(this).off('click').on('click', function () {
                    var tr_element = $(this).parent().parent().parent().parent().parent();
                    var cusId = $(this).attr('cid');
                    sysmess.confirm('Do you want to receive this customer ?', function () {
                        var option = {
                            controller:'Customer'
                            ,action :'ReceiveGetListing'
                            ,data :{
                                customerId : cusId,
                                signatureToken :''
                            }
                            ,callback: function (response) {
                                if(response.Code == ResponseCode.Success){
                                    customer_list.change_row_data(tr_element,cusId);
                                    sysmess.info(response.Message);
                                }
                                else if(response.Code = ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        //sysrequest.send(option);

                        var option_request_token = {
                            controller :'Customer',
                            action:'GetTokenKeyForOneRequest',
                            data:{
                                id:cusId
                            },
                            callback : function(response){
                                if(response.Code == ResponseCode.Success){
                                    option.data.signatureToken = response.Data;
                                    sysrequest.send(option);
                                }
                                else if(response.Code == ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        sysrequest.send(option_request_token);

                    });
                });
            });

            $('a.resetpass',area).each(function () {
                $(this).off('click').on('click', function () {
                    var customerId = $(this).attr('cid');

                    sysmess.confirm('Do you want to reset password this customer ?', function () {
                        var option = {
                            controller :'Customer',
                            action:'ResetPassword',
                            data:{
                                customerId : customerId,
                                signatureToken :''
                            },
                            callback: function (response) {
                                if(response.Code == ResponseCode.Success){
                                    sysmess.info(response.Message);
                                }
                                else if(response.Code == ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        //sysrequest.send(option);

                        var option_request_token = {
                            controller :'Customer',
                            action:'GetTokenKeyForOneRequest',
                            data:{
                                id:customerId
                            },
                            callback : function(response){
                                if(response.Code == ResponseCode.Success){
                                    option.data.signatureToken = response.Data;
                                    sysrequest.send(option);
                                }
                                else if(response.Code == ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        sysrequest.send(option_request_token);
                    });
                });
            });
        });
    },
    get_current_page: function () {
        var curr_Page = 1;
        $('.dataTables_paginate li.active a').each(function () {
            var id_html = $(this).text().trim();
            if($.isNumeric(id_html)){
                curr_Page = parseInt(id_html);
            }
        });
        return curr_Page;
    },
    change_row_data : function(tr_element,customerId ){
        var index = $('td[name=wrapIndex]',tr_element).text();
        var option = {
            controller:'Customer',
            action:'GetViewByCustomerId',
            data:{
                index : index,
                customerId : customerId
            },
            dataType:'html',
            element : tr_element,
            callback: function () {
                customer_list.init();
            }
        };
        sysrequest.send(option);
    }
};
var customer_create =   {
    init: function () {
        $(document).ready(function () {
            var form = $('#frmCreateCustomer');
            //app.initChoosenSelect($('#frmCreateCustomer'));
            if ($('#txaDescription', form).length > 0 && $("#cke_txaDescription", form).length == 0) {
                syseditor.create('txaDescription');
                CKEDITOR.addCss('.cke_editable { height:160px; }');
            }
            $("#btnAddProfile").off("click").on("click", function () {
                profile_create.clear_error($("#frmCreateProfile"));
                profile_create.init();
            });
            $("#ddlRegion", form).off("change").on("change", function () {
                var regionId = $(this).val();
                var cityId = 0;
                if(regionId > 0){
                    var option = {
                        controller: "City",
                        action: "GetCityByRegion",
                        data: {
                            regionId: regionId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlCity", form).empty();
                                if (response.Data.length > 0) {
                                    cityId = response.Data[0].Id;
                                    $("#ddlCity", form).append('<option value="">--Province / City--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlCity", form).append(html);
                                $("#ddlCity", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlDistrict", form).empty();
                                if (response.Data.length > 0) {
                                    $("#ddlDistrict", form).append('<option value="">--District--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlDistrict", form).append(html);
                                $("#ddlDistrict", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            $("#ddlCity", form).off("change").on("change", function () {
                var cityId = $(this).val();
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlDistrict", form).empty();
                                if (response.Data.length > 0) {
                                    $("#ddlDistrict", form).append('<option value="">--District--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlDistrict", form).append(html);
                                $("#ddlDistrict", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            $("#btnAddNewCustomer", form).off("click").on("click", function () {
                $("span.help-inline", form).each(function () {
                    $(this).parent("div").parent("div").removeClass("error");
                    $(this).remove();
                });
                var checkValue = customer_create.valid_data();
                if (checkValue == true) {
                    var arrProfile = new Array();
                    $('#tblProfile tbody tr').each(function () {
                        arrProfile.push({
                            Name: $('td:eq(0) input', $(this)).val(),
                            PhoneNumber: $('td:eq(1) input', $(this)).val(),
                            PhoneAlias: $('td:eq(2) input', $(this)).val(),
                            Email: $('td:eq(3) input', $(this)).val()
                        });
                    });
                    var data = {
                        FullName: $("input#txtFullName", form).val(),
                        PhoneNumber: $("#txtPhoneNumber", form).val(),
                        //Status: $('#ddlCustomerStatus', form).val(),
                        PostingType: $("#ddlPostingType", form).val(),
                        Email: $("#txtEmail", form).val(),
                        Description: encodeURIComponent(syseditor.get_data('txaDescription')),
                        CityId: $("#ddlCity", form).val(),
                        DistrictId: $("#ddlDistrict", form).val(),
                        RegionId: $("#ddlRegion", form).val(),
                        CompanyId: 0,
                        Address:$('#txtAddress',form).val(),
                        ListProfile: arrProfile
                    };
                    var option = {
                        controller: "Customer",
                        action: "CreateAction",
                        data: JSON.stringify(data),
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                customer_index.search(1);
                                $("#divBodyListCustomer").removeClass("hide");
                                $("#divCreateCustomer").addClass("hide");
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error("Error: " + response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            $("#btnCloseCustInput", form).off("click").on("click", function () {
                $("#divBodyListCustomer").removeClass("hide");
                $("#divCreateCustomer").addClass("hide");
            });

            customer_create.reset();
        });
    },
    valid_data: function () {
        var valid = true;
        try {
            var focus = null;
            var form = $('#frmCreateCustomer');

            var fullName = $.trim($("#txtFullName", form).val());
            if (fullName == null || fullName == "") {
                focus = $("#txtFullName", form);
                $("#txtFullName", form).parent("div").parent("div").addClass("error");
                $("#txtFullName", form).next('span.help-inline').remove();
                $("#txtFullName", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter FullName of customer.</span>");
                valid = false;
            }

            var phoneNumber = $.trim($("#txtPhoneNumber", form).val());
            if (phoneNumber == null || phoneNumber == "") {
                $("#txtPhoneNumber", form).parent("div").parent("div").addClass("error");
                $("#txtPhoneNumber", form).next('span.help-inline').remove();
                $("#txtPhoneNumber", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Phone Number.</span>");
                valid = false;
            }

            //var custStatus = $.trim($("#ddlCustomerStatus", form).val());
            //if (custStatus == null || custStatus == '') {
            //    if (focus == null) {
            //        focus = $("#ddlCustomerStatus", form);
            //    }
            //    $("#ddlCustomerStatus", form).parent("div").parent("div").addClass("error");
            //    $("#ddlCustomerStatus", form).next('span.help-inline').remove();
            //    $("#ddlCustomerStatus", form).parent("div").append("<span class=\"help-inline no-left-padding\>Enter Status.</span>");
            //    valid = false;
            //}

            var custType = $.trim($("#ddlPostingType", form).val());
            if (custType == null || custType == "") {
                if (focus == null) {
                    focus = $("#ddlPostingType", form);
                }
                $("#ddlPostingType", form).parent("div").parent("div").addClass("error");
                $("#ddlPostingType", form).next('span.help-inline').remove();
                $("#ddlPostingType", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Type Of Customer.</span>");
                valid = false;
            }
            
            var city = $.trim($("#ddlCity", form).val());
            if (city == null || city == "") {
                if (focus == null) {
                    focus = $("#ddlCity", form);
                }
                $("#ddlCity", form).parent("div").parent("div").addClass("error");
                $("#ddlCity", form).next('span.help-inline').remove();
                $("#ddlCity", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Province.</span>");
                valid = false;
            }

            var district = $.trim($("#ddlDistrict", form).val());
            if (district == null || district == "") {
                if (focus == null) {
                    focus = $("#ddlDistrict", form);
                }
                $("#ddlDistrict", form).parent("div").parent("div").addClass("error");
                $("#ddlDistrict", form).next('span.help-inline').remove();
                $("#ddlDistrict", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Kota/Kab.</span>");
                valid = false;
            }

            var email = $.trim($("#txtEmail", form).val());
            if (email == null || email == "") {
                $("#txtEmail", form).parent("div").parent("div").parent("div").addClass("error");
                $("#txtEmail", form).parent("div").next('span.help-inline').remove();
                $("#txtEmail", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Email Address.</span>");
                if (focus == null) {
                    focus = $("#txtEmail", form);
                }
                valid = false;
            } else if (email != '' && !sysvalid.email(email)) {
                if (focus == null) {
                    focus = $("#txtEmail", form);
                }
                $("#txtEmail", form).parent("div").parent("div").parent("div").addClass("error");
                $("#txtEmail", form).parent("div").next('span.help-inline').remove();
                $("#txtEmail", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Invalid Email Address.</span>");
                valid = false;
            }

            var address = $('#txtAddress',form).val();
            if(address == null  || address==''){
                if (focus == null) {
                    focus = $("#txtAddress", form);
                }
                $("#txtAddress", form).parent("div").parent("div").addClass("error");
                $("#txtAddress", form).parent("div").next('span.help-inline').remove();
                $("#txtAddress", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Address is not empty.</span>");
                valid = false;
            }

            var description= syseditor.get_data('txaDescription');
            if(description != null && description != ''){
                if(description.length > 499){
                    $("#txaDescription", form).parent("div").parent("div").addClass("error");
                    $("#txaDescription", form).parent("div").next('span.help-inline').remove();
                    $("#txaDescription", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Description is too long (max length is 500 characters) !</span>");
                    valid = false;
                }
            }

            if (focus != null) {
                focus.focus();
                syscommon.scroll(focus);
            }
        } catch (e) {
            sysmess.error('Error javascript!<br />' + e);
            valid = false;
        }
        return valid;
    },
    clear_error: function (form) {
        $("span.help-inline", form).each(function () {
            $(this).parent("div").parent("div").removeClass("error");
            $(this).parent("div").parent("div").parent("div").removeClass("error");
            $(this).remove();
        });
    },
    get_city_district_when_change_region : function () {
        var form = $('#frmCreateCustomer');
        var regionId = $("#ddlRegion", form).val();
        var cityId = 0;
        if(regionId > 0){
            var option = {
                controller: "City",
                action: "GetCityByRegion",
                data: {
                    regionId: regionId
                },
                callback: function (response) {
                    if (response.Code == ResponseCode.Success) {
                        $("#ddlCity", form).empty();
                        if (response.Data.length > 0) {
                            cityId = response.Data[0].Id;
                            $("#ddlCity", form).append('<option value="">--Province / City--</option>');
                        }
                        var html = "";
                        for (var i = 0; i < response.Data.length; i++) {
                            html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                        }
                        $("#ddlCity", form).append(html);
                        $("#ddlCity", form).trigger('liszt:updated');
                    } else {
                        sysmess.error(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        }
        if (cityId > 0) {
            var option = {
                controller: "City",
                action: "GetDistrictByCity",
                data: {
                    cityId: cityId
                },
                callback: function (response) {
                    if (response.Code == ResponseCode.Success) {
                        $("#ddlDistrict", form).empty();
                        if (response.Data.length > 0) {
                            $("#ddlDistrict", form).append('<option value="">--District--</option>');
                        }
                        var html = "";
                        for (var i = 0; i < response.Data.length; i++) {
                            html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                        }
                        $("#ddlDistrict", form).append(html);
                        $("#ddlDistrict", form).trigger('liszt:updated');
                    } else {
                        sysmess.error(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        }
    },
    get_district_when_change_city : function () {
        var form = $('#frmCreateCustomer');
        var cityId = $("#ddlCity", form).val();
        if (cityId > 0) {
            var option = {
                controller: "City",
                action: "GetDistrictByCity",
                data: {
                    cityId: cityId
                },
                callback: function (response) {
                    if (response.Code == ResponseCode.Success) {
                        $("#ddlDistrict", form).empty();
                        if (response.Data.length > 0) {
                            $("#ddlDistrict", form).append('<option value="">--District--</option>');
                        }
                        var html = "";
                        for (var i = 0; i < response.Data.length; i++) {
                            html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                        }
                        $("#ddlDistrict", form).append(html);
                        $("#ddlDistrict", form).trigger('liszt:updated');
                    } else {
                        sysmess.error(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        }
    },
    reset : function () {
        var form = $('#frmCreateCustomer');
        $('input#txtFullName',form).val('');
        $('#txtPhoneNumber',form).val('');
        $('input#txtEmail',form).val('');
        $('select#ddlPostingType',form).val($('select#ddlPostingType option:first',form).val());
        $('select#ddlPostingType',form).trigger('liszt:updated');
        $('select#ddlRegion',form).val($('select#ddlRegion option:first',form).val() );
        $('select#ddlRegion',form).trigger('liszt:updated');
        $('input#txtAddress',form).val('');
        customer_create.get_city_district_when_change_region();
        customer_create.get_district_when_change_city();
        syseditor.set_data('txaDescription','');
    }
};
var customer_edit = {
    init: function(){
        $(document).ready(function () {
            var form = $('#frmUpdateCustomer');
            //app.initChoosenSelect(form);
            if ($('#txaDescriptionUpdate', form).length > 0 && $("#cke_txaDescriptionUpdate", form).length == 0) {
                syseditor.create('txaDescriptionUpdate');
                CKEDITOR.addCss('.cke_editable { height:160px; }');
            }
        });
    },
    getById: function (custId) {
        var form = $("#frmUpdateCustomer");
        customer_create.clear_error(form);
        var option = {
            controller: "Customer",
            action: "GetById",
            data: { customerId: custId },
            callback: function (response) {
                switch (response.Code) {
                    case ResponseCode.Success:
                        app.initChoosenSelect(form);
                        $("#hidCustomerId", form).val(response.Data.Id);
                        $('#hiddChecksumCustomerEditId',form).val(response.Data.SignatureChecksum);
                        $("#txtFullName", form).val(response.Data.FullName);
                        $('#txtPhoneNumberEdit',form).val(response.Data.PhoneNumber);
                        $("#ddlPostingTypeEdit", form).val(response.Data.PostingType);
                        $("#ddlPostingTypeEdit", form).trigger('liszt:updated');
                        $("#txtEmail", form).val(response.Data.Email);                        
                        syseditor.set_data('txaDescriptionUpdate', response.Data.Description);
                        $("#ddlRegionEdit", form).val(response.Data.RegionId);
                        $("#ddlRegionEdit", form).trigger('liszt:updated');
                        $('#txtAddress',form).val(response.Data.Address);
                        $('#txtAssignTo',form).val(response.Data.AssignTo);

                        // Kiểm tra xem số điện thoại
                        if( response.Data.PhoneNumber        == null || response.Data.PhoneNumber == ''
                         || response.Data.PhoneNumber.trim() == '' ){
                            $('#btnEditProfile',form).removeClass('hide');
                        }
                        else{
                            $('#btnEditProfile',form).addClass('hide');
                        }

                        if(response.Data.RegionId ==0){
                            response.Data.RegionId = 1;
                        }

                        var option2 = {
                            controller: "City",
                            action: "GetCityByRegion",
                            data: {
                                regionId: response.Data.RegionId
                            },
                            callback: function (response2) {
                                if (response2.Code == ResponseCode.Success) {
                                    $("#ddlCityEdit", form).empty();
                                    if (response2.Data.length > 0) {
                                        $("#ddlCityEdit", form).append('<option value="">--City--</option>');
                                    }
                                    var html = "";
                                    for (var i = 0; i < response2.Data.length; i++) {
                                        if (response2.Data[i].Id == response.Data.CityId) {
                                            html += '<option value="' + response2.Data[i].Id + '" selected>' + response2.Data[i].Name + '</option>';
                                        }
                                        else {
                                            html += '<option value="' + response2.Data[i].Id + '">' + response2.Data[i].Name + '</option>';
                                        }
                                    }
                                    $("#ddlCityEdit", form).append(html);
                                    $("#ddlCityEdit", form).trigger('liszt:updated');
                                } else {
                                    sysmess.error(response1.Message);
                                }
                            }
                        };
                        sysrequest.send(option2);

                        var option1 = {
                            controller: "City",
                            action: "GetDistrictByCity",
                            data: {
                                cityId: response.Data.CityId
                            },
                            callback: function (response1) {
                                if (response1.Code == ResponseCode.Success) {
                                    $("#ddlDistrictEdit", form).empty();
                                    if (response1.Data.length > 0) {
                                        $("#ddlDistrictEdit", form).append('<option value="">--District--</option>');
                                    }
                                    var html = "";
                                    for (var i = 0; i < response1.Data.length; i++) {
                                        if (response1.Data[i].Id == response.Data.DistrictId)
                                        {
                                            html += '<option value="' + response1.Data[i].Id + '" selected>' + response1.Data[i].Name + '</option>';
                                        }
                                        else {
                                            html += '<option value="' + response1.Data[i].Id + '">' + response1.Data[i].Name + '</option>';
                                        }                                        
                                    }
                                    $("#ddlDistrictEdit", form).append(html);
                                    $("#ddlDistrictEdit", form).trigger('liszt:updated');
                                } else {
                                    sysmess.error(response1.Message);
                                }
                            }
                        };
                        sysrequest.send(option1);
                        customer_edit.update();
                        break;
                    case ResponseCode.Error:
                        sysmess.error("Error: " + response.Message);
                        sysmess.log(response.Message);
                        break;
                }
            }
        };
        sysrequest.send(option);
    },
    update: function () {
        $(document).ready(function () {
            var form = $('#frmUpdateCustomer');
            $("#ddlRegionEdit", form).off("change").on("change", function () {
                var regionId = $(this).val();
                var cityId = 0;
                if (regionId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetCityByRegion",
                        data: {
                            regionId: regionId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlCityEdit", form).empty();
                                if (response.Data.length > 0) {
                                    cityId = response.Data[0].Id;
                                    $("#ddlCityEdit", form).append('<option value="">--City--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlCityEdit", form).append(html);
                                $("#ddlCityEdit", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlDistrictEdit", form).empty();
                                if (response.Data.length > 0) {
                                    $("#ddlDistrictEdit", form).append('<option value="">--District--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlDistrictEdit", form).append(html);
                                $("#ddlDistrictEdit", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            $("#ddlCityEdit", form).off("change").on("change", function () {
                var cityId = $(this).val();
                if (cityId > 0) {
                    var option = {
                        controller: "City",
                        action: "GetDistrictByCity",
                        data: {
                            cityId: cityId
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                $("#ddlDistrictEdit", form).empty();
                                if (response.Data.length > 0) {
                                    $("#ddlDistrictEdit", form).append('<option value="">--District--</option>');
                                }
                                var html = "";
                                for (var i = 0; i < response.Data.length; i++) {
                                    html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                }
                                $("#ddlDistrictEdit", form).append(html);
                                $("#ddlDistrictEdit", form).trigger('liszt:updated');
                            } else {
                                sysmess.error(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            $("#btnUpdateCustomer", form).off("click").on("click", function () {
                $("span.help-inline", form).each(function () {
                    $(this).parent("div").parent("div").removeClass("error");
                    $(this).remove();
                });
                var checkValue = customer_edit.valid_data();
                if (checkValue == true) {
                    var ddlDistrict = $('#ddlDistrictEdit',form);
                    var districtId = 0;
                    if(ddlDistrict.val() != null && ddlDistrict.val() != ''){
                        districtId = ddlDistrict.val();
                    }

                    var arrProfile = new Array();
                    $('#tblEditProfile tbody tr').each(function () {
                        arrProfile.push({
                            Name: $('td:eq(0) input', $(this)).val(),
                            PhoneNumber: $('td:eq(1) input', $(this)).val(),
                            PhoneAlias: $('td:eq(2) input', $(this)).val(),
                            Email: $('td:eq(3) input', $(this)).val()
                        });
                    });

                    var data = {
                        Id: $("#hidCustomerId", form).val(),
                        FullName: $("#txtFullName", form).val(),
                        PhoneNumber: $("#txtPhoneNumberEdit", form).val(),
                        //Status: $('#ddlCustomerStatus', form).val(),
                        PostingType: $("#ddlPostingTypeEdit", form).val(),
                        Email: $("#txtEmail", form).val(),
                        Description: encodeURIComponent(syseditor.get_data('txaDescriptionUpdate')),
                        CityId: $("#ddlCityEdit", form).val(),
                        DistrictId: districtId,
                        RegionId: $("#ddlRegionEdit", form).val(),
                        CompanyId: 0,
                        Address:$('#txtAddress',form).val(),
                        SignatureChecksum : $('input[name=SignatureChecksum]',form).val(),
                        ListProfile: arrProfile
                    };
                    var option = {
                        controller: "Customer",
                        action: "EditAction",
                        data: JSON.stringify(data),
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                customer_index.search(1);
                                $("#divBodyListCustomer").removeClass("hide");
                                $("#divEditCustomer").addClass("hide");
                            }
                            else if (response.Code == ResponseCode.Error) {
                                sysmess.error("Error: " + response.Message);
                            }
                            else {
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option);
                }
            });
            $("#btnCloseInput", form).off("click").on("click", function () {
                $("#divBodyListCustomer").removeClass("hide");
                $("#divEditCustomer").addClass("hide");
            });
            $('#btnEditProfile',form).off('click').on('click', function () {
                var id = $("#hidCustomerId", form).val();
                var options = {
                    controller: 'Profile',
                    action: 'GetListByCustomer',
                    dataType: 'html',
                    data: {
                        customerId: id
                    },
                    element: $('#divListProfile'),
                    blockUI: $('div#main-content'),
                    scrollUI: $('#divListProfile').parent(),
                    callback: function () {
                        profile_edit.init();
                    }
                };
                sysrequest.send(options);
            });
        });
    },
    valid_data: function () {
        var valid = true;
        try {
            var focus = null;
            var form = $('#frmUpdateCustomer');

            var fullName = $.trim($("#txtFullName", form).val());
            if (fullName == null || fullName == "") {
                focus = $("#txtFullName", form);
                $("#txtFullName", form).parent("div").parent("div").addClass("error");
                $("#txtFullName", form).next('span.help-inline').remove();
                $("#txtFullName", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter FullName of customer.</span>");
                valid = false;
            }
            var phoneNumber = $.trim($("#txtPhoneNumberEdit", form).val());
            if (phoneNumber == null || phoneNumber == "") {
                $("#txtPhoneNumberEdit", form).parent("div").parent("div").addClass("error");
                $("#txtPhoneNumberEdit", form).next('span.help-inline').remove();
                $("#txtPhoneNumberEdit", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Phone Number.</span>");
                valid = false;
            }

            var custType = $.trim($("#ddlPostingTypeEdit", form).val());
            if (custType == null || custType == "") {
                if (focus == null) {
                    focus = $("#ddlPostingTypeEdit", form);
                }
                $("#ddlPostingTypeEdit", form).parent("div").parent("div").addClass("error");
                $("#ddlPostingTypeEdit", form).next('span.help-inline').remove();
                $("#ddlPostingTypeEdit", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Type Of Customer.</span>");
                valid = false;
            }

            var city = $.trim($("#ddlCityEdit", form).val());
            if (city == null || city == "") {
                if (focus == null) {
                    focus = $("#ddlCityEdit", form);
                }
                $("#ddlCityEdit", form).parent("div").parent("div").addClass("error");
                $("#ddlCityEdit", form).next('span.help-inline').remove();
                $("#ddlCityEdit", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Province.</span>");
                valid = false;
            }

            var district = $.trim($("#ddlDistrictEdit", form).val());
            if (district == null || district == "") {
                if (focus == null) {
                    focus = $("#ddlDistrictEdit", form);
                }
                $("#ddlDistrictEdit", form).parent("div").parent("div").addClass("error");
                $("#ddlDistrictEdit", form).next('span.help-inline').remove();
                $("#ddlDistrictEdit", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter District.</span>");
                valid = false;
            }

            var email = $.trim($("#txtEmail", form).val());
            if (email == null || email == "") {
                $("#txtEmail", form).parent("div").parent("div").parent("div").addClass("error");
                $("#txtEmail", form).parent("div").next('span.help-inline').remove();
                $("#txtEmail", form).parent("div").append("<span class=\"help-inline no-left-padding\">Enter Email Address.</span>");
                if (focus == null) {
                    focus = $("#txtEmail", form);
                }
                valid = false;
            }else if (email != '' && !sysvalid.email(email)) {
                if (focus == null) {
                    focus = $("#txtEmail", form);
                }
                $("#txtEmail", form).parent("div").parent("div").parent("div").addClass("error");
                $("#txtEmail", form).parent("div").next('span.help-inline').remove();
                $("#txtEmail", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Invalid Email Address.</span>");
                valid = false;
            }

            if(custType == '0'){
                var address = $('#txtAddress',form).val();
                if(address == null  || address==''){
                    if (focus == null) {
                        focus = $("#txtAddress", form);
                    }
                    $("#txtAddress", form).parent("div").parent("div").addClass("error");
                    $("#txtAddress", form).parent("div").next('span.help-inline').remove();
                    $("#txtAddress", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Address is not empty.</span>");
                    valid = false;
                }
            }

            var description= syseditor.get_data('txaDescriptionUpdate');
            if(description != null && description != ''){
                if(description.length > 499){
                    $("#txaDescriptionUpdate", form).parent("div").parent("div").addClass("error");
                    $("#txaDescriptionUpdate", form).parent("div").next('span.help-inline').remove();
                    $("#txaDescriptionUpdate", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Description is too long (max length is 500 characters) !</span>");
                    valid = false;
                }
            }

            if (focus != null) {
                focus.focus();
                syscommon.scroll(focus);
            }
        } catch (e) {
            sysmess.error('Lỗi javascript!<br />' + e);
            valid = false;
        }
        return valid;
    }
};
var profile_create = {
    init: function () {
        $(document).ready(function () {
            var form = $("#frmCreateProfile");
            $('.number').inputmask("decimal", {
                autoGroup: true,
                groupSeparator: ",",
                groupSize: 13,
                rightAlignNumerics: false,
                repeat: 13
            });
            //profile_create.reset();
            $("#lnkAddProfile", form).off("click").on("click", function () {
                var html = "";
                html += '<tr>';
                html += '<td>';
                html += '<input name="Name" type="text" class="m-wrap span12" value="" placeholder="--Name--"/>';
                html += '</td>';
                html += '<td>';
                html += '<input name="PhoneNumber" type="number" class="m-wrap span12 phone-number" value="" placeholder="--PhoneNumber-"/>';
                html += '</td>';
                html += '<td>';
                html += '<input name="PhoneAlias" type="text" class="m-wrap span12" value="" placeholder="--PhoneAlias--" />';
                html += '</td>';
                html += '<td>';
                html += '<input name="Email" type="text" class="m-wrap span12" value="" placeholder="--Email--" />';
                html += '</td>';
                html += '<td>';
                html += '<input type=\"hidden\" value="0"/>';
                html += '<a href="javascript:;" class=\"btn red icn-only\" title=\"Cancel\"><i class=\"icon-remove icon-white\"></i></a>';
                html += '</td>';
                html += '</tr>';
                $("#tblProfile tbody").append(html);
                $('.number').inputmask("decimal", {
                    autoGroup: true,
                    groupSeparator: ",",
                    groupSize: 13,
                    rightAlignNumerics: false,
                    repeat: 13
                });
                profile_create.push_action();
            });

            $("#btnSaveProfile", form).off("click").on("click", function () {
                var phoneNumber = "";
                var arrProfile = new Array();
                $('#tblProfile tbody tr').each(function () {
                    arrProfile.push({
                        Name: $('td:eq(0) input', $(this)).val(),
                        PhoneNumber: $('td:eq(1) input', $(this)).val(),
                        PhoneAlias: $('td:eq(2) input', $(this)).val(),
                        Email: $('td:eq(3) input', $(this)).val()
                    });

                    if (phoneNumber == "") {
                        phoneNumber = $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                    }
                    else {
                        phoneNumber = phoneNumber + "; " + $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                    }
                });
                if (profile_create.validate(arrProfile, form)) {
                    var data = {
                        ListProfile: arrProfile,
                        CustomerId: 0
                    };
                    var option = {
                        controller: "Profile",
                        action: "CheckPhoneNumber",
                        data: JSON.stringify(data),
                        callback: function (response) {
                            if (response.Code == ResponseCode.ErrorExist) {
                                $("#tblProfile", form).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
                                $("#tblProfile", form).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Phone Number "+ response.Data +" exist in other file customer.</span>");
                            }
                            else if(response.Code == ResponseCode.NotPermitted){
                                $("#tblProfile", form).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
                                $("#tblProfile", form).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Phone Number existed in crawler customer file .</span>");
                            }
                            else if(response.Code == ResponseCode.NotValid){
                                $("#tblProfile", form).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
                                $("#tblProfile", form).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">"+response.Message +"</span>");
                            }
                            else{
                                $("button.close", form).trigger("click");
                                if (phoneNumber != "") {
                                    $("#txtPhoneNumber", $("#frmCreateCustomer")).val(phoneNumber);
                                }                                
                            }
                        }
                    };
                    sysrequest.send(option);                                      
                }
            });
        });
    },
    push_action: function () {
        $('#tblProfile tbody tr').each(function () {
            var current = $(this);
            $("a", current).off("click");
            $("a", current).on("click", function () {
                var a = $(this);
                a.parent().parent().remove();
            });
        });
    },
    validate: function (data, form) {
        var result = true;
        var focus = null;
        profile_create.clear_error(form);
        if (data.length > 0) {
            $('#tblProfile tbody tr', form).each(function () {
                if ($('td:eq(0) input', $(this)).val() == '' || $('td:eq(0) input', $(this)).val() == null) {
                    result = false;
                    $('td:eq(0) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(0)', $(this)).append(" <span class=\"help-inline no-left-padding w150\" style=\"color: #b94a48\">Enter Customer Name.</span>");
                    focus = $('td:eq(0) input', $(this));
                }
                if ($('td:eq(1) input', $(this)).val() == null || $('td:eq(1) input', $(this)).val() == '' ) {
                    result = false;
                    $('td:eq(1) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(1)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Enter Phone Number.</span>");
                    if (focus == null) {
                        focus = $('td:eq(1) input', $(this));
                    }
                }
                else{
                    if(!$('td:eq(1) input', $(this)).val().match(/^0\d{1,4}\d{4}\d{4}$/g)){
                        result = false;
                        $('td:eq(1) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                        $('td:eq(1)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Telephone Format : 0xy/xyz12345678.</span>");
                        if (focus == null) {
                            focus = $('td:eq(1) input', $(this));
                        }
                    }
                }

                var email = $.trim($('td:eq(3) input', $(this)).val());
                if (email != '' && !sysvalid.email(email)) {
                    $('td:eq(3) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(3)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Invalid Email Address.</span>");
                    result = false;
                    if (focus == null) {
                        focus = $('td:eq(3) input', $(this));
                    }
                }
            });
        }
        else {
            result = false;
            $("#tblProfile", form).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
            $("#tblProfile", form).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Enter Infomation Phone Number.</span>");
        }
        if (focus != null) {
            focus.focus();
            syscommon.scroll(focus);
        }
        return result;
    },
    clear_error: function (form) {
        $("#tblProfile", form).removeAttr('style');
        $('#tblProfile', form).next('span.help-inline').remove();

        $("#tblProfile tbody tr").each(function () {
            $("td span.help-inline", $(this)).remove();
            $("td input[type='text']", $(this)).each(function () {
                $(this).removeAttr('style');
            });
        });
    },
    reset : function () {
        $('input[name=Name]',$('#tblProfile')).each(function () {
            $(this).val('');
        });
        $('input[name=PhoneNumber]',$('#tblProfile')).each(function () {
            $(this).val('');
        });
        $('input[name=PhoneAlias]',$('#tblProfile')).each(function () {
            $(this).val('');
        });
        $('input[name=Email]',$('#tblProfile')).each(function () {
            $(this).val('');
        });
    }
};
var profile_list = {
    tr_row_edit_phone:null,
    customerid : 0,
    init: function(){
        $(document).ready(function () {
            var form = $("#frmManageProfile");
            profile_list.push_action();
            profile_list.clear_error(form);
            $('.number').inputmask("decimal", {
                autoGroup: true,
                groupSeparator: ",",
                groupSize: 13,
                rightAlignNumerics: false,
                repeat: 13
            });
            $("#lnkAddNew", form).off("click");
            $("#lnkAddNew", form).on("click", function () {
                var html = "";
                html += '<tr>';
                html += '<td>';
                html += '<input name="Name" type="text" class="m-wrap span12" value="" placeholder="--Name--"/>';
                html += '</td>';
                html += '<td>';
                html += '<input name="PhoneNumber" type="number" class="m-wrap span12 phone-number" value="" placeholder="--PhoneNumber-"/>';
                html += '</td>';
                html += '<td>';
                html += '<input name="PhoneAlias" type="text" class="m-wrap span12" value="" placeholder="--PhoneAlias--" />';
                html += '</td>';
                html += '<td>';
                html += '<input name="Email" type="text" class="m-wrap span12" value="" placeholder="--Email--" />';
                html += '</td>';
                html += '<td>';
                html += '<input type=\"hidden\" value="0"/>';
                html += '<a href="javascript:;" class=\"btn red icn-only\" title=\"Cancel\"><i class=\"icon-remove icon-white\"></i></a>';
                html += '</td>';
                html += '</tr>';
                $("#tblManageProfile tbody").append(html);
                $('.number').inputmask("decimal", {
                    autoGroup: true,
                    groupSeparator: ",",
                    groupSize: 13,
                    rightAlignNumerics: false,
                    repeat: 13
                });
            });

            $("#btnSaveManageProfile", form).off("click").on("click", function () {
                var phoneNumber = "";
                var arrProfile = new Array();
                $('#tblManageProfile tbody tr').each(function () {
                    arrProfile.push({
                        Id: $('td:eq(4) input', $(this)).val(),
                        Name: $('td:eq(0) input', $(this)).val(),
                        PhoneNumber: $('td:eq(1) input', $(this)).val(),
                        PhoneAlias: $('td:eq(2) input', $(this)).val(),
                        Email: $('td:eq(3) input', $(this)).val()
                    });
                    if (phoneNumber == "") {
                        phoneNumber = $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                    }
                    else {
                        phoneNumber = phoneNumber + "; " + $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                    }
                });
                profile_list.clear_error(form);
                if (profile_list.validate(arrProfile, form)) {
                    if (phoneNumber != "") {
                        var data = {
                            CustomerId: $("#hiddCustomerId", form).val(),
                            PhoneNumber: phoneNumber,
                            ListProfile: arrProfile,
                            TokenChecksum :$('input[name=ProfileTokenChecksum]').val()
                        };
                        var option = {
                            controller: "Profile",
                            action: "Save",
                            data: JSON.stringify(data),
                            callback: function (response) {
                                $("button.close", form).trigger("click");
                                if(response.Code == ResponseCode.Success ){
                                    sysmess.info(response.Message);
                                    customer_list.change_row_data(profile_list.tr_row_edit_phone,profile_list.customerid);
                                }
                                else if(response.Code == ResponseCode.Error){
                                    sysmess.error(response.Message);
                                }
                                else{
                                    sysmess.warning(response.Message);
                                }
                            }
                        };
                        sysrequest.send(option);
                    }                    
                }
            });
        });
    },
    push_action: function () {
        $('#tblManageProfile tbody tr').each(function () {
            var current = $(this);
            $("a", current).off("click");
            $("a", current).on("click", function () {
                var a = $(this);
                a.parent().parent().remove();
            });
        });
    },
    validate: function (data, form) {
        var result = true;
        var focus = null;
        if (data.length > 0) {
            $('#tblManageProfile tbody tr', form).each(function () {
                if ($('td:eq(0) input', $(this)).val() == '' || $('td:eq(0) input', $(this)).val() == null) {
                    result = false;
                    $('td:eq(0) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(0)', $(this)).append(" <span class=\"help-inline no-left-padding w150\" style=\"color: #b94a48\">Enter Customer Name.</span>");
                    focus = $('td:eq(0) input', $(this));
                }
                if ($('td:eq(1) input', $(this)).val() == null || $('td:eq(1) input', $(this)).val() == '') {
                    result = false;
                    $('td:eq(1) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(1)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Enter Phone Number.</span>");
                    if (focus == null) {
                        focus = $('td:eq(1) input', $(this));
                    }
                }
                else{
                    if(!$('td:eq(1) input', $(this)).val().match(/^0\d{1,4}\d{4}\d{4}$/g)){
                        result = false;
                        $('td:eq(1) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                        $('td:eq(1)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Telephone Format : 0xy/xyz12345678.</span>");
                        if (focus == null) {
                            focus = $('td:eq(1) input', $(this));
                        }
                    }
                }

                var email = $.trim($('td:eq(3) input', $(this)).val());
                if (email != '' && !sysvalid.email(email)) {
                    $('td:eq(3) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(3)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Invalid Email Address.</span>");
                    result = false;
                    if (focus == null) {
                        focus = $('td:eq(3) input', $(this));
                    }
                }
            });
        }
        else {
            result = false;
            $("#tblManageProfile", form).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
            $("#tblManageProfile", form).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Enter Infomation Phone Number.</span>");
        }
        if (focus != null) {
            focus.focus();
            syscommon.scroll(focus);
        }
        return result;
    },
    clear_error: function (form) {
        $("#tblManageProfile", form).removeAttr('style');
        $('#tblManageProfile', form).next('span.help-inline').remove();

        $("#tblManageProfile tbody tr").each(function () {
            $("td span.help-inline", $(this)).remove();
            $("td input[type='text']", $(this)).each(function () {
                $(this).removeAttr('style');
            });
        });
    }
};
var profile_edit = {
    init: function () {
        profile_edit.clear_error();
        profile_edit.action();
    }
   ,action : function () {
        var frm_edit_profile = $('#divEditProfile');
        $('button#btnSaveEditProfile',frm_edit_profile).off('click').on('click', function () {
            var phoneNumber = "";
            var arrProfile = new Array();
            $('#tblEditProfile tbody tr').each(function () {
                arrProfile.push({
                    Name: $('td:eq(0) input', $(this)).val(),
                    PhoneNumber: $('td:eq(1) input', $(this)).val(),
                    PhoneAlias: $('td:eq(2) input', $(this)).val(),
                    Email: $('td:eq(3) input', $(this)).val()
                });

                if (phoneNumber == "") {
                    phoneNumber = $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                }
                else {
                    phoneNumber = phoneNumber + "; " + $('td:eq(1) input', $(this)).val() + "(" + $('td:eq(0) input', $(this)).val() + ")";
                }
            });
            if (profile_edit.validate(arrProfile, frm_edit_profile)) {
                var data = {
                    ListProfile: arrProfile,
                    CustomerId: $('#hidCustomerId',$("#frmUpdateCustomer")).val()
                };
                var option = {
                    controller: "Profile",
                    action: "CheckPhoneNumber",
                    data: JSON.stringify(data),
                    callback: function (response) {
                        if (response.Code == ResponseCode.ErrorExist) {
                            $("#tblEditProfile", frm_edit_profile).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
                            $("#tblEditProfile", frm_edit_profile).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Phone Number "+ response.Data +" exist in other file customer.</span>");
                        }
                        else if(response.Code == ResponseCode.NotPermitted){
                            $("#tblEditProfile", frm_edit_profile).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
                            $("#tblEditProfile", frm_edit_profile).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Phone Number existed in crawler customer file .</span>");
                        }
                        else{
                            $("button.close", frm_edit_profile).trigger("click");
                            if (phoneNumber != "") {
                                $("#txtPhoneNumberEdit", $("#frmUpdateCustomer")).val(phoneNumber);
                            }
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
    }
   ,validate: function (data, form) {
        var result = true;
        var focus = null;
        profile_edit.clear_error(form);
        if (data.length > 0) {
            $('#tblEditProfile tbody tr', form).each(function () {
                if ($('td:eq(0) input', $(this)).val() == '' || $('td:eq(0) input', $(this)).val() == null) {
                    result = false;
                    $('td:eq(0) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(0)', $(this)).append(" <span class=\"help-inline no-left-padding w150\" style=\"color: #b94a48\">Enter Customer Name.</span>");
                    focus = $('td:eq(0) input', $(this));
                }
                if ($('td:eq(1) input', $(this)).val() == null || $('td:eq(1) input', $(this)).val() == '' ) {
                    result = false;
                    $('td:eq(1) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(1)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Enter Phone Number.</span>");
                    if (focus == null) {
                        focus = $('td:eq(1) input', $(this));
                    }
                }
                else{
                    if(!$('td:eq(1) input', $(this)).val().match(/^0\d{1,4}\d{4}\d{4}$/g)){
                        result = false;
                        $('td:eq(1) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                        $('td:eq(1)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Telephone Format : 0xy/xyz12345678.</span>");
                        if (focus == null) {
                            focus = $('td:eq(1) input', $(this));
                        }
                    }
                }

                var email = $.trim($('td:eq(3) input', $(this)).val());
                if (email != '' && !sysvalid.email(email)) {
                    $('td:eq(3) input', $(this)).attr('style', 'border-color: #b94a48; border-right: 1px solid #b94a48');
                    $('td:eq(3)', $(this)).append(" <span class=\"help-inline no-left-padding w160\" style=\"color: #b94a48\">Invalid Email Address.</span>");
                    result = false;
                    if (focus == null) {
                        focus = $('td:eq(3) input', $(this));
                    }
                }
            });
        }
        else {
            result = false;
            $("#tblEditProfile", form).attr('style', 'border-color: #b94a48; border-left: 1px solid #b94a48');
            $("#tblEditProfile", form).parent("div").append(" <span class=\"help-inline no-left-padding w300\" style=\"color: #b94a48;\">Enter Infomation Phone Number.</span>");
        }
        if (focus != null) {
            focus.focus();
            syscommon.scroll(focus);
        }
        return result;
    }
   ,clear_error: function (form) {
        $("#tblEditProfile", form).removeAttr('style');
        $('#tblEditProfile', form).next('span.help-inline').remove();

        $("#tblEditProfile tbody tr").each(function () {
            $("td span.help-inline", $(this)).remove();
            $("td input[type='text']", $(this)).each(function () {
                $(this).removeAttr('style');
            });
        });
    }
};
