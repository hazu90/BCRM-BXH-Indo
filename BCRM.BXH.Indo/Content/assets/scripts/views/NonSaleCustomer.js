﻿var non_sale_customer_index = {
    init: function () {
        $(document).ready(function () {
            non_sale_customer_index.action();
            $('#frmSearchNonsaleCustomer').off('keydown').on('keydown', function (e) {
                non_sale_customer_index.hitting_enter(e);
            });
            non_sale_customer_index.search(1);
        });
    },
    action: function () {
        var frm_search = $('#frmSearchNonsaleCustomer');
        $('button#btnSearch',frm_search).off('click').on('click', function () {
            non_sale_customer_index.search(1);
        });
        $('button#btnReset',frm_search).off('click').on('click', function () {
            non_sale_customer_index.reset();
        });
    },
    search: function (page) {
        var frm_search = $('#frmSearchNonsaleCustomer');
        var input_filterKeyword = $('textarea[name=FilterKeyword]',frm_search);
        var select_receiver = $('select[name=Receiver]',frm_search);
        var select_status = $('select[name=Status]',frm_search);
        var input_dateOfCreationFrom = $('input#txtDateOfCreationFrom',frm_search);
        var input_dateOfCreationTo = $('input#txtDateOfCreationTo',frm_search);

        var option = {
            controller :'NonSaleCustomer',
            action:'Search',
            data:{
                FilterKeyword       : input_filterKeyword.val()
               ,Status              : select_status.val()
               ,DateOfCreationFrom  : input_dateOfCreationFrom.val()
               ,DateOfCreationTo    : input_dateOfCreationTo.val()
               ,Receiver            : select_receiver.val()
               ,PageIndex           : page
            },
            dataType:'html',
            element:$('#ResultNonsaleCustomer'),
            callback: function () {
                non_sale_customer_list.action();
            }
        };
        sysrequest.send(option);
    },
    hitting_enter: function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $(this).blur();
            non_sale_customer_index.search(1);
        }
    },
    reset: function () {
        var frm_search = $('#frmSearchNonsaleCustomer');
        $('textarea[name=FilterKeyword]',frm_search).val('');
        $('select[name=Receiver]',frm_search).val($('select[name=Receiver] option:first',frm_search).val() ) ;
        $('select[name=Receiver]',frm_search).trigger('liszt:updated');
        $('select[name=Status]',frm_search).val($('select[name=Status] option:first',frm_search).val()) ;
        $('select[name=Status]',frm_search).trigger('liszt:updated');
        $('input#txtDateOfCreationFrom',frm_search).val('');
        $('input#txtDateOfCreationTo',frm_search).val('');
    }
};
var non_sale_customer_list = {
    action: function () {
        var frm_grid = $('#wrapNonsaleCustomer');
        $('a[name=receive]',frm_grid).each(function () {
            $(this).off('click').on('click', function () {
                var customerid = $(this).data('customerid');
                var carehistoryid= $(this).data('carehistoryid');
                var option = {
                    controller:'NonSaleCustomer',
                    action :'Receive',
                    data:{
                        customerId : customerid
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success ){
                            sysmess.info(response.Message);
                            non_sale_customer_index.search(non_sale_customer_list.get_current_page()) ;
                        }
                        else if(response.Code == ResponseCode.Error ){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysmess.confirm('Do you want to receive this customer ?', function () {
                    sysrequest.send(option);
                });

            });
        });
        $('a[name=contactsuccess]',frm_grid).each(function () {
            $(this).off('click').on('click', function () {
                var customerid = $(this).data('customerid');
                var carehistoryid= $(this).data('carehistoryid');
                var option = {
                    controller:'NonSaleCustomer',
                    action :'ContactSuccess',
                    data:{
                        customerId : customerid,
                        careHistoryId : carehistoryid
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success ){
                            sysmess.info(response.Message);
                            non_sale_customer_index.search(non_sale_customer_list.get_current_page()) ;
                        }
                        else if(response.Code == ResponseCode.Error ){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };

                sysmess.confirm('Do you contact this customer successfully ?', function () {
                    sysrequest.send(option);
                });
            });
        });
        $('a[name=contactfailure]',frm_grid).each(function () {
            $(this).off('click').on('click', function () {
                var customerid = $(this).data('customerid');
                var carehistoryid= $(this).data('carehistoryid');
                var option = {
                    controller:'NonSaleCustomer',
                    action :'ContactFailure',
                    data:{
                        customerId : customerid,
                        careHistoryId : carehistoryid
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success ){
                            sysmess.info(response.Message);
                            non_sale_customer_index.search(non_sale_customer_list.get_current_page()) ;
                        }
                        else if(response.Code == ResponseCode.Error ){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };

                sysmess.confirm('Do you contact this customer failure ?', function () {
                    sysrequest.send(option);
                });
            });
        });
    },
    get_current_page: function () {
        var curr_Page = 1;
        $('.dataTables_paginate li.active a').each(function () {
            var id_html = $(this).text().trim();
            if($.isNumeric(id_html)){
                curr_Page = parseInt(id_html);
            }
        });
        return curr_Page;
    }
};