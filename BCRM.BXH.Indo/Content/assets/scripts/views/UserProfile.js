﻿var userProfile = {
    init: function () {
        $(document).ready(function () {
            $("#btnSaveChangePass").on("click", function () {
                userProfile.changePass();
            });
            $("[name='btnBackToProfile']").on("click", function () {
                $("#tab_1_1").addClass("active");
                $("#tab_1_2").removeClass("active");
                $(".nav-tabs li:eq(0)").addClass("active");
                $(".nav-tabs li:eq(1)").removeClass("active");
            });

            $("#btnSaveChangeProfile").on("click", function () {
                userProfile.change_profile();
            });

            $(".profile-edit").on("click", function () {
                $("#tab_1_1").removeClass("active");
                $("#tab_1_2").addClass("active");
                $(".nav-tabs li:eq(0)").removeClass("active");
                $(".nav-tabs li:eq(1)").addClass("active");
                $("[name='ProfileAction'] li:eq(1) a").trigger("click");
                $("#ViewImage").attr("src", $("#ImageAvatar").attr("src"));
            });
            $("#btnSaveChangeAvatar").on("click", function () {
                $("#frmChangeAvatar").submit();
            });
            $('#frmChangeAvatar').ajaxForm({
                url: '/Image/UploadImage', // or whatever
                dataType: 'json',
                success: function (response) {
                    if (response.Code == ResponseCode.Error) {
                        sysmess.error(response.Message);
                    }
                },
                error: function (response) {
                    alert(response.Message);
                    if (response.Code == ResponseCode.Error) {
                        sysmess.error(response.Message);
                    }
                }
            });
        });
    },
    changePass: function () {
        var option = {
            controller: "Account",
            action: "ChangePasswordUser",
            data: {
                OldPassword: $("#txtCurPass").val(),
                NewPassword: $("#txtNewPass").val(),
                ConfirmPassword: $("#txtConfirmPass").val()
            },
            callback: function (response) {
                if (response.Code == ResponseCode.Success) {
                    sysmess.info("Change Pass Success.");
                } else {
                    sysmess.error(response.Message);
                }
            }
        };
        if (userProfile.change_pass_validate() == true) {
            sysrequest.send(option);
            $("#txtCurPass").val("");
            $("#txtNewPass").val("");
            $("#txtConfirmPass").val("");
        }
    },
    change_pass_validate: function () {
        $("span.help-inline").each(function () {
            $(this).parent("div").parent("div").removeClass("error");
            $(this).remove();

        });
        var result = true;
        var focus = null;
        if ($("#txtCurPass").val() == null || $("#txtCurPass").val() == "") {
            $("#txtCurPass").parent("div").parent("div").addClass("error");
            $("#txtCurPass").next().remove();
            $("#txtCurPass").parent("div")
                .append(" <span class=\"help-inline no-left-padding\">Enter Current Password.</span>");
            if (focus == null) {
                focus = $("#txtCurPass");
            }
            result = false;
        }
        if ($("#txtNewPass").val() == null || $("#txtNewPass").val() == "") {
            $("#txtNewPass").parent("div").parent("div").addClass("error");
            $("#txtNewPass").next().remove();
            $("#txtNewPass").parent("div")
                .append(" <span class=\"help-inline no-left-padding\">Enter New Password.</span>");
            if (focus == null) {
                focus = $("#txtNewPass");
            }
            result = false;
        }
        if ($("#txtConfirmPass").val() == null || $("#txtConfirmPass").val() == "") {
            $("#txtConfirmPass").focus();
            $("#txtConfirmPass").parent("div").parent("div").addClass("error");
            $("#txtConfirmPass").next().remove();
            $("#txtConfirmPass").parent("div")
                .append(" <span class=\"help-inline no-left-padding\">Enter Confirm Password.</span>");
            if (focus == null) {
                focus = $("#txtConfirmPass");
            }
            result = false;
        }

        if ($("#txtNewPass").val() != $("#txtConfirmPass").val()) {
            $("#txtConfirmPass").focus();
            $("#txtConfirmPass").parent("div").parent("div").addClass("error");
            $("#txtConfirmPass").next().remove();
            $("#txtConfirmPass").parent("div")
                .append(" <span class=\"help-inline no-left-padding\">Invalid Confirm Password.</span>");
            if (focus == null) {
                focus = $("#txtConfirmPass");
            }
            result = false;
        }
        if (focus != null) {
            syscommon.scroll(focus);
            focus.focus();
        }
        return result;
    },
    change_profile: function () {
        var dob = $("#txtDOB").val();
        if (dob == "") {
            dob = Date.now;
        }
        var option = {
            controller: "User",
            action: "ChangeProfile",
            data: {
                UserId: $("#hiddUserId").val(),
                DisplayName: $("#txtDisplayName").val(),
                Mobile: $("#txtMobile").val(),
                Email: $("#txtEmail").val(),
                DOB: dob
            },
            callback: function (response) {
                if (response.Code == ResponseCode.Success) {
                    sysmess.info("Change Profile Suceess.");
                } else {
                    sysmess.error(response.Message);
                }
            }
        };
        sysrequest.send(option);
    }
};