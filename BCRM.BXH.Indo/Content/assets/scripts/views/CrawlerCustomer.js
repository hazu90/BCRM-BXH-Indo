﻿var crawler_customer_index = {
    language_res: {
        msg_cusreceive_confirm: ' Do you want to receive this customer ?',
        act_crawlersearch_contactfailure: 'Contact failure',
        act_crawlersearch_contactsuccess: 'Contact success',
        act_crawlersearch_manager: 'Manager',
        act_crawlersearch_note: 'Note',
        act_crawlersearch_receive: 'Receiving',
        act_crawlersearch_return: 'Return Customer',
        msg_cusfailcontact_confirm: 'Did this customer contact failure ?',
        msg_cusreturn_confirm: 'Do you want to return this customer ?',
        option_no_city: '--Provinsi--',
        option_no_district: '--Kota/kab--',
        msg_required_fullname: 'FullName is required.',
        msg_required_phonenumber: 'Phone Number is required.',
        msg_required_province: 'Enter Province.',
        msg_required_address: 'Address is not empty.',
        msg_invalid_email: 'Invalid Email Address.'
    },
    curr_tr_element: null,
    init: function (lan_res) {
        $.extend(crawler_customer_index.language_res, lan_res);

        $(document).ready(function () {
            crawler_customer_index.action();
            crawler_customer_index.note_customer_action();
            crawler_customer_index.hidden_reason_action();
            crawler_customer_index.search(1);
        });
    },
    search: function (page,offScroll) {
        var frm_search = $('#frmSearchCrawlerCustomer');
        var text_filterkeyword = $('textarea[name=FilterKeyword]', frm_search);
        var select_assignto = $('select[name=AssignTo]', frm_search);
        var assignTo = select_assignto.val();
        if (assignTo == null) {
            assignTo = '';
        }
        var select_status = $('select[name=Status]', frm_search);
        var select_posting_type = $('select[name=PostingType]',frm_search);
        var date_dateofcreationfrom = $('input[name=DateOfCreationFrom]', frm_search);
        var date_dateofcreationto = $('input[name=DateOfCreationTo]', frm_search);

        var hiddenStatus = -1;
        var select_hiddenstatus  = $('select[name=HiddenStatus]',frm_search);
        if(select_hiddenstatus.length > 0){
            hiddenStatus = select_hiddenstatus.val();
        }

        if (page == null) {
            page = 1;
        }
        var option = {
            controller: 'CrawlerCustomer',
            action: 'Search',
            data: {
                FilterKeyword: text_filterkeyword.val(),
                Status: select_status.val(),
                PostingType : select_posting_type.val(),
                DateOfCreationFrom: date_dateofcreationfrom.val(),
                DateOfCreationTo: date_dateofcreationto.val(),
                AssignTo: assignTo,
                HiddenStatus : hiddenStatus,
                PageIndex: page,
                PageSize: 25
            },
            dataType: 'html',
            callback: function () {
                crawler_customer_list.action();
            },
            element: $('#divListCrawlerCustomer')
        };
        sysrequest.send(option,offScroll);
    },
    action: function () {
        var frm_search = $('#frmSearchCrawlerCustomer');
        $('button[name=search]', frm_search).off('click').on('click', function () {
            crawler_customer_index.search(1);
        });
        $('button[name=reset]', frm_search).off('click').on('click', function () {
            $('textarea[name=FilterKeyword]', frm_search).val('');
            $('select[name=AssignTo]', frm_search).val('-1');
            $('select[name=AssignTo]', frm_search).trigger('liszt:updated');
            $('select[name=Status]', frm_search).val('-1');
            $('select[name=Status]', frm_search).trigger('liszt:updated');
            $('input[name=DateOfCreationFrom]', frm_search).val('');
            $('input[name=DateOfCreationTo]', frm_search).val('');
            $('select[name=PostingType]', frm_search).val('-1');
            $('select[name=PostingType]', frm_search).trigger('liszt:updated');
        });
        frm_search.off('keydown').on('keydown', function (e) {
            crawler_customer_index.hitting_enter(e);
        });
    },
    hitting_enter: function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $(this).blur();
            crawler_customer_index.search(1);
        }
    },
    success_contact_action: function () {
        var wrapContactSuccess = $('#wrapContactSuccess');
        app.initChoosenSelect(wrapContactSuccess);

        $('select[name=RegionId]', wrapContactSuccess).off('change').on('change', function () {
            var regionId = $(this).val();
            var cityId = 0;
            if (regionId > 0) {
                var option = {
                    controller: "City",
                    action: "GetCityByRegion",
                    data: {
                        regionId: regionId
                    },
                    callback: function (response) {
                        if (response.Code == ResponseCode.Success) {
                            $("select[name=CityId]", wrapContactSuccess).empty();
                            if (response.Data.length > 0) {
                                cityId = response.Data[0].Id;
                                $("select[name=CityId]", wrapContactSuccess).append('<option value="">' + crawler_customer_index.language_res.option_no_city + '</option>');
                            }
                            var html = "";
                            for (var i = 0; i < response.Data.length; i++) {
                                html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                            }
                            $("select[name=CityId]", wrapContactSuccess).append(html);
                            $("select[name=CityId]", wrapContactSuccess).trigger('liszt:updated');
                        } else {
                            sysmess.error(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
            if (cityId > 0) {
                var option = {
                    controller: "City",
                    action: "GetDistrictByCity",
                    data: {
                        cityId: cityId
                    },
                    callback: function (response) {
                        if (response.Code == ResponseCode.Success) {
                            $("select[name=DistrictId]", wrapContactSuccess).empty();
                            if (response.Data.length > 0) {
                                $("select[name=DistrictId]", wrapContactSuccess).append('<option value="">' + crawler_customer_index.language_res.option_no_district + '</option>');
                            }
                            var html = "";
                            for (var i = 0; i < response.Data.length; i++) {
                                html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                            }
                            $("select[name=DistrictId]", wrapContactSuccess).append(html);
                            $("select[name=DistrictId]", wrapContactSuccess).trigger('liszt:updated');
                        } else {
                            sysmess.error(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
        $("select[name=CityId]", wrapContactSuccess).off("change").on("change", function () {
            var cityId = $(this).val();
            if (cityId > 0) {
                var option = {
                    controller: "City",
                    action: "GetDistrictByCity",
                    data: {
                        cityId: cityId
                    },
                    callback: function (response) {
                        if (response.Code == ResponseCode.Success) {
                            $("select[name=DistrictId]", wrapContactSuccess).empty();
                            if (response.Data.length > 0) {
                                $("select[name=DistrictId]", wrapContactSuccess).append('<option value="">' + crawler_customer_index.language_res.option_no_district + '</option>');
                            }
                            var html = "";
                            for (var i = 0; i < response.Data.length; i++) {
                                html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                            }
                            $("select[name=DistrictId]", wrapContactSuccess).append(html);
                            $("select[name=DistrictId]", wrapContactSuccess).trigger('liszt:updated');
                        } else {
                            sysmess.error(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
        $('button[name=save]', wrapContactSuccess).off('click').on('click', function () {
            $("span.help-inline", wrapContactSuccess).each(function () {
                $(this).parent("div").parent("div").removeClass("error");
                $(this).remove();
            });

            var checkValue = crawler_customer_index.success_contact_valid_data();

            if (checkValue) {
                var districtId = $("select[name=DistrictId]", wrapContactSuccess).val();
                if(districtId == null || districtId == ''){
                    districtId = 0;
                }

                var data = {
                    FullName: $("input[name=FullName]", wrapContactSuccess).val(),
                    PhoneNumber: $("select[name=PhoneNumber]", wrapContactSuccess).val(),
                    PostingType: $("select[name=PostingType]", wrapContactSuccess).val(),
                    Email: $("input[name=Email]", wrapContactSuccess).val(),
                    //Description: $('#txaDescription', wrapContactSuccess).val(),
                    Description : encodeURIComponent(syseditor.get_data('txaDescription')),
                    CityId: $("select[name=CityId]", wrapContactSuccess).val(),
                    DistrictId: districtId,
                    RegionId: $("select[name=RegionId]", wrapContactSuccess).val(),
                    CompanyId: $('select[name=CompanyId]', wrapContactSuccess).val(),
                    Address: $('input[name=Address]', wrapContactSuccess).val(),
                    CrawlerId: $(this).data('adminid'),
                    CareHistoryId: $(this).data('carehistoryid'),
                    TokenChecksum : $('input[name=TokenChecksum]',wrapContactSuccess).val()
                };

                var option = {
                    controller: "CrawlerCustomer",
                    action: "ContactSuccess",
                    data: JSON.stringify(data),
                    callback: function (response) {
                        if (response.Code == ResponseCode.Success) {
                            $("#divBodyListCrawlerCustomer").removeClass("hide");
                            $("#wrapContactSuccess").addClass("hide");
                            crawler_customer_index.search(1);
                        }
                        else if (response.Code == ResponseCode.Error) {
                            sysmess.error("Error: " + response.Message);
                        }
                        else {
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);
            }
        });
        $('button[name=close]', wrapContactSuccess).off('click').on('click', function () {
            $('#divBodyListCrawlerCustomer').removeClass('hide');
            $('#wrapContactSuccess').addClass('hide');
        });
    },
    success_contact_valid_data: function () {
        var valid = true;
        try {
            var focus = null;
            var form = $('#frmSuccessContact');

            var textFullName = $('input[name=FullName]', form);

            var fullName = $.trim(textFullName.val());
            if (fullName == null || fullName == "") {
                focus = textFullName;
                textFullName.parent("div").parent("div").addClass("error");
                textFullName.next('span.help-inline').remove();
                textFullName.parent("div").append("<span class=\"help-inline no-left-padding\">" + crawler_customer_index.language_res.msg_required_fullname + "</span>");
                valid = false;
            }

            var textPhoneNumber = $("select[name=PhoneNumber]", form);

            var phoneNumber = $.trim(textPhoneNumber.val());
            if (phoneNumber == null || phoneNumber == "") {
                textPhoneNumber.parent("div").parent("div").addClass("error");
                textPhoneNumber.next('span.help-inline').remove();
                textPhoneNumber.parent("div").append("<span class=\"help-inline no-left-padding\">" + crawler_customer_index.language_res.msg_required_phonenumber + "</span>");
                valid = false;
            }

            var selectType = $("select[name=PostingType]", form);

            var custType = $.trim(selectType.val());
            if (custType == null || custType == "") {
                if (focus == null) {
                    focus = $("select[name=PostingType]", form);
                }
                selectType.parent("div").parent("div").addClass("error");
                selectType.next('span.help-inline').remove();
                selectType.parent("div").append("<span class=\"help-inline no-left-padding\">Enter Type Of Customer.</span>");
                valid = false;
            }

            var selectCity = $("select[name=CityId]", form);
            var city = $.trim(selectCity.val());
            if (city == null || city == "") {
                if (focus == null) {
                    focus = $("#ddlCity", form);
                }
                selectCity.parent("div").parent("div").addClass("error");
                selectCity.next('span.help-inline').remove();
                selectCity.parent("div").append("<span class=\"help-inline no-left-padding\">" + crawler_customer_index.language_res.msg_required_province + "</span>");
                valid = false;
            }

            var textEmail = $("input[name=Email]", form);

            var email = textEmail.val();
            if (email != null && email != '') {
                email = $.trim(email);
                if (email != '' && !sysvalid.email(email)) {
                    if (focus == null) {
                        focus = textEmail;
                    }
                    textEmail.parent("div").parent("div").parent("div").addClass("error");
                    textEmail.parent("div").next('span.help-inline').remove();
                    textEmail.parent("div").append(" <span class=\"help-inline no-left-padding\">" + crawler_customer_index.language_res.msg_invalid_email + "</span>");
                    valid = false;
                }
            }
            else{
                if (focus == null) {
                    focus = textEmail;
                }
                textEmail.parent("div").parent("div").parent("div").addClass("error");
                textEmail.parent("div").next('span.help-inline').remove();
                textEmail.parent("div").append(" <span class=\"help-inline no-left-padding\">Email is required</span>");
                valid = false;
            }

            var textAddress = $('input[name=Address]', form);

            var address = textAddress.val();
            if (address == null || address == '') {
                if (focus == null) {
                    focus = textAddress;
                }
                textAddress.parent("div").parent("div").addClass("error");
                textAddress.parent("div").next('span.help-inline').remove();
                textAddress.parent("div").append(" <span class=\"help-inline no-left-padding\">" + crawler_customer_index.language_res.msg_required_address + "</span>");
                valid = false;
            }

            var description= syseditor.get_data('txaDescription');
            if(description != null && description != ''){
                if(description.length > 499){
                    $("#txaDescription", form).parent("div").parent("div").addClass("error");
                    $("#txaDescription", form).parent("div").next('span.help-inline').remove();
                    $("#txaDescription", form).parent("div").append(" <span class=\"help-inline no-left-padding\">Description is too long (max length is 500 characters) !</span>");
                    valid = false;
                }
            }
        } catch (e) {
            sysmess.error('Error javascript!<br />' + e);
            valid = false;
        }
        return valid;
    },
    note_customer_action: function () {
        var wrapNote = $('#wrapNote');
        $('button[name=save]', wrapNote).off('click').on('click', function () {
            $("span.help-inline", wrapNote).each(function () {
                $(this).parent("div").parent("div").removeClass("error");
                $(this).remove();
            });

            var careHistoryId = $(this).data('carehistoryid');
            var textNote = $('textarea[name=note]', wrapNote);

            var note = textNote.val();
            if (note == null || note == '') {
                textNote.parent().parent().addClass('error');
                textNote.next('span.help-inline').remove();
                textNote.parent("div").append("<span class=\"help-inline no-left-padding\">Note is required.</span>");
                return;
            }

            var option = {
                controller: 'CrawlerCustomer',
                action: 'Note',
                data: {
                    careHistoryId: careHistoryId,
                    note: note,
                    signatureToken :$('input[name=NoteTokenChecksum]',$('#wrapNote')).val()
                },
                callback: function (response) {
                    if (response.Code == ResponseCode.Success) {
                        sysmess.info(response.Message);
                        $('td[name=wrapNoted]', crawler_customer_index.curr_tr_element).html(note);
                    }
                    else if (response.Code == ResponseCode.Error) {
                        sysmess.error(response.Message);
                    }
                    else {
                        sysmess.warning(response.Message);
                    }
                }
            };
            sysrequest.send(option);
        });
    },
    hidden_reason_action: function () {
        var wrapHideFromList = $('#wrapHideFromList');
        $('button[name=save]',wrapHideFromList).off('click').on('click', function () {
            if(crawler_customer_index.hidden_reason_validate()){
                var adminId = $('input[name=hiddAdminId]',wrapHideFromList).val();
                var reasonType = $('input[name=chkReasonType]:checked',wrapHideFromList).val();
                var phoneNumber = $('input[name=hiddPhoneNumber]',wrapHideFromList).val();
                var note = $('textarea[name=Note]',wrapHideFromList).val();

                var option ={
                    controller:'CrawlerCustomer',
                    action:'Hide',
                    data:{
                        adminId : adminId,
                        reasonType : reasonType,
                        phoneNumber : phoneNumber,
                        note : note,
                        signatureToken : $('input[name=HideFromSaleTokenChecksum]',wrapHideFromList).val()
                    },
                    callback : function(response){
                        if(response.Code == ResponseCode.Success){
                            sysmess.info(response.Message);
                            crawler_customer_index.search(crawler_customer_list.get_current_page(),true);
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    }
                };
                sysrequest.send(option);

            }
        });
        $('input[name=chkReasonType]',wrapHideFromList).off('click').on('click', function () {
            if($(this).val() == '6'){
                $('textarea[name=Note]',wrapHideFromList).closest('.row-fluid').removeClass('hide');
            }
            else{
                $('textarea[name=Note]',wrapHideFromList).closest('.row-fluid').addClass('hide');
            }
        });
    },
    hidden_reason_validate: function () {
        var wrapHideFromList = $('#wrapHideFromList');
        var is_false = true;
        var reasonType = $('input[name=chkReasonType]:checked',wrapHideFromList).val();
        if(reasonType == 6){
            var note = $('textarea[name=Note]',wrapHideFromList).val();
            if(note == null || note == ''){
                var note_control_group =$('textarea[name=Note]',wrapHideFromList).closest('.control-group');
                note_control_group.addClass('error');
                $('div[name=msg_error]',note_control_group).html('<span class="help-block">Note is required.</span>');
                is_false = false;
            }
        }
        return is_false;
    }
};
var crawler_customer_list = {
    action: function () {
        var divBodyListCrawlerCustomer = $('#divBodyListCrawlerCustomer');
        $('a[name=receive]', divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                var adminId = $(this).data('adminid');
                var tr = $(this).parent().parent().parent().parent().parent();
                sysmess.confirm(crawler_customer_index.language_res.msg_cusreceive_confirm, function () {
                    var option = {
                        controller: 'CrawlerCustomer',
                        action: 'Receive',
                        data: {
                            crawlerAdminId: adminId,
                            signatureToken :''
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                sysmess.info(response.Message);
                                $('td[name=wrapAssignTo]', tr).html(response.Data.Receiver);
                                var actionHtml = '<div class="btn-group none-bottom">';
                                actionHtml += '    <button class="btn w80 blue mini dropdown-toggle" data-toggle="dropdown">';
                                actionHtml += '        <i class="icon-cog"></i>' + crawler_customer_index.language_res.act_crawlersearch_manager + '<i class="icon-angle-down"></i>';
                                actionHtml += '    </button>';
                                actionHtml += '    <ul class="dropdown-menu pull-right">';
                                actionHtml += '        <li>';
                                actionHtml += '            <a name="contactsuccess" data-adminid="' + response.Data.CrawlerAdminId + '" data-carehistoryid="' + response.Data.CareHistoryId + '" title="Confirm customer to contact success">';
                                actionHtml += '                <i class="icon-ok"></i> ' + crawler_customer_index.language_res.act_crawlersearch_contactsuccess;
                                actionHtml += '            </a>';
                                actionHtml += '        </li>';
                                actionHtml += '        <li>';
                                actionHtml += '            <a name="contactfailure" data-adminid="' + response.Data.CrawlerAdminId + '" data-carehistoryid="' + response.Data.CareHistoryId + '" title="Confirm customer to contact failure">';
                                actionHtml += '                <i class="icon-remove"></i>' + crawler_customer_index.language_res.act_crawlersearch_contactfailure;
                                actionHtml += '            </a>';
                                actionHtml += '        </li>';
                                actionHtml += '        <li class="hide">';
                                actionHtml += '            <a name="return" data-adminid="' + response.Data.CrawlerAdminId + '" data-carehistoryid="' + response.Data.CareHistoryId + '" title="Confirm customer to contact failure">';
                                actionHtml += '                <i class="icon-hand-right"></i> ' + crawler_customer_index.language_res.act_crawlersearch_return;
                                actionHtml += '            </a>';
                                actionHtml += '        </li>';
                                actionHtml += '        <li>';
                                actionHtml += '            <a name="note" data-adminid="' + response.Data.CrawlerAdminId + '" data-carehistoryid="' + response.Data.CareHistoryId + '" href="#wrapNote" data-toggle="modal" title="note">';
                                actionHtml += '                <i class="icon-book"></i> ' + crawler_customer_index.language_res.act_crawlersearch_note;
                                actionHtml += '            </a>';
                                actionHtml += '        </li>';
                                actionHtml += '    </ul>';
                                actionHtml += '</div>';
                                $('td[name=wrapAction]', tr).html(actionHtml);
                                crawler_customer_list.action();
                            }
                            else if (response.Code == ResponseCode.Error) {
                                sysmess.error(response.Message);
                            }
                            else {
                                sysmess.warning(response.Message);
                            }
                        }
                    };

                    //sysrequest.send(option);
                    var option_request_token = {
                        controller :'CrawlerCustomer',
                        action:'GetTokenKeyForOneRequest',
                        data:{
                            id:adminId
                        },
                        callback : function(response){
                            if(response.Code == ResponseCode.Success){
                                option.data.signatureToken = response.Data;
                                sysrequest.send(option);
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option_request_token);

                });
            });
        });
        $('a[name=contactsuccess]', divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                $('#divBodyListCrawlerCustomer').addClass('hide');
                $('#wrapContactSuccess').removeClass('hide');
                var tr = $(this).parent().parent().parent().parent().parent();
                var fullName = $('span[name=wrapFullName]', tr).text();
                var email = $('span[name=wrapEmail]', tr).text();
                var phoneNumber = $('td[name=wrapPhoneNumber]', tr).text();
                var address = $.trim($('span[name=wrapAddress]', tr).text());
                var crawlerAdminId = $(this).data('adminid');
                var careHistoryId = $(this).data('carehistoryid');
                var postingType = $('span[name=wrapPostingtype]', tr).data('postingtype');
                var option = {
                    controller: 'CrawlerCustomer',
                    action: 'ConfirmSuccessContact',
                    data: {
                        CrawlerId: crawlerAdminId,
                        FullName: fullName,
                        PhoneNumber: phoneNumber,
                        Email: email,
                        CareHistoryId: careHistoryId,
                        Address: address,
                        PostingType : postingType
                    },
                    dataType: 'html',
                    callback: function () {
                        var regionId = $('select[name=RegionId]',$('#wrapContactSuccess')).val();
                        if (regionId > 0) {
                            var option = {
                                controller: "City",
                                action: "GetCityByRegion",
                                data: {
                                    regionId: regionId
                                },
                                callback: function (response) {
                                    if (response.Code == ResponseCode.Success) {
                                        $("select[name=CityId]", $('#wrapContactSuccess')).empty();
                                        if (response.Data.length > 0) {
                                            $("select[name=CityId]", $('#wrapContactSuccess')).append('<option value="">' + crawler_customer_index.language_res.option_no_city + '</option>');
                                        }
                                        var html = "";
                                        for (var i = 0; i < response.Data.length; i++) {
                                            html += '<option value="' + response.Data[i].Id + '">' + response.Data[i].Name + '</option>';
                                        }
                                        $("select[name=CityId]", $('#wrapContactSuccess')).append(html);
                                        $("select[name=CityId]", $('#wrapContactSuccess')).trigger('liszt:updated');
                                    } else {
                                        sysmess.error(response.Message);
                                    }
                                }
                            };
                            sysrequest.send(option);
                        }
                        syseditor.create('txaDescription');
                        CKEDITOR.addCss('.cke_editable { height:160px; }');
                        crawler_customer_index.success_contact_action();
                    },
                    element: $('#wrapContactSuccess')
                };
                sysrequest.send(option);
            });
        });
        $('a[name=contactfailure]', divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                var tr = $(this).parent().parent().parent().parent().parent();
                var adminId = $(this).data('adminid');
                var careHistoryId = $(this).data('carehistoryid');

                sysmess.confirm(crawler_customer_index.language_res.msg_cusfailcontact_confirm, function () {
                    var option = {
                        controller: 'CrawlerCustomer',
                        action: 'ContactFailure',
                        data: {
                            crawlerId: adminId,
                            careHistoryId: careHistoryId,
                            signatureToken :''
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                sysmess.info(response.Message);
                                var statusHtml = '<span class="label label-important status">';
                                statusHtml += '  <i class="icon-ban-circle"></i>';
                                statusHtml += '  Reject   ';
                                statusHtml += '</span>';
                                $('td[name=wrapStatus]', tr).html(statusHtml);
                                $('a[name=contactsuccess]', tr).parent().remove();
                                $('a[name=contactfailure]', tr).parent().remove();
                            }
                            else if (response.Code == ResponseCode.Error) {
                                sysmess.error(response.Message);
                            }
                            else {
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    //sysrequest.send(option);

                    var option_request_token = {
                        controller :'CrawlerCustomer',
                        action:'GetTokenKeyForOneRequest',
                        data:{
                            id:adminId
                        },
                        callback : function(response){
                            if(response.Code == ResponseCode.Success){
                                option.data.signatureToken = response.Data;
                                sysrequest.send(option);
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option_request_token);

                });
            });
        });
        $('a[name=note]', divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                var crawlerId = $(this).data('adminid');
                var careHistoryId = $(this).data('carehistoryid');
                $('button[name=save]', $('#wrapNote')).data('adminid', crawlerId);
                $('button[name=save]', $('#wrapNote')).data('carehistoryid', careHistoryId);
                $('textarea[name=note]', $('#wrapNote')).val('');
                crawler_customer_index.curr_tr_element = $(this).parent().parent().parent().parent().parent();
                var option = {
                    controller:'CrawlerCustomer',
                    action:'GetTokenKeyForGeneralView',
                    data:{
                        id : careHistoryId
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            $('input[name=NoteTokenChecksum]',$('#wrapNote')).val(response.Data);
                        }
                        else if(response.Code == ResponseCode.Error){
                            $('input[name=NoteTokenChecksum]',$('#wrapNote')).val('');
                        }
                        else{
                            $('input[name=NoteTokenChecksum]',$('#wrapNote')).val('');
                        }
                    }
                };
                sysrequest.send(option);
            });
        });
        $('a[name=return]', divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                var tr = $(this).parent().parent().parent().parent().parent();
                var careHistoryId = $(this).data('carehistoryid');

                sysmess.confirm(crawler_customer_index.language_res.msg_cusreturn_confirm, function () {
                    var option = {
                        controller: 'CrawlerCustomer',
                        action: 'Return',
                        data: {
                            careHistoryId: careHistoryId,
                            signatureToken :''
                        },
                        callback: function (response) {
                            if (response.Code == ResponseCode.Success) {
                                sysmess.info(response.Message);
                                $('td[name=wrapAssignTo]', tr).html('');
                                var actionHtml = '<div class="btn-group none-bottom">';
                                actionHtml += '    <button class="btn w80 blue mini dropdown-toggle" data-toggle="dropdown">';
                                actionHtml += '        <i class="icon-cog"></i>' + crawler_customer_index.language_res.act_crawlersearch_manager + '<i class="icon-angle-down"></i>';
                                actionHtml += '    </button>';
                                actionHtml += '    <ul class="dropdown-menu pull-right">';
                                actionHtml += '        <li>';
                                actionHtml += '            <a name="receive" data-adminid="' + response.Data.CrawlerAdminId + '" data-carehistoryid="' + response.Data.CareHistoryId + '" title="Receive Crawler Customer">';
                                actionHtml += '                <i class="icon-edit"></i>' + crawler_customer_index.language_res.act_crawlersearch_receive;
                                actionHtml += '            </a>';
                                actionHtml += '        </li>';
                                actionHtml += '    </ul>';
                                actionHtml += '</div>';
                                $('td[name=wrapAction]', tr).html(actionHtml);
                                crawler_customer_list.action();
                            }
                            else if (response.Code == ResponseCode.Error) {
                                sysmess.error(response.Message);
                            }
                            else {
                                sysmess.warning(response.Message);
                            }
                        }
                    };

                    var option_request_token = {
                        controller :'CrawlerCustomer',
                        action:'GetTokenKeyForOneRequest',
                        data:{
                            id:careHistoryId
                        },
                        callback : function(response){
                            if(response.Code == ResponseCode.Success){
                                option.data.signatureToken = response.Data;
                                sysrequest.send(option);
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option_request_token);
                });
            });
        });
        $('a[name=hidefromsale]', divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                var adminId = $(this).data('adminid');
                var phoneNumber =$(this).data('phone');
                var wrapReason = $('#wrapHideFromList');
                $('input[name=hiddAdminId]',wrapReason).val(adminId);
                $('input[name=hiddPhoneNumber]',wrapReason).val(phoneNumber);
                $('textarea[name=Note]',wrapReason).closest('.row-fluid').addClass('hide');
                $('input[name=chkReasonType]:first').attr('checked','checked');
                
                var option = {
                    controller:'CrawlerCustomer',
                    action:'GetTokenKeyForGeneralView',
                    data:{
                        id : adminId
                    },
                    callback: function (response) {
                        if(response.Code == ResponseCode.Success){
                            $('input[name=HideFromSaleTokenChecksum]',wrapReason).val(response.Data);
                        }
                        else if(response.Code == ResponseCode.Error){
                            $('input[name=HideFromSaleTokenChecksum]',wrapReason).val('');
                        }
                        else{
                            $('input[name=HideFromSaleTokenChecksum]',wrapReason).val('');
                        }
                    }
                };
                sysrequest.send(option);
            });
        });
        $('a[name=unhidefromsale]',divBodyListCrawlerCustomer).each(function () {
            $(this).off('click').on('click', function () {
                var adminId = $(this).data('adminid');
                var phoneNumber = $(this).data('phone');
                var isHide = $(this).data('ishide');
                var tr_element = $(this).closest('tr');
                sysmess.confirm('Do you want to unhide this lead ?', function () {
                    var option ={
                        controller :'CrawlerCustomer',
                        action:'UnHide',
                        data:{
                            adminId : adminId,
                            signatureToken :''
                        },
                        callback: function (response) {
                            if(response.Code == ResponseCode.Success){
                                sysmess.info(response.Message);
                                var html    = '<li>';
                                html   += '   <a name="hidefromsale" data-adminid="'+ adminId + '" data-phone="'+ phoneNumber +'" href="#wrapHideFromList" data-toggle="modal" title="hide from list">';
                                html   += '      <i class="icon-eye-close"></i>';
                                html   += '      Hide';
                                html   += '   </a>';
                                html   += '</li>';
                                if(isHide == 'True'){
                                    $('a[name=unhidefromsale]',tr_element).closest('ul').append(html);
                                }
                                $('a[name=unhidefromsale]',tr_element).parent().remove();
                                $('div[name=wrapHiddenSale]',tr_element).remove();

                                crawler_customer_list.action();
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    //sysrequest.send(option);

                    var option_request_token = {
                        controller :'CrawlerCustomer',
                        action:'GetTokenKeyForOneRequest',
                        data:{
                            id:adminId
                        },
                        callback : function(response){
                            if(response.Code == ResponseCode.Success){
                                option.data.signatureToken = response.Data;
                                sysrequest.send(option);
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message);
                            }
                            else{
                                sysmess.warning(response.Message);
                            }
                        }
                    };
                    sysrequest.send(option_request_token);
                });
            });
        });
    },
    get_current_page: function () {
        var curr_Page = 1;
        $('.dataTables_paginate li.active a').each(function () {
            var id_html = $(this).text().trim();
            if($.isNumeric(id_html)){
                curr_Page = parseInt(id_html);
            }
        });
        return curr_Page;
    }
};