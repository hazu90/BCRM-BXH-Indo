/**
Core script to handle the entire layout and base functions
**/
var app = function () {

    // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;

    var responsiveHandlers = [];

    // theme layout color set
    var layoutColorCodes = { 'blue': '#4b8df8', 'red': '#e02222', 'green': '#35aa47', 'purple': '#852b99', 'grey': '#555555', 'light-grey': '#fafafa', 'yellow': '#ffb848' };

    //* BEGIN:CORE HANDLERS *//
    // this function handles responsive layout on screen size resize or mobile device rotate.
    var handleResponsive = function() {

        if ($('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) == 8) {
            isIE8 = true; // detect IE8 version
        }

        if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) == 9) {
            isIE9 = true; // detect IE9 version
        }

        var isIE10 = !!navigator.userAgent.match(/MSIE 10/);

        if (isIE10) {
            jQuery('html').addClass('ie10'); // detect IE10 version
        }

        // loops all page elements with "responsive" class and applies classes for tablet mode
        // For metornic  1280px or less set as tablet mode to display the content properly
        var _handleTabletElements = function() {
            if ($(window).width() <= 1280) {
                $(".responsive").each(function() {
                    var forTablet = $(this).attr('data-tablet');
                    var forDesktop = $(this).attr('data-desktop');
                    if (forTablet) {
                        $(this).removeClass(forDesktop);
                        $(this).addClass(forTablet);
                    }
                });
            }
        };

        // loops all page elements with "responsive" class and applied classes for desktop mode
        // For metornic  higher 1280px set as desktop mode to display the content properly
        var _handleDesktopElements = function() {
            if ($(window).width() > 1280) {
                $(".responsive").each(function() {
                    var forTablet = $(this).attr('data-tablet');
                    var forDesktop = $(this).attr('data-desktop');
                    if (forTablet) {
                        $(this).removeClass(forTablet);
                        $(this).addClass(forDesktop);
                    }
                });
            }
        };

        // remove sidebar toggler if window width smaller than 900(for table and phone mode)
        var _handleSidebar = function() {
            if ($(window).width() < 900) {
                $.cookie('sidebar-closed', null);
                $('.page-container').removeClass("sidebar-closed");
            }
        };

        // handle all elements which require to re-initialize on screen width change(on resize or on rotate mobile device)
        var handleElements = function() {
            // reinitialize core elements
            handleTooltip();
            _handleSidebar();
            _handleTabletElements();
            _handleDesktopElements();
            handleSidenarAndContentHeight();
            handleChoosenSelect();

            // reinitialize other subscribed elements
            for (var i in responsiveHandlers) {
                var each = responsiveHandlers[i];
                each.call();
            }
        };

        // handles responsive breakpoints.
        $(window).setBreakpoints({
            breakpoints: [320, 480, 768, 900, 1024, 1280]
        });

        $(window).bind('exitBreakpoint320', function() {
            handleElements();
        });
        $(window).bind('enterBreakpoint320', function() {
            handleElements();
        });

        $(window).bind('exitBreakpoint480', function() {
            handleElements();
        });
        $(window).bind('enterBreakpoint480', function() {
            handleElements();
        });

        $(window).bind('exitBreakpoint768', function() {
            handleElements();
        });
        $(window).bind('enterBreakpoint768', function() {
            handleElements();
        });

        $(window).bind('exitBreakpoint900', function() {
            handleElements();
        });
        $(window).bind('enterBreakpoint900', function() {
            handleElements();
        });

        $(window).bind('exitBreakpoint1024', function() {
            handleElements();
        });
        $(window).bind('enterBreakpoint1024', function() {
            handleElements();
        });

        $(window).bind('exitBreakpoint1280', function() {
            handleElements();
        });
        $(window).bind('enterBreakpoint1280', function() {
            handleElements();
        });
    };

    var handleSidenarAndContentHeight = function() {
        var content = $('.page-content');
        var sidebar = $('.page-sidebar');

        if (!content.attr("data-height")) {
            content.attr("data-height", content.height());
        }

        if (sidebar.height() > content.height()) {
            content.css("min-height", sidebar.height() + 20);
        } else {
            content.css("min-height", content.attr("data-height"));
        }
    };

    var handleHorizontalMenu = function() {
        //handle hor menu item click
        $('.header .hor-menu .dropdown-menu li').off('click');
        $('.header .hor-menu .dropdown-menu li').on('click', function() {
            if ($(this).hasClass('l1')) {
                return;
            }
            else {
                $('.header .hor-menu .nav li').removeClass('active').remove('span.selected');
                if ($(this).hasClass('l2')) {
                    if ($('span.selected', $(this).parent().parent().parent().prev()).length == 0) {
                        $(this).parent().parent().parent().prev().append($('<span class="selected"></span>'));
                    }    
                    $(this).parent().parent().addClass('active');
                    $(this).parent().parent().parent().parent().addClass('active');
                }
                else {
                    if ($('span.selected', $(this).parent().prev()).length == 0) {
                        $(this).parent().prev().append($('<span class="selected"></span>'));
                    }    
                    $(this).parent().parent().addClass('active');
                }
                $(this).addClass('active');
                var controller = $('a', $(this)).attr('controller');
                if (controller != null && controller.length > 0) {
                    var action = $('a', $(this)).attr('action');
                    if(action == "" || action == null) {
                        action = "Index";
                    }
                    var options = {
                        controller: controller,
                        dataType: 'html',
                        action:action
                    };
                    sysrequest.send(options);
                }
            }
        });
        $('.header .user .dropdown-menu li').off('click');
        $('.header .user .dropdown-menu li').on('click', function () {
            var controller = $('a', $(this)).attr('controller');
            if (controller != null && controller.length > 0) {
                var action = $('a', $(this)).attr('action');
                if(action == "" || action == null) {
                    action = "Index";
                }
                var options = {
                    controller: controller,
                    dataType: 'html',
                    action:action
                };
                sysrequest.send(options);
            }
        });
    };

    var handleGoTop = function() {
        /* set variables locally for increased performance */
        jQuery('.footer .go-top').click(function(e) {
            app.scrollTo();
            e.preventDefault();
        });
    };

    var handleAccordions = function() {
        $(".accordion").collapse().height('auto');

        var lastClicked;

        //add scrollable class name if you need scrollable panes
        jQuery('.accordion.scrollable .accordion-toggle').click(function() {
            lastClicked = jQuery(this);
        }); //move to faq section

        jQuery('.accordion.scrollable').on('shown', function() {
            jQuery('html,body').animate({
                scrollTop: lastClicked.offset().top - 150
            }, 'slow');
        });
    };

    var handleScrollers = function() {

        $('.scroller').each(function() {
            $(this).slimScroll({
                size: '7px',
                color: '#a1b2bd',
                position: isRTL ? 'left' : 'right',
                height: $(this).attr("data-height"),
                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: false
            });
        });
    };

    var handleTooltip = function() {
        if (app.isTouchDevice()) { // if touch device, some tooltips can be skipped in order to not conflict with click events
            jQuery('.tooltips:not(.no-tooltip-on-touch-device)').tooltip();
        } else {
            jQuery('.tooltips').tooltip();
        }
    };

    var handlePopover = function() {
        jQuery('.popovers').popover({ html : true });
    };

    var handleFancybox = function() {
        if (!jQuery.fancybox) {
            return;
        }

        if (jQuery(".fancybox-button").size() > 0) {
            jQuery(".fancybox-button").fancybox({
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });
        }
    };

    var handleStyler = function () {

        var panel = $('.color-panel');

        $('.icon-color', panel).click(function () {
            $('.color-mode').show();
            $('.icon-color-close').show();
        });

        $('.icon-color-close', panel).click(function () {
            $('.color-mode').hide();
            $('.icon-color-close').hide();
        });

        $('li', panel).click(function () {
            var color = $(this).attr("data-style");
            setColor(color);
            $('.inline li', panel).removeClass("current");
            $(this).addClass("current");
        });

        $('input', panel).change(function () {
            setLayout();
        });

        var setColor = function(color) {
            $('#style_color').attr("href", "assets/css/themes/" + color + ".css");
        };

        var setLayout = function() {
            if ($('input.header', panel).is(":checked")) {
                $("body").addClass("fixed-top");
                $(".header").addClass("navbar-fixed-top");
            } else {
                $("body").removeClass("fixed-top");
                $(".header").removeClass("navbar-fixed-top");
            }
        };
    };

    var handleFixInputPlaceholderForIE = function () {
        //fix html5 placeholder attribute for ie7 & ie8
        if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) <= 9) { // ie7&ie8

            // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
            jQuery('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function () {

                var input = jQuery(this);

                jQuery(input).addClass("placeholder").val(input.attr('placeholder'));

                jQuery(input).focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                jQuery(input).blur(function () {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    };

    var handleDateTimePickers = function() {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker();
        }

        if (jQuery().timepicker) {
            $('.timepicker-default').timepicker();
            $('.timepicker-24').timepicker({
                minuteStep: 1,
                showSeconds: true,
                showMeridian: false
            });
        }

        if (!jQuery().daterangepicker) {
            return;
        }

        $('.date-range').daterangepicker(
            {
                opens: (app.isRTL() ? 'left' : 'right'),
                format: 'MM/dd/yyyy',
                separator: ' to ',
                startDate: Date.today().add({
                    days: -29
                }),
                endDate: Date.today(),
                minDate: '01/01/2012',
                maxDate: '12/31/2014',
            }
        );

        $('#form-date-range').daterangepicker({
                ranges: {
                    'Today': ['today', 'today'],
                    'Yesterday': ['yesterday', 'yesterday'],
                    'Last 7 Days': [Date.today().add({
                        days: -6
                    }), 'today'],
                    'Last 29 Days': [Date.today().add({
                        days: -29
                    }), 'today'],
                    'This Month': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
                    'Last Month': [Date.today().moveToFirstDayOfMonth().add({
                            months: -1
                        }), Date.today().moveToFirstDayOfMonth().add({
                            days: -1
                        })]
                },
                opens: (app.isRTL() ? 'left' : 'right'),
                format: 'MM/dd/yyyy',
                separator: ' to ',
                startDate: Date.today().add({
                    days: -29
                }),
                endDate: Date.today(),
                minDate: '01/01/2012',
                maxDate: '12/31/2014',
                locale: {
                    applyLabel: 'Submit',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom Range',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                },
                showWeekNumbers: true,
                buttonClasses: ['btn-danger']
            },
            function(start, end) {
                $('#form-date-range span').html(start.toString('MMMM d, yyyy') + ' - ' + end.toString('MMMM d, yyyy'));
            });

        $('#form-date-range span').html(Date.today().add({
            days: -29
        }).toString('MMMM d, yyyy') + ' - ' + Date.today().toString('MMMM d, yyyy'));


        if (!jQuery().datepicker || !jQuery().timepicker) {
            return;
        }
    };

    var handleNewDatetimePicker = function () {

        $(".form_datetime").datetimepicker({
            format: "dd/mm/yyyy - hh:ii",
            pickerPosition: "bottom-left"
        });

        $(".form_advance_datetime").datetimepicker({
            format: "dd/mm/yyyy - hh:ii",
            autoclose: true,
            todayBtn: true,
            startDate: "2013-02-14 10:00",
            pickerPosition: "bottom-left",
            minuteStep: 10
        });

        $(".form_meridian_datetime").datetimepicker({
            format: "dd MM yyyy - HH:ii P",
            showMeridian: true,
            autoclose: true,
            pickerPosition: "bottom-left",
            todayBtn: true
        });
    };
    // Create dropdownlist city and district
    var handleDropdownCityDistrict = function() {
        $(".city").each(function() {
            var cboCity = $(this);
            var cboDistrict = $('#' + cboCity.attr('control-relation'));
            for (var i = 0; i < jsonCity.length; i++) {
                cboCity.append("<option value=" + jsonCity[i].Id + ">" + jsonCity[i].CityName + "</option>");
            }
            if (cboCity.attr('cid') > 0) {
                var cityId = cboCity.attr('cid');
                cboCity.val(cityId);
                for (var i = 0; i < jsonDistrict.length; i++) {
                    if (jsonDistrict[i].CityId == cityId) {
                        cboDistrict.append("<option value=" + jsonDistrict[i].Id + ">" + jsonDistrict[i].DistrictName + "</option>");
                    }
                }
                cboDistrict.val(cboDistrict.attr('did'));
            }
            cboCity.off('change');
            cboCity.on("change", function() {
                $("option[value!='']", cboDistrict).remove();
                for (var i = 0; i < jsonDistrict.length; i++) {
                    if (jsonDistrict[i].CityId == cboCity.val()) {
                        cboDistrict.append("<option value=" + jsonDistrict[i].Id + ">" + jsonDistrict[i].DistrictName + "</option>");
                    }
                }
                if (cboDistrict.hasClass('chosen')) {
                    var events = jQuery._data($('input', $('div#' + cboCity.attr('control-relation') + '_chzn'))[0], "events");
                    cboDistrict.removeClass('chzn-done');
                    $('div#' + cboCity.attr('control-relation') + '_chzn').remove();
                    cboDistrict.show();
                    cboDistrict.chosen({
                        allow_single_deselect: $(this).attr("data-with-diselect") === "1" ? true : false
                    });
                    if (events.keyup.length > 1) {
                        $('input', $('div#' + cboCity.attr('control-relation') + '_chzn')).on('keyup', function(event) {
                            events.keyup[1].handler(event);
                        });
                    }
                }
            });
        });
    };
    //* END:CORE HANDLERS *//

    //*START: CORE OPTIMAL*//
    var handleDatePicker = function(obj) {
        if (jQuery().datepicker) {
            if (obj != null) {
                $('.date-picker', obj).datepicker().on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });
            }
            else {
                $('.date-picker').datepicker().on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });
            }
        }
    };

    var handlePortletTools = function(obj) {
        var lnkRemove = null;
        var lnkReload = null;
        var lnkColexp = null;
        if (obj != null) {
            lnkRemove = jQuery('.portlet .tools a.remove', obj);
            lnkReload = jQuery('.portlet .tools a.reload', obj);
            lnkColexp = jQuery('.portlet .tools .collapse, .portlet .tools .expand', obj);
        }
        else {
            lnkRemove = jQuery('.portlet .tools a.remove');
            lnkReload = jQuery('.portlet .tools a.reload');
            lnkColexp = jQuery('.portlet .tools .collapse, .portlet .tools .expand');
        }
        lnkRemove.off('click');
        lnkRemove.on('click', function() {
            var control = jQuery(this).attr('control-relation');
            var remove = jQuery(this).attr('remove');
            var removable = jQuery(this).parents(".portlet");
            if (removable.next().hasClass('portlet') || removable.prev().hasClass('portlet')) {
                if (remove) {
                    jQuery(this).parents(".portlet").remove();    
                }
                else {
                    jQuery(this).parents(".portlet").addClass('hide');
                }
                if (control != null) {
                    jQuery('#' + control).parents(".portlet").removeClass('hide');
                    syscommon.scroll(jQuery('#' + control).parents(".portlet"));
                }
            } else {
                if (remove) {
                    jQuery(this).parents(".portlet").parent().remove();
                }
                else {
                    jQuery(this).parents(".portlet").parent().addClass('hide');
                }
                if (control != null) {
                    jQuery('#' + control).parents(".portlet").parent().removeClass('hide');
                    syscommon.scroll(jQuery('#' + control).parents(".portlet").parent());
                }
            }
        });

        lnkReload.off('click');
        lnkReload.on('click', function() {
            var el = jQuery(this).parents(".portlet");
            app.blockUI(el);
            window.setTimeout(function() {
                app.unblockUI(el);
            }, 1000);
        });

        lnkColexp.off('click');
        lnkColexp.on('click', function() {
            var el = jQuery(this).parents(".portlet").children(".portlet-body");
            if (jQuery(this).hasClass("collapse")) {
                jQuery(this).removeClass("collapse").addClass("expand");
                el.slideUp(200);
            } else {
                jQuery(this).removeClass("expand").addClass("collapse");
                el.slideDown(200);
            }
        });
    };

    var handleChoosenSelect = function(obj) {
        if (!jQuery().chosen) {
            return;
        }
        var chosen = null;
        if (obj != null) {
            chosen = $(".chosen", obj);
        }
        else 
        {
            chosen = $(".chosen");
        }
        chosen.each(function() {
            $(this).chosen({
                allow_single_deselect: $(this).attr("data-with-diselect") === "1" ? true : false
            });
        });
    };

    var handleUniform = function(obj) {
        if (!jQuery().uniform) {
            return;
        }
        var test = null;
        if (obj != null) {
            test = $("input[type=checkbox]:not(.toggle), input[type=radio]:not(.toggle, .star)", obj);
        }
        else {
            test = $("input[type=checkbox]:not(.toggle), input[type=radio]:not(.toggle, .star)");
        }
        if (test.size() > 0) {
            test.each(function() {
                if ($(this).parents(".checker").size() == 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }
    };
    //*END: CORE OPTIMAL*//

    return {

        //main function to initiate template pages
        init: function () {
            //core handlers
            handleDropdownCityDistrict();//handle dropdownlist city and district
            handleResponsive(); // set and handle responsive
            handleGoTop(); //handles scroll to top functionality in the footer
            handleStyler(); // handles style customer tool
            handleHorizontalMenu(); // handles horizontal menu
            //core optimal
            //handleDatePicker();
            //handlePortletTools(); // handles portlet action bar functionality(refresh, configure, toggle, remove)
            //handleChoosenSelect(); // handles bootstrap chosen dropdowns
            //handleUniform(); // handles uniform elements
        },

        initDatePicker: function (obj) {
            handleDatePicker(obj);
        },

        initPortletTools: function(obj) {
            handlePortletTools(obj);
        },

        initChoosenSelect: function(obj) {
            handleChoosenSelect(obj);
        },

        initUniform: function (obj) {
            handleUniform(obj);
        },

        initPopover: function() {
            handlePopover();
        },

        initTooltip: function() {
            handleTooltip();
        },

        initDateTimePickers: function() {
            handleDateTimePickers();
        },

        initNewDateTimePickers: function() {
           handleNewDatetimePicker();     
        },

        initDropdownCityDistrict: function() {
            handleDropdownCityDistrict();
        },

        fixContentHeight: function () {
            handleSidenarAndContentHeight();
        },

        addResponsiveHandler: function (func) {
            responsiveHandlers.push(func);
        },

        initHandleScrollers: function() {
           handleScrollers();  
        },
        // useful function to make equal height for contacts stand side by side
        setEqualHeight: function (els) {
            var tallestEl = 0;
            els = jQuery(els);
            els.each(function () {
                var currentHeight = $(this).height();
                if (currentHeight > tallestEl) {
                    tallestColumn = currentHeight;
                }
            });
            els.height(tallestEl);
        },

        // wrapper function to scroll to an element
        scrollTo: function (el, offeset) {
            pos = el ? el.offset().top : 0;
            jQuery('html,body').animate({
                scrollTop: pos + (offeset ? offeset : 0)
            }, 'slow');
        },

        scrollTop: function () {
            app.scrollTo();
        },

        scroller: function() {
            handleScrollers();
        },

        // wrapper function to  block element(indicate loading)
        blockUI: function (el, centerY) {
            jQuery(el).block({
                message: '<img src="/Content/assets/img/ajax-loading.gif" align="">',
                centerY: centerY != undefined ? centerY : true,
                css: {
                    top: '10%',
                    border: 'none',
                    padding: '2px',
                    backgroundColor: 'none'
                },
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.05,
                    cursor: 'wait'
                }
            });
        },

        // wrapper function to  un-block element(finish loading)
        unblockUI: function (el) {
            jQuery(el).unblock({
                onUnblock: function () {
                    if (!jQuery(el).is('.modal, .in')) {
                        jQuery(el).removeAttr("style");
                    }
                }
            });
        },

        initFancybox: function () {
            handleFancybox();
        },

        getActualVal: function (el) {
            var el = jQuery(el);
            if (el.val() === el.attr("placeholder")) {
                return "";
            }

            return el.val();
        },

        getURLParameter: function (paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }
            return null;
        },

        // check for device touch support
        isTouchDevice: function () {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        },

        isIE8: function () {
            return isIE8;
        },

        isRTL: function () {
            return isRTL;
        },

        getLayoutColorCode: function (name) {
            if (layoutColorCodes[name]) {
                return layoutColorCodes[name];
            } else {
                return '';
            }
        }
    };

} ();