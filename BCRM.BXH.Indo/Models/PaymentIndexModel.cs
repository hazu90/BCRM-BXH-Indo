﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class PaymentIndexModel
    {
        public string FilterKeyword { get; set; }
        public int CreatorId { get; set; }
        public short Status { get; set; }
        public System.DateTime? CreatedFromDate { get; set; }
        public System.DateTime? CreatedToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get { return 25; } }
    }

    public class PaymentCreateModel
    {
        public int CustomerId { get; set; }
        public int Money { get; set; }
        public DateTime PaymentDate { get; set; }
    }

    public class PaymentSearchModel
    {
        public int Index { get; set; }
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string CustomerFullName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public int Money { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public short Status { get; set; }
        public string Feedback { get; set; }
    }
}