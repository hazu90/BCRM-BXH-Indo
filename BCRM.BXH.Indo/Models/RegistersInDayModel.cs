﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class RegistersInDayModel
    {
        public int CustomerId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}