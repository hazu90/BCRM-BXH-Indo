﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class RoleSystem
    {
        /// <summary>
        /// Quyền sale
        /// </summary>
        public const int Sale = 6;
        /// <summary>
        /// Quyền sale manager
        /// </summary>
        public const int Manager = 7;
        /// <summary>
        /// Quyền admin
        /// </summary>
        public const int Admin = 8;
        /// <summary>
        /// Quyền điều phối
        /// </summary>
        public const int Coordinate = 9;
        /// <summary>
        /// Quyền xem của sale supervisor
        /// </summary>
        public const int SaleSupervisorView = 10;
        /// <summary>
        /// Quyền Aftersale
        /// </summary>
        public const int AfterSale = 11;
        /// <summary>
        /// Quyền CSKH
        /// </summary>
        public const int CustomerCarer = 12;
        /// <summary>
        /// Quyền sale supervisor editor
        /// </summary>
        public const int SaleSupervisorEditor = 13;
    }
}