﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class AdminWorkingAccountReportIndexModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class AdminWorkingAccountReportResult
    {
        public int ReportDate { get; set; }
        public int CountRegisteredCustomer { get; set; }
        public int CountActiveCustomer { get; set; }
    }
}