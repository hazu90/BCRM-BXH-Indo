﻿using BCRM.BXH.Indo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class AfterSaleAssistantReportIndexModel
    {
        public string AssignTo { get; set; }
        public int GroupId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<KeyValuePair<int, string>> LstGroup { get; set; }
        public List<KeyValuePair<string, string>> LstUser { get; set; }

        public void SetListGroup(bool isSearchAll, List<Group> lstGroup)
        {
            this.LstGroup = new List<KeyValuePair<int, string>>();
            if (isSearchAll)
            {
                this.LstGroup.Add(new KeyValuePair<int, string>(0, "--All groups--"));
                this.LstGroup.AddRange(lstGroup.Select(o => new KeyValuePair<int, string>(o.Id, o.Name)).ToList());
            }
            else
            {
                this.LstGroup.AddRange(lstGroup.Select(o => new KeyValuePair<int, string>(o.Id, o.Name)).ToList());
            }
        }
        public void SetListUsers(bool isSearchAll, List<UserSearchModel> lstUsers)
        {
            this.LstUser = new List<KeyValuePair<string, string>>();
            if (isSearchAll)
            {
                this.LstUser.Add(new KeyValuePair<string, string>("", "--All after-salers--"));
                this.LstUser.AddRange(lstUsers.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList());
            }
            else
            {
                this.LstUser.AddRange(lstUsers.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList());
            }
        }
    }
    public class AfterSaleAssistantViewModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int SumAssistanceCustomer { get; set; }
        public int SumPosting { get; set; }
        public decimal AverageDailyPosting { get; set; }
        public int ActiveAccount { get; set; }
    }
}