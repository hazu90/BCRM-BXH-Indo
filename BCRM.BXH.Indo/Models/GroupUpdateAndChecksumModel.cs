﻿using BCRM.BXH.Indo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class GroupUpdateAndChecksumModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public int Leader { get; set; }
        public int Priority { get; set; }
        public string TokenChecksum { get; set; }
        public GroupUpdateAndChecksumModel() { }
        public GroupUpdateAndChecksumModel(Group model, string tokenChecksum)
        {
            this.Id = model.Id;
            this.Name = model.Name;
            this.Alias = model.Alias;
            this.Description = model.Description;
            this.Leader = model.Leader;
            this.Priority = model.Priority;
            this.TokenChecksum = tokenChecksum;
        }
    }
}