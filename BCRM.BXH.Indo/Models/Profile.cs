﻿using BCRM.BXH.Indo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class ProfileSave
    {
        public int CustomerId { get; set; }
        public List<Profile> ListProfile { get; set; }
        public string PhoneNumber { get; set; }
        public string TokenChecksum { get; set; }
    }
}