﻿using BCRM.BXH.Indo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class CollaboratorSearchModel
    {
        public string FilterKeyword { get; set; }
        public int GroupId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalRecord { get; set; }
        public int? Status { get; set; }
        public List<Group> LstGroup { get; set; }
        public List<CollaboratorReportModel> LstResult { get; set; }
    }
}