﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class ShowroomSearchModel
    {
        public string FilterKeyword { get; set; }
        public string AssignTo { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int ShowroomType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalRecord { get; set; }
    }
}