﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Entity;
using System.ComponentModel.DataAnnotations;

using System.Web.Mvc;

namespace BCRM.BXH.Indo.Models
{
    public class CustomerIndexModel
    {
        [AllowHtml]
        public string FilterKeyword { get; set; }
        private DateTime _startDate;
        public DateTime StartDate
        {
            get
            {
                if(_startDate == DateTime.MinValue)
                {
                    return new DateTime(2000, 1, 1, 0, 0, 0);
                }
                else
                {
                    return _startDate;
                }
            }
            set
            {
                if (value == DateTime.MinValue)
                {
                    _startDate = new DateTime(2000, 1, 1, 0, 0, 0);
                }
                else
                {
                    _startDate = value;
                }
            }
        }
        private DateTime _endDate;
        public DateTime EndDate
        {
            get
            {
                if (_endDate == DateTime.MinValue)
                {
                    return new DateTime(9999, 1, 1, 0, 0, 0);
                }
                else
                {
                    return _endDate;
                }
            }
            set
            {
                if (value == DateTime.MinValue)
                {
                    _endDate = new DateTime(9999, 1, 1, 0, 0, 0);
                }
                else
                {
                    _endDate = new DateTime(value.Year, value.Month, value.Day, 23, 59, 59);
                }
                
            }
        }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        private string _assignTo;
        public string AssignTo 
        {
            get
            {
                if (string.IsNullOrEmpty(_assignTo))
                {
                    return "";
                }
                else
                {
                    return _assignTo;
                }
            }
            set 
            {
                if (string.IsNullOrEmpty(value))
                {
                    _assignTo = "";
                }
                else 
                {
                    _assignTo = value;
                }
            } 
        }
        private string _creator;
        public string Creator 
        {
            get
            {
                if (string.IsNullOrEmpty(_creator))
                {
                    return "";
                }
                else
                {
                    return _creator;
                }
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _creator = "";
                }
                else
                {
                    _creator = value;
                }
            }
        }
        private string _postingAssistant;
        public string PostingAssistant 
        {
            get
            {
                if (string.IsNullOrEmpty(_postingAssistant))
                {
                    return "";
                }
                else
                {
                    return _postingAssistant;
                }
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _postingAssistant = "";
                }
                else
                {
                    _postingAssistant = value;
                }
            }
        }
        public int PostingNumber { get; set; }
        public int PostingNumberFrom 
        { 
            get
            {
                switch (this.PostingNumber)
                {
                    case 0: return -1;
                    case 1: return -1;
                    case 2: return 1;
                    case 3: return 10;
                    case 4: return 25;
                    case 5: return 50;
                    default: return -1;
                }
            } 
        }
        public int PostingNumberTo
        {
            get
            {
                switch (this.PostingNumber)
                {
                    case 0: return -1;
                    case 1: return 2;
                    case 2: return 11;
                    case 3: return 26;
                    case 4: return 51;
                    case 5: return 1000000;
                    default: return -1;
                }
            }
        }
        public int PostingAssistanceStatus 
        { 
            get 
            {
                return -1;
            } 
        }
        public int? Type { get; set; }
        public int Sort { get; set; }       
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int ActiveStatus { get; set; }
        public int ActiveStatusInMonth { get; set; }
        //public bool IsNull
        //{
        //    get
        //    {
        //        if (FilterKeyword != null
        //            || StartDate != null
        //            || EndDate != null
        //            || CityId != null
        //            || DistrictId != null
        //            || AssignTo != null
        //            || Type != null)
        //        {
        //            return false;
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //}
        public CustomerIndexModel()
        {
            this.PageIndex = 1;
            this.PageSize = 25;
            this.Sort = -1;
        }
        public List<KeyValuePair<string,string>> LstCreator { get; set; }
        public List<KeyValuePair<string, string>> LstAftersale { get; set; }
        public List<Group> LstGroup { get; set; }
        public List<City> ListCity  { get; set; }
        public List<CustomerForIndex> ListCustomer { get; set; }
        public List<KeyValuePair<string, string>> LstUser { get; set; }
        public List<KeyValuePair<string, string>> LstPostingNumberDuration 
        {
            get 
            {
                return new List<KeyValuePair<string, string>>() 
                {
                    new KeyValuePair<string, string>("","--Total listings--"),
                    new KeyValuePair<string, string>("1","0-1 listings"),
                    new KeyValuePair<string, string>("2","2-10 listings"),
                    new KeyValuePair<string, string>("3","11-25 listings"),
                    new KeyValuePair<string, string>("4","26-50 listings"),
                    new KeyValuePair<string, string>("5","above 50 listings")
                };
            }
        }
        public void SetLstSaler(bool isManager,List<UserSearchModel> lstUsr)
        {
            this.LstUser = new List<KeyValuePair<string, string>>();
            //if(isManager)
            //{
            this.LstUser.Add(new KeyValuePair<string, string>("", "--Assign To--"));
            //}
            foreach(var item in lstUsr)
            {
                this.LstUser.Add(new KeyValuePair<string, string>(item.UserName, item.UserName));
            }
        }
        public void SetLstCreator(List<UserSearchModel> lstUsr)
        {
            this.LstCreator = new List<KeyValuePair<string, string>>();
            this.LstCreator.Add(new KeyValuePair<string, string>("", "--Creator--"));
            this.LstCreator.Add(new KeyValuePair<string, string>("Guest", "--Guest--"));
            foreach (var item in lstUsr)
            {
                this.LstCreator.Add(new KeyValuePair<string, string>(item.UserName, item.UserName));
            }
        }
        public void SetLstAftersale( List<UserSearchModel> lstUsr)
        {
            this.LstAftersale = new List<KeyValuePair<string, string>>();
            this.LstAftersale.Add(new KeyValuePair<string, string>("", "--After-sale--"));
            foreach (var item in lstUsr)
            {
                this.LstAftersale.Add(new KeyValuePair<string, string>(item.UserName, item.UserName));
            }
        }

    }

    public class CustomerForIndex
    {
        public int Id { get; set; }
        public string FullName { get; set; }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value.WrapPhoneNumber(); }
        }
        public string AssignTo { get; set; }
        public int Status { get; set; }
        public string StatusName
        {
            get
            {
                return ((CustomerStatus)this.Status).ToCustomerStatusName();
            }
        }
        public int CityId { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int ICareDate { get; set; }
        public DateTime? CareDate { get; set; }
        public DateTime? StartCareDate { get; set; }
        //public int Type { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public int TotalRecord { get; set; }
        public int PostingType { get; set; }
        public string Email { get; set; }
        public string CityName { get; set; }
        public string CreatedBy { get; set; }
        public string PostingAssistant { get; set; }
        public int PostingAssistanceStatus { get; set; }
        public int PostingNews { get; set; }
        public bool IsBlacklist { get; set; }
        public int PostingNumber { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public bool IsEditable(bool isManager, string userName)
        {
            if (isManager)
            {
                return true;
            }
            if (this.AssignTo.Equals(userName) || this.PostingAssistant.Equals(userName))
            {
                return this.IsDeleted ? false : true;
            }
            return false;
        }
        public bool IsBlockable(bool isManager)
        {
            return (!this.IsDeleted && isManager) ? true : false;
        }
        public bool IsUnblockable(bool isManager)
        {
            return (this.IsDeleted && isManager) ? true : false;
        }
        public bool IsAssigningToSaler(bool isManager)
        {
            return (!this.IsDeleted && isManager) ? true : false;
        }
        //public bool IsRequestRetrievePosting()
        //{
        //    return (!this.IsDeleted ) ? true : false;
        //}
        public bool IsAssigningToAfterSaler(bool isManager)
        {
            return (!this.IsDeleted && isManager) ? true : false;
        }
        public bool IsBlacklisted(bool isManager)
        {
            return (isManager && !this.IsBlacklist && !this.IsDeleted) ? true : false;
        }
        public bool IsUnblacklisted(bool isManager)
        {
            return (isManager && this.IsBlacklist && !this.IsDeleted) ? true : false;
        }
        public bool IsReceived()
        {
            return false;
            //return string.IsNullOrEmpty(this.AssignTo) ? true : false;
        }
        public bool IsReceivedToGetListings( bool isAfterSale)
        {
            if(!isAfterSale) return false;
            return !this.IsDeleted &&  string.IsNullOrEmpty(this.PostingAssistant)  ? true : false;
        }
        public bool IsNoted(bool isManager, string userName)
        {
            if (isManager)
            {
                return true;
            }
            if (this.AssignTo.Equals(userName) || this.PostingAssistant.Equals(userName))
            {
                return this.IsDeleted ? false : true;
            }
            return false;
        }
        public bool IsViewDetail()
        {
            return (this.IsDeleted ) ? false : true;
        }
        public bool IsEditablePhone( bool isManager)
        {
            if(isManager)
            {
                return true;
            }
            return false;
        }
    }

    public class CustomerRequest
    {
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public int RegionId { get; set; }
        public int Status { get; set; }
        public int PostingType { get; set; }
        public bool IsDeleted { get; set; }
        private string _email;
        public string Email 
        {
            get
            {
                return _email;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _email = "";
                }
                else
                {
                    _email = value.ToLower().Trim();
                }
            }
        }
        private string _description;
        public string Description 
        {
            get
            {
                return string.IsNullOrEmpty(_description) ? "" : System.Uri.UnescapeDataString(this._description);
            }
            set
            {
                _description = string.IsNullOrEmpty(value) ? "" : value;
            }
        }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public string AssignTo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int ICareDate { get; set; }
        public DateTime StartCareDate { get; set; }
        [MaxLength(256, ErrorMessage = "MaxAddress")]
        public string Address { get; set; }
        public List<Profile> ListProfile { get; set; }
        public string SignatureChecksum { get; set; }
    }

    public class CustomerDetailModel
    {
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string StatusName { get; set; }
        public string TypeOfCustomerName { get; set; }
        public string RegionName { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string AssignTo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public List<CustomerHistory> LstActivityHistory { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int SumRejectionTimes { get; set; }

        public CustomerDetailModel()
        {
            this.LstActivityHistory = new List<CustomerHistory>();
        }
    }
}