﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;

namespace BCRM.BXH.Indo.Models
{
    public class NonSaleCustomerIndexModel
    {
        private string _filterKeyword;
        public string FilterKeyword 
        { 
            get
            {
                return _filterKeyword;
            }
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    _filterKeyword = "";
                }
                else
                {
                    _filterKeyword = value.Trim();
                }
            }
        }
        public int Status { get; set; }
        private DateTime _dateOfCreationFrom;
        public DateTime DateOfCreationFrom 
        {
            get
            {
                if(_dateOfCreationFrom == DateTime.MinValue)
                {
                    return new DateTime(2000, 1, 1, 0, 0, 0);
                }
                else
                {
                    return _dateOfCreationFrom;
                }
            }
            set 
            {
                if(value == DateTime.MinValue)
                {
                    _dateOfCreationFrom = new DateTime(2000, 1, 1, 0, 0, 0);
                }
                else
                {
                    _dateOfCreationFrom = value;
                }
            } 
        }
        private DateTime _dateOfCreationTo;
        public DateTime DateOfCreationTo 
        {
            get
            {
                if (_dateOfCreationTo == DateTime.MinValue)
                {
                    return new DateTime(9999, 1, 1, 0, 0, 0);
                }
                else
                {
                    return _dateOfCreationTo;
                }
            }
            set
            {
                if (value == DateTime.MinValue)
                {
                    _dateOfCreationTo = new DateTime(9999, 1, 1, 0, 0, 0);
                }
                else
                {
                    _dateOfCreationTo = value;
                }
            } 
        }
        public string Receiver { get; set; }
        public List<Group> LstGroup { get; set; }
        public List<KeyValuePair<string,string>> LstReceiver { get; set; }
        public int PageIndex { get; set; }
        public void SetLstReceiver(List<UserSearchModel> lstUser)
        {
            this.LstReceiver = new List<KeyValuePair<string, string>>();
            this.LstReceiver.Add(new KeyValuePair<string, string>("-1000","--Received by--"));
            this.LstReceiver.Add(new KeyValuePair<string, string>("-1", "--Available--"));
            foreach(var item in lstUser)
            {
                LstReceiver.Add(new KeyValuePair<string, string>(item.UserName, item.UserName));
            }
        }
    }

    public class NonSaleCustomerSearchModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public string StatusName 
        { 
            get 
            {
                var contactStatusName = this.Status.ToContactStatusName();

                if (this.Status == ContactStatus.NotCalled.GetHashCode())
                {
                    return string.Format("<span class=\"label label-warning status\"><i class=\"icon-ok\"></i>{0}</span>", contactStatusName);
                }
                else if(this.Status == ContactStatus.Refused.GetHashCode())
                {
                    return string.Format("<span class=\"label label-important status\"><i class=\"icon-ban-circle\"></i>{0}</span>",contactStatusName);
                }
                return (this.Status).ToContactStatusName();
            } 
        }
        public string PhoneNumber { get; set; }
        public string ReceivedBy { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Address { get; set; }
        public bool IsChangeContactStatus(string currUserName )
        {
            if(currUserName.Equals(this.ReceivedBy) && this.Status == ContactStatus.NotCalled.GetHashCode() )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int CustomerCareId  { get; set; }
        public bool IsReceived 
        { 
            get 
            {
                if(string.IsNullOrEmpty(this.ReceivedBy))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            } 
        }
        public int PostingNews { get; set; }
    }
}