﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class ShowroomReportModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string CityName { get; set; }
        public int NumOfPosting { get; set; }
        public int ShowroomType { get; set; }
        public DateTime JoinDate { get; set; }
        public int NumPostingIn1stMonth { get; set; }

    }
}