﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class CollaboratorReportModel
    {
        public string UserName { get; set; }
        public string Displayname { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Status { get; set; }
        public int NumShowRoomA { get; set; }
        public int NumShowRoomB { get; set; }
        public int NumShowRoomC { get; set; }
        public int NumShowRoomD { get; set; }
        public int TotalShowRoom { get; set; }
        public int TotalPosting { get; set; }

    }
}