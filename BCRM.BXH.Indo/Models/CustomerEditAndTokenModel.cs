﻿using BCRM.BXH.Indo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class CustomerEditAndTokenModel
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public int RegionId { get; set; }
        public string Address { get; set; }
        public int CompanyId { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public string SignatureChecksum { get; set; }

        public int PostingType { get; set; }
        public string AssignTo { get; set; }

        public CustomerEditAndTokenModel() { }
        public CustomerEditAndTokenModel(Customer model, string checkSumToken)
        {
            this.Id = model.Id;
            this.PhoneNumber = model.PhoneNumber;
            this.FullName = model.FullName;
            this.Status = model.Status;
            this.Type = model.Type;
            this.Email = model.Email;
            this.Description = model.Description;
            this.RegionId = model.RegionId;
            this.Address = model.Address;
            this.CompanyId = model.CompanyId;
            this.CityId = model.CityId;
            this.DistrictId = model.DistrictId;
            this.SignatureChecksum = checkSumToken;
            this.PostingType = model.PostingType;
            this.AssignTo = model.AssignTo;
        } 
    }
}