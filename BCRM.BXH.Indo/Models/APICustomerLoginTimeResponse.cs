﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace BCRM.BXH.Indo.Models
{
    public class APICustomerLoginTimeResponse
    {
        [JsonProperty("total_user")]
        public int TotalUser { get; set; }
        [JsonProperty("data")]
        public List<UserLoginTimeModel> Data { get; set; }
    }

    public class UserLoginTimeModel
    {
        [JsonProperty("day_series")]
        public List<int> LstDayLoggedInInInteger { get; set; }
        [JsonProperty("total_times")]
        public int TotalTimesLoggedIn { get; set; }
        [JsonProperty("time_series")]
        public List<DateTime> LstLoggedInInTime { get; set; }
        [JsonProperty("user_name")]
        public string PhoneOrEmail { get; set; }
        [JsonProperty("total_days")]
        public int TotalLoggedInInDay { get; set; }
    }

    public class CustomerLoginInDayModel
    {
        public string PhoneOrEmail { get; set; }
        public int AdminId { get; set; }
        public int LoggedInDay { get; set; }

    }
}