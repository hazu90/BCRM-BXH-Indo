﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class AdminWorkingAccountInTimeModel
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public List<AdminProductAppovedInWorkingUserStatisticModel> LstAdminPosting { get; set; }
        public AdminWorkingAccountInTimeModel()
        {
            this.LstAdminPosting = new List<AdminProductAppovedInWorkingUserStatisticModel>();
        }
    }
}