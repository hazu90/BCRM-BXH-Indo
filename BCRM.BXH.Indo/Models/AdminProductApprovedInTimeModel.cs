﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class AdminProductApprovedInTimeModel
    {
        public int AdminId { get; set; }
        public int ProductId { get; set; }
        public DateTime ApprovedDate { get; set; }
    }

    public class AdminProductAppovedInTotalModel
    {
        public int AdminId { get; set; }
        public int ProductCount { get; set; }
    }
    public class AdminProductAppovedInWorkingUserStatisticModel
    {
        public int AdminId { get; set; }
        public DateTime ApprovedDate { get; set; }
    }
}