﻿using BCRM.BXH.Indo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.Models
{
    public class UserForCreateChecksumModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public int GroupId { get; set; }
        public DateTime? DOB { get; set; }
        public string SignatureChecksum { get; set; }

        public UserForCreateChecksumModel(Users model, string signatureCheckSum)
        {
            this.Id = model.Id;
            this.UserName = model.UserName;
            this.Password = model.Password;
            this.Email = model.Email;
            this.DisplayName = model.DisplayName;
            this.GroupId = model.GroupId;
            this.DOB = model.DOB;
            this.SignatureChecksum = signatureCheckSum;
        }
        public UserForCreateChecksumModel()
        {

        }
    }

    public class UserForCreateUpdateModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public int GroupId { get; set; }
        public DateTime? DOB { get; set; }

        public UserForCreateUpdateModel()
        { }
        public UserForCreateUpdateModel(int id)
        {
            this.Id = id;
        }
        public UserForCreateUpdateModel(UserForCreateChecksumModel model)
        {
            this.Id = model.Id;
            this.UserName = model.UserName;
            this.Password = model.Password;
            this.Email = model.Email;
            this.DisplayName = model.DisplayName;
            this.GroupId = model.GroupId;
            this.DOB = model.DOB;
        }
    }
}