﻿using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class FailureLoggingLayer
    {
        public void Create(int priority, string source, string data, string currentUser, string message, DateTime dateNow, string note)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("failurelogging_create_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_priority", priority));
                    command.Parameters.Add(new NpgsqlParameter("_source", source));
                    command.Parameters.Add(new NpgsqlParameter("_data", data));
                    command.Parameters.Add(new NpgsqlParameter("_currentuser", currentUser));
                    command.Parameters.Add(new NpgsqlParameter("_message", message));
                    command.Parameters.Add(new NpgsqlParameter("_createddate", dateNow));
                    command.Parameters.Add(new NpgsqlParameter("_note", note));
                    
                     command.ExecuteNonQuery();
                }
            }
        }

        public List<FailureLogging> GetListInDate(DateTime fromDate,DateTime toDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("failurelogging_getindate", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_fromdate", fromDate));
                    command.Parameters.Add(new NpgsqlParameter("_todate", toDate));
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<FailureLogging>();
                        while (reader.Read())
                        {
                            var syncModel = new FailureLogging();
                            if (!reader.HasRows)
                            {
                                return new List<FailureLogging>();
                            }

                            EntityBase.SetObjectValue(reader, ref syncModel);
                            ret.Add(syncModel);
                        }
                        return ret;
                    }
                }
            }
        }
    }
}