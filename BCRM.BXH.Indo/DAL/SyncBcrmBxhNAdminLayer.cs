﻿using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class SyncBcrmBxhNAdminLayer
    {
        public void Create(int customerId, int accountId, string email,string phoneNumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_create", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_accountid", accountId));
                    command.Parameters.Add(new NpgsqlParameter("_email", email));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Update(int customerId,string email,string phonenumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_update", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_email", email));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phonenumber));
                    command.ExecuteNonQuery();
                }
            }
        }

        public SyncBcrmBxhNAdmin GetByCustomerId(int customerId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_getbycustomerid", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        reader.Read();
                        var syncModel = new SyncBcrmBxhNAdmin();
                        if (reader.HasRows)
                        {
                            EntityBase.SetObjectValue(reader, ref syncModel);
                            return syncModel;    
                        }
                        else 
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public List<SyncBcrmBxhNAdmin> GetByListCustomerId(string customerIds)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_getbylistcustomerids", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_customerids", customerIds));
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<SyncBcrmBxhNAdmin>();
                        while (reader.Read())
                        {
                            var syncModel = new SyncBcrmBxhNAdmin();
                            if (!reader.HasRows)
                            {
                                return new List<SyncBcrmBxhNAdmin>();
                            }
                            
                            EntityBase.SetObjectValue(reader, ref syncModel);
                            ret.Add(syncModel);
                        }
                        return ret;
                    }
                }
            }
        }

        public List<SyncBcrmBxhNAdmin> GetByListAdminId(List<int> lstAdminId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_getbyadminid", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_lstadminid", lstAdminId));
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<SyncBcrmBxhNAdmin>();
                        while (reader.Read())
                        {
                            var syncModel = new SyncBcrmBxhNAdmin();
                            if (!reader.HasRows)
                            {
                                return new List<SyncBcrmBxhNAdmin>();
                            }

                            EntityBase.SetObjectValue(reader, ref syncModel);
                            ret.Add(syncModel);
                        }
                        return ret;
                    }
                }
            }
        }

        public List<SyncBcrmBxhNAdmin> GetByListEmailOrPhone(List<string> lstEmail, List<string> lstPhone)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_getlistemailorphone", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_lstemail", lstEmail));
                    command.Parameters.Add(new NpgsqlParameter("_lstphone", lstPhone));
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<SyncBcrmBxhNAdmin>();
                        while (reader.Read())
                        {
                            var syncModel = new SyncBcrmBxhNAdmin();
                            if (!reader.HasRows)
                            {
                                return new List<SyncBcrmBxhNAdmin>();
                            }

                            EntityBase.SetObjectValue(reader, ref syncModel);
                            ret.Add(syncModel);
                        }
                        return ret;
                    }
                }
            }
        }
        
    }
}