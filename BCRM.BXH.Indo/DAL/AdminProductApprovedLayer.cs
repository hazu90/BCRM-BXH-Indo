﻿using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class AdminProductApprovedLayer
    {
        public List<AdminProductApprovedInTimeModel> GetByListMemberAndTime(List<int> lstAdminId,DateTime startDate,DateTime endDate)
        {
            var ret = new List<AdminProductApprovedInTimeModel>();

            //using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHIndo"].ToString()))
            using (var db = new PostgresSQL())
            {
                //using (var command = db.CreateCommand("func_bcrm_productapproved_getbylistmemberandtime", true))
                using (var command = db.CreateCommand("productapproved_getbylistmemberandtime_v03", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_listadminid", lstAdminId));
                    command.Parameters.Add(new NpgsqlParameter("_fromdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_todate", endDate));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new AdminProductApprovedInTimeModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                            
                        }
                        return ret;
                    }
                }
            }
        }

        public List<AdminProductAppovedInTotalModel> GetTotalProductAppovedByListMember(List<int> lstAdminId)
        {
            var ret = new List<AdminProductAppovedInTotalModel>();
            //using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHIndo"].ToString()))
            using (var db = new PostgresSQL())
            {
                //using (var command = db.CreateCommand("func_bcrm_productapproved_getbylistmemberid", true))
                using (var command = db.CreateCommand("productapproved_getbylistmemberid_v03", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_listadminid", lstAdminId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new AdminProductAppovedInTotalModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }

                        }
                        return ret;
                    }
                }
            }
        }

        public List<AdminProductAppovedInWorkingUserStatisticModel> CheckCustomerActive_V02(DateTime fromDate, DateTime toDate)
        {
            var lstRet = new List<AdminProductAppovedInWorkingUserStatisticModel>();
            try
            {
                //using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHThailand"].ToString()))
                using (var db = new PostgresSQL())
                {
                    string store = "func_bcrm_checkmemberactive_v2";
                    using (var command = db.CreateCommand(store, true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_fromdate", fromDate));
                        command.Parameters.Add(new NpgsqlParameter("_todate", toDate));
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    var cust = new AdminProductAppovedInWorkingUserStatisticModel();
                                    EntityBase.SetObjectValue(reader, ref cust);
                                    if (cust != null)
                                    {
                                        lstRet.Add(cust);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return lstRet;
        }
    }
}