﻿using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class AdminArticleCategoriesLayer
    {
        public List<ShowroomReportModel> GetShowroomReport(DateTime fromDate, DateTime toDate, List<int> lstShowroomId)
        {
            var ret = new List<ShowroomReportModel>();
            using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHIndo"].ToString()))
            {
                using (var command = db.CreateCommand("func_bcrm_statisticshowroom", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_fromdate", fromDate.Ticks));
                    command.Parameters.Add(new NpgsqlParameter("_todate", toDate.Ticks));
                    
                    command.Parameters.Add("@_listshowroomid", NpgsqlDbType.Array | NpgsqlDbType.Integer).Value = lstShowroomId;
                    //command.Parameters.Add(new NpgsqlParameter("_lastdateofjoinmonth", firstDayNextMonth.Ticks));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new ShowroomReportModel();
                            if (!reader.HasRows)
                            {
                                return new List<ShowroomReportModel>();
                            }
                            //Hiện giờ chưa có thông tin của loại showroom
                            cust.Id = (int)reader["showroomid"];
                            cust.JoinDate = (DateTime)reader["createdate"];
                            cust.NumOfPosting = (int)reader["totalnews"];
                            cust.NumPostingIn1stMonth = (int)reader["totalnewsin1stmonth"];
                            cust.ShowroomType = (int)reader["type"];
                            ret.Add(cust);
                        }
                    }
                }
            }
            return ret;
        }
    }
}