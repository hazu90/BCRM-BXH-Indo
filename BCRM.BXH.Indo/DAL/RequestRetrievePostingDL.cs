﻿using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class RequestRetrievePostingDL
    {
        public void Create(int customerId,int status, int postingNews,string createdBy,DateTime createdDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("requestretrieveposting_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_postingrequestamount", postingNews));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    command.Parameters.Add(new NpgsqlParameter("_createddate", createdDate));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Update(int id, int postingNews,string updatedBy,DateTime updatedDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("requestretrieveposting_update", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_postingrequestamount", postingNews));
                    command.Parameters.Add(new NpgsqlParameter("_updateddate", updatedDate));
                    command.Parameters.Add(new NpgsqlParameter("_updatedby", updatedBy));
                    command.ExecuteNonQuery();
                }
            }
        }
        public List<RequestRetrievePosting> GetByCustomerId(int customerId)
        {
            var ret = new List<RequestRetrievePosting>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("requestretrieveposting_getbycustomerid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var info = new RequestRetrievePosting();
                            EntityBase.SetObjectValue(reader, ref info);
                            if (info != null)
                            {
                                ret.Add(info);
                            }
                        }
                        return ret;
                    }
                }
            }
        }
        public RequestRetrievePosting GetById(int id)
        {
            var ret = new RequestRetrievePosting();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("requestretrieveposting_getbyid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            return ret;
        }
        public void Delete(int requestId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("requestretrieveposting_delete", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", requestId));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateStatus(int requestId, int status)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("requestretrieveposting_updatestatus", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", requestId ));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}