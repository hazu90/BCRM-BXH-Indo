﻿using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class CustomerLayer
    {
        //public List<CustomerForIndex> Search(int sort, int status, int type, string assignTo, int cityId, int districtId, int startDate,
        //                                        int endDate, string keyword, int pageIndex, int pageSize, out int totalRecord)
        public List<CustomerForIndex> Search(int sort, int type, string assignTo, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, int pageIndex, int pageSize,
                                             string createdBy,string postingAssistant,
                                             int postingNumberFrom, int postingNumberTo, bool? isActive, int activeInMonth, 
                                            out int totalRecord)
        {
            var ret = new List<CustomerForIndex>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                //using (var command = db.CreateCommand("customer_search_gettotalrecord_v02", true))
                using (var command = db.CreateCommand("customer_search_gettotalrecord_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingAssistant));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                    if (isActive == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                    }
                    command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));
                    //command.Parameters.Add(new NpgsqlParameter("_isviewedblacklist", isViewedBlacklist));
                    total = (int)command.ExecuteScalar();
                }
                if(total > 0)
                {
                    //using (var command = db.CreateCommand("customer_search_v02", true))
                    using (var command = db.CreateCommand("customer_search_v03", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                        command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                        command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                        command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                        command.Parameters.Add(new NpgsqlParameter("_type", type));
                        command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                        command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingAssistant));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                        //command.Parameters.Add(new NpgsqlParameter("_isviewedblacklist", isViewedBlacklist));
                        if (isActive == null)
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                        }
                        else
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                        }
                        command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new CustomerForIndex();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }
        public List<CustomerForIndex> Aftersale_Search(int type, string assignTo, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, int pageIndex, int pageSize,
                                             string createdBy, string postingAssistant,
                                             int postingNumberFrom, int postingNumberTo,bool? isActive,int activeInMonth, 
                                             out int totalRecord)
        {
            var ret = new List<CustomerForIndex>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                //using (var command = db.CreateCommand("customer_aftersale_search_gettotalrecord_v02", true))
                using (var command = db.CreateCommand("customer_aftersale_search_gettotalrecord_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingAssistant));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                    if (isActive == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                    }
                    command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));
                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    //using (var command = db.CreateCommand("customer_aftersale_search_v02", true))
                    using (var command = db.CreateCommand("customer_aftersale_search_v03", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                        command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                        command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                        command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                        command.Parameters.Add(new NpgsqlParameter("_type", type));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                        command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingAssistant));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                        if (isActive == null)
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                        }
                        else
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                        }
                        command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new CustomerForIndex();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }
        public List<CustomerForIndex> Aftersale_SearchAll(int type, string assignTo, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, int pageIndex, int pageSize,
                                             string createdBy, string postingAssistant, 
                                            int postingNumberFrom,int postingNumberTo,bool? isActive,int activeInMonth,
                                            out int totalRecord)
        {
            var ret = new List<CustomerForIndex>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                //using (var command = db.CreateCommand("customer_aftersale_searchall_gettotalrecord_v02", true))
                using (var command = db.CreateCommand("customer_aftersale_searchall_gettotalrecord_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingAssistant));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                    if (isActive == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                    }
                    command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));
                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    //using (var command = db.CreateCommand("customer_aftersale_searchall_v02", true))
                    using (var command = db.CreateCommand("customer_aftersale_searchall_v03", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                        command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                        command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                        command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                        command.Parameters.Add(new NpgsqlParameter("_type", type));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                        command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingAssistant));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                        if (isActive == null)
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                        }
                        else
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                        }
                        command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new CustomerForIndex();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }


        public List<CustomerForIndex> Sale_SearchAll(int type, string assignTo, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, int pageIndex, int pageSize,
                                             string createdBy, int postingNumberFrom, int postingNumberTo, bool? isActive, int activeInMonth,
                                            out int totalRecord)
        {
            var ret = new List<CustomerForIndex>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                //using (var command = db.CreateCommand("customer_sale_searchall_gettotalrecord_v02", true))
                using (var command = db.CreateCommand("customer_sale_searchall_gettotalrecord_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                    if (isActive == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                    }
                    command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));
                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    //using (var command = db.CreateCommand("customer_sale_searchall_v02", true))
                    using (var command = db.CreateCommand("customer_sale_searchall_v03", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                        command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                        command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                        command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                        command.Parameters.Add(new NpgsqlParameter("_type", type));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                        if (isActive == null)
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                        }
                        else
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                        }
                        command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new CustomerForIndex();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }
        public List<CustomerForIndex> SearchToExport(int sort, int type, string assignTo, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword,
                                             string createdBy, string postingAssistant)
        {
            var ret = new List<CustomerForIndex>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_searchtoexport_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingAssistant));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new CustomerForIndex();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public List<CustomerForIndex> SearchAll(int sort, int type, string assignTo,List<int> lstGroupId,  int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, int pageIndex, int pageSize,
                                             string createdBy, string postingAssistant, int postingNumberFrom, int postingNumberTo, bool? isActive,int activeInMonth,
                                            out int totalRecord)
        {
            var ret = new List<CustomerForIndex>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                //using (var command = db.CreateCommand("customer_searchall_gettotalrecord_v02", true))
                using (var command = db.CreateCommand("customer_searchall_gettotalrecord_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_lstgroupid", lstGroupId));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingAssistant));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                    if (isActive == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                    }
                    command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));

                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    //using (var command = db.CreateCommand("customer_searchall_v02", true))
                    using (var command = db.CreateCommand("customer_searchall_v03", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                        command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                        command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                        command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                        command.Parameters.Add(new NpgsqlParameter("_lstgroupid", lstGroupId));
                        command.Parameters.Add(new NpgsqlParameter("_type", type));
                        command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                        command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingAssistant));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                        command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                        if(isActive == null)
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", DBNull.Value));
                        }
                        else
                        {
                            command.Parameters.Add(new NpgsqlParameter("_isactive", isActive.Value));
                        }
                        command.Parameters.Add(new NpgsqlParameter("_activeinmonth", activeInMonth));
                        
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new CustomerForIndex();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }

        public List<CustomerForIndex> SearchAllToExport(int sort, int type, string assignTo, List<int> lstGroupId, int cityId, int districtId, DateTime startDate,
                                                DateTime endDate, string keyword, string createdBy, string postingAssistant, int postingNumberFrom, int postingNumberTo)
        {
            var ret = new List<CustomerForIndex>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_searchalltoexport_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", keyword));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_lstgroupid", lstGroupId));
                    command.Parameters.Add(new NpgsqlParameter("_type", type));
                    command.Parameters.Add(new NpgsqlParameter("_sort", sort));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberfrom", postingNumberFrom));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumberto", postingNumberTo));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new CustomerForIndex();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public List<CustomerForIndex> SelectByPhoneNumber(string[] phoneNumber)
        {
            var ret = new List<CustomerForIndex>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_selectphonenumber_v03", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new CustomerForIndex();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }

        public List<Customer> GetByLstCustomerId(List<int> lstCustomerId)
        {
            var ret = new List<Customer>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_getbylstid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_lstcustomerid", lstCustomerId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new Customer();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public int Create(Customer model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_fullname", model.FullName));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", model.PhoneNumber));
                    //command.Parameters.Add(new NpgsqlParameter("_status", model.Status));
                    command.Parameters.Add(new NpgsqlParameter("_postingtype", model.PostingType));
                    command.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                    command.Parameters.Add(new NpgsqlParameter("_description", model.Description));
                    command.Parameters.Add(new NpgsqlParameter("_regionid", model.RegionId));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", model.CityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", model.DistrictId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", model.AssignTo));
                    command.Parameters.Add(new NpgsqlParameter("_caredate", model.CareDate));
                    command.Parameters.Add(new NpgsqlParameter("_icaredate", model.ICareDate));
                    command.Parameters.Add(new NpgsqlParameter("_startcaredate", model.StartCareDate));
                    command.Parameters.Add(new NpgsqlParameter("_companyid", model.CompanyId));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", model.CreatedBy));
                    command.Parameters.Add(new NpgsqlParameter("_createddate", model.CreatedDate));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifiedby", model.LastModifiedBy));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifieddate", model.LastModifiedDate));
                    command.Parameters.Add(new NpgsqlParameter("_address", model.Address));
                    return (int)command.ExecuteScalar();
                }
            }
        }
        public Customer GetById(int customerId)
        {
            var ret = new Customer();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_getbyid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);
                        }
                    }
                }
            }
            return ret;
        }
        public void Update(Customer model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_update_v02", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_fullname", model.FullName));
                    //command.Parameters.Add(new NpgsqlParameter("_status", model.Status));
                    command.Parameters.Add(new NpgsqlParameter("_postingtype", model.PostingType));
                    command.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                    command.Parameters.Add(new NpgsqlParameter("_description", model.Description));
                    command.Parameters.Add(new NpgsqlParameter("_regionid", model.RegionId));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", model.CityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", model.DistrictId));
                    //command.Parameters.Add(new NpgsqlParameter("_assignto", model.AssignTo));
                    //command.Parameters.Add(new NpgsqlParameter("_caredate", model.CareDate));
                    //command.Parameters.Add(new NpgsqlParameter("_icaredate", model.ICareDate));
                    //command.Parameters.Add(new NpgsqlParameter("_startcaredate", model.StartCareDate));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifiedby", model.LastModifiedBy));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifieddate", model.LastModifiedDate));
                    command.Parameters.Add(new NpgsqlParameter("_id", model.Id));
                    command.Parameters.Add(new NpgsqlParameter("_address", model.Address));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", model.PhoneNumber));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void Block(int customerId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updateisdeleted", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_isdeleted", true));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UnBlock(int customerId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updateisdeleted", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_isdeleted", false));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdatePhoneNumber(int customerId, string phoneNumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updatephonenumber", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    command.ExecuteNonQuery();
                }
            }
        }
        public List<ShowroomReportModel> GetShowroomReport(string filterKeyword,string assignTo,int cityId,int districtId)
        {
            var ret = new List<ShowroomReportModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_getshowroomreport", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", filterKeyword));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_cityid", cityId));
                    command.Parameters.Add(new NpgsqlParameter("_districtid", districtId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new ShowroomReportModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public bool IsExistedPhoneNumber(int id, string phoneNumber)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("customer_isexistedphonenumber", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    cmd.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    var iIsExisted = (int)cmd.ExecuteScalar();
                    return (iIsExisted > 0);
                }
            }
        }
        public bool IsExistedEmail(int id, string email)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("customer_isexistedemail", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    cmd.Parameters.Add(new NpgsqlParameter("_email", email));
                    var iIsExisted = (int)cmd.ExecuteScalar();
                    return (iIsExisted > 0);
                }
            }
        }
        public List<NonSaleCustomerSearchModel> Nonsale_GetList(string filterKeyword,
            string receiver,DateTime startDate,DateTime endDate,int contactStatus
            ,DateTime dateNow,int pageIndex,int pageSize, out int totalRecord
            )
        {
            var ret = new List<NonSaleCustomerSearchModel>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_nonsalegetlisttotalrecord", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", filterKeyword));
                    command.Parameters.Add(new NpgsqlParameter("_receiver", receiver));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_contactstatus", contactStatus));
                    command.Parameters.Add(new NpgsqlParameter("_datenow", dateNow));
                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    using (var command = db.CreateCommand("customer_nonsalegetlist", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", filterKeyword));
                        command.Parameters.Add(new NpgsqlParameter("_receiver", receiver));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_contactstatus", contactStatus));
                        command.Parameters.Add(new NpgsqlParameter("_datenow", dateNow));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new NonSaleCustomerSearchModel();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }
        public List<NonSaleCustomerSearchModel> Nonsale_GetAll(string filterKeyword,
            string receiver,List<int> lstGroupId ,DateTime startDate,DateTime endDate,int contactStatus
            ,DateTime dateNow,int pageIndex,int pageSize, out int totalRecord)
        {
            var ret = new List<NonSaleCustomerSearchModel>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_nonsalegetalltotalrecord", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_filterkeyword", filterKeyword));
                    command.Parameters.Add(new NpgsqlParameter("_receiver", receiver));
                    command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    command.Parameters.Add(new NpgsqlParameter("_contactstatus", contactStatus));
                    command.Parameters.Add(new NpgsqlParameter("_datenow", dateNow));
                    command.Parameters.Add(new NpgsqlParameter("_lstgroupid", lstGroupId));
                    total = (int)command.ExecuteScalar();
                }
                if (total > 0)
                {
                    using (var command = db.CreateCommand("customer_nonsalegetall", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_filterkeyword", filterKeyword));
                        command.Parameters.Add(new NpgsqlParameter("_receiver", receiver));
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                        command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                        command.Parameters.Add(new NpgsqlParameter("_contactstatus", contactStatus));
                        command.Parameters.Add(new NpgsqlParameter("_datenow", dateNow));
                        command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                        command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                        command.Parameters.Add(new NpgsqlParameter("_lstgroupid", lstGroupId));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var cust = new NonSaleCustomerSearchModel();
                                EntityBase.SetObjectValue(reader, ref cust);
                                if (cust != null)
                                {
                                    ret.Add(cust);
                                }
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }
        public void UpdateStatus(int customerId,int status)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updatestatus", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateAssignTo(int customerId,string assignTo,DateTime careDate,int iCareDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updateassignto", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_assignto", assignTo));
                    command.Parameters.Add(new NpgsqlParameter("_caredate", careDate));
                    command.Parameters.Add(new NpgsqlParameter("_icaredate", iCareDate));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateLastModifidation(int customerId,string updatedBy,DateTime updateDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updatelastmodify", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifiedby", updatedBy));
                    command.Parameters.Add(new NpgsqlParameter("_lastmodifieddate", updateDate));
                    command.ExecuteNonQuery();
                }
            }
        }
        public CustomerForIndex SearchByCustomerId(int customerId)
        {
            var ret = new List<CustomerForIndex>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_searchbycustomerid_v02", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var cust = new CustomerForIndex();
                            EntityBase.SetObjectValue(reader, ref cust);
                            return cust;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        public void UpdateBlacklist(int id, bool isBlacklist)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updateblacklist", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_isblacklist", isBlacklist));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdatePostingAssistant(int customerId,string postingassistant)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_updatepostingassistant", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_postingassistant", postingassistant));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdatePostingAssistanceStatus(int customerId, int postingAssistanceStatus,int postingNumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_changepostingassistancestatus", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_postingassistancestatus", postingAssistanceStatus));
                    command.Parameters.Add(new NpgsqlParameter("_postingnumber", postingNumber));
                    command.ExecuteNonQuery();
                }
            }
        }

        public int CountRegisteredCustomer(DateTime? startDate, DateTime endDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_countregisteredcustomer", true))
                {
                    if (startDate == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_startdate", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    }
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));

                    return (int)command.ExecuteScalar();
                }
            }
        }

        public List<RegistersInDayModel> GetRegisteredCustomer(DateTime? startDate, DateTime endDate)
        {
            var lstResult = new List<RegistersInDayModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_getregisteredcustomer", true))
                {
                    if (startDate == null)
                        command.Parameters.Add(new NpgsqlParameter("_startdate", DBNull.Value));
                    else
                        command.Parameters.Add(new NpgsqlParameter("_startdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_enddate", endDate));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new RegistersInDayModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                lstResult.Add(cust);
                            }
                        }
                    }
                }
            }
            return lstResult;
        }

        public Customer GetByEmail(string email)
        {
            var ret = new Customer();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customer_getbyemail", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_email", email));
                    using (var reader = command.ExecuteReader())
                    {
                        if(reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);
                            return ret;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
    }
}