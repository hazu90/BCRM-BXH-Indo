﻿using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class PaymentLayer
    {
        public void Create(int customerId,int money,DateTime paymentDate,DateTime createdDate,string createdBy,int createdId,short status)
        {
            using (var db = new PostgresSQL())
            {
                db.BeginTransaction();
                try
                {
                    var id = 0;
                    using (var command = db.CreateCommand("payment_create", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                        command.Parameters.Add(new NpgsqlParameter("_money", money));
                        command.Parameters.Add(new NpgsqlParameter("_paymentdate", paymentDate));
                        command.Parameters.Add(new NpgsqlParameter("_createddate", createdDate));
                        command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                        command.Parameters.Add(new NpgsqlParameter("_createduserid", createdId));
                        command.Parameters.Add(new NpgsqlParameter("_status", status));
                        id = (int) command.ExecuteScalar();
                    }

                    using (var command = db.CreateCommand("paymenthistory_create", true))
                    {
                        var data = new 
                        {
                            CustomerId = customerId,
                            Money = money,
                            PaymentDate  = paymentDate,
                            CreatedDate = createdDate,
                            CreatedBy = createdBy,
                            CreatedUserId = createdId,
                            Status = status
                        };

                        command.Parameters.Add(new NpgsqlParameter("_action", PaymentAction.New.GetHashCode()));
                        command.Parameters.Add(new NpgsqlParameter("_actionby", createdBy));
                        command.Parameters.Add(new NpgsqlParameter("_actiontime", createdDate));
                        command.Parameters.Add(new NpgsqlParameter("_data", JsonConvert.SerializeObject(data)));
                        command.Parameters.Add(new NpgsqlParameter("_paymentid", id));
                        command.ExecuteNonQuery();
                    }

                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                }

                
            }
        }
        public List<PaymentSearchModel> GetList(string filterKeyword, int creatorId, short status, DateTime? createdTimeFrom, DateTime? createdTimeTo,int pageIndex,int pageSize,out int totalRecord)
        {
            var ret = new List<PaymentSearchModel>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("payment_getlist_gettotalrecord", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_creatorid", creatorId));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    if(createdTimeFrom == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_createddatefrom", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_createddatefrom", createdTimeFrom));
                    }
                    if(createdTimeTo == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_createddateto", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_createddateto", createdTimeTo));
                    }
                    total = (int)command.ExecuteScalar();
                }
                using (var command = db.CreateCommand("payment_getlist", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_creatorid", creatorId));
                    command.Parameters.Add(new NpgsqlParameter("_status", status));
                    if (createdTimeFrom == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_createddatefrom", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_createddatefrom", createdTimeFrom));
                    }
                    if (createdTimeTo == null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("_createddateto", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add(new NpgsqlParameter("_createddateto", createdTimeTo));
                    }
                    command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                    command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));
                    
                    using(var reader = command.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            var payment = new PaymentSearchModel();
                            EntityBase.SetObjectValue(reader, ref payment);
                            if(payment != null)
                            {
                                ret.Add(payment);
                            }
                        }
                    }

                }
            }

            totalRecord = total;
            return ret;
        }
        public List<PaymentSearchModel> GetListByCustomerId(int customerId,int pageIndex,int pageSize,out int totalRecord)
        {
            var ret = new List<PaymentSearchModel>();
            var total = 0;
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("payment_getlistcusid_gettotalrecord", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    total = (int)command.ExecuteScalar();
                }
                using (var command = db.CreateCommand("payment_getlistcusid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_pageindex", pageIndex));
                    command.Parameters.Add(new NpgsqlParameter("_pagesize", pageSize));

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var payment = new PaymentSearchModel();
                            EntityBase.SetObjectValue(reader, ref payment);
                            if (payment != null)
                            {
                                ret.Add(payment);
                            }
                        }
                    }
                }
            }
            totalRecord = total;
            return ret;
        }
        public PaymentSearchModel GetViewByPaymentId(int id)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("payment_getviewbypaymentid", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_paymentid", id));
                    var item = new PaymentSearchModel();
                    //Nếu trả về 1 table thì phải dùng reader để đọc
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            //cách lấy 2, dùng class EntityBase
                            EntityBase.SetObjectValue(reader, ref item);
                            return item;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        public void Approved(int id, int customerId, int userId,int money, DateTime verifiedDate,int adminTransactionId,DateTime approvedTime,string approvedBy)
        {
            using (var db = new PostgresSQL())
            {
                db.BeginTransaction();
                try
                {
                    var revenueId = 0;
                    using (var command = db.CreateCommand("revenue_create", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                        command.Parameters.Add(new NpgsqlParameter("_receiveduserid", userId));
                        command.Parameters.Add(new NpgsqlParameter("_money", money));
                        command.Parameters.Add(new NpgsqlParameter("_verifieddate", verifiedDate));
                        command.Parameters.Add(new NpgsqlParameter("_adminTransactionid", adminTransactionId));
                        revenueId= (int)command.ExecuteScalar();
                    }
                    using (var command = db.CreateCommand("revenuehistory_create", true))
                    {
                        var data = new 
                        {
                            CustomerId = customerId,
                            ReceivedUserId = userId,
                            Money = money,
                            VerifiedDate = verifiedDate,
                            AdminTransactionId = adminTransactionId
                        };
                        command.Parameters.Add(new NpgsqlParameter("_action", RevenueAction.ApprovingPaymentAndNew.GetHashCode()));
                        command.Parameters.Add(new NpgsqlParameter("_actionby", approvedBy));
                        command.Parameters.Add(new NpgsqlParameter("_actiontime", approvedTime));
                        command.Parameters.Add(new NpgsqlParameter("_data", JsonConvert.SerializeObject(data)));
                        command.Parameters.Add(new NpgsqlParameter("_revenueid", revenueId));
                        command.ExecuteNonQuery();
                    }

                    using (var command = db.CreateCommand("payment_approved", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_id", id));
                        command.Parameters.Add(new NpgsqlParameter("_revenueid", revenueId));
                        command.Parameters.Add(new NpgsqlParameter("_verifiedpaymentdate", verifiedDate));
                        command.Parameters.Add(new NpgsqlParameter("_changestatustime", approvedTime));
                        command.Parameters.Add(new NpgsqlParameter("_changestatusby", approvedBy));
                        command.ExecuteNonQuery();
                    }

                    using (var command = db.CreateCommand("paymenthistory_create", true))
                    {
                        var data = new 
                        {
                            Id = id,
                            RevenueId = revenueId,
                            VerifiedPaymentDate = verifiedDate,
                            ChangeStatusTime = approvedTime,
                            ChangeStatusBy = approvedBy
                        };

                        command.Parameters.Add(new NpgsqlParameter("_action", PaymentAction.Approved.GetHashCode()));
                        command.Parameters.Add(new NpgsqlParameter("_actionby", approvedBy));
                        command.Parameters.Add(new NpgsqlParameter("_actiontime", verifiedDate));
                        command.Parameters.Add(new NpgsqlParameter("_data", JsonConvert.SerializeObject(data)));
                        command.Parameters.Add(new NpgsqlParameter("_paymentid", id));
                        command.ExecuteNonQuery();
                    }
                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                }
            }
        }
        public void Reject(int id,string feedBack,short status,DateTime rejectTime,string rejectBy)
        {
            using (var db = new PostgresSQL())
            {
                db.BeginTransaction();
                try
                {
                    using (var command = db.CreateCommand("payment_reject", true))
                    {
                        command.Parameters.Add(new NpgsqlParameter("_id", id));
                        command.Parameters.Add(new NpgsqlParameter("_feedback", feedBack));
                        command.Parameters.Add(new NpgsqlParameter("_status", status));
                        command.Parameters.Add(new NpgsqlParameter("_changestatustime", rejectTime));
                        command.Parameters.Add(new NpgsqlParameter("_changestatusby", rejectBy));
                        command.ExecuteNonQuery();
                    }

                    using (var command = db.CreateCommand("paymenthistory_create", true))
                    {
                        var data = new 
                        {
                            Id = id,
                            Status = status,
                            Feedback = feedBack,
                            ChangeStatusTime = rejectTime,
                            ChangeStatusBy = rejectBy
                        };

                        command.Parameters.Add(new NpgsqlParameter("_action", PaymentAction.Reject.GetHashCode()));
                        command.Parameters.Add(new NpgsqlParameter("_actionby", rejectBy));
                        command.Parameters.Add(new NpgsqlParameter("_actiontime", rejectTime));
                        command.Parameters.Add(new NpgsqlParameter("_data", JsonConvert.SerializeObject(data)));
                        command.Parameters.Add(new NpgsqlParameter("_paymentid", id));
                        command.ExecuteNonQuery();
                    }
                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                }
            }
        }
        public Payment GetById(int id)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("payment_getbyid", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_id", id));
                    var item = new Payment();
                    //Nếu trả về 1 table thì phải dùng reader để đọc
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            //cách lấy 2, dùng class EntityBase
                            EntityBase.SetObjectValue(reader, ref item);
                            return item;
                        }
                        else
                        {
                            return null;
                        }

                        
                    }

                }
            }
        }
    }
}