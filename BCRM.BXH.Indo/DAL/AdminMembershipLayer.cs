﻿using BCRM.BXH.Indo.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class AdminMembershipLayer
    {
        public int Create(string displayName,string mobileAlias,string email,string address,string userName, string password,string hashcode,string activatedCode)
        {
            using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHIndo"].ToString()))
            {
                using (var command = db.CreateCommand("func_bcrm_showroom_add", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_displayname", displayName));
                    command.Parameters.Add(new NpgsqlParameter("_mobilealias", mobileAlias));
                    command.Parameters.Add(new NpgsqlParameter("_email", email));
                    command.Parameters.Add(new NpgsqlParameter("_address", address));
                    command.Parameters.Add(new NpgsqlParameter("_username", userName));
                    command.Parameters.Add(new NpgsqlParameter("_password", password));
                    command.Parameters.Add(new NpgsqlParameter("_hashcode", hashcode));
                    command.Parameters.Add(new NpgsqlParameter("_activatecode", activatedCode));
                    command.Parameters.Add(new NpgsqlParameter("_createdate", DateTime.Now));
                    command.Parameters.Add(new NpgsqlParameter("_createdatespan", DateTime.Now.Ticks));
                    return (int)command.ExecuteScalar();
                }
            }
        }
        public int Update(int id, string displayName,string mobileAlias,string email,string address)
        {
            using (var db = new PostgresSQL(ConfigurationManager.ConnectionStrings["AdminBXHIndo"].ToString()))
            {
                using (var command = db.CreateCommand("func_bcrm_showroom_update", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_displayname", displayName ));
                    command.Parameters.Add(new NpgsqlParameter("_mobilealias",mobileAlias ));
                    command.Parameters.Add(new NpgsqlParameter("_email", email));
                    command.Parameters.Add(new NpgsqlParameter("_address",address));
                    return (int)command.ExecuteScalar();
                }
            }
        }
    }
}