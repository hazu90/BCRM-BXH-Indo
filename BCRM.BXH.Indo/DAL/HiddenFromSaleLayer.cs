﻿using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class HiddenFromSaleLayer
    {
        public void Create(int adminId, short reasonType, string note
                        , string createdBy,DateTime createdDate,long phoneNumber )
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("hiddenfromsale_create", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_reasontype", reasonType));
                    cmd.Parameters.Add(new NpgsqlParameter("_note", note));
                    cmd.Parameters.Add(new NpgsqlParameter("_adminid", adminId));
                    cmd.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    cmd.Parameters.Add(new NpgsqlParameter("_createddate", createdDate));
                    cmd.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    cmd.ExecuteNonQuery();
                }
            }
            
        }

        public HiddenFromSale GetByAdminAndPhonenumber(int adminId, long phoneNumber)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("hiddenfromsale_getbyadminandphonenumber", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_adminid", adminId));
                    cmd.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var ret = new HiddenFromSale();
                            EntityBase.SetObjectValue(reader, ref ret);
                            return ret;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public List<HiddenFromSale> GetByPhoneNumber(long phoneNumber)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("hiddenfromsale_getbyphonenumber", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<HiddenFromSale>();
                        while (reader.Read())
                        {
                            var syncModel = new HiddenFromSale();
                            if (!reader.HasRows)
                            {
                                return new List<HiddenFromSale>();
                            }

                            EntityBase.SetObjectValue(reader, ref syncModel);
                            ret.Add(syncModel);
                        }
                        return ret;
                    }
                }
            }
        }

        public List<HiddenFromSale> GetByAdminId(int adminId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("hiddenfromsale_getbyadminid", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_adminid", adminId));
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<HiddenFromSale>();
                        while (reader.Read())
                        {
                            var syncModel = new HiddenFromSale();
                            if (!reader.HasRows)
                            {
                                return new List<HiddenFromSale>();
                            }

                            EntityBase.SetObjectValue(reader, ref syncModel);
                            ret.Add(syncModel);
                        }
                        return ret;
                    }
                }
            }
        }

        public void Delete(int adminId)
        {
            using (var context = new PostgresSQL())
            {
                using (var cmd = context.CreateCommand("hiddenfromsale_deletebyadminid", true))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("_adminid", adminId));
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}