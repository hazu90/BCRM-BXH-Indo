﻿using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class UserOTPAuthenticatorLayer
    {
        public void Create(UserOTPAuthenticator model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("userotpauthenticator_create", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_userid", model.UserId));
                    command.Parameters.Add(new NpgsqlParameter("_username", model.UserName));
                    command.Parameters.Add(new NpgsqlParameter("_source", model.Source));
                    command.Parameters.Add(new NpgsqlParameter("_link", model.Link));
                    command.Parameters.Add(new NpgsqlParameter("_expiredtime", model.ExpiredTime));

                    command.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int userId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("userotpauthenticator_delete", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_userid", userId));
                    command.ExecuteNonQuery();
                }
            }
        }

        public UserOTPAuthenticator GetByUserId(int userId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("userotpauthenticator_getbyuserid", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_userid", userId));
                    using (var reader = command.ExecuteReader())
                    {
                        if(reader.Read())
                        {
                            var ret = new UserOTPAuthenticator();
                            EntityBase.SetObjectValue(reader, ref ret);
                            return ret;
                        }
                        else
                        { return null; }
                    }
                }
            }
        }

    }
}