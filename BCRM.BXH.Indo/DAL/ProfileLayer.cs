﻿using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class ProfileLayer
    {
        public void Create(Profile model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("profile_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", model.PhoneNumber));
                    command.Parameters.Add(new NpgsqlParameter("_phonealias", model.PhoneAlias));
                    command.Parameters.Add(new NpgsqlParameter("_name", model.Name));
                    command.Parameters.Add(new NpgsqlParameter("_positiontype", model.PositionType));
                    command.Parameters.Add(new NpgsqlParameter("_customerid", model.CustomerId));
                    command.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                    command.Parameters.Add(new NpgsqlParameter("_avatar", model.Avatar));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", model.CreatedBy));
                    command.Parameters.Add(new NpgsqlParameter("_createddate", model.CreatedDate));
                    command.ExecuteNonQuery();
                }
            }
        }
        public void Update(Profile model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("profile_update", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", model.PhoneNumber));
                    command.Parameters.Add(new NpgsqlParameter("_phonealias", model.PhoneAlias));
                    command.Parameters.Add(new NpgsqlParameter("_name", model.Name));
                    command.Parameters.Add(new NpgsqlParameter("_positiontype", model.PositionType));
                    command.Parameters.Add(new NpgsqlParameter("_customerid", model.CustomerId));
                    command.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                    command.Parameters.Add(new NpgsqlParameter("_avatar", model.Avatar));
                    command.Parameters.Add(new NpgsqlParameter("_id", model.Id));
                    command.ExecuteNonQuery();
                }
            }
        }
        public Profile GetByPhoneNumber(string phoneNumber)
        {
            var ret = new Profile();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("profile_getbyphonenumber", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);                            
                        }
                    }
                }
            }
            return ret;
        }
        public List<Profile> GetByCustomer(int customerId)
        {
            var ret = new List<Profile>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("profile_getbycustomerid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var profile = new Profile();
                            EntityBase.SetObjectValue(reader, ref profile);
                            if(profile != null)
                            {
                                ret.Add(profile);
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public void Delete(int id)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("profile_delete", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}