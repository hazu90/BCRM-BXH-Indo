﻿using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class PostingAssistanceHistoryLayer
    {
        public void Create(int userId,DateTime receivedDate,int customerId
                         ,string createdBy,DateTime createdDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("postingassistancehistory_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_userid", userId));
                    command.Parameters.Add(new NpgsqlParameter("_receiveddate", receivedDate));
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", createdBy));
                    command.Parameters.Add(new NpgsqlParameter("_createddate", createdDate));
                    command.ExecuteNonQuery();
                }
            }
        }

        public List<PostingAssistanceHistory> GetByTime(DateTime startDate,DateTime endDate)
        {
            var ret = new List<PostingAssistanceHistory>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("postingassistancehistory_getbytime", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_fromtime", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_totime", endDate));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new PostingAssistanceHistory();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                        return ret;
                    }
                }
            }
        }
        public List<PostingAssistanceHistory> GetByCustomerId(int customerId)
        {
            var ret = new List<PostingAssistanceHistory>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("postingassistancehistory_getbycustomerid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var resultInfo = new PostingAssistanceHistory();
                            EntityBase.SetObjectValue(reader, ref resultInfo);
                            if (resultInfo != null)
                            {
                                ret.Add(resultInfo);
                            }
                        }
                        return ret;
                    }
                }
            }
        }

        public void UpdateReturnDate(int id, DateTime returnDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("postingassistancehistory_updatereturndate", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.Parameters.Add(new NpgsqlParameter("_returndate", returnDate));
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}