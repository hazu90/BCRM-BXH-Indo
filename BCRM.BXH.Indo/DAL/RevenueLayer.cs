﻿using BCRM.BXH.Indo.Library;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.BXH.Indo.DAL
{
    public class RevenueLayer
    {
        public int Create(int customerId,int receivedUserId,int money,DateTime verifiedDate)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("revenue_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    command.Parameters.Add(new NpgsqlParameter("_receiveduserid", receivedUserId));
                    command.Parameters.Add(new NpgsqlParameter("_money", money));
                    command.Parameters.Add(new NpgsqlParameter("_verifieddate", verifiedDate));
                    return (int) command.ExecuteScalar();
                }
            }
        }
    }
}