﻿using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DVS.Algorithm;
using BCRM.BXH.Indo.Library;
using System.Web.Security;
using Newtonsoft.Json;
using BCRM.BXH.Indo.Cache;
using System.Configuration;

namespace BCRM.BXH.Indo.Controllers
{
    public class AccountController : BaseController
    {
        //
        // GET: /Account/

        public ActionResult LogOn()
        {
            return View(new LogonViewModel());
        }

        [HttpPost]
        public ActionResult LogOn(string username, string password,string otpprivateKey, bool remember = false)
        {
            var userLayer = new UserLayer();
            var user = userLayer.GetByUserName(username);
            if (user == null || !password.Trim().ToMD5().Equals(user.Password) || user.Status != UserStatus.Avaiable.GetHashCode())
            {
                var response = new LogonViewModel
                {
                    UserName = username,
                    Password = password,
                    Status = false
                };
                return View(response);
            }
            #region Check OTP user
            //var ignoreOTPPrivateKey = ConfigurationManager.AppSettings["IgnoreOTPPrivateKey"].ToString();
            //var lstIgnore = new List<string>();
            //if (!string.IsNullOrEmpty(ignoreOTPPrivateKey))
            //{
            //    lstIgnore = ignoreOTPPrivateKey.Split(',').ToList();
            //}
            //if (!lstIgnore.Contains(user.UserName))
            //{
            //    var secretKey = ConfigurationManager.AppSettings["OTPSecretKey"] + user.OTPPrivateKey;
            //    if (!GoogleTOTP.IsVaLid(secretKey, otpprivateKey))
            //    {
            //        var response = new LogonViewModel
            //        {
            //            UserName = username,
            //            Password = password,
            //            Status = false
            //        };
            //        return View(response);
            //    }
            //}
            
            #endregion

            var cacheUser = new IISUser();
            cacheUser.RemoveUserInfo(user.UserName);
            var authentication = cacheUser.SetUserInfo(user);
            FormsAuthentication.SetAuthCookie(JsonConvert.SerializeObject(authentication, Formatting.None), remember);
            userLayer.UpdateLastActivity(user.Id);
            return RedirectToAction("Index", "Home");
        }
        public RedirectResult LogOff()
        {
            FormsAuthentication.SignOut();
            return Redirect(Url.Action("LogOn", "Account"));
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [FilterAuthorize]
        public JsonResult ChangePasswordUser(ChangePasswordModel model)
        {
            var response = new Response();

            var user = UserContext;
            var userLayer = new UserLayer();
            try
            {
                var password = userLayer.GetByUserName(user.UserName).Password;
                if (password == null)
                {
                    response.Code = SystemCode.Error;
                    response.Message = "Old password is not correct!";
                }
                user.Password = password;
                if (user.Password == model.OldPassword.ToMD5())
                {
                    if (model.NewPassword.Equals(model.ConfirmPassword))
                    {
                        user.Password = model.NewPassword.ToMD5();

                        var rpm = new ResetPasswordModel { Id = user.Id, NewPassword = model.NewPassword.ToMD5() };
                        userLayer.ResetPassword(rpm);
                        response.Code = SystemCode.Success;
                    }
                }
                else
                {
                    response.Code = SystemCode.Error;
                    response.Message = "Old password is not correct!";
                }

            }
            catch (Exception exception)
            {
                response.Code = SystemCode.Error;
                response.Message = exception.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
