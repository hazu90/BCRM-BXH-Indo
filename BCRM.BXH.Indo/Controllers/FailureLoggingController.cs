﻿using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class FailureLoggingController : BaseController
    {
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin)]
        public JsonResult CheckLogging(int fromDate,int toDate)
        {
            var yyyy = fromDate / 10000;
            var mm = (fromDate - yyyy * 10000) / 100;
            var dd = fromDate - yyyy * 10000 - mm * 100;
            var fromDateInTime = new DateTime(yyyy, mm, dd, 0, 0, 0);
            yyyy = toDate / 10000;
            mm = (toDate - yyyy * 10000) / 100;
            dd = toDate - yyyy * 10000 - mm * 100;
            var toDateInTime = (new DateTime(yyyy, mm, dd, 0, 0, 0)).AddDays(1);
            var failureLoggingLayer = new FailureLoggingLayer();
            var lstFailure = failureLoggingLayer.GetListInDate(fromDateInTime, toDateInTime);

            return Json(new Response() 
            {
                Code = SystemCode.Success,
                Data = lstFailure
            },JsonRequestBehavior.AllowGet);
        }
    }
}
