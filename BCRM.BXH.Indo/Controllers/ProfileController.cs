﻿using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Language;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using DVS.Algorithm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class ProfileController : BaseController
    {
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult GetListByCustomer(int customerId)
        {
            var profileLayer = new ProfileLayer();
            var lstProfile = profileLayer.GetByCustomer(customerId);
            ViewBag.CustomerId = customerId;
            ViewBag.TokenCheckSum = ChecksumHelper.GenToken(new { Id = customerId, CurrUser = UserContext.UserName });
            return View(lstProfile);
        }

        [FilterAuthorize]
        [JsonModelBinder(ActionParameterType = typeof(ProfileSave), ActionParameterName = "model")]
        public JsonResult Save(ProfileSave model)
        {
            var response = new Response();
            try
            {
                // Xác thực lại giá trị của dữ liệu truyền lên
                var validateToken = ChecksumHelper.ValidateToken(new { Id =model.CustomerId, CurrUser = UserContext.UserName }, model.TokenChecksum);
                if (!validateToken.IsValid)
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.NotPermitted,
                        Message = LanguageRes.Msg_NotPermitted
                    }, JsonRequestBehavior.DenyGet);
                }

                var customerLayer = new CustomerLayer();
                var profileLayer = new ProfileLayer();
                //Check tính hợp lệ của dữ liệu
                if (model.ListProfile.Count == 0)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "Invalid Phone Number. Please check again!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                var lstError = new List<string>();
                foreach (var phone in model.ListProfile)
                {
                    var pro = profileLayer.GetByPhoneNumber(phone.PhoneNumber);
                    if (pro.CustomerId > 0 && pro.CustomerId != model.CustomerId)
                    {
                        lstError.Add(pro.PhoneNumber);                        
                    }
                }
                if(lstError.Count > 0)
                {
                    response.Code = SystemCode.NotValid;
                    response.Message = "Phonenumber " + string.Join("; ", lstError) + " exist in other file customer . Please check again!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                var customer = customerLayer.GetById(model.CustomerId);
                if(customer == null)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "File Customer not exits!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                #region Kiểm tra xem số điện thoại khai báo có nằm trong tập danh sách khách hàng crawler hay không
                //var apiCheckExistPhoneNumberResponse = CallAPICheckExistPhoneNumberInCrawlerCustomer(model.ListProfile[0].PhoneNumber);
                //if (apiCheckExistPhoneNumberResponse.Code != SystemCode.Success)
                //{
                //    return Json(apiCheckExistPhoneNumberResponse, JsonRequestBehavior.DenyGet);
                //}
                //var resModel = JsonConvert.DeserializeObject<CrawlerCustomerIndexModel>((string)apiCheckExistPhoneNumberResponse.Data);
                //if (resModel.ListData.Count > 0)
                //{
                //    return Json(new Response()
                //    {
                //        Code = SystemCode.NotPermitted,
                //        Message = LanguageRes.Msg_Existed_Crawlerlist
                //    }, JsonRequestBehavior.DenyGet);
                //}
                #endregion

                //Xóa các thông tin profile cũ đã bị xóa
                var lstProfile = profileLayer.GetByCustomer(customer.Id);
                var lstProfileId = model.ListProfile.Select(o => o.Id).Distinct().ToList();
                var lstExcept = lstProfile.FindAll(o => !lstProfileId.Contains(o.Id));
                foreach (var item in lstExcept)
                {
                    profileLayer.Delete(item.Id);
                }

                foreach (var profile in model.ListProfile)
                {
                    var pro = profileLayer.GetByPhoneNumber(profile.PhoneNumber);                    
                    profile.CustomerId = model.CustomerId;
                    profile.CreatedBy = UserContext.UserName;
                    profile.CreatedDate = DateTime.Now;
                    profile.Email = string.IsNullOrEmpty(profile.Email) ? " " : profile.Email;
                    profile.Avatar = string.IsNullOrEmpty(profile.Avatar) ? " " : profile.Avatar;
                    if (profile.Id > 0)
                    {
                        profileLayer.Update(profile);
                    }
                    else
                    {
                        profileLayer.Create(profile);
                    }                    
                }
                // Trường hợp cập nhật số điện thoại của file khách hàng là showroom =>
                var syncBcrmBxhNAdminLayer = new SyncBcrmBxhNAdminLayer();
                var adminBXHMembershipLayer = new AdminMembershipLayer();
                // Cập nhật lại dữ liệu bên Admin BXH
                var strPhone = model.ListProfile[0].PhoneNumber;

                var syncModel = syncBcrmBxhNAdminLayer.GetByCustomerId(customer.Id);
                if (syncModel != null)
                {
                    if (string.IsNullOrEmpty(customer.Address))
                    {
                        customer.Address = "";
                    }
                    var isChangedPhone = false;
                    if(!syncModel.PhoneNumber.Equals(strPhone))
                    {
                        isChangedPhone = true;
                    }
                    // Chỉ khi có sự thay đổi số điện thoại thì mới cập nhật lại với bên admin
                    if(isChangedPhone)
                    {
                        var apiResponse = CallAPIAdminInsertUpdateAccount(syncModel.AccountId, customer.FullName, customer.Email, strPhone, customer.Address, isChangedPhone);
                        if (apiResponse.Code != SystemCode.Success)
                        {
                            return Json(apiResponse, JsonRequestBehavior.DenyGet);
                        }
                        // Cập nhật lại bảng syncbcrmBXHNAdmin khi thay đổi thông tin email
                        if (isChangedPhone)
                        {
                            syncBcrmBxhNAdminLayer.Update(customer.Id, customer.Email, strPhone);
                        }
                    }
                }

                customerLayer.UpdatePhoneNumber(model.CustomerId, strPhone);
                // Kiểm tra có thay đổi số điện thoại hay không
                if (!customer.PhoneNumber.Equals(strPhone))
                {
                    var customerHistoryLayer = new CustomerHistoryLayer();
                    customerHistoryLayer.Create(new CustomerHistory()
                    {
                        Action = "Phone number",
                        CustomerId = model.CustomerId,
                        OldValue = customer.PhoneNumber,
                        NewValue = strPhone,
                        UpdatedBy = UserContext.UserName,
                        UpdatedDate = DateTime.Now,
                        Note = ""
                    });
                }


                response.Message = "Successfully!";
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        [FilterAuthorize]
        [JsonModelBinder(ActionParameterType = typeof(ProfileSave), ActionParameterName = "model")]
        public JsonResult CheckPhoneNumber(ProfileSave model)
        {
            var response = new Response();
            try
            {
                var profileLayer = new ProfileLayer();
                var strError = new List<string>();
                //Check tính hợp lệ của dữ liệu
                foreach(var profile in model.ListProfile)
                {
                    var pro = profileLayer.GetByPhoneNumber(profile.PhoneNumber);
                    if (pro.CustomerId > 0 && pro.CustomerId != model.CustomerId)
                    {
                        strError.Add(pro.PhoneNumber);
                    }
                }

                // Kiểm tra xem có số điện thoại nào bị ẩn hay không nếu có
                // thì check xem lí do có phải là tài khoản này
                // đã nằm trong 
                var hiddenFromSaleLayer = new HiddenFromSaleLayer();
                var lstHidden = hiddenFromSaleLayer.GetByPhoneNumber(model.ListProfile[0].PhoneNumber.ToLong(0));
                if (lstHidden.Count > 0)
                {
                    if (lstHidden.Find(o => o.ReasonType == HideFromLeadReason.PhoneOwnerNotWorkingThatShowroom.GetHashCode()) == null)
                    {
                        return Json(new Response()
                        {
                            Code = SystemCode.NotValid,
                            Message = "Phonenumber is hidden by another sale in leads . Please contact manager to support more !"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    #region Kiểm tra xem số điện thoại khai báo có nằm trong tập danh sách khách hàng crawler hay không
                    var apiCheckExistPhoneNumberResponse = CallAPICheckExistPhoneNumberInCrawlerCustomer(model.ListProfile[0].PhoneNumber);
                    if (apiCheckExistPhoneNumberResponse.Code != SystemCode.Success)
                    {
                        return Json(apiCheckExistPhoneNumberResponse, JsonRequestBehavior.DenyGet);
                    }
                    var resModel = JsonConvert.DeserializeObject<CrawlerCustomerIndexModel>((string)apiCheckExistPhoneNumberResponse.Data);
                    if (resModel.ListData.Count > 0)
                    {
                        return Json(new Response()
                        {
                            Code = SystemCode.NotPermitted,
                            Message = LanguageRes.Msg_Existed_Crawlerlist
                        }, JsonRequestBehavior.DenyGet);
                    }
                    #endregion
                }
                if(strError.Count > 0)
                {
                    response.Message =   string.Join("; ", strError);
                    response.Code = SystemCode.ErrorExist;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }                
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public Response CallAPIAdminInsertUpdateAccount(long adminId, string fullName, string email, string phoneNumber, string address, bool isChangePhone)
        {
            // Gọi api thêm mới thông tin tài khoản khách hàng trên site
            var url = string.Format("{0}/BCRMService.svc/UpdateCustomerInfo", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var token = Security.CreateKey();

            var data = new
            {
                Token = Security.CreateKey(),
                AdminId = adminId,
                FullName = fullName,
                Email = string.IsNullOrEmpty(email) ? "" : email.ToLower().Trim(),
                PhoneNumber = phoneNumber,
                Address = address,
                IsChangePhoneNumber = isChangePhone
            };
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                return new Response()
                {
                    Code = SystemCode.ErrorConnect,
                    Message = "Error in process , Contact IT for detail !"
                };
            }
            var hrmResponse = JsonConvert.DeserializeObject<Response>(res);
            return hrmResponse;
        }
        [NonAction]
        public Response CallAPICheckExistPhoneNumberInCrawlerCustomer(string phoneNumber)
        {
            string nullString = null;
            // Kiểm tra thông tin số điện thoại có tồn tại trong các khoản crawler hay không
            var url = string.Format("{0}/BCRMService.svc/GetListCrawlerCustomer", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = JsonConvert.SerializeObject(new
            {
                Token = Security.CreateKey(),
                FilterKeyword = phoneNumber,
                Status = -1,
                PageIndex = 1,
                PageSize = 25,
                DateOfCreationFrom = nullString,
                DateOfCreationTo = nullString,
                TotalRecord = 0,
                ListData = new List<CrawlerCustomerSearchModel>(),
                ListIgnoreAdminId = new List<long>(),
                ListFilterAdminId = new List<long>()
            });
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, data, "application/json");
            var result = JsonConvert.DeserializeObject<Response>(res);
            return result;
        }
    }
}
