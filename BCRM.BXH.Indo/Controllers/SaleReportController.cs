﻿using BCRM.BXH.Indo.Common;
using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class SaleReportController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            // Lấy ra danh sách mà nhân viên có quyền 
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            var groupLayer = new GroupLayer();
            var isSearchAll = false;
            var lstGroup = new List<Group>();
            var lstUsers = new List<UserSearchModel>();
            var lstSaler = new List<UserSearchModel>();
            if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                isSearchAll = true;
                var allGroups = groupLayer.GetAll();
                lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                var userLayer = new UserLayer();
                lstUsers = userLayer.GetList(groupIds);
                lstUsers = lstUsers.FindAll(o => !o.IsHiddenFromReport);
                var userRoleGroupLayer = new UserRoleGroupLayer();
                foreach (var item in lstUsers)
                {
                    // Lấy role của người dùng
                    var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                    if (roles.Find(o => o.RoleId == RoleSystem.Admin.GetHashCode()
                                   || o.RoleId == RoleSystem.Manager.GetHashCode()
                                   || o.RoleId == RoleSystem.Sale.GetHashCode()) != null)
                    {
                        lstSaler.Add(item);
                    }
                }
            }
            else
            {
                var groupInfo = groupLayer.GetById(UserContext.GroupId);
                lstGroup = new List<Group>() { groupInfo };
                lstSaler = new List<UserSearchModel>() { new UserSearchModel() { UserName = UserContext.UserName } };
            }
            var model = new SaleReportIndexModel();
            if (DateTime.Now.Day <= 6)
            {
                var beforeMonth = DateTime.Now.AddMonths(-1);
                model.StartDate = new DateTime(beforeMonth.Year, beforeMonth.Month, 1);
                model.EndDate = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddDays(-1);
            }
            else
            {
                var afterMonth = DateTime.Now.AddMonths(1);
                model.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                model.EndDate = (new DateTime(afterMonth.Year, afterMonth.Month, 1)).AddDays(-1);
            }
            model.SetListGroup(isSearchAll, lstGroup);
            model.SetListUsers(isSearchAll, lstSaler);
            return View(model);
        }
        [FilterAuthorize]
        public ActionResult Search(SaleReportIndexModel model)
        {
            ViewBag.TotalLead = 0;
            ViewBag.TotalConvertedAccount = 0;
            ViewBag.TotalAddedAccount = 0;
            ViewBag.TotalAccounts = 0;
            ViewBag.TotalActivedAccount = 0;
            ViewBag.TotalListing = 0;

            //Logger.GetInstance().Write("Test");


            var isManager = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager);

            if (!isManager && (string.IsNullOrEmpty(model.AssignTo) || !model.AssignTo.Equals(UserContext.UserName)))
            {
                return View(new List<SaleReportViewModel>());
            }
            var userLayer = new UserLayer();
            var lstUserResult = new List<Users>();
            var groupLayer = new GroupLayer();
            if (!string.IsNullOrEmpty(model.AssignTo))
            {
                var userInfo = userLayer.GetByUserName(model.AssignTo);
                if (userInfo == null)
                {
                    return View(new List<SaleReportViewModel>());
                }
                lstUserResult.Add(userInfo);
            }
            else
            {
                // Lấy ra danh sách mà nhân viên có quyền 
                var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
                if (model.GroupId == 0)
                {
                    var allGroups = groupLayer.GetAll();
                    var lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                    ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                    var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                    lstUserResult.AddRange(userLayer.GetList(groupIds).FindAll(o=>!o.IsHiddenFromReport) .Select(o=>new Users() {UserName =o.UserName,Id = o.Id  }));
                }
                else
                {
                    if (!lstRoledGroup.Contains(DefaultSystemParameter.AllGroup) && !lstRoledGroup.Contains(model.GroupId))
                    {
                        return View(new List<SaleReportViewModel>());
                    }
                    lstUserResult = userLayer.GetByGroupId(model.GroupId).FindAll(o => !o.IsHiddenFromReport);
                }
            }

            var userRoleGroupLayer = new UserRoleGroupLayer();
            var lstViewedSaler = new List<Users>();
            foreach (var item in lstUserResult)
            {
                // Lấy role của người dùng
                var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                if (roles.Find(o => o.RoleId == RoleSystem.Admin.GetHashCode()
                               || o.RoleId   == RoleSystem.Manager.GetHashCode()
                               || o.RoleId   == RoleSystem.Sale.GetHashCode()) != null)
                {
                    lstViewedSaler.Add(item);
                }
            }
            model.EndDate = model.EndDate.AddDays(1);

            // Lấy ra tất cả các tài khoản được lập trong khoảng thời gian này
            var customerCareHistoryLayer = new CustomerCareHistoryLayer();
            var lstAccountCreation = customerCareHistoryLayer.GetSuccessContactInTime(lstViewedSaler.Select(o => o.Id).ToList(), model.StartDate, model.EndDate);
            var result = new List<SaleReportViewModel>();
            // Lấy ra tất cả các tài khoản đã được chăm sóc bởi các sale trong khoảng thời gian này
            var lstAccountCare = customerCareHistoryLayer.GetCaredCustomerInTime(lstViewedSaler.Select(o => o.Id).ToList(), model.StartDate, model.EndDate);
            // Lấy ra tất cả các tài khoản crawler đã được nhận trong khoảng thời gian này
            var lstReceivedCrawler = customerCareHistoryLayer.GetReceivedCrawlerInTime(lstViewedSaler.Select(o => o.Id).ToList(), model.StartDate, model.EndDate);

            var totalLead = 0;
            var totalCovertedAccount = 0;
            var totalAddedAccount = 0;
            var totalAccount = 0;
            var totalActiveAccount = 0;
            var totalListing = 0;
            // Lấy ra các id  customer
            var lstCusId = new List<int>();
            lstCusId.AddRange(lstAccountCreation.Select(o=>o.CustomerId).ToList());
            lstCusId.AddRange(lstAccountCare.Select(o=>o.CustomerId).ToList());
            lstCusId = lstCusId.Distinct().ToList();
            if(lstCusId.Count == 0)
            {   
                foreach (var viewedSalerInfo in lstViewedSaler)
                {
                    var saleReportInfo = new SaleReportViewModel();
                    saleReportInfo.Username = viewedSalerInfo.UserName;
                    saleReportInfo.SumLeads = lstReceivedCrawler.FindAll(o => o.UserId == viewedSalerInfo.Id).Count;

                    totalLead += saleReportInfo.SumLeads;
                    result.Add(saleReportInfo);
                }
                ViewBag.TotalLead = totalLead;
                return View(result);
            }

            // Lấy ra thông tin tài khoản động bộ bên admin tương ứng
            var syncBCRMNAdminLayer = new SyncBcrmBxhNAdminLayer();
            var lstAdminId= syncBCRMNAdminLayer.GetByListCustomerId(string.Join(",", lstCusId));
            // Lay ra tat ca lich su tin dang cua cac tai khoan trong khoang thoi gian nay
            var adminProductAppovedLayer = new AdminProductApprovedLayer();
            var lstProductApprovedHistory = adminProductAppovedLayer.GetByListMemberAndTime(lstAdminId.Select(o => o.AccountId).ToList(), model.StartDate, model.EndDate.AddDays(DefaultSystemParameter.DayMaximumToMarkAccountActive));

            // Lấy tất cả lịch sử login vào hệ thống trong khoảng thời gian này
            var traceUserLoginHistory = CallAPITraceCustomerLogin(model.StartDate, model.EndDate.AddDays(DefaultSystemParameter.DayMaximumToMarkAccountActive));

            if (traceUserLoginHistory == null)
            {
                return View(new List<SaleReportViewModel>());
            }

            // Tìm tất cả các tài khoản mà có login trong khoảng thời gian này
            var lstAdminLoginInTime = new List<SyncBcrmBxhNAdmin>(); 
            foreach(var userItem in traceUserLoginHistory.Data)
            {
                var info = lstAdminId.Find(o=>(!string.IsNullOrEmpty(o.PhoneNumber) && o.PhoneNumber.Substring(1).Equals(userItem.PhoneOrEmail))
                                     ||(!string.IsNullOrEmpty(o.Email)  && o.Email.ToLower().Equals(userItem.PhoneOrEmail.ToLower()))  );
                if(info != null)
                {
                    lstAdminLoginInTime.Add(info);
                }
            }

            foreach (var viewedSalerInfo in lstViewedSaler)
            {
                var saleReportInfo = new SaleReportViewModel();
                saleReportInfo.Id = viewedSalerInfo.Id;
                saleReportInfo.Username = viewedSalerInfo.UserName;
                // Lấy ra các tài khoản crawler được nhận bởi sale này trong khoảng thời gian lọc
                saleReportInfo.SumLeads = lstReceivedCrawler.FindAll(o => o.UserId == viewedSalerInfo.Id).Count;
                
                // Lấy ra các tài khoản được tạo trực tiếp bởi sale này trong khoảng thời gian lọc
                var lstSaledCustomer = lstAccountCreation.FindAll(o => o.UserId == viewedSalerInfo.Id);
                saleReportInfo.SumAddedAccount = lstSaledCustomer.FindAll(o => o.AdminId > 0).Count;
                // Lấy ra các tài khoản được convert từ crawler lên của sale này
                saleReportInfo.SumConvertedAccount = lstSaledCustomer.FindAll(o => o.CrawlerAdminId > 0).Count;
                
                var sumActivedAccount = 0;
                // Lấy ra các tài khoản được chăm sóc bởi sale này
                var lstCustomerCareEachSale = lstAccountCare.FindAll(o => o.UserId == viewedSalerInfo.Id);
                // Lấy ra các id customer tương ứn
                var lstCustomerIdEachSale = lstCustomerCareEachSale.Select(o => o.CustomerId).Distinct().ToList();
                foreach (var saledCustomerIdItem in lstCustomerIdEachSale)
                {
                    var adminInfo = lstAdminId.Find(o => o.CustomerId == saledCustomerIdItem);
                    if(adminInfo != null)
                    {
                        var lstProductEachCustomer = lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId);
                        if(lstProductEachCustomer.Count >= 2)
                        {
                            sumActivedAccount++;
                        }
                        else if (lstAdminLoginInTime.Find(o => o.AccountId == adminInfo.AccountId) != null)
                        {
                            sumActivedAccount++;
                        }

                    }
                }
                // Số tài khoản active lifetime
                saleReportInfo.ActiveAccount = sumActivedAccount;
                // Lấy ra các adminid tương ứng với các tài khoản được tạo bởi sale này
                // Tính ra số tin đã lấy đăng của khách trong khoảng thời gian này
                
                // Tính tổng số tin đăng đã lấy về của khách hàng
                var sumPosting = 0;
                foreach (var item in lstCustomerCareEachSale)
                {
                    // Lấy adminid tương ứng
                    var adminInfo = lstAdminId.Find(o => o.CustomerId == item.CustomerId);
                    if (adminInfo == null) continue;

                    if (item.ReceivedDate < model.StartDate)
                    {
                        if (item.StopCareDate == null || item.StopCareDate == DateTime.MinValue || item.StopCareDate >= model.EndDate)
                        {
                            sumPosting += lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId && o.ApprovedDate >= model.StartDate && o.ApprovedDate <= model.EndDate).Count;
                        }
                        else
                        {
                            sumPosting += lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId && o.ApprovedDate >= model.StartDate && o.ApprovedDate <= item.StopCareDate).Count;
                        }
                    }
                    else
                    {
                        if (item.StopCareDate == null || item.StopCareDate == DateTime.MinValue || item.StopCareDate >= model.EndDate)
                        {
                            sumPosting += lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId && o.ApprovedDate >= item.ReceivedDate && o.ApprovedDate <= model.EndDate).Count;
                        }
                        else
                        {
                            sumPosting += lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId && o.ApprovedDate >= item.ReceivedDate && o.ApprovedDate <= item.StopCareDate).Count;
                        }
                    }
                }
                saleReportInfo.SumListings = sumPosting;

                totalLead += saleReportInfo.SumLeads;
                totalCovertedAccount += saleReportInfo.SumConvertedAccount;
                totalAddedAccount += saleReportInfo.SumAddedAccount;
                totalAccount += saleReportInfo.SumOverAccount;
                totalActiveAccount += saleReportInfo.ActiveAccount;
                totalListing += saleReportInfo.SumListings;

                result.Add(saleReportInfo);
            }
            ViewBag.TotalLead = totalLead;
            ViewBag.TotalConvertedAccount = totalCovertedAccount;
            ViewBag.TotalAddedAccount = totalAddedAccount;
            ViewBag.TotalAccounts = totalAccount;
            ViewBag.TotalActivedAccount = totalActiveAccount;
            ViewBag.TotalListing = totalListing;

            return View(result);
        }
        [FilterAuthorize]
        public JsonResult GetUserByGroupId(int groupId)
        {
            var lstShowedAssignTo = new List<KeyValuePair<string, string>>();
            if (!UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                lstShowedAssignTo.Add(new KeyValuePair<string, string>(UserContext.UserName, UserContext.UserName));
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstShowedAssignTo
                }, JsonRequestBehavior.AllowGet);
            }

            var groupLayer = new GroupLayer();
            var userLayer = new UserLayer();
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            if (groupId == 0)
            {
                var allGroups = groupLayer.GetAll();
                var lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                var lstUser = userLayer.GetList(groupIds).FindAll(o => !o.IsHiddenFromReport);
                var lstSaler = new List<UserSearchModel>();
                var userRoleGroupLayer = new UserRoleGroupLayer();
                foreach (var item in lstUser)
                {
                    // Lấy role của người dùng
                    var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                    if (roles.Find(o => o.RoleId == RoleSystem.Admin.GetHashCode()
                                   || o.RoleId == RoleSystem.Manager.GetHashCode()
                                   || o.RoleId == RoleSystem.Sale.GetHashCode()) != null)
                    {
                        lstSaler.Add(item);
                    }
                }
                var lstShowSaler = new List<KeyValuePair<string, string>>();
                lstShowSaler.Add(new KeyValuePair<string, string>("", "--All salers--"));
                lstShowSaler.AddRange(lstSaler.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList());

                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstShowSaler
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Lấy danh sách người dùng thuộc nhóm đó
                var lstUser = userLayer.GetByGroupId(groupId).FindAll(o => !o.IsHiddenFromReport);
                var lstSaler = new List<Users>();
                var userRoleGroupLayer = new UserRoleGroupLayer();
                foreach (var item in lstUser)
                {
                    // Lấy role của người dùng
                    var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                    if(roles.Find(o=> o.RoleId == RoleSystem.Admin.GetHashCode() 
                                   || o.RoleId == RoleSystem.Manager.GetHashCode()
                                   || o.RoleId == RoleSystem.Sale.GetHashCode() ) != null )
                    {
                        lstSaler.Add(item);
                    }
                }

                var lstShowSaler = new List<KeyValuePair<string, string>>();
                lstShowSaler.Add(new KeyValuePair<string, string>("", "--All salers--"));
                lstShowSaler.AddRange(lstSaler.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList());
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstShowSaler
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonAction]
        public List<Group> LoadGroup()
        {
            var groupLayer = new GroupLayer();
            var permittedGroups = new List<Group>();
            var allGroup = groupLayer.GetAll();
            //Check if this user has permission Admin
            if (UserContext.HasRole(RoleSystem.Admin))
            {
                permittedGroups = allGroup;
            }
            else
            {
                if (UserContext.ListRoleGroup.ContainsKey(RoleSystem.Manager))
                {
                    var lstGroupId = UserContext.ListRoleGroup[RoleSystem.Manager];
                    if (lstGroupId.Contains(DefaultSystemParameter.AllGroup))
                    {
                        permittedGroups = allGroup;
                    }
                    else
                    {
                        permittedGroups = allGroup.FindAll(o => lstGroupId.Contains(o.Id));
                    }
                }
                else
                {
                    permittedGroups.Add(allGroup.Find(o => o.Id == UserContext.GroupId));
                }
            }
            return permittedGroups;
        }

        [FilterAuthorize]
        public APICustomerLoginTimeResponse CallAPITraceCustomerLogin(DateTime fromDate, DateTime toDate)
        {
            var data = new
            {
                webId = 7,
                startDate = fromDate.ToString("yyyyMMdd").ToInt(0),
                endDate = toDate.ToString("yyyyMMdd").ToInt(0),
                threshHold = 2
            };

            var url = string.Format("{0}?param={1}"
                                , ConfigurationManager.AppSettings["APITraceCustomerLogin"]
                                , HttpUtility.UrlEncode(AESEncryption.EncryptString(JsonConvert.SerializeObject(data))));
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(new { }), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                var failLogging = new FailureLoggingCommon();
                failLogging.WriteLog(1, "SaleReport.CallAPITraceCustomerLogin", JsonConvert.SerializeObject(new { fromDate = fromDate, toDate = toDate }), UserContext.UserName, "Không thể kết nối đến API hoặc lỗi trong quá trình xử lý API", DateTime.Now, "");
                return null;
            }
            try
            {
                var resCode = JsonConvert.DeserializeObject<APICustomerLoginTimeResponse>(res);
                return resCode;
            }
            catch (Exception ex)
            {
                var failLogging = new FailureLoggingCommon();
                failLogging.WriteLog(1, "SaleReport.CallAPITraceCustomerLogin", JsonConvert.SerializeObject(new { fromDate = fromDate, toDate = toDate }), UserContext.UserName, ex.Message, DateTime.Now, "");
                return null;
            }
        }
    }
}
