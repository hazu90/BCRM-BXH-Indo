﻿using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class OTPManagerController : Controller
    {
        public ActionResult GenerateQRCode(string userName)
        {
            var userLayer = new UserLayer();
            var userinfo = userLayer.GetByUserName(userName);

            var otpManagerBl = new UserOTPAuthenticatorLayer();
            var otpManager = otpManagerBl.GetByUserId(userinfo.Id);
            var retVal = new OTPManagerModel();
            if (otpManager != null && otpManager.ExpiredTime > DateTime.Now)
            {
                retVal.Image = otpManager.Source;
                retVal.Status = 1;
            }
            else
            {
                retVal.Image = "You have passed time to active QRCode. Please contact manager for assistance.";
                retVal.Status = 2;
            }
            return View(retVal);
        }
    }
}
