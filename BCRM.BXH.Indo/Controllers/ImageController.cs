﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.BXH.Indo.Library;
using System.Configuration;
using System.Text;

namespace BCRM.BXH.Indo.Controllers
{
    public class ImageController : Controller
    {
        public static byte[] ReadToEnd(Stream stream)
        {
            long originalPosition = 0;
            try
            {
                if (stream.CanSeek)
                {
                    originalPosition = stream.Position;
                    stream.Position = 0;
                }
                byte[] readBuffer = new byte[4096];
                int totalBytesRead = 0;
                int bytesRead;
                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;
                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }
                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }

        [FilterAuthorize]
        public JsonResult UploadImage(HttpPostedFileBase file)
        {
            Response response;
            try
            {
                if (file != null)
                {
                    var fileSize = file.ContentLength;
                    if (fileSize / (1024 * 1024) > 2)
                    {
                        response = new Response
                        {
                            Code = SystemCode.NotValid
                        };
                    }
                    else
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        if (fileName != null)
                        {
                            var link = string.Format("{0:yyyy/MM}", DateTime.Now);
                            var directory = string.Format("{0}\\{1:yyyy/MM}", ConfigurationManager.AppSettings["StoragePath"], DateTime.Now).Replace("/", "\\");
                            if (!Directory.Exists(directory))
                            {
                                Directory.CreateDirectory(directory);
                            }
                            var saveName = string.Format("{0}_{1}", RandomString(8), fileName);
                            directory = string.Format("{0}\\{1}", directory, saveName);
                            link = string.Format("{0}/{1}", link, saveName);
                            file.SaveAs(directory);
                            response = new Response { Code = SystemCode.Success, Data = link };
                        }
                        else
                        {
                            response = new Response { Code = SystemCode.DataNull };
                        }
                    }
                }
                else
                {
                    response = new Response { Code = SystemCode.DataNull };
                }
            }
            catch (Exception exception)
            {
                response = new Response
                {
                    Code = SystemCode.Error,
                    Message = exception.Message
                };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private static string RandomString(int size)
        {
            var random = new Random();
            const string input = "abcdefghijklmnopqrstuvwxyz0123456789";
            var builder = new StringBuilder();
            for (var i = 0; i < size; i++)
            {
                var ch = input[random.Next(0, input.Length)];
                builder.Append(ch);
            }
            return builder.ToString();
        }

    }
}
