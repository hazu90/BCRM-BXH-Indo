﻿using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class ShowroomReportController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            var cityLayer = new CityLayer();
            ViewBag.ListCity = cityLayer.GetAll();
            // Lấy ra danh sách các group mà người dùng có quyền quản lý
            var lstGroupIds = LoadGroup().Select(o=>o.Id).ToList() ;
            var userLayer = new UserLayer();
            var allUsers = userLayer.GetAll();
            var lstManagedUser = new List<Users>();
            // TH người dùng không có quyền quản lý trên bất kì một nhóm nào => add thông tin 
            // của người dùng để tiến hành tìm kiếm
            if(lstGroupIds.Count == 0)
            {
                lstManagedUser.Add(new Users() { UserName = UserContext.UserName});
            }
            else
            {
                lstManagedUser.AddRange(allUsers.FindAll(o => lstGroupIds.Contains(o.GroupId)));
                if(lstManagedUser.Find(o=>o.UserName == UserContext.UserName ) == null)
                {
                    lstManagedUser.Add(new Users() { UserName = UserContext.UserName });
                }
            }
            ViewBag.ListUser = lstManagedUser;
            ViewBag.AssignTo = UserContext.UserName;
            ViewBag.CurrentUser = UserContext;
            
            var model = new ShowroomSearchModel();
            return View(model);
        }

        [FilterAuthorize]
        public ActionResult Search(ShowroomSearchModel model)
        {
            if(string.IsNullOrEmpty(model.FilterKeyword))
            {
                model.FilterKeyword = "";
            }
            if(string.IsNullOrEmpty(model.AssignTo))
            {
                model.AssignTo = "";
            }
            model.EndDate = new DateTime(model.EndDate.Year, model.EndDate.Month, model.EndDate.Day, 23, 59, 59);
            // Lấy danh sách tìm kiếm showroom từ các điều kiện tìm kiếm
            var customerLayer = new CustomerLayer();
            var lstShowroom = customerLayer.GetShowroomReport(model.FilterKeyword, model.AssignTo, model.CityId, model.DistrictId);
            if(lstShowroom.Count>0)
            {
                // Lấy ra các id tương ứng của showroom với bên admin cintamobil
                var syncBcrmNAdminLayer = new SyncBcrmBxhNAdminLayer();
                var lstSyncId = syncBcrmNAdminLayer.GetByListCustomerId(string.Join(",", lstShowroom.Select(o => o.Id).ToList()));
                
                if(lstSyncId.Count>0)
                {
                    //var nextMonth = model.StartDate.AddMonths(1);
                    //var firstDayInNextMonth = new DateTime(nextMonth.Year, nextMonth.Month, 1, 0, 0, 0);

                    // Lấy ra các thông tin thống kê đăng tin từ bên showroom
                    var adminArticleCategoryLayer = new AdminArticleCategoriesLayer();
                    var lstAdminShowroomStatistic = adminArticleCategoryLayer.GetShowroomReport(model.StartDate, model.EndDate, lstSyncId.Select(o => o.AccountId).ToList());
                    foreach (var item in lstShowroom)
                    {
                        var eq = lstSyncId.Find(o => o.CustomerId == item.Id);
                        if (eq != null)
                        {
                            var reportInfo = lstAdminShowroomStatistic.Find(o => o.Id == eq.AccountId);
                            if (reportInfo != null)
                            {
                                item.NumOfPosting = reportInfo.NumOfPosting;
                                item.JoinDate = reportInfo.JoinDate;
                                item.NumPostingIn1stMonth = reportInfo.NumPostingIn1stMonth;
                                item.ShowroomType = reportInfo.ShowroomType;
                                // Hiển thị thông tin loại showroom
                            }
                        }
                    }
                }
            }

            if(model.ShowroomType != 0)
            {
                lstShowroom = lstShowroom.FindAll(o => o.ShowroomType == model.ShowroomType);
            }
            return View(lstShowroom);
        }

        [NonAction]
        public List<Group> LoadGroup()
        {
            var groupLayer = new GroupLayer();
            var permittedGroups = new List<Group>();
            var allGroup = groupLayer.GetAll();
            //Check if this user has permission Admin
            if (UserContext.HasRole(RoleSystem.Admin))
            {
                permittedGroups = allGroup;
            }
            else
            {
                if (UserContext.ListRoleGroup.ContainsKey(RoleSystem.Manager))
                {
                    var lstGroupId = UserContext.ListRoleGroup[RoleSystem.Manager];
                    if (lstGroupId.Contains(-1000))
                    {
                        permittedGroups = allGroup;
                    }
                    else
                    {
                        permittedGroups = allGroup.FindAll(o => lstGroupId.Contains(o.Id));
                    }
                }
            }
            return permittedGroups;
        }
    }
}
