﻿using BCRM.BXH.Indo.Common;
using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class AdminWorkingAccountReportController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            var model = new AdminWorkingAccountReportIndexModel();
            var firstDayInMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            model.StartDate = firstDayInMonth;
            model.EndDate = DateTime.Now;
            return View(model);
        }

        [FilterAuthorize]
        public JsonResult ShowReport(AdminWorkingAccountReportIndexModel model)
        {
            // Tính ra ngày mà bắt đầu trước 30 ngày so với ngày bắt đầu
            var before30DaysFromStartDate = model.StartDate.AddDays((-1) * DefaultSystemParameter.DaysCalcActiveCustomer);
            before30DaysFromStartDate = new DateTime(before30DaysFromStartDate.Year, before30DaysFromStartDate.Month, before30DaysFromStartDate.Day, 23, 59, 59);

            var countMonthToSplit = 2;
            if (model.StartDate > model.EndDate)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotValid,
                    Message = "Start date 's not larger than end date !"
                }, JsonRequestBehavior.AllowGet);
            }

            var customerLayer = new CustomerLayer();
            var adminProductApproveBL = new AdminProductApprovedLayer();
            var reportResult = new List<AdminWorkingAccountReportResult>();
            var lstPosting = adminProductApproveBL.CheckCustomerActive_V02(before30DaysFromStartDate, model.EndDate.AddDays(1).AddSeconds(-1));

            var traceUserLoginHistory = CallAPITraceCustomerLogin(before30DaysFromStartDate, model.EndDate);

            if (traceUserLoginHistory == null)
            {
                return Json(new Response() 
                {
                    Code = SystemCode.ErrorConnect,
                    Message ="Error when call API Trace Customer Login . Contact IT for detail !"
                }, JsonRequestBehavior.AllowGet);
            }
            // Tạo danh sách lưu lịch sử login của từng tài khoản
            var lstTraceLoginHistory = new List<CustomerLoginInDayModel>();
            // Lấy ra danh sách các tài khoản của người dùng
            var lstUserName = traceUserLoginHistory.Data.Select(o => o.PhoneOrEmail).ToList();

            var lstPhoneAcc = new List<string>();
            var lstEmailAcc = new List<string>();

            foreach (var userLoginHistory in traceUserLoginHistory.Data)
            {
                var tempPhone = (long)0;
                // Kiểm tra tài khoản có phải dùng sdt hay không
                if (long.TryParse(userLoginHistory.PhoneOrEmail, out tempPhone))
                {
                    lstPhoneAcc.Add("0" + tempPhone.ToString());
                }
                else
                {
                    lstEmailAcc.Add(userLoginHistory.PhoneOrEmail);
                }
            }
            // Lấy ra danh sách các tài khoản đồng bộ
            var syncAccLayer = new SyncBcrmBxhNAdminLayer();
            var lstSyncAcc = syncAccLayer.GetByListEmailOrPhone(lstEmailAcc, lstPhoneAcc);

            var lstCustomerLoginInDay = new List<CustomerLoginInDayModel>();

            foreach (var userLoginHistory in traceUserLoginHistory.Data)
            {
                var tempPhone = (long)0;
                // Kiểm tra tài khoản có phải dùng sdt hay không
                if (long.TryParse(userLoginHistory.PhoneOrEmail, out tempPhone))
                {
                    var fullPhone = "0"+tempPhone.ToString();
                    var syncInfo = lstSyncAcc.Find(o => !string.IsNullOrEmpty(o.PhoneNumber) &&  o.PhoneNumber.Equals(fullPhone));
                    if (syncInfo != null)
                    {
                        foreach (var dayItem in userLoginHistory.LstDayLoggedInInInteger)
                        {
                            lstCustomerLoginInDay.Add(new CustomerLoginInDayModel()
                            {
                                PhoneOrEmail = fullPhone,
                                AdminId = syncInfo.AccountId,
                                LoggedInDay = dayItem
                            });    
                        }
                    }
                }
                else
                {
                    var syncInfo = lstSyncAcc.Find(o => !string.IsNullOrEmpty(o.Email) && o.Email.Equals(userLoginHistory.PhoneOrEmail));
                    if (syncInfo != null)
                    {
                        foreach (var dayItem in userLoginHistory.LstDayLoggedInInInteger)
                        {
                            lstCustomerLoginInDay.Add(new CustomerLoginInDayModel()
                            {
                                PhoneOrEmail = userLoginHistory.PhoneOrEmail,
                                AdminId = syncInfo.AccountId,
                                LoggedInDay = dayItem
                            });
                        }
                    }
                }
            }

            // Tách dữ liệu tin đăng và dữ liệu login theo từng khoảng thời gian
            var currTimeStamp = before30DaysFromStartDate;
            var endTimeInTimeStamp = model.EndDate.AddDays(1).AddSeconds(-1);
            var lstSplitter = new List<AdminWorkingAccountInTimeModel>();
            do
            {
                var next = currTimeStamp.AddMonths(countMonthToSplit);
                if (next >= endTimeInTimeStamp)
                {
                    next = endTimeInTimeStamp;
                }

                var lstTempPosting = lstPosting.FindAll(o => o.ApprovedDate > currTimeStamp && o.ApprovedDate < next);
                lstSplitter.Add(new AdminWorkingAccountInTimeModel()
                {
                    StartTime = currTimeStamp,
                    EndTime = next,
                    LstAdminPosting = lstTempPosting
                });
                currTimeStamp = currTimeStamp.AddMonths(countMonthToSplit);
            }
            while (currTimeStamp < endTimeInTimeStamp);

            // Lấy ra toàn bộ các tài khoản được tạo từ trước ngày bắt đầu tìm kiếm
            var beforeTimeOfStartDate = (new DateTime(model.StartDate.Year, model.StartDate.Month, model.StartDate.Day, 0, 0, 0)).AddSeconds(-1);
            var totalRegisteredBeforeFromStartDate = customerLayer.CountRegisteredCustomer(null, beforeTimeOfStartDate);
            // Lấy ra toàn bộ các tài khoản được tạo từ 
            // thời điểm bắt đầu tìm kiếm 
            var endTimeOfEndDate = new DateTime(model.EndDate.Year, model.EndDate.Month, model.EndDate.Day, 23, 59, 59);
            var lstRegisterInTime = customerLayer.GetRegisteredCustomer(model.StartDate, endTimeOfEndDate);
            var totalRegister = totalRegisteredBeforeFromStartDate;
            for (var currDay = model.StartDate; currDay <= model.EndDate; currDay = currDay.AddDays(1))
            {
                // Lấy ra tổng số khách hàng đăng kí lập tài khoản trong ngày
                var sInDay = new DateTime(currDay.Year, currDay.Month, currDay.Day, 0, 0, 0);
                var eInDay = new DateTime(currDay.Year, currDay.Month, currDay.Day, 23, 59, 59);
                totalRegister += lstRegisterInTime.FindAll(o => o.CreatedDate >= sInDay && o.CreatedDate <= eInDay).Count;

                var before30DaysFromCurrDay = currDay.AddDays((-1) * DefaultSystemParameter.DaysCalcActiveCustomer);
                before30DaysFromCurrDay = new DateTime(before30DaysFromCurrDay.Year, before30DaysFromCurrDay.Month, before30DaysFromCurrDay.Day, 23, 59, 59);

                var lstActive = new List<int>();
                var lstTT = lstSplitter.FindAll(o => (o.StartTime <= before30DaysFromCurrDay && o.EndTime >= before30DaysFromCurrDay)
                                                  || (o.StartTime <= eInDay && o.EndTime >= eInDay));
                var lstTempPosting = new List<AdminProductAppovedInWorkingUserStatisticModel>();

                foreach (var tt in lstTT)
                {
                    lstTempPosting.AddRange(tt.LstAdminPosting);
                }

                var lstCheckPosting = lstTempPosting.FindAll(p => p.ApprovedDate >= before30DaysFromCurrDay && p.ApprovedDate <= eInDay).ToList();
                var lst2Posting = lstCheckPosting.GroupBy(o => o.AdminId).Where(x => x.Count() > 1).Select(o => o.Key).ToList();

                var yyMMddBefore30Day = before30DaysFromCurrDay.ToString("yyyyMMdd").ToInt(0);
                var yyMMddEndDay = eInDay.ToString("yyyyMMdd").ToInt(0);
                var lstCheckLogin = lstCustomerLoginInDay.FindAll(o => o.LoggedInDay >= yyMMddBefore30Day && o.LoggedInDay <= yyMMddEndDay)
                                            .Select(o => o.AdminId).ToList() ;

                lstActive.AddRange(lst2Posting);
                lstActive.AddRange(lstCheckLogin);
                lstActive = lstActive.Distinct().ToList();

                var item = new AdminWorkingAccountReportResult()
                {
                    ReportDate = currDay.ToString("yyyyMMdd").ToInt(0),
                    CountActiveCustomer = lstActive.Count,
                    CountRegisteredCustomer = totalRegister
                };
                reportResult.Add(item);
            }
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = reportResult
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public APICustomerLoginTimeResponse CallAPITraceCustomerLogin(DateTime fromDate, DateTime toDate)
        {
            var data = new {
                webId = 7,
                startDate= fromDate.ToString("yyyyMMdd").ToInt(0),
                endDate = toDate.ToString("yyyyMMdd").ToInt(0),
                threshHold = 2
            };

            var url = string.Format("{0}?param={1}"
                                , ConfigurationManager.AppSettings["APITraceCustomerLogin"]
                                , HttpUtility.UrlEncode(AESEncryption.EncryptString(JsonConvert.SerializeObject(data))));
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(new { }), "application/json");



            if (string.IsNullOrEmpty(res))
            {
                var failLogging = new FailureLoggingCommon();
                failLogging.WriteLog(1, "AdminWorkingAccountReport.CallAPITraceCustomerLogin", JsonConvert.SerializeObject(new { fromDate = fromDate, toDate = toDate }), UserContext.UserName, "Không thể kết nối đến API hoặc lỗi trong quá trình xử lý API", DateTime.Now, "");
                return null;
            }
            try
            {
                var resCode = JsonConvert.DeserializeObject<APICustomerLoginTimeResponse>(res);

                return resCode;
            }
            catch (Exception ex)
            {
                var failLogging = new FailureLoggingCommon();
                failLogging.WriteLog(1, "AdminWorkingAccountReport.CallAPITraceCustomerLogin", JsonConvert.SerializeObject(new { fromDate = fromDate, toDate = toDate }), UserContext.UserName, ex.Message, DateTime.Now, "");
                return null;
            }
        }

    }
}
