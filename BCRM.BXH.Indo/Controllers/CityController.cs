﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.DAL;

namespace BCRM.BXH.Indo.Controllers
{
    public class CityController : Controller
    {
        //
        // GET: /City/
        public JsonResult GetAllCity()
        {
            var response = new Response();
            try
            {
                var cityLayer = new CityLayer();
                var lstCity = cityLayer.GetAll();
                response.Data = lstCity;
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDistrictByCity(int cityId)
        {
            var response = new Response();
            try
            {
                var districtLayer = new DistrictLayer();
                var lstDistrict = districtLayer.GetByCityId(cityId);
                response.Data = lstDistrict;
            }
            catch(Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCityByRegion(int regionId)
        {
            var response = new Response();
            try
            {
                var cityLayer = new CityLayer();
                var lstCity = cityLayer.GetByRegionId(regionId);
                response.Data = lstCity;
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
