﻿using DVS.Algorithm;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.BXH.Indo.Models;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Language;
using System.Configuration;
using BCRM.BXH.Indo.Cache;

namespace BCRM.BXH.Indo.Controllers
{
    public class PaymentController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            return View();
        }
        [FilterAuthorize]
        public ActionResult Search(PaymentIndexModel model)
        {
            var phone =(long) 0;
            var isPhone = false;
            // Tìm theo email hoặc số điện thoại của khách hàng
            var customerLayer = new CustomerLayer();
            var profileLayer = new ProfileLayer();
            var paymentLayer = new PaymentLayer();
            var totalRecord = 0;
            var result = new List<PaymentSearchModel>();
            if(!string.IsNullOrEmpty(model.FilterKeyword))
            {
                isPhone = long.TryParse(model.FilterKeyword, out phone);
                if(isPhone)
                {
                    var profileInfo = profileLayer.GetByPhoneNumber(model.FilterKeyword);
                    if(profileInfo == null)
                    {
                        totalRecord = 0;
                        result = new List<PaymentSearchModel>();
                    }
                    else
                    {
                        result = paymentLayer.GetListByCustomerId(profileInfo.CustomerId, model.PageIndex, model.PageSize, out totalRecord);
                    }
                }
                else
                {
                    var customerInfo = customerLayer.GetByEmail(model.FilterKeyword);
                    if(customerInfo == null)
                    {
                        totalRecord = 0;
                        result = new List<PaymentSearchModel>();
                    }
                    else
                    {
                        result = paymentLayer.GetListByCustomerId(customerInfo.Id, model.PageIndex, model.PageSize, out totalRecord);
                    }
                }
            }
            else 
            {
                result = paymentLayer.GetList(model.FilterKeyword, model.CreatorId, model.Status, model.CreatedFromDate, model.CreatedToDate, model.PageIndex, model.PageSize, out totalRecord);
            }
            var lstCustomerId = result.Select(o => o.CustomerId).Distinct().ToList();
            var lstCustomer = new List<Customer>();
            if(lstCustomerId.Count > 0)
            {
                lstCustomer = customerLayer.GetByLstCustomerId(lstCustomerId);
            }
            
            foreach (var paymentItem in result)
            {
                var customerInfo = lstCustomer.Find(o => o.Id == paymentItem.CustomerId);
                paymentItem.CustomerEmail = customerInfo.Email;
                paymentItem.CustomerFullName = customerInfo.FullName;
                paymentItem.CustomerPhone = customerInfo.PhoneNumber;
            }

            ViewBag.Pager = new Pager()
            {
                ActionScript = "payment_index.search",
                CurrentPage = model.PageIndex,
                PageSize = model.PageSize,
                TotalItem = totalRecord
            };
            return View(result);
        }
        [FilterAuthorize]
        public ActionResult GetViewByPaymentId(int index, int id)
        {
            var paymentLayer = new PaymentLayer();
            var searchInfo = paymentLayer.GetViewByPaymentId(id);

            searchInfo.Index = index;

            var customerLayer = new CustomerLayer();
            var lstCustomerId = new List<int>() { searchInfo.CustomerId};
            var lstCustomer = new List<Customer>();
            if (lstCustomerId.Count > 0)
            {
                lstCustomer = customerLayer.GetByLstCustomerId(lstCustomerId);
            }

            var customerInfo = lstCustomer.Find(o => o.Id == searchInfo.CustomerId);
            searchInfo.CustomerEmail = customerInfo.Email;
            searchInfo.CustomerFullName = customerInfo.FullName;
            searchInfo.CustomerPhone = customerInfo.PhoneNumber;
            return View(searchInfo);
        }
        [FilterAuthorize]
        public ActionResult Create()
        {
            return View();
        }
        [FilterAuthorize]
        public JsonResult CreateAction(PaymentCreateModel model)
        {
            var paymentLayer = new PaymentLayer();
            paymentLayer.Create(model.CustomerId,model.Money,model.PaymentDate,DateTime.Now,UserContext.UserName,UserContext.Id,(short)PaymentStatus.Pending );
            return Json(new Response() 
            {
                Code = SystemCode.Success,
                Message = "Create a new payment successfully !"
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult GetByPhoneOrEmail(string phoneOrEmail)
        {
            var customerLayer = new CustomerLayer();
            var profileLayer = new ProfileLayer();
            var phone =(long) 0;
            var isPhone = false;
            if(string.IsNullOrEmpty(phoneOrEmail)|| phoneOrEmail.Length <=1)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "Not found customer !"
                }, JsonRequestBehavior.AllowGet);
            }

            isPhone = long.TryParse(phoneOrEmail, out phone);

            if (isPhone)
            {
                var profileInfo = profileLayer.GetByPhoneNumber(phoneOrEmail);
                if(profileInfo == null)
                {
                    return Json(new Response() 
                    {
                        Code = SystemCode.DataNull,
                        Message = "Not found customer !"
                    }, JsonRequestBehavior.AllowGet);
                }
                var customerInfo = customerLayer.GetById(profileInfo.CustomerId);
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = customerInfo
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var customerInfo = customerLayer.GetByEmail(phoneOrEmail);
                if(customerInfo == null)
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.DataNull,
                        Message = "Not found customer !"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = customerInfo
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [FilterAuthorize]
        public JsonResult Approved(int id,DateTime verifiedPaymentDate, string token)
        {
            var validateToken = ChecksumHelper.ValidateToken(new { Id = id, CurrUser = UserContext.UserName }, token);
            if(!validateToken.IsValid)
            {
                return Json(new Response(SystemCode.NotPermitted, LanguageRes.Msg_NotPermitted,null), JsonRequestBehavior.DenyGet);
            }
            var cachePayment = new IISPaymentApprovingCache();
            if (cachePayment.Contain(id))
            {
                return Json(new Response(SystemCode.NotPermitted, "This payment is approving !",null),JsonRequestBehavior.DenyGet);
            }
            cachePayment.Add(id);
            // Lấy thông tin payment
            var paymentLayer = new PaymentLayer();
            var paymentInfo = paymentLayer.GetById(id);
            if (paymentInfo.Status != (short)PaymentStatus.Pending)
            {
                return Json(new Response(SystemCode.NotPermitted,"This payment is approved or rejected !",null), JsonRequestBehavior.DenyGet);
            }
            // Goi api sang bên web để thực hiện nạp tiền
            var transaction_id = 1;
            // Nếu thành công thì cập nhật trạng thái bảng payment 
            // và tính doanh thu cho nhân viên đó
            // Tính doanh thu cho sale đang chăm sóc khách hàng này
            var careHistoryLayer = new CustomerCareHistoryLayer();
            var lstCareHistory = careHistoryLayer.GetByCustomerId(paymentInfo.CustomerId);
            // Kiểm tra xem có nhân viên nào đang chăm trong khoảng thời gian này
            var careInfo = lstCareHistory.Find(o => o.IsCared && o.ReceivedDate <= verifiedPaymentDate && (!o.IsReturned || (o.IsReturned && o.StopCareDate >= verifiedPaymentDate)));
            paymentLayer.Approved(id, paymentInfo.CustomerId, careInfo.UserId, paymentInfo.Money, verifiedPaymentDate, transaction_id,DateTime.Now,UserContext.UserName);
            cachePayment.Remove(id);
            return Json(new Response(SystemCode.Success, "Successfully!",null) , JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult Reject(int id,string feedBack, string token)
        {
            var validateToken = ChecksumHelper.ValidateToken(new { Id = id, CurrUser = UserContext.UserName }, token);
            if (!validateToken.IsValid)
            {
                return Json(new Response(SystemCode.NotPermitted, LanguageRes.Msg_NotPermitted, null), JsonRequestBehavior.DenyGet);
            }

            var cachePayment = new IISPaymentApprovingCache();
            if (cachePayment.Contain(id))
            {
                return Json(new Response(SystemCode.NotPermitted, "This payment is approving or rejecting !", null), JsonRequestBehavior.DenyGet);
            }
            cachePayment.Add(id);

            var paymentLayer = new PaymentLayer();
            var paymentInfo = paymentLayer.GetById(id);
            if (paymentInfo.Status != (short)PaymentStatus.Pending)
            {
                return Json(new Response(SystemCode.NotPermitted, "This payment is approved or rejected !", null), JsonRequestBehavior.DenyGet);
            }
            paymentLayer.Reject(id, feedBack,(short) PaymentStatus.Rejected, DateTime.Now, UserContext.UserName);

            cachePayment.Remove(id);
            return Json(new Response(SystemCode.Success, "Successfully!", null), JsonRequestBehavior.DenyGet);
        }

        [FilterAuthorize]
        public JsonResult GetTokenKeyForOneRequest(int id)
        {
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = ChecksumHelper.GenToken(new { Id = id, CurrUser = UserContext.UserName }, ConfigurationManager.AppSettings["TimeExpiredTokenForConfirmRequest"].ToString().ToInt(1))
            }, JsonRequestBehavior.AllowGet);
        }

        [FilterAuthorize]
        public JsonResult GetTokenKeyForGeneralView(int id)
        {
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = ChecksumHelper.GenToken(new { Id = id, CurrUser = UserContext.UserName }, ConfigurationManager.AppSettings["API_CheckSum_ExpiredInMinute"].ToString().ToInt(15))
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
