﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Home/
        [FilterAuthorize]
        public ActionResult Index()
        {
            if (UserContext != null)
            {
                return View();
            }
            else return RedirectToAction("LogOn", "Account");
        }

    }
}
