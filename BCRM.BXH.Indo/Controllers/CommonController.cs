﻿using BCRM.BXH.Indo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class CommonController : BaseController
    {
        //
        // GET: /Common/
        public ActionResult Header()
        {
            return View(UserContext);
        }
        public ActionResult EmptyHeader()
        {
            return View();
        }
        public ActionResult Paging(Pager pager)
        {
            return View(pager);
        }
        public ActionResult ErrorPage(HandleErrorInfo err)
        {
            return View(err);
        }
    }
}
