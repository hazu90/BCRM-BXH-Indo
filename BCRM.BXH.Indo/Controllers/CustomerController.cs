﻿using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Cache;
using BCRM.BXH.Indo.Entity;
using DVS.Algorithm;
using System.Configuration;
using Newtonsoft.Json;
using BCRM.BXH.Indo.Language;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.IO;
using BCRM.BXH.Indo.Common;

namespace BCRM.BXH.Indo.Controllers
{
    public class CustomerController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index(CustomerIndexModel indexModel)
        {
            //if (indexModel.IsNull)
            //{
            //    indexModel = Session["viewModel"] as CustomerIndexModel ??
            //                 new CustomerIndexModel { AssignTo = UserContext.UserName };
            //}
            
            var cityLayer = new CityLayer();
            indexModel.ListCity = cityLayer.GetAll();

            var groupLayer = new GroupLayer();
            // Lấy ra danh sách mà nhân viên có quyền 
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            var isAdminOrManager = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager);
            if (isAdminOrManager)
            {
                if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
                {
                    indexModel.LstGroup = groupLayer.GetAll();
                }
                else
                {
                    indexModel.LstGroup = groupLayer.GetAll().FindAll(o => lstRoledGroup.Contains(o.Id));
                }
            }
            var groupids = "";
            if (UserContext.HasRole(RoleSystem.AfterSale))
            {
                groupids = string.Join(",", groupLayer.GetAll().Select(o => o.Id).ToList());
            }
            else
            {
                groupids = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup) ? string.Join(",", groupLayer.GetAll().Select(o => o.Id).ToList()) : string.Join(",", lstRoledGroup);
            }
            var userLayer = new UserLayer();
            var lstUsr = string.IsNullOrEmpty(groupids) ? new List<UserSearchModel>() { new UserSearchModel() { UserName = UserContext.UserName,Id = UserContext.Id } } : userLayer.GetList(groupids);
            // Lấy ra danh sách các saler
            // Lấy ra danh sách các after-saler
            var lstSaler = new List<UserSearchModel>();
            var lstAfterSaler = new List<UserSearchModel>();
            var userRoleGroupLayer = new UserRoleGroupLayer();
            foreach (var item in lstUsr)
            {
                // Lấy role của người dùng
                var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                if(roles.Find(o=> o.RoleId == RoleSystem.Admin.GetHashCode() 
                               || o.RoleId == RoleSystem.Manager.GetHashCode()
                               || o.RoleId == RoleSystem.Sale.GetHashCode() ) != null )
                {
                    lstSaler.Add(item);
                }
                if ((item.UserName.ToLower().Equals(UserContext.UserName.ToLower()) || isAdminOrManager) && roles.Find(o => o.RoleId == RoleSystem.AfterSale.GetHashCode()) != null)
                {
                    lstAfterSaler.Add(item);
                }
            }
            indexModel.SetLstSaler(isAdminOrManager, lstSaler);
            indexModel.SetLstCreator(lstSaler);
            indexModel.SetLstAftersale(lstAfterSaler);

            //Session["viewModel"] = indexModel;
            ViewBag.AssignTo = UserContext.UserName;
            ViewBag.CurrentUser = UserContext;
            //indexModel.PostingAssistanceStatus = -1;
            return View(indexModel);
        }
        [HttpPost]
        [FilterAuthorize]
        public ActionResult GetListCustomer(CustomerIndexModel indexModel)
        {
            ViewBag.CurrentUser = UserContext;
            var hasPermit = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager);
            var lstPhone = new List<string>();
            if (!string.IsNullOrEmpty(indexModel.FilterKeyword))
            {
                indexModel.FilterKeyword = indexModel.FilterKeyword.Trim().ToLower();
                lstPhone = Ultility.DetectPhoneNumber(indexModel.FilterKeyword);               
            }
            var custLayer = new CustomerLayer();
            var pager = new Pager();
            // TH người dùng tìm theo số điện thoại khách hàng
            if (lstPhone.Count > 0)
            {
                string[] arrPhone = lstPhone.Select(p => p).ToArray();
                var lstCus = new List<CustomerForIndex>();
                var lstTempCus = custLayer.SelectByPhoneNumber(arrPhone);
                var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin.GetHashCode(), RoleSystem.Manager.GetHashCode());
                var usrLayer = new UserLayer();
                foreach (var phone in lstPhone)
                {
                    var lstTemp = lstTempCus.FindAll(t => t.PhoneNumber.Contains(phone));
                    if (lstTemp != null && lstTemp.Count > 0)
                    {
                        foreach (var temp in lstTemp)
                        {
                            if(string.IsNullOrEmpty(temp.AssignTo))
                            {
                                lstCus.Add(temp);
                            }
                            else
                            {
                                var assigneeInfo = usrLayer.GetByUserName(temp.AssignTo);
                                if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                 || lstRoledGroup.Contains(assigneeInfo.GroupId)
                                 || UserContext.UserName.Equals(assigneeInfo.UserName)
                                 ||(UserContext.HasRole(RoleSystem.AfterSale)
                                    && (string.IsNullOrEmpty(temp.PostingAssistant)
                                    || temp.PostingAssistant.Equals(UserContext.UserName))))
                                {
                                    if (lstCus.Find(c => c.Id == temp.Id) == null)
                                    {
                                        lstCus.Add(temp);
                                    }
                                }
                            }
                        }
                    }
                }
                pager.CurrentPage = indexModel.PageIndex;
                pager.PageSize = indexModel.PageSize;
                pager.TotalItem = lstCus.Count;
                indexModel.ListCustomer = lstCus;
            }
            else
            {
                bool? isActive = null;
                if(indexModel.ActiveStatus == 1)
                {
                    isActive = true;
                }
                else if(indexModel .ActiveStatus == 0)
                {
                    isActive = false;
                }

                // TH hợp tìm tất cả
                var isManager = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager);
                var isAfterSale = UserContext.HasRole(RoleSystem.AfterSale);
                int total = 0;
                var lstCus = new List<CustomerForIndex>();
                if(string.IsNullOrEmpty(indexModel.AssignTo))
                {
                    if(!isManager)
                    {
                        if(isAfterSale)
                        {
                            indexModel.AssignTo = "";
                            if (string.IsNullOrEmpty(indexModel.PostingAssistant))
                            {
                                indexModel.PostingAssistant = UserContext.UserName;
                                lstCus = custLayer.Aftersale_SearchAll(indexModel.Type.HasValue ? indexModel.Type.Value : -1,
                                                                       indexModel.AssignTo,
                                                                       indexModel.CityId.HasValue ? indexModel.CityId.Value : -1,
                                                                       indexModel.DistrictId.HasValue ? indexModel.DistrictId.Value : -1,
                                                                       indexModel.StartDate,
                                                                       indexModel.EndDate,
                                                                       indexModel.FilterKeyword ?? string.Empty,
                                                                       indexModel.PageIndex,
                                                                       indexModel.PageSize,
                                                                       indexModel.Creator,
                                                                       indexModel.PostingAssistant,
                                                                       indexModel.PostingNumberFrom,
                                                                       indexModel.PostingNumberTo,
                                                                       isActive,
                                                                       indexModel.ActiveStatusInMonth,
                                                                       out total);
                            }
                            else
                            {
                                indexModel.PostingAssistant = UserContext.UserName;
                                lstCus = custLayer.Aftersale_Search(indexModel.Type.HasValue ? indexModel.Type.Value : -1,
                                                                   indexModel.AssignTo,
                                                                   indexModel.CityId.HasValue ? indexModel.CityId.Value : -1,
                                                                   indexModel.DistrictId.HasValue ? indexModel.DistrictId.Value : -1,
                                                                   indexModel.StartDate,
                                                                   indexModel.EndDate,
                                                                   indexModel.FilterKeyword ?? string.Empty,
                                                                   indexModel.PageIndex,
                                                                   indexModel.PageSize,
                                                                   indexModel.Creator,
                                                                   indexModel.PostingAssistant,
                                                                   indexModel.PostingNumberFrom,
                                                                   indexModel.PostingNumberTo,
                                                                   isActive,
                                                                   indexModel.ActiveStatusInMonth,
                                                                   out total);
                            }
                        }
                        else
                        {
                            indexModel.AssignTo = UserContext.UserName;
                            lstCus = custLayer.Sale_SearchAll(indexModel.Type.HasValue ? indexModel.Type.Value : -1,
                                                               indexModel.AssignTo,
                                                               indexModel.CityId.HasValue ? indexModel.CityId.Value : -1,
                                                               indexModel.DistrictId.HasValue ? indexModel.DistrictId.Value : -1,
                                                               indexModel.StartDate,
                                                               indexModel.EndDate,
                                                               indexModel.FilterKeyword ?? string.Empty,
                                                               indexModel.PageIndex,
                                                               indexModel.PageSize,
                                                               indexModel.Creator,
                                                               indexModel.PostingNumberFrom,
                                                               indexModel.PostingNumberTo,
                                                               isActive,
                                                               indexModel.ActiveStatusInMonth,
                                                               out total);
                        }
                    }
                    else
                    {
                        indexModel.AssignTo = UserContext.UserName;
                        var lstSearchGroupId = new List<int>();
                        var groupLayer = new GroupLayer();
                        // Lấy ra danh sách mà nhân viên có quyền 
                        var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
                        if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
                        {
                            lstSearchGroupId = groupLayer.GetAll().Select(o=>o.Id).ToList();
                        }
                        else
                        {
                            lstSearchGroupId = groupLayer.GetAll().FindAll(o => lstRoledGroup.Contains(o.Id)).Select(o => o.Id).ToList();
                        }

                        lstCus = custLayer.SearchAll(indexModel.Sort,
                                                       indexModel.Type.HasValue ? indexModel.Type.Value : -1,
                                                       indexModel.AssignTo,
                                                       lstSearchGroupId,
                                                       indexModel.CityId.HasValue ? indexModel.CityId.Value : -1,
                                                       indexModel.DistrictId.HasValue ? indexModel.DistrictId.Value : -1,
                                                       indexModel.StartDate,
                                                       indexModel.EndDate,
                                                       indexModel.FilterKeyword ?? string.Empty,
                                                       indexModel.PageIndex,
                                                       indexModel.PageSize,
                                                       indexModel.Creator,
                                                       indexModel.PostingAssistant,
                                                       indexModel.PostingNumberFrom,
                                                       indexModel.PostingNumberTo,
                                                       isActive,
                                                       indexModel.ActiveStatusInMonth,
                                                       out total);
                    }
                }
                else
                {
                    if (isAfterSale)
                    {
                        if (string.IsNullOrEmpty(indexModel.PostingAssistant))
                        {
                            indexModel.PostingAssistant = UserContext.UserName;
                            lstCus = custLayer.Aftersale_SearchAll(indexModel.Type.HasValue ? indexModel.Type.Value : -1,
                                                                   indexModel.AssignTo,
                                                                   indexModel.CityId.HasValue ? indexModel.CityId.Value : -1,
                                                                   indexModel.DistrictId.HasValue ? indexModel.DistrictId.Value : -1,
                                                                   indexModel.StartDate,
                                                                   indexModel.EndDate,
                                                                   indexModel.FilterKeyword ?? string.Empty,
                                                                   indexModel.PageIndex,
                                                                   indexModel.PageSize,
                                                                   indexModel.Creator,
                                                                   indexModel.PostingAssistant,
                                                                   indexModel.PostingNumberFrom,
                                                                   indexModel.PostingNumberTo,
                                                                   isActive,
                                                                   indexModel.ActiveStatusInMonth,
                                                                   out total);
                        }
                        else
                        {
                            indexModel.PostingAssistant = UserContext.UserName;
                            lstCus = custLayer.Aftersale_Search(indexModel.Type.HasValue ? indexModel.Type.Value : -1,
                                                               indexModel.AssignTo,
                                                               indexModel.CityId.HasValue ? indexModel.CityId.Value : -1,
                                                               indexModel.DistrictId.HasValue ? indexModel.DistrictId.Value : -1,
                                                               indexModel.StartDate,
                                                               indexModel.EndDate,
                                                               indexModel.FilterKeyword ?? string.Empty,
                                                               indexModel.PageIndex,
                                                               indexModel.PageSize,
                                                               indexModel.Creator,
                                                               indexModel.PostingAssistant,
                                                               indexModel.PostingNumberFrom,
                                                                indexModel.PostingNumberTo,
                                                                isActive,
                                                                indexModel.ActiveStatusInMonth,
                                                               out total);
                        }
                    }
                    else
                    {
                        lstCus = custLayer.Search(indexModel.Sort,
                                           indexModel.Type.HasValue ? indexModel.Type.Value : -1,
                                           indexModel.AssignTo,
                                           indexModel.CityId.HasValue ? indexModel.CityId.Value : -1,
                                           indexModel.DistrictId.HasValue ? indexModel.DistrictId.Value : -1,
                                           indexModel.StartDate,
                                           indexModel.EndDate,
                                           indexModel.FilterKeyword ?? string.Empty,
                                           indexModel.PageIndex,
                                           indexModel.PageSize,
                                           indexModel.Creator,
                                           indexModel.PostingAssistant,
                                           indexModel.PostingNumberFrom,
                                           indexModel.PostingNumberTo,
                                           isActive,
                                           indexModel.ActiveStatusInMonth,
                                           //hasPermit,
                                           out total);
                    }
                }
                indexModel.ListCustomer = lstCus;
                pager.CurrentPage = indexModel.PageIndex;
                pager.PageSize = indexModel.PageSize;
                pager.TotalItem = total;
            }

            var lstAccountHasPermissionResetPass = ConfigurationManager.AppSettings["AccountResetCustomerPassword"].ToString().Split(',').Select(o=>o.ToLower()).ToList();
            ViewBag.IsResetPassword = UserContext.HasRole(RoleSystem.Admin) || lstAccountHasPermissionResetPass.Contains(UserContext.UserName.ToLower());
            ViewBag.Pager = pager;
            return View(indexModel);
        }
        [FilterAuthorize]
        private ActionResult ExportExcel(CustomerIndexModel indexModel)
        {
            using (var package = new ExcelPackage())
            {
                #region Dựng template cho file excel
                var worksheet = package.Workbook.Worksheets.Add("CUSTOMERS");
                worksheet.Cells["A1:A4"].Merge = true;
                worksheet.Cells["B1:B4"].Merge = true;
                worksheet.Cells["B1:B4"].Value = "DAIVIET GROUP";
                worksheet.Cells["B1:B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B1:B4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                worksheet.Cells["B1:B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["B1:B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["B1:B4"].Style.Font.Bold = true;
                worksheet.Cells["B1:B4"].Style.Font.Size = 16;

                worksheet.Cells["C1:L4"].Merge = true;
                worksheet.Cells["C1:L4"].Value = "LIST OF CUSTOMERS";
                worksheet.Cells["C1:L4"].Style.Font.Bold = true;
                worksheet.Cells["C1:L4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["C1:L4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                worksheet.Cells["C1:L4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["C1:L4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["C1:L4"].Style.Font.Bold = true;
                worksheet.Cells["C1:L4"].Style.Font.Size = 16;

                worksheet.Cells["A5:A5"].Value = "#";
                worksheet.Cells["A5:A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["A5:A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A5:A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A5:A5"].Style.Font.Size = 14;

                worksheet.Cells["B5:B5"].Value = "Fullname";
                worksheet.Cells["B5:B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["B5:B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["B5:B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B5:B5"].Style.Font.Size = 14;

                worksheet.Cells["C5:C5"].Value = "Email";
                worksheet.Cells["C5:C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["C5:C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["C5:C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["C5:C5"].Style.Font.Size = 14;

                worksheet.Cells["D5:D5"].Value = "Phone number";
                worksheet.Cells["D5:D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["D5:D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["D5:D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["D5:D5"].Style.Font.Size = 14;

                worksheet.Cells["E5:E5"].Value = "City";
                worksheet.Cells["E5:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["E5:E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["E5:E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["E5:E5"].Style.Font.Size = 14;

                worksheet.Cells["F5:F5"].Value = "Number of posting news";
                worksheet.Cells["F5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["F5:F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["F5:F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["F5:F5"].Style.Font.Size = 14;

                worksheet.Cells["G5:G5"].Value = "Last modified date";
                worksheet.Cells["G5:G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["G5:G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["G5:G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["G5:G5"].Style.Font.Size = 14;

                worksheet.Cells["H5:H5"].Value = "Care date";
                worksheet.Cells["H5:H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["H5:H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["H5:H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["H5:H5"].Style.Font.Size = 14;

                worksheet.Cells["I5:I5"].Value = "Type of customer";
                worksheet.Cells["I5:I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["I5:I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["I5:I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["I5:I5"].Style.Font.Size = 14;

                worksheet.Cells["J5:J5"].Value = "Creator";
                worksheet.Cells["J5:J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["J5:J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["J5:J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["J5:J5"].Style.Font.Size = 14;

                worksheet.Cells["K5:K5"].Value = "Date of creation";
                worksheet.Cells["K5:K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["K5:K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["K5:K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["K5:K5"].Style.Font.Size = 14;

                worksheet.Cells["L5:L5"].Value = "Assign to";
                worksheet.Cells["L5:L5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["L5:L5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["L5:L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["L5:L5"].Style.Font.Size = 14;

                worksheet.Column(1).Width = 10;
                worksheet.Column(2).Width = 30;
                worksheet.Column(3).Width = 20;
                worksheet.Column(4).Width = 20;
                worksheet.Column(5).Width = 20;
                worksheet.Column(6).Width = 20;
                worksheet.Column(7).Width = 20;
                worksheet.Column(8).Width = 20;
                worksheet.Column(9).Width = 20;
                worksheet.Column(10).Width = 20;
                worksheet.Column(11).Width = 20;
                worksheet.Column(12).Width = 20;

                worksheet.Row(5).Height = 30;
                #endregion

                #region Lấy dữ liệu từ các điều kiện tìm kiếm
                var lstExportedCustomer  = new List<CustomerForIndex>();
                var hasPermit = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager, RoleSystem.Coordinate);
                var lstPhone = new List<string>();
                if (!string.IsNullOrEmpty(indexModel.FilterKeyword))
                {
                    indexModel.FilterKeyword = indexModel.FilterKeyword.Trim().ToLower();
                    lstPhone = Ultility.DetectPhoneNumber(indexModel.FilterKeyword);
                }
                var custLayer = new CustomerLayer();
                var pager = new Pager();
                if (lstPhone.Count > 0)
                {
                    string[] arrPhone = lstPhone.Select(p => p).ToArray();
                    var lstTempCus = custLayer.SelectByPhoneNumber(arrPhone);
                    var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin.GetHashCode(), RoleSystem.Manager.GetHashCode());
                    var usrLayer = new UserLayer();
                    foreach (var phone in lstPhone)
                    {
                        var lstTemp = lstTempCus.FindAll(t => t.PhoneNumber.Contains(phone));
                        if (lstTemp != null && lstTemp.Count > 0)
                        {
                            foreach (var temp in lstTemp)
                            {
                                if(string.IsNullOrEmpty(temp.AssignTo))
                                {
                                    lstExportedCustomer.Add(temp);
                                }
                                else
                                {
                                    var assigneeInfo = usrLayer.GetByUserName(temp.AssignTo);
                                    if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                    || lstRoledGroup.Contains(assigneeInfo.GroupId)
                                    || UserContext.UserName.Equals(assigneeInfo.UserName))
                                    {
                                        if (lstExportedCustomer.Find(c => c.Id == temp.Id) == null)
                                        {
                                            lstExportedCustomer.Add(temp);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    // TH hợp tìm tất cả
                    var isManager = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager);
                    var isAfterSale = UserContext.HasRole(RoleSystem.AfterSale);

                    var lstCus = new List<CustomerForIndex>();
                    if (string.IsNullOrEmpty(indexModel.AssignTo))
                    {
                        if (isManager)
                        {
                            indexModel.AssignTo = UserContext.UserName;
                            var lstSearchGroupId = new List<int>();
                            var groupLayer = new GroupLayer();
                            // Lấy ra danh sách mà nhân viên có quyền 
                            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
                            if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
                            {
                                lstSearchGroupId = groupLayer.GetAll().Select(o => o.Id).ToList();
                            }
                            else
                            {
                                lstSearchGroupId = groupLayer.GetAll().FindAll(o => lstRoledGroup.Contains(o.Id)).Select(o => o.Id).ToList();
                            }

                            lstExportedCustomer = custLayer.SearchAllToExport(indexModel.Sort,
                                                                           indexModel.Type.HasValue ? indexModel.Type.Value : -1,
                                                                           indexModel.AssignTo,
                                                                           lstSearchGroupId,
                                                                           indexModel.CityId.HasValue ? indexModel.CityId.Value : -1,
                                                                           indexModel.DistrictId.HasValue ? indexModel.DistrictId.Value : -1,
                                                                           indexModel.StartDate,
                                                                           indexModel.EndDate,
                                                                           indexModel.FilterKeyword ?? string.Empty,
                                                                           indexModel.Creator,
                                                                           indexModel.PostingAssistant,
                                                                           indexModel.PostingNumberFrom,
                                                                           indexModel.PostingNumberTo);
                        }
                    }
                    else
                    {
                        lstExportedCustomer = custLayer.SearchToExport(indexModel.Sort,
                                                                       indexModel.Type.HasValue ? indexModel.Type.Value : -1,
                                                                       indexModel.AssignTo,
                                                                       indexModel.CityId.HasValue ? indexModel.CityId.Value : -1,
                                                                       indexModel.DistrictId.HasValue ? indexModel.DistrictId.Value : -1,
                                                                       indexModel.StartDate,
                                                                       indexModel.EndDate,
                                                                       indexModel.FilterKeyword ?? string.Empty,
                                                                       indexModel.Creator,
                                                                       indexModel.PostingAssistant);
                    }
                    // Lấy ra các tài khoản adminid
                    //var syncBCRMAdminLayer = new SyncBcrmBxhNAdminLayer();
                    //var adminProductAppovedLayer = new AdminProductApprovedLayer();
                    //if (lstExportedCustomer.Count > 0)
                    //{
                    //    var lstCustomerId = lstExportedCustomer.Select(o => o.Id).ToList();
                    //    var lstSync = syncBCRMAdminLayer.GetByListCustomerId(string.Join(",", lstCustomerId));

                    //    var lstProductAppovedTotal = adminProductAppovedLayer.GetTotalProductAppovedByListMember(lstSync.Select(o => o.AccountId).ToList());
                    //    foreach (var item in lstExportedCustomer)
                    //    {
                    //        // Lấy ra tài khoản admin
                    //        var syncInfo = lstSync.Find(o => o.CustomerId == item.Id);
                    //        if (syncInfo != null)
                    //        {
                    //            var productAppovedTotalInfo = lstProductAppovedTotal.Find(o => o.AdminId == syncInfo.AccountId);
                    //            if (productAppovedTotalInfo != null)
                    //            {
                    //                item.PostingNews = productAppovedTotalInfo.ProductCount;
                    //            }
                    //        }
                    //    }
                    //}
                }
                #endregion

                #region Đẩy thông tin dữ liệu vào file excel
                var countFilledRecord = 1;
                for (var index = 0; index < lstExportedCustomer.Count; index++)
                {
                    worksheet.Cells[index + 6, 1].Value = countFilledRecord;
                    worksheet.Cells[index + 6, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[index + 6, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 1].Style.Font.Size = 12;

                    worksheet.Cells[index + 6, 2].Value = lstExportedCustomer[index].FullName;
                    worksheet.Cells[index + 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 2].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 2].Style.WrapText = true;

                    worksheet.Cells[index + 6, 3].Value = lstExportedCustomer[index].Email;
                    worksheet.Cells[index + 6, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 3].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 3].Style.WrapText = true;

                    worksheet.Cells[index + 6, 4].Value = lstExportedCustomer[index].PhoneNumber;
                    worksheet.Cells[index + 6, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 4].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 4].Style.WrapText = true;

                    worksheet.Cells[index + 6, 5].Value = lstExportedCustomer[index].CityName;
                    worksheet.Cells[index + 6, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 5].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 5].Style.WrapText = true;

                    worksheet.Cells[index + 6, 6].Value = lstExportedCustomer[index].PostingNumber;
                    worksheet.Cells[index + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 6].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 6].Style.WrapText = true;

                    worksheet.Cells[index + 6, 7].Value = lstExportedCustomer[index].LastModifiedDate == DateTime.MinValue ? "" : lstExportedCustomer[index].LastModifiedDate.ToString("dd/MM/yyyy");
                    worksheet.Cells[index + 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 7].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 7].Style.WrapText = true;

                    worksheet.Cells[index + 6, 8].Value = lstExportedCustomer[index].CareDate == DateTime.MinValue ? "" : lstExportedCustomer[index].CareDate.Value.ToString("dd/MM/yyyy") ;
                    worksheet.Cells[index + 6, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 8].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 8].Style.WrapText = true;

                    worksheet.Cells[index + 6, 9].Value = lstExportedCustomer[index].PostingType.ToPostingTypeName();
                    worksheet.Cells[index + 6, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 9].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 9].Style.WrapText = true;

                    worksheet.Cells[index + 6, 10].Value = lstExportedCustomer[index].CreatedBy;
                    worksheet.Cells[index + 6, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 10].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 10].Style.WrapText = true;

                    worksheet.Cells[index + 6, 11].Value = lstExportedCustomer[index].CreatedDate.ToString("dd/MM/yyyy");
                    worksheet.Cells[index + 6, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 11].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 11].Style.WrapText = true;

                    worksheet.Cells[index + 6, 12].Value = lstExportedCustomer[index].AssignTo;
                    worksheet.Cells[index + 6, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[index + 6, 12].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[index + 6, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[index + 6, 12].Style.Font.Size = 12;
                    worksheet.Cells[index + 6, 12].Style.WrapText = true;

                    countFilledRecord++;
                }

                using (var range = worksheet.Cells[1, 1, countFilledRecord + 4, 14])
                {
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.WrapText = true;
                    range.Style.Fill.BackgroundColor.SetColor(Color.White);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Top.Color.SetColor(Color.Black);
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Color.SetColor(Color.Black);
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Color.SetColor(Color.Black);
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }

                worksheet.Cells["A5:L5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A5:L5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#4d90fe"));
                worksheet.Cells["A5:L5"].Style.Font.Color.SetColor(Color.White);
                #endregion

                var stream = new MemoryStream();
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                package.SaveAs(stream);
                stream.Position = 0;
                return File(stream, contentType, "CUSTOMERS-" + DateTime.Now.ToString("yyyy-MM-dd--hh-mm-ss") + ".xlsx");
            }
        }
        [FilterAuthorize]
        public ActionResult Create()
        {
            var customer = new Customer();
            var cityLayer = new CityLayer();
            var regionLayer = new RegionLayer();

            var lstRegion = regionLayer.GetAll();
            ViewBag.ListCity = lstRegion.Count > 0 ? cityLayer.GetByRegionId(lstRegion.FirstOrDefault().Id) : new List<City>();
            ViewBag.ListRegion = lstRegion;
            ViewBag.CurrentUser = UserContext;
            return View(customer);
        }
        [HttpPost]
        [FilterAuthorize]
        [JsonModelBinder(ActionParameterType = typeof(CustomerRequest), ActionParameterName = "model")]
        public JsonResult CreateAction(CustomerRequest model)
        {
            var response = new Response();
            try
            {
                var profileLayer = new ProfileLayer();
                //Check tính hợp lệ của dữ liệu
                if(model.ListProfile.Count == 0)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "Invalid Phone Number. Please check again!";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                foreach(var phone in model.ListProfile)
                {
                    var pro = profileLayer.GetByPhoneNumber(phone.PhoneNumber);
                    if(pro.CustomerId > 0)
                    {
                        response.Code = SystemCode.NotValid;
                        response.Message = "Phonenumber "+ phone.PhoneNumber +" exist in other file customer. Please check again!";
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }

                var strPhone = "";
                //// Đồng bộ tất cả tài khoản
                var lstPhoneNumber = Ultility.DetectPhoneNumber(model.PhoneNumber);
                //// Tách ra số đầu tiên để chuyển sang admin làm tài khoản đăng nhập
                strPhone = lstPhoneNumber[0];

                // Kiểm tra xem có số điện thoại nào bị ẩn hay không nếu có
                // thì check xem lí do có phải là tài khoản này
                // đã nằm trong 
                var hiddenFromSaleLayer = new HiddenFromSaleLayer();
                var lstHidden = hiddenFromSaleLayer.GetByPhoneNumber(strPhone.ToLong(0));
                if (lstHidden.Count > 0)
                {
                    if (lstHidden.Find(o => o.ReasonType == HideFromLeadReason.PhoneOwnerNotWorkingThatShowroom.GetHashCode()) == null)
                    {
                        return Json(new Response()
                        {
                            Code = SystemCode.NotPermitted,
                            Message = "Phonenumber is hidden by another sale in leads . Please contact manager to support more !"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    #region Kiểm tra xem số điện thoại khai báo có nằm trong tập danh sách khách hàng crawler hay không
                    var apiCheckExistPhoneNumberResponse = CallAPICheckExistPhoneNumberInCrawlerCustomer(model.ListProfile[0].PhoneNumber);
                    if (apiCheckExistPhoneNumberResponse.Code != SystemCode.Success)
                    {

                        return Json(apiCheckExistPhoneNumberResponse, JsonRequestBehavior.DenyGet);
                    }
                    var resModel = JsonConvert.DeserializeObject<CrawlerCustomerIndexModel>((string)apiCheckExistPhoneNumberResponse.Data);
                    if (resModel.ListData.Count > 0)
                    {
                        return Json(new Response(SystemCode.NotPermitted, LanguageRes.Msg_Existed_Crawlerlist, null), JsonRequestBehavior.DenyGet);
                    }
                    #endregion
                }
                
                var cust = new Customer
                {
                    FullName = model.FullName,
                    PhoneNumber = model.PhoneNumber,
                    Status = ContactStatus.Accepted.GetHashCode(),
                    PostingType = model.PostingType,
                    Email= model.Email,
                    Description = model.Description,
                    RegionId = model.RegionId,
                    CityId = model.CityId,
                    DistrictId = model.DistrictId, 
                    AssignTo = UserContext.UserName,
                    CareDate = DateTime.Now,
                    ICareDate = DateTime.Now.ToInt32(),
                    StartCareDate = DateTime.Now,
                    CompanyId = 0,
                    CreatedBy = UserContext.UserName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = UserContext.UserName,
                    LastModifiedDate= DateTime.Now,
                    Address = string.IsNullOrEmpty(model.Address) ? "":model.Address 
                };
                // Trường hợp tạo mới showroom => gọi sang bên Admin BXH Indo để tạo mới tài khoản khách mời
                // Lấy id tương ứng với file khách hàng ở bên admin BXH
                #region gọi API sang bên Admin để đồng bộ tài khoản
                
                // Gọi api thêm mới tài khoản bên site
                var apiResponse = CallAPIAdminInsertUpdateAccount(0, cust.FullName, model.Email, strPhone, cust.Address, true);
                if (apiResponse.Code != SystemCode.Success)
                {
                    (new FailureLoggingCommon()).WriteLog(3, "Customer/CreateAction", JsonConvert.SerializeObject(model), UserContext.UserName, apiResponse.Message, DateTime.Now, "");
                    return Json(apiResponse, JsonRequestBehavior.DenyGet);
                }
                // Lấy thông tin tài khoản admin
                var adminAccountId = ((int)apiResponse.Data);
                #endregion
                
                // Tạo mới file khách mời
                var customerLayer = new CustomerLayer();
                var customerId = customerLayer.Create(cust);
                customerLayer.UpdateStatus(customerId, ContactStatus.Accepted.GetHashCode());

                foreach (var profile in model.ListProfile)
                {
                    profile.CustomerId = customerId;
                    profile.CreatedBy = UserContext.UserName;
                    profile.CreatedDate = DateTime.Now;
                    profile.Email = string.IsNullOrEmpty(profile.Email) ? " " : profile.Email;
                    profile.Avatar = string.IsNullOrEmpty(profile.Avatar) ? " " : profile.Avatar;
                    profileLayer.Create(profile);
                }

                // Lưu vào bảng syncbcrmbxhnadmin để đồng bộ
                var syncBcrmBxhNAdminLayer = new SyncBcrmBxhNAdminLayer();
                if (adminAccountId > 0)
                {
                    syncBcrmBxhNAdminLayer.Create(customerId, adminAccountId, cust.Email, strPhone);
                }
                // Lưu lịch sử thông tin thao tác khách hàng
                var customerHistoryLayer = new CustomerHistoryLayer();
                customerHistoryLayer.Create(new CustomerHistory()
                {
                    Action = "The customer is created",
                    OldValue = "",
                    NewValue = "",
                    Note = "",
                    CustomerId = customerId,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now
                });
                // Lưu thông tin lịch sử chăm sóc khách hàng
                var customerCareHistoryLayer = new CustomerCareHistoryLayer();
                var careId= customerCareHistoryLayer.Create(new CustomerCareHistory() 
                {
                    Note = ""
                   ,CustomerId = customerId
                   ,AdminId = adminAccountId
                   ,PhoningStatus = PhoningStatus.Success.GetHashCode()
                   ,ReceivedBy = UserContext.UserName
                   ,ReceivedDate     = DateTime.Now
                   ,SuccessContactDate = DateTime.Now
                   ,UserId = UserContext.Id
                });
                // Lưu thông tin tài khoản được liên hệ thành công bởi sale này
                customerCareHistoryLayer.UpdateSuccessContact(careId, PhoningStatus.Success.GetHashCode(), DateTime.Now);
                customerCareHistoryLayer.UpdateIsCared(careId, true);

                response.Code = SystemCode.Success;
                response.Message = "You insert a new customer info successfully!";
            }
            catch(Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult Edit()
        {
            var customer = new Customer();
            var cityLayer = new CityLayer();
            var regionLayer = new RegionLayer();

            var lstRegion = regionLayer.GetAll();
            ViewBag.ListCity = lstRegion.Count > 0 ? cityLayer.GetByRegionId(lstRegion.FirstOrDefault().Id) : new List<City>();
            ViewBag.ListRegion = lstRegion;
            ViewBag.CurrentUser = UserContext;
            return View(customer);
        }
        [FilterAuthorize]
        public JsonResult GetById(int customerId)
        {
            var response = new Response();
            try
            {
                var checkSumToken = ChecksumHelper.GenToken(new { Id = customerId, CurrUser = UserContext.UserName });
                var customerLayer = new CustomerLayer();
                var customerInfo = customerLayer.GetById(customerId);
                var customerAndChecksumToken = new CustomerEditAndTokenModel(customerInfo, checkSumToken);

                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = customerAndChecksumToken
                }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [JsonModelBinder(ActionParameterType = typeof(CustomerRequest), ActionParameterName = "model")]
        public JsonResult EditAction(CustomerRequest model)
        {
            var response = new Response();
            try
            {
                // Xác thực lại giá trị của dữ liệu truyền lên
                var validateToken = ChecksumHelper.ValidateToken(new { Id = model.Id, CurrUser = UserContext.UserName }, model.SignatureChecksum);
                if (!validateToken.IsValid)
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.NotPermitted,
                        Message = LanguageRes.Msg_NotPermitted
                    }, JsonRequestBehavior.DenyGet);
                }

                var customerLayer = new CustomerLayer();
                var oldCust = customerLayer.GetById(model.Id);
                if(oldCust.IsDeleted)
                {
                    return Json(new Response(SystemCode.Locked, "Customer File has blocked!",null), JsonRequestBehavior.DenyGet);
                }

                // Kiểm tra xem email của khách hàng đã tồn tại trưa
                model.Email = string.IsNullOrEmpty(model.Email) ? "" : model.Email.Trim().ToLower();
                if(string.IsNullOrEmpty(model.Email) )
                {
                    return Json(new Response(SystemCode.NotPermitted, "Email is required !",null), JsonRequestBehavior.DenyGet);
                }
                if(customerLayer.IsExistedEmail(model.Id,model.Email))
                {
                    return Json(new Response(SystemCode.NotPermitted, "Email is existed in another customer !",null), JsonRequestBehavior.DenyGet);
                }
                // Kiểm tra xem nếu số điện thoại trước kia là không có thì nhập
                // thông tin của số điện thoại mới
                if(!string.IsNullOrEmpty(oldCust.PhoneNumber))
                {
                    model.PhoneNumber = oldCust.PhoneNumber;
                }
                // Cập nhật lại dữ liệu bên Admin BXH
                //var lstPhone = Ultility.DetectPhoneNumber(oldCust.PhoneNumber);
                var lstPhone = Ultility.DetectPhoneNumber(model.PhoneNumber);
                // Lấy số điện thoại
                var strPhone = lstPhone[0];
                var profileLayer = new ProfileLayer();
                var pro = profileLayer.GetByPhoneNumber(strPhone);
                if (pro.CustomerId > 0 && pro.CustomerId != model.Id)
                {
                    return Json(new Response() 
                    {
                        Code = SystemCode.NotValid
                       ,Message = "Phonenumber " + strPhone + " exist in other file customer. Please check again!"
                    }, JsonRequestBehavior.DenyGet);
                }
                // Với quyền admin / manager mới được phép sửa thông tin email
                if(UserContext.HasRole(RoleSystem.Admin,RoleSystem.Manager))
                {
                    model.Email = string.IsNullOrEmpty(model.Email) ? "" : model.Email;
                }
                else
                {
                    model.Email = string.IsNullOrEmpty(oldCust.Email) ? "" : oldCust.Email;
                }
                var syncBcrmBxhNAdminLayer = new SyncBcrmBxhNAdminLayer();
                
                // Lấy thông tin tài khoản đồng bộ của bên admin    
                var syncModel = syncBcrmBxhNAdminLayer.GetByCustomerId(model.Id);
                if (syncModel != null)
                {
                    // Kiểm tra số điện thoại có bị thay đổi không
                    var isChangedPhone = false;
                    if(!syncModel.PhoneNumber.Equals(strPhone))
                    {
                        isChangedPhone = true;
                    }

                    var email = model.Email.ToLower().Trim();
                    var apiResponse = CallAPIAdminInsertUpdateAccount(syncModel.AccountId, model.FullName, email, strPhone, model.Address, isChangedPhone);
                    if (apiResponse.Code != SystemCode.Success)
                    {
                        return Json(apiResponse, JsonRequestBehavior.DenyGet);
                    }

                    // Kiểm tra xem email có bị thay đổi hay ko ?
                    var isChangedEmail = false;
                    if (!syncModel.Email.Equals(email))
                    {
                        isChangedEmail = true;
                    }
                    // Cập nhật lại bảng syncbcrmBXHNAdmin khi thay đổi thông tin email hoặc số điện thoại
                    if (isChangedEmail || isChangedPhone)
                    {
                        syncBcrmBxhNAdminLayer.Update(model.Id, model.Email, strPhone);
                    }
                }
                else {
                    // lower dữ liệu email trước khi đẩy sang bên admin
                    var email = model.Email.ToLower().Trim();
                    var apiResponse = CallAPIAdminInsertUpdateAccount(0, model.FullName, email, strPhone, model.Address, true);
                    if (apiResponse.Code != SystemCode.Success)
                    {
                        return Json(apiResponse, JsonRequestBehavior.DenyGet);
                    }
                    // Lưu thông tin id đồng bộ dữ liệu của bên admin với crm
                    syncBcrmBxhNAdminLayer.Create(model.Id, (int)apiResponse.Data, model.Email, strPhone);
                }
                
                // Cập nhật lại thông tin khách hàng
                var cust = new Customer()
                {
                    Id = model.Id,
                    FullName = model.FullName,
                    PhoneNumber = model.PhoneNumber,
                    PostingType = model.PostingType,
                    Email = model.Email,
                    Description = model.Description,
                    RegionId = model.RegionId,
                    CityId = model.CityId,
                    DistrictId = model.DistrictId,
                    LastModifiedBy = UserContext.UserName,
                    LastModifiedDate = DateTime.Now,
                    Address = string.IsNullOrEmpty(model.Address) ? "" : model.Address 
                };
                customerLayer.Update(cust);
                // Kiểm tra xem nếu số điện thoại trước kia là không có 
                // thì nhập thông tin của số điện thoại mới
                if(string.IsNullOrEmpty(oldCust.PhoneNumber))
                {
                    // Xóa các thông tin profile cũ đi
                    var lstOldProfile = profileLayer.GetByCustomer(model.Id);
                    foreach (var item in lstOldProfile)
                    {
                        profileLayer.Delete(item.Id);
                    }
                    foreach (var profile in model.ListProfile)
                    {
                        profile.CustomerId = model.Id;
                        profile.CreatedBy = UserContext.UserName;
                        profile.CreatedDate = DateTime.Now;
                        profile.Email  = string.IsNullOrEmpty(profile.Email) ? " " : profile.Email;
                        profile.Avatar = string.IsNullOrEmpty(profile.Avatar) ? " " : profile.Avatar;
                        profileLayer.Create(profile);
                    }
                }
                // TH thông tin cũ có số điện thoại thì hiện giờ không thay đổi thông tin số điện thoại

                // Lưu lịch sử cập nhật lại thông tin khách hàng
                var customerHistoryLayer = new CustomerHistoryLayer();
                var lstChanged = GetChangedCustomerInfo(oldCust, cust);
                foreach (var changeItem in lstChanged)
                {
                    customerHistoryLayer.Create(changeItem);
                }
                response.Code = SystemCode.Success;
                response.Message = "You updated customer info successfully!";
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult Block(int customerId)
        {
            var response = new Response();
            try
            {
                var customerLayer = new CustomerLayer();
                var customer = customerLayer.GetById(customerId);
                if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager) || customer.AssignTo.ToLower() == UserContext.UserName.ToLower())
                {
                    customerLayer.Block(customerId);
                    // Lưu lịch sử thông tin của người block khách hàng này
                    var customerHistoryLayer = new CustomerHistoryLayer();
                    customerHistoryLayer.Create(new CustomerHistory()
                    {
                        Action = "Blocking",
                        CustomerId = customerId,
                        UpdatedBy = UserContext.UserName,
                        UpdatedDate = DateTime.Now,
                        NewValue = "",
                        OldValue = "",
                        Note = ""
                    });
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }                
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult UnBlock(int customerId)
        {
            var response = new Response();
            try
            {
                var customerLayer = new CustomerLayer();
                var customer = customerLayer.GetById(customerId);
                if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager) || customer.AssignTo.ToLower() == UserContext.UserName.ToLower())
                {
                    customerLayer.UnBlock(customerId);
                    // Lưu lịch sử thông tin của người block khách hàng này
                    var customerHistoryLayer = new CustomerHistoryLayer();
                    customerHistoryLayer.Create(new CustomerHistory()
                    {
                        Action = "Unblocking",
                        CustomerId = customerId,
                        UpdatedBy = UserContext.UserName,
                        UpdatedDate = DateTime.Now,
                        NewValue = "",
                        OldValue = "",
                        Note = ""
                        //Note = string.Format("{0} has unblocked this customer !", UserContext.UserName)
                    });
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult GetSaleByGroupId(int groupId)
        {
            var groupLayer = new GroupLayer();
            var lstAllGroup = groupLayer.GetAll();
            var groupInfo = lstAllGroup.Find(o => o.Id == groupId);
            if (groupInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "The group you search is not found !"
                }, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra xem người dùng có quyền với nhóm này
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            if (!lstRoledGroup.Contains(DefaultSystemParameter.AllGroup) && !lstRoledGroup.Contains(groupId))
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "You don't have a permission with this group !"
                }, JsonRequestBehavior.AllowGet);
            }

            var userLayer = new UserLayer();
            var lstUser = userLayer.GetByGroupId(groupId);
            var userRoleLayer = new UserRoleGroupLayer();
            var lstSalers = new List<Users>();
            foreach (var item in lstUser)
            {
                var lstRole = userRoleLayer.GetRoleGroupByUserId(item.Id);
                // Kiểm tra xem người dùng có quyền after sale hay không
                if (lstRole.Find(o => o.RoleId == RoleSystem.Admin.GetHashCode()
                                    ||o.RoleId == RoleSystem.Manager.GetHashCode()
                                    ||o.RoleId == RoleSystem.Sale.GetHashCode() ) != null)
                {
                    lstSalers.Add(item);
                }
            }

            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = lstSalers
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult AssignToSaler(int customerId, string userName, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = customerId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            // Check authenticated user whether has admin,manager role or not
            if (!UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
            // Check whether saler is existed or not
            var userLayer = new UserLayer();
            var userInfo = userLayer.GetByUserName(userName);
            if (userInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "saler is assigned to this customer is not found!"
                }, JsonRequestBehavior.DenyGet);
            }

            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(customerId);
            customerLayer.UpdateAssignTo(customerId, userInfo.UserName, DateTime.Now, DateTime.Now.ToInt32());

            // Lưu lịch sử thông tin của người block khách hàng này
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Assigning to",
                CustomerId = customerId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                NewValue = userName,
                OldValue = customerInfo.AssignTo,
                Note = ""
            });

            var syncBCRMToAdminLayer = new SyncBcrmBxhNAdminLayer();
            var syncInfoModel = syncBCRMToAdminLayer.GetByCustomerId(customerId);

            var careHistoryLayer = new CustomerCareHistoryLayer();
            var lstCareHistory = careHistoryLayer.GetByCustomerId(customerId);
            var lstCaredInfo = lstCareHistory.FindAll(o => o.IsCared && o.StopCareDate == DateTime.MinValue);
            foreach (var item in lstCaredInfo)
            {
                careHistoryLayer.UpdateStopCareDate(item.Id, DateTime.Now.AddMilliseconds(-1));
            }
            
            var careHistoryId= careHistoryLayer.Create(new CustomerCareHistory()
            {
                AdminId = syncInfoModel.AccountId,
                CustomerId = customerId,
                PhoningStatus = PhoningStatus.Success.GetHashCode(),
                ReceivedBy = userInfo.UserName,
                ReceivedDate = DateTime.Now,
                UserId = userInfo.Id,
                Note = ""
            });
            careHistoryLayer.UpdateIsCared(careHistoryId, true);
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult Note(int id, string note, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = id, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(id);
            if (customerInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "Cannot find customer info !"
                }, JsonRequestBehavior.DenyGet);
            }
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Note",
                CustomerId = id,
                OldValue = "",
                NewValue = "",
                Note = note,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public ActionResult GetViewByCustomerId(int index, int customerId)
        {
            var custLayer = new CustomerLayer();
            var model = custLayer.SearchByCustomerId(customerId);
            ViewBag.Index = index;
            ViewBag.CurrentUser = UserContext;
            var lstAccountHasPermissionResetPass = ConfigurationManager.AppSettings["AccountResetCustomerPassword"].ToString().Split(',').Select(o => o.ToLower()).ToList();
            ViewBag.IsResetPassword = lstAccountHasPermissionResetPass.Contains(UserContext.UserName.ToLower());
            return View(model);
        }
        [FilterAuthorize]
        public JsonResult Blacklist(int customerId, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = customerId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            var customerLayer = new CustomerLayer();
            customerLayer.UpdateBlacklist(customerId, true);
            // Lưu lịch sử thông tin của người block khách hàng này
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Blacklisting",
                CustomerId = customerId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                NewValue = "",
                OldValue = "",
                Note = ""
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult UnBlacklist(int customerId, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = customerId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            var customerLayer = new CustomerLayer();
            customerLayer.UpdateBlacklist(customerId, false);

            // Lưu lịch sử thông tin của người block khách hàng này
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Unblacklisting",
                CustomerId = customerId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                NewValue = "",
                OldValue = "",
                Note = ""
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public ActionResult Detail(int customerId)
        {
            var customerLayer = new CustomerLayer();

            // Lấy thông tin khách hàng
            var customerInfo = customerLayer.GetById(customerId);
            if (customerInfo == null)
            {
                return View(new CustomerDetailModel());
            }
            // Lấy thông tin vùng
            var regionLayer = new RegionLayer();
            var regionInfo = regionLayer.GetById(customerInfo.RegionId);
            // Lấy thông tin thành phố
            var cityLayer = new CityLayer();
            var cityInfo = cityLayer.GetById(customerInfo.CityId);
            // Lấy thông tin quận huyện
            var districtLayer = new DistrictLayer();
            var districtInfo = districtLayer.GetById(customerInfo.DistrictId);
            

            var model = new CustomerDetailModel()
            {
                FullName = customerInfo.FullName,
                Address = customerInfo.Address,
                AssignTo = customerInfo.AssignTo,
                RegionName = regionInfo == null ? "" : regionInfo.Name,
                CityName = cityInfo == null ? "" : cityInfo.Name,
                DistrictName = districtInfo == null ? "" : districtInfo.Name,
                Description = customerInfo.Description,
                Email = customerInfo.Email,
                PhoneNumber = customerInfo.PhoneNumber,
                LastModifiedBy = customerInfo.LastModifiedBy,
                LastModifiedDate = customerInfo.LastModifiedDate,
                TypeOfCustomerName = (customerInfo.PostingType).ToPostingTypeName()
            };
            // Lấy thông tin lịch sử thao tác của khách hàng đó
            var customerHistoryLayer = new CustomerHistoryLayer();
            model.LstActivityHistory = customerHistoryLayer.GetByCustomerId(customerId).OrderByDescending(o => o.UpdatedDate).ToList();
            // Lấy thông tin chăm sóc khách hàng này
            var customerCareHistoryLayer = new CustomerCareHistoryLayer();
            var lstCareHistory = customerCareHistoryLayer.GetByCustomerId(customerId);
            // Tính số lần từ chối của khách hàng này
            model.SumRejectionTimes = lstCareHistory.FindAll(o => o.PhoningStatus == PhoningStatus.Failure.GetHashCode()).Count;
            return View(model);
        }
        [FilterAuthorize]
        public JsonResult GetAfterSaleByGroupId(int groupId)
        {
            var groupLayer = new GroupLayer();
            var lstAllGroup = groupLayer.GetAll();
            var groupInfo = lstAllGroup.Find(o => o.Id == groupId);
            if (groupInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "The group you search is not found !"
                }, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra xem người dùng có quyền với nhóm này
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            if (!lstRoledGroup.Contains(DefaultSystemParameter.AllGroup) && !lstRoledGroup.Contains(groupId))
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "You don't have a permission with this group !"
                }, JsonRequestBehavior.AllowGet);
            }

            var userLayer = new UserLayer();
            var lstUser = userLayer.GetByGroupId(groupId);

            var userRoleLayer = new UserRoleGroupLayer();
            var lstAfterSale = new List<Users>();
            foreach (var item in lstUser)
            {
                var lstRole=  userRoleLayer.GetRoleGroupByUserId(item.Id);
                // Kiểm tra xem người dùng có quyền after sale hay không
                if(lstRole.Find(o=>o.RoleId== RoleSystem.AfterSale.GetHashCode()) != null)
                {
                    lstAfterSale.Add(item);
                }
            }

            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = lstAfterSale
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult AssignToAfterSaler(int customerId, string userName, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = customerId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            // Check authenticated user whether has admin,manager role or not
            if (!UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
            // Check whether saler is existed or not
            var userLayer = new UserLayer();
            var userInfo = userLayer.GetByUserName(userName);
            if (userInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "after-saler is assigned to this customer is not found!"
                }, JsonRequestBehavior.DenyGet);
            }
            // Lưu lịch sử nhận lấy tin rao của after sale
            var postingAssistanceHistoryLayer = new PostingAssistanceHistoryLayer();
            // Lấy lịch sử các after sale đã nhận hỗ trợ khách hàng này
            var lstAfterSaleHistory = postingAssistanceHistoryLayer.GetByCustomerId(customerId);
            // Lấy ra các after sale vẫn đang nhận chăm sóc khách hàng này
            var lstCaringAfterSaleHistory = lstAfterSaleHistory.FindAll(o => o.ReturnDate == null || o.ReturnDate == DateTime.MinValue);

            foreach (var item in lstCaringAfterSaleHistory)
            {
                postingAssistanceHistoryLayer.UpdateReturnDate(item.Id, DateTime.Now.AddSeconds(-1));
            }

            postingAssistanceHistoryLayer.Create(userInfo.Id, DateTime.Now, customerId, UserContext.UserName, DateTime.Now);

            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(customerId);
            customerLayer.UpdatePostingAssistant(customerId, userInfo.UserName);
            // Lưu lịch sử thông tin của người block khách hàng này
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Assigning to after-sale",
                CustomerId = customerId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                NewValue = userName,
                OldValue = string.IsNullOrEmpty(customerInfo.PostingAssistant) ? "" : customerInfo.PostingAssistant,
                Note = ""
            });
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        
        [FilterAuthorize]
        public ActionResult ViewRequestRetrievePostingHistory(int customerId)
        {
            var requestRetrievePostingDL = new RequestRetrievePostingDL();
            var lstRequestHistory = requestRetrievePostingDL.GetByCustomerId(customerId);
            lstRequestHistory = lstRequestHistory.OrderByDescending(o => o.CreatedDate).ToList();
            ViewBag.CurrUser = UserContext;
            return View(lstRequestHistory);
        }
        [FilterAuthorize]
        public JsonResult AddRequestRetrievePosting(int customerId,int requestId,  int postingNews)
        {
            var requestRetrievePostingDL = new RequestRetrievePostingDL();
            var lstPendingRequest = requestRetrievePostingDL.GetByCustomerId(customerId);
            var customerLayer = new CustomerLayer();
            if(requestId == 0)
            {
                // Kiểm tra xem đã có yêu cầu nào đang pending chưa
                if(lstPendingRequest.Find(o=>o.Status == RequestRetrievePostingStatus.Pending.GetHashCode()) != null)
                {
                    return Json(new Response() 
                    {
                        Code = SystemCode.NotPermitted
                       ,Message = "There is a pending request for this customer !"
                    }, JsonRequestBehavior.DenyGet);
                }
                requestRetrievePostingDL.Create(customerId, RequestRetrievePostingStatus.Pending.GetHashCode(), postingNews, UserContext.UserName, DateTime.Now);
                customerLayer.UpdatePostingAssistanceStatus(customerId, RequestRetrievePostingStatus.Pending.GetHashCode(),postingNews);
            }
            else
            {
                requestRetrievePostingDL.Update(requestId, postingNews, UserContext.UserName, DateTime.Now);
                customerLayer.UpdatePostingAssistanceStatus(customerId, RequestRetrievePostingStatus.Pending.GetHashCode(), postingNews);
            }
            //customerLayer.UpdatePostingAssistanceStatus(customerId, RequestRetrievePostingStatus.Pending.GetHashCode());
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = "Successfully"
            }, JsonRequestBehavior.DenyGet );
        }
        [FilterAuthorize]
        public JsonResult RemoveRequestRetrievePosting(int customerId, int requestId)
        {
            var requestRetrievePostingDL = new RequestRetrievePostingDL();
            var requestInfo = requestRetrievePostingDL.GetById(requestId);
            if(requestInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull
                   ,Message = "The information is not found !"
                }, JsonRequestBehavior.DenyGet);
            }

            if (requestInfo.Status == RequestRetrievePostingStatus.Done.GetHashCode())
            {
                return Json(new Response() 
                {
                    Code    = SystemCode.NotPermitted
                   ,Message = "You don't have a permission to process this action"
                }, JsonRequestBehavior.DenyGet);
            }
            requestRetrievePostingDL.Delete(requestId);

            var customerLayer = new CustomerLayer();
            customerLayer.UpdatePostingAssistanceStatus(customerId, RequestRetrievePostingStatus.None.GetHashCode(),0);

            return Json(new Response()
            {
                Code = SystemCode.Success
               ,Message = "Successfully"
               ,Data = requestInfo.CustomerId
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult ReceiveGetListing(int customerId, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = customerId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            // Check authenticated user whether has admin,manager role or not
            if (!UserContext.HasRole(RoleSystem.AfterSale))
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
            // Check whether saler is existed or not
            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(customerId);
            if(!string.IsNullOrEmpty(customerInfo.PostingAssistant))
            {
                return Json(new Response() 
                {
                    Code = SystemCode.NotPermitted
                   ,Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            // Lưu lịch sử nhận lấy tin rao của after sale
            var postingAssistanceHistoryLayer = new PostingAssistanceHistoryLayer();
            // Lấy lịch sử các after sale đã nhận hỗ trợ khách hàng này
            var lstAfterSaleHistory = postingAssistanceHistoryLayer.GetByCustomerId(customerId);
            // Lấy ra các after sale vẫn đang nhận chăm sóc khách hàng này
            var lstCaringAfterSaleHistory = lstAfterSaleHistory.FindAll(o => o.ReturnDate == null || o.ReturnDate == DateTime.MinValue);
            foreach (var item in lstCaringAfterSaleHistory)
            {
                postingAssistanceHistoryLayer.UpdateReturnDate(item.Id, DateTime.Now.AddSeconds(-1));
            }
            postingAssistanceHistoryLayer.Create(UserContext.Id, DateTime.Now, customerId, UserContext.UserName, DateTime.Now);
            customerLayer.UpdatePostingAssistant(customerId, UserContext.UserName);
            // Lưu lịch sử thông tin của người block khách hàng này
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "After-sale receive customer",
                CustomerId = customerId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                NewValue = UserContext.UserName,
                OldValue = string.IsNullOrEmpty(customerInfo.PostingAssistant) ? "" : customerInfo.PostingAssistant,
                Note = ""
            });
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult ResetPassword(int customerId, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = customerId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            var syncBL = new SyncBcrmBxhNAdminLayer();
            var syncAdminCrmInfo = syncBL.GetByCustomerId(customerId);
            // Gọi API
            var apiResponse = CallAPIResetPassword(syncAdminCrmInfo.AccountId);
            if (apiResponse == null || apiResponse.Code != SystemCode.Success)
            {
                return Json(apiResponse, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Message = "Reset password successfully . Current customer password 's  123456 !"
                }, JsonRequestBehavior.DenyGet);
            }
        }
        [FilterAuthorize]
        public JsonResult GetTokenKeyForOneRequest(int id)
        {
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = ChecksumHelper.GenToken(new { Id = id, CurrUser = UserContext.UserName }, ConfigurationManager.AppSettings["TimeExpiredTokenForConfirmRequest"].ToString().ToInt(1))
            }, JsonRequestBehavior.AllowGet);
        }

        [FilterAuthorize]
        public JsonResult GetTokenKeyForGeneralView(int id)
        {
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = ChecksumHelper.GenToken(new { Id = id, CurrUser = UserContext.UserName }, ConfigurationManager.AppSettings["API_CheckSum_ExpiredInMinute"].ToString().ToInt(15))
            }, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public Response CallAPIAdminInsertUpdateAccount(long adminId, string fullName, string email, string phoneNumber, string address, bool isChangePhone)
        {
            // Gọi api thêm mới thông tin tài khoản khách hàng trên site
            var url = string.Format("{0}/BCRMService.svc/UpdateCustomerInfo", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var token = Security.CreateKey();

            var data = new
            {
                Token = Security.CreateKey(),
                AdminId = adminId,
                FullName = fullName,
                Email = string.IsNullOrEmpty(email) ? "" : email.ToLower().Trim(),
                PhoneNumber = phoneNumber,
                Address = address,
                IsChangePhoneNumber = isChangePhone
            };
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                return new Response()
                {
                    Code = SystemCode.ErrorConnect,
                    Message = "Error in process , Contact IT for detail !"
                };
            }
            var hrmResponse = JsonConvert.DeserializeObject<Response>(res);
            return hrmResponse;
        }
        [NonAction]
        public List<CustomerHistory> GetChangedCustomerInfo(Customer oldCustomerInfo, Customer newCustomerInfo)
        {
            var lstChanged = new List<CustomerHistory>();
            // Kiểm tra có thay đổi tên khách hàng hay không
            if (!oldCustomerInfo.FullName.Equals(newCustomerInfo.FullName))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Fullname",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.FullName,
                    NewValue = newCustomerInfo.FullName,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi số điện thoại hay không
            if (!oldCustomerInfo.PhoneNumber.Equals(newCustomerInfo.PhoneNumber))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Phone number",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.PhoneNumber,
                    NewValue = newCustomerInfo.PhoneNumber,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi email hay không
            if (!oldCustomerInfo.Email.Equals(newCustomerInfo.Email))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Email",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.Email,
                    NewValue = newCustomerInfo.Email,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi loại khách hàng hay không
            if (!oldCustomerInfo.PostingType.Equals(newCustomerInfo.PostingType))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Type of customer",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = (oldCustomerInfo.PostingType).ToPostingTypeName(),
                    NewValue = (newCustomerInfo.PostingType).ToPostingTypeName(),
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }

            // Kiểm tra có thay đổi vùng miền hay không
            if (!oldCustomerInfo.RegionId.Equals(newCustomerInfo.RegionId))
            {
                var regionLayer = new RegionLayer();
                var oldRegion = regionLayer.GetById(oldCustomerInfo.RegionId);
                var oldRegionName = oldRegion == null ? "" : oldRegion.Name;
                var newRegion = regionLayer.GetById(newCustomerInfo.RegionId);
                var newRegionName = newRegion == null ? "" : newRegion.Name;

                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Region",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldRegionName,
                    NewValue = newRegionName,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }

            // Kiểm tra có thay đổi thành phố hay không
            if (!oldCustomerInfo.CityId.Equals(newCustomerInfo.CityId))
            {
                var cityLayer = new CityLayer();
                var oldCity = cityLayer.GetById(oldCustomerInfo.CityId);
                var oldCityName = oldCity == null ? "" : oldCity.Name;
                var newCity = cityLayer.GetById(newCustomerInfo.CityId);
                var newCityName = newCity == null ? "" : newCity.Name;

                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Province/City",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCityName,
                    NewValue = newCityName,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }

            // Kiểm tra có thay đổi quận huyện hay không
            if (!oldCustomerInfo.DistrictId.Equals(newCustomerInfo.DistrictId))
            {
                var districtLayer = new DistrictLayer();
                var oldDistrict = districtLayer.GetById(oldCustomerInfo.DistrictId);
                var oldDistrictName = oldDistrict == null ? "" : oldDistrict.Name;
                var newDistrict = districtLayer.GetById(newCustomerInfo.DistrictId);
                var newDistrictName = newDistrict == null ? "" : newDistrict.Name;

                lstChanged.Add(new CustomerHistory()
                {
                    Action = "District",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldDistrictName,
                    NewValue = newDistrictName,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            // Kiểm tra có thay đổi thông tin địa chỉ hay không
            if (!oldCustomerInfo.Address.Equals(newCustomerInfo.Address))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Address",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.Address,
                    NewValue = newCustomerInfo.Address,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }

            // Kiểm tra có thay đổi thông tin miêu tả hay không
            if (!oldCustomerInfo.Description.Equals(newCustomerInfo.Description))
            {
                lstChanged.Add(new CustomerHistory()
                {
                    Action = "Description",
                    CustomerId = oldCustomerInfo.Id,
                    OldValue = oldCustomerInfo.Description,
                    NewValue = newCustomerInfo.Description,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Note = ""
                });
            }
            return lstChanged;
        }
        [NonAction]
        public Response CallAPICheckExistPhoneNumberInCrawlerCustomer(string phoneNumber)
        {
            string nullString = null;
            // Kiểm tra thông tin số điện thoại có tồn tại trong các khoản crawler hay không
            var url = string.Format("{0}/BCRMService.svc/GetListCrawlerCustomer", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = JsonConvert.SerializeObject(new
            {
                Token = Security.CreateKey(),
                FilterKeyword = phoneNumber,
                Status = -1,
                PageIndex = 1,
                PageSize = 25,
                DateOfCreationFrom = nullString,
                DateOfCreationTo = nullString,
                TotalRecord = 0,
                ListData = new List<CrawlerCustomerSearchModel>(),
                ListIgnoreAdminId = new List<long>(),
                ListFilterAdminId = new List<long>()
            });
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, data, "application/json");
            var result = JsonConvert.DeserializeObject<Response>(res);
            return result;
        }
        [NonAction]
        public Response CallAPICheckExistEmailInCrawlerCustomer(string email)
        {
            string nullString = null;
            // Kiểm tra thông tin số điện thoại có tồn tại trong các khoản crawler hay không
            var url = string.Format("{0}/BCRMService.svc/GetListCrawlerCustomer", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = JsonConvert.SerializeObject(new
            {
                Token = Security.CreateKey(),
                FilterKeyword = email,
                Status = -1,
                PageIndex = 1,
                PageSize = 25,
                DateOfCreationFrom = nullString,
                DateOfCreationTo = nullString,
                TotalRecord = 0,
                ListData = new List<CrawlerCustomerSearchModel>(),
                ListIgnoreAdminId = new List<long>(),
                ListFilterAdminId = new List<long>()
            });
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, data, "application/json");
            var result = JsonConvert.DeserializeObject<Response>(res);
            return result;
        }
        [NonAction]
        public Response CallAPIResetPassword(int adminId)
        {
            // Kiểm tra thông tin số điện thoại có tồn tại trong các khoản crawler hay không
            var url = string.Format("{0}/BCRMService.svc/ResetPassword"
                            , ConfigurationManager.AppSettings["AdminBXHAPI"].ToString() );
            var data = new
            {
                Token = Security.CreateKey(),
                AdminId = adminId
            };
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");
            var result = JsonConvert.DeserializeObject<Response>(res);

            if (result == null)
            {
                return new Response()
                {
                    Code = SystemCode.ErrorConnect,
                    Message = "There is a trouble connect to Admin API . Contact IT for more detail !"
                };
            }
            else
            {
                return result;
            }
        }

    }
}
