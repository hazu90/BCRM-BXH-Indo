﻿using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Language;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class NonSaleCustomerController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            var indexModel = new NonSaleCustomerIndexModel();
            var groupLayer = new GroupLayer();
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            
            var allGroup = groupLayer.GetAll();
            if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
                {
                    indexModel.LstGroup = allGroup;
                }
                else
                {
                    indexModel.LstGroup = allGroup.FindAll(o => lstRoledGroup.Contains(o.Id));
                }
            }

            var groupIds = "";
            if (!lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
            {
                groupIds = string.Join(",", lstRoledGroup);
            }
            else
            {
                groupIds = string.Join(",", allGroup.Select(o => o.Id).ToList());
            }

            var userLayer = new UserLayer();
            var lstUser = string.IsNullOrEmpty(groupIds) ? 
                        new List<UserSearchModel>() { 
                            new UserSearchModel() { 
                                UserName = UserContext.UserName 
                               ,Id = UserContext.Id } } : userLayer.GetList(groupIds);

            // Lấy ra danh sách các saler
            // Lấy ra danh sách các after-saler
            var lstSaler = new List<UserSearchModel>();
            var userRoleGroupLayer = new UserRoleGroupLayer();
            foreach (var item in lstUser)
            {
                // Lấy role của người dùng
                var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                if (roles.Find(o => o.RoleId == RoleSystem.Admin.GetHashCode()
                               || o.RoleId == RoleSystem.Manager.GetHashCode()
                               || o.RoleId == RoleSystem.Sale.GetHashCode()) != null)
                {
                    lstSaler.Add(item);
                }
            }

            indexModel.SetLstReceiver(lstSaler);
            ViewBag.AssignTo = UserContext.UserName;
            return View(indexModel);
        }
        [FilterAuthorize]
        public ActionResult Search(NonSaleCustomerIndexModel model)
        {
            var lstPhone = new List<string>();
            if (!string.IsNullOrEmpty(model.FilterKeyword))
            {
                model.FilterKeyword = model.FilterKeyword.Trim();
                lstPhone = Ultility.DetectPhoneNumber(model.FilterKeyword);
            }
            var result = new List<NonSaleCustomerSearchModel>();
            var custLayer = new CustomerLayer();
            var custCareLayer = new CustomerCareHistoryLayer();
            var pager = new Pager();
            var totalRecord = 0;
            if (lstPhone.Count > 0)
            {
                var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin.GetHashCode(), RoleSystem.Manager.GetHashCode());
                var usrLayer = new UserLayer();
                string[] arrPhone = lstPhone.Select(p => p).ToArray();
                var lstCus = new List<CustomerForIndex>();
                // Tìm tất cả các khách hàng chưa liên hệ thành công
                var lstTempCus = custLayer.SelectByPhoneNumber(arrPhone).FindAll(p => p.Status != ContactStatus.Accepted.GetHashCode());
                var lstCusInSearch = new List<NonSaleCustomerSearchModel>();
                foreach (var phone in lstPhone)
                {
                    var lstTemp = lstTempCus.FindAll(t => t.PhoneNumber.Contains(phone));
                    if (lstTemp != null && lstTemp.Count > 0)
                    {
                        foreach (var temp in lstTemp)
                        {
                            if (lstCusInSearch.Find(c => c.Id == temp.Id) == null)
                            {
                                lstCusInSearch.Add(new NonSaleCustomerSearchModel()
                                {
                                    FullName = temp.FullName
                                    ,IsDeleted = temp.IsDeleted
                                    ,Email = temp.Email
                                    ,Address = temp.Address
                                    ,Status = temp.Status
                                    ,PhoneNumber = temp.PhoneNumber
                                    ,Id = temp.Id
                                    ,CreatedDate = temp.CreatedDate
                                });
                            }
                        }
                    }
                }

                var beforeDate = DateTime.Now.AddDays(-1 * DefaultSystemParameter.DaysOverContactDeadline.GetHashCode());
                foreach (var item in lstCusInSearch)
                {
                    var lstCareInfo = custCareLayer.GetByCustomerId(item.Id);
                    if(lstCareInfo.Count >0)
                    {
                        var lastCareInfo = lstCareInfo.Find(o => (o.PhoningStatus == PhoningStatus.NoContact.GetHashCode() && o.ReceivedDate >= beforeDate)
                                                                    || (o.PhoningStatus == PhoningStatus.Failure.GetHashCode() && o.WaitingOverReject >= DateTime.Now)
                                                                    || o.PhoningStatus == PhoningStatus.Success.GetHashCode());

                        if(lastCareInfo != null)
                        {
                            var receiverInfo = usrLayer.GetByUserName(lastCareInfo.ReceivedBy);
                            if(receiverInfo != null)
                            {
                                if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                || lstRoledGroup.Contains(receiverInfo.GroupId)
                                || UserContext.UserName.Equals(receiverInfo.UserName))
                                {
                                    item.CustomerCareId = lastCareInfo.Id;
                                    item.ReceivedBy = lastCareInfo.ReceivedBy;
                                    if (result.Find(c => c.Id == item.Id) == null)
                                    {
                                        result.Add(item);
                                    }
                                }
                            }
                        }
                        else
                        {
                            result.Add(item);
                        }
                    }
                    else
                    {
                        result.Add(item);
                    }
                }
                 
                // Lấy ra tổng số bản ghi
               totalRecord = result.Count;
            }
            else
            {
                var customerLayer = new CustomerLayer();
                if(model.Receiver.Equals("-1000"))
                {
                    var groupLayer = new GroupLayer();
                    var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
                    if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
                    {
                        lstRoledGroup = groupLayer.GetAll().Select(o => o.Id).ToList();
                    }
                    model.Receiver = UserContext.UserName;
                    result = customerLayer.Nonsale_GetAll(model.FilterKeyword, model.Receiver,lstRoledGroup, model.DateOfCreationFrom, model.DateOfCreationTo, model.Status, DateTime.Now, model.PageIndex, DefaultSystemParameter.PageSize, out totalRecord);
                }
                else
                {
                    result = customerLayer.Nonsale_GetList(model.FilterKeyword, model.Receiver, model.DateOfCreationFrom, model.DateOfCreationTo, model.Status, DateTime.Now, model.PageIndex, DefaultSystemParameter.PageSize, out totalRecord);
                }
            }
            
            // Lấy ra các tài khoản adminid
            var syncBCRMAdminLayer = new SyncBcrmBxhNAdminLayer();
            var adminProductAppovedLayer = new AdminProductApprovedLayer();
            if(result.Count >0)
            {
                var lstCustomerId = result.Select(o => o.Id).ToList();
                var lstSync = syncBCRMAdminLayer.GetByListCustomerId(string.Join(",", lstCustomerId));
                
                var lstProductAppovedTotal = adminProductAppovedLayer.GetTotalProductAppovedByListMember(lstSync.Select(o => o.AccountId).ToList());
                foreach (var item in result)
                {
                    // Lấy ra tài khoản admin
                    var syncInfo = lstSync.Find(o=>o.CustomerId == item.Id);
                    if(syncInfo != null)
                    {
                        var productAppovedTotalInfo = lstProductAppovedTotal.Find(o => o.AdminId == syncInfo.AccountId);
                        if(productAppovedTotalInfo != null)
                        {
                            item.PostingNews = productAppovedTotalInfo.ProductCount;
                        }
                    }
                }
            }
            
            // Bổ sung API lấy số tin rao đang có của khách hàng
            ViewBag.Pager = new Pager()
            {
                ActionScript = "non_sale_customer_index.search"
               ,CurrentPage = model.PageIndex
               ,PageSize = DefaultSystemParameter.PageSize
               ,TotalItem = totalRecord
            };
            ViewBag.CurrentUser = UserContext;
            return View(result);
        }
        [FilterAuthorize]
        public JsonResult Receive(int customerId)
        {
            var customerCareHistoryLayer = new CustomerCareHistoryLayer();
            var lstCareHistory = customerCareHistoryLayer.GetByCustomerId(customerId);
            //Kiểm tra xem khách hàng này đã được tạo tài khoản thực hay chưa
            var realAccount = lstCareHistory.Find(o => o.PhoningStatus == PhoningStatus.Success.GetHashCode());
            if (realAccount != null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "This customer is cared and create real account !"
                }, JsonRequestBehavior.DenyGet);
            }

            // Kiểm tra xem khách hàng này đã hết hạn liên lạc đối với sale 
            // mà nhận khách này từ trước hay chưa
            var notOverContactDeadline = lstCareHistory.Find(o => o.PhoningStatus == PhoningStatus.NoContact.GetHashCode()
                                                            && o.ReceivedDate.AddDays(DefaultSystemParameter.DaysOverContactDeadline) > DateTime.Now);
            if (notOverContactDeadline != null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "This customer is received by another saler !"
                }, JsonRequestBehavior.DenyGet);
            }
            // Kiểm tra xem hiện giờ đã quá thời gian mà khách hàng từ chối

            var notEndAfterRejected = lstCareHistory.Find(o => o.PhoningStatus == PhoningStatus.Failure.GetHashCode()
                                                      && o.WaitingOverReject > DateTime.Now);

            if (notEndAfterRejected != null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "This customer is rejected before and not yet over this time !"
                }, JsonRequestBehavior.DenyGet);
            }

            // Lấy thông tin adminid của tài khoản
            var syncBCRMBxhAdminLayer = new SyncBcrmBxhNAdminLayer();
            var syncInfo = syncBCRMBxhAdminLayer.GetByCustomerId(customerId);

            // Lưu lịch sử chăm sóc khách hàng
            customerCareHistoryLayer.Create(new CustomerCareHistory()
            {
                AdminId = (syncInfo == null ? 0 : syncInfo.AccountId),
                CrawlerAdminId = 0,
                CustomerId = customerId,
                Note = "",
                PhoningStatus = PhoningStatus.NoContact.GetHashCode(),
                ReceivedBy = UserContext.UserName,
                ReceivedDate = DateTime.Now,
                UserId = UserContext.Id
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult ContactSuccess(int customerId,int careHistoryId)
        {
            var careHistoryLayer = new CustomerCareHistoryLayer();
            var careInfo = careHistoryLayer.GetById(careHistoryId);
            if(!UserContext.UserName.Equals(careInfo.ReceivedBy))
            {
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Message = "You don't have a permission to process this action !"
                }, JsonRequestBehavior.DenyGet);
            }

            careHistoryLayer.UpdateSuccessContact(careHistoryId, PhoningStatus.Success.GetHashCode(), DateTime.Now);
            careHistoryLayer.UpdateIsCared(careHistoryId, true);
            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(customerId);
            // Cập nhật lại trạng thái liên hệ khách hàng
            customerLayer.UpdateStatus(customerId, ContactStatus.Accepted.GetHashCode());
            // Cập nhật lại thông tin giao nhận khách hàng
            customerLayer.UpdateAssignTo(customerId, UserContext.UserName, DateTime.Now, DateTime.Now.ToInt32());
            customerLayer.UpdateLastModifidation(customerId, UserContext.UserName, DateTime.Now);

            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Contact Status",
                CustomerId = customerId,
                OldValue = customerInfo.Status.ToContactStatusName(),
                NewValue = ContactStatus.Accepted.GetHashCode().ToContactStatusName(),
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                Note = ""
            });
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Assign to",
                CustomerId = customerId,
                OldValue = customerInfo.AssignTo,
                NewValue = UserContext.UserName,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                Note = ""
            });

            return Json(new Response() 
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult ContactFailure(int customerId, int careHistoryId)
        {
            var careHistoryLayer = new CustomerCareHistoryLayer();
            var careInfo = careHistoryLayer.GetById(careHistoryId);
            if (!UserContext.UserName.Equals(careInfo.ReceivedBy))
            {
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Message = "You don't have a permission to process this action !"
                }, JsonRequestBehavior.DenyGet);
            }
            careHistoryLayer.UpdateFailureContact(careHistoryId, PhoningStatus.Failure.GetHashCode(), DateTime.Now.AddDays(DefaultSystemParameter.DaysWaitingAfterRejecting));
            var customerLayer = new CustomerLayer();
            var customerInfo = customerLayer.GetById(customerId);
            customerLayer.UpdateStatus(customerId, ContactStatus.Refused.GetHashCode());
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "Contact Status",
                CustomerId = customerId,
                OldValue = customerInfo.Status.ToContactStatusName(),
                NewValue = ContactStatus.Refused.GetHashCode().ToContactStatusName(),
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                Note = ""
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
    }
}
