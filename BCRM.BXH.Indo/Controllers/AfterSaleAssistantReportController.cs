﻿using BCRM.BXH.Indo.Common;
using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class AfterSaleAssistantReportController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            // Lấy ra danh sách mà nhân viên có quyền 
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            var groupLayer = new GroupLayer();
            var isSearchAll = false;
            var lstGroup = new List<Group>();
            var lstUsers = new List<UserSearchModel>();
            if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                isSearchAll = true;
                var allGroups = groupLayer.GetAll();
                lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                var userLayer = new UserLayer();
                lstUsers = userLayer.GetList(groupIds).FindAll(o=> !o.IsHiddenFromReport);
            }
            else
            {
                var groupInfo = groupLayer.GetById(UserContext.GroupId);
                lstGroup = new List<Group>() { groupInfo };
                lstUsers = new List<UserSearchModel>() { new UserSearchModel() { UserName = UserContext.UserName,Id = UserContext.Id } };
            }
            var userRoleGroupLayer = new UserRoleGroupLayer();
            var lstAfterSaler = new List<UserSearchModel>();
            foreach (var item in lstUsers)
            {
                // Lấy role của người dùng
                var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                if (roles.Find(o => o.RoleId == RoleSystem.AfterSale.GetHashCode()) != null)
                {
                    lstAfterSaler.Add(item);
                }
            }

            var model = new AfterSaleAssistantReportIndexModel();
            model.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            model.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            model.SetListGroup(isSearchAll, lstGroup);
            model.SetListUsers(isSearchAll, lstAfterSaler);
            return View(model);
        }
        [FilterAuthorize]
        public JsonResult GetUserByGroupId(int groupId)
        {
            var lstShowedAssignTo = new List<KeyValuePair<string, string>>();
            if (!UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
            {
                lstShowedAssignTo.Add(new KeyValuePair<string, string>(UserContext.UserName, UserContext.UserName));
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstShowedAssignTo
                }, JsonRequestBehavior.AllowGet);
            }

            var groupLayer = new GroupLayer();
            var userLayer = new UserLayer();
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            if (groupId == 0)
            {
                var allGroups = groupLayer.GetAll();
                var lstGroup = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                                ? allGroups : allGroups.FindAll(o => lstRoledGroup.Contains(o.Id));
                var groupIds = string.Join(",", lstGroup.Select(o => o.Id).ToList());
                var lstUser = userLayer.GetList(groupIds).FindAll(o=>!o.IsHiddenFromReport);
                var lstSaler = new List<UserSearchModel>();
                var userRoleGroupLayer = new UserRoleGroupLayer();
                foreach (var item in lstUser)
                {
                    // Lấy role của người dùng
                    var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                    if (roles.Find(o => o.RoleId == RoleSystem.AfterSale.GetHashCode()) != null)
                    {
                        lstSaler.Add(item);
                    }
                }
                var lstShowSaler = new List<KeyValuePair<string, string>>();
                lstShowSaler.Add(new KeyValuePair<string, string>("", "--All after-salers--"));
                lstShowSaler.AddRange(lstSaler.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList());

                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstShowSaler
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Lấy danh sách người dùng thuộc nhóm đó
                var lstUser = userLayer.GetByGroupId(groupId).FindAll(o => !o.IsHiddenFromReport);
                var lstSaler = new List<Users>();
                var userRoleGroupLayer = new UserRoleGroupLayer();
                foreach (var item in lstUser)
                {
                    // Lấy role của người dùng
                    var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                    if (roles.Find(o => o.RoleId == RoleSystem.AfterSale.GetHashCode()) != null)
                    {
                        lstSaler.Add(item);
                    }
                }

                var lstShowSaler = new List<KeyValuePair<string, string>>();
                lstShowSaler.Add(new KeyValuePair<string, string>("", "--All after-salers--"));
                lstShowSaler.AddRange(lstSaler.Select(o => new KeyValuePair<string, string>(o.UserName, o.UserName)).ToList());
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = lstShowSaler
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [FilterAuthorize]
        public ActionResult Search(AfterSaleAssistantReportIndexModel model)
        {
            ViewBag.TotalReceived = 0;
            ViewBag.TotalActive = 0;
            ViewBag.TotalListings = 0;
            ViewBag.TotalAverageDailyListing = 0;

            // Nếu không phải là manager / admin mà tìm kiếm thông tin after khác thì trả về rỗng
            var isManager = UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager);
            if (!isManager && (string.IsNullOrEmpty(model.AssignTo) 
                          || !model.AssignTo.Equals(UserContext.UserName)))
            {
                return View(new List<AfterSaleAssistantViewModel>());
            }
            // Lấy ra danh sách các tài khoản mà người dùng đăng nhập có quyền view
            var userLayer = new UserLayer();
            var lstViewedUsers = new List<Users>();
            var groupLayer = new GroupLayer();
            if (!string.IsNullOrEmpty(model.AssignTo))
            {
                var userInfo = userLayer.GetByUserName(model.AssignTo);
                if (userInfo == null)
                {
                    return View(new List<AfterSaleAssistantViewModel>());
                }
                lstViewedUsers.Add(userInfo);
            }
            else
            {
                // Lấy ra danh sách mà nhân viên có quyền 
                var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
                if (model.GroupId == 0)
                {
                    var allGroups = groupLayer.GetAll();
                    var groupIds = lstRoledGroup.Contains(DefaultSystemParameter.AllGroup)
                        ? string.Join(",", allGroups.Select(o=>o.Id).ToList()) : string.Join(",", lstRoledGroup);
                    lstViewedUsers.AddRange(userLayer.GetList(groupIds).FindAll(o=>!o.IsHiddenFromReport).Select(o => new Users() { UserName = o.UserName, Id = o.Id }));
                }
                else
                {
                    if (!lstRoledGroup.Contains(DefaultSystemParameter.AllGroup) 
                      && !lstRoledGroup.Contains(model.GroupId))
                    {
                        return View(new List<SaleReportViewModel>());
                    }
                    lstViewedUsers = userLayer.GetByGroupId(model.GroupId).FindAll(o => !o.IsHiddenFromReport);
                }
            }
            // Chỉ lọc lấy những tài khoản là after-sale còn những tài khoản còn lại thì bỏ qua
            var userRoleGroupLayer = new UserRoleGroupLayer();
            var lstViewedAfterSale = new List<Users>();
            foreach (var item in lstViewedUsers)
            {
                // Lấy role của người dùng
                var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                if (roles.Find(o => o.RoleId == RoleSystem.AfterSale.GetHashCode()) != null)
                {
                    lstViewedAfterSale.Add(item);
                }
            }
            // Cộng thêm 1 ngày để tính ngày cuối
            model.EndDate = model.EndDate.AddDays(1);

            // Lấy ra tất cả các tài khoản được giao đi lấy tin rao trong khoảng thời gian này
            var postingAssistanceHistoryLayer = new PostingAssistanceHistoryLayer();
            var lstAssigningAfterSaleHistory = postingAssistanceHistoryLayer.GetByTime(model.StartDate,model.EndDate);
            // Thông tin báo cáo hiển thị
            var reportResult = new List<AfterSaleAssistantViewModel>();
            // TH không tìm thấy thông tin lịch sử giao tài khoản cho after-sale chăm thì hiển thị 0 trên báo cáo
            if (lstAssigningAfterSaleHistory.Count <= 0)
            {
                foreach (var saleIdInfo in lstViewedAfterSale)
                {
                    var eachAfterSaleReportInfo = new AfterSaleAssistantViewModel();
                    eachAfterSaleReportInfo.Username = saleIdInfo.UserName;
                    reportResult.Add(eachAfterSaleReportInfo);
                }
                return View(reportResult);
            }
            // Lấy ra các id tài khoản đã giao đi lấy trong thời gian này
            var lstAssignedCustomerId = lstAssigningAfterSaleHistory.Select(o => o.CustomerId).Distinct().ToList();
            // Lấy ra thông tin tài khoản động bộ bên admin tương ứng
            var syncBCRMNAdminLayer = new SyncBcrmBxhNAdminLayer();
            var lstAdminSyncInfo = syncBCRMNAdminLayer.GetByListCustomerId(string.Join(",", lstAssignedCustomerId));
            // Lay ra tat ca lich su tin dang cua cac tai khoan trong khoang thoi gian nay
            var adminProductAppovedLayer = new AdminProductApprovedLayer();
            var lstProductApprovedHistory = adminProductAppovedLayer.GetByListMemberAndTime(lstAdminSyncInfo.Select(o => o.AccountId).ToList(), model.StartDate, model.EndDate.AddDays(DefaultSystemParameter.DayMaximumToMarkAccountActive));

            // Lấy tất cả lịch sử login vào hệ thống trong khoảng thời gian này
            var traceUserLoginHistory = CallAPITraceCustomerLogin(model.StartDate, model.EndDate.AddDays(DefaultSystemParameter.DayMaximumToMarkAccountActive));

            if (traceUserLoginHistory == null)
            {
                return View(new List<AfterSaleAssistantViewModel>());
            }

            // Tìm tất cả các tài khoản mà có login trong khoảng thời gian này
            var lstAdminLoginInTime = new List<SyncBcrmBxhNAdmin>();
            foreach (var userItem in traceUserLoginHistory.Data)
            {
                var info = lstAdminSyncInfo.Find(o => (!string.IsNullOrEmpty(o.PhoneNumber) && o.PhoneNumber.Substring(1).Equals(userItem.PhoneOrEmail))
                                     || (!string.IsNullOrEmpty(o.Email) && o.Email.ToLower().Equals(userItem.PhoneOrEmail.ToLower())));
                if (info != null)
                {
                    lstAdminLoginInTime.Add(info);
                }
            }

            var totalReceived = 0;
            var totalActive = 0;
            var totalListing = 0;
            var totalAverage =(decimal) 0;
            
            foreach (var viewdAfterSaleInfo in lstViewedAfterSale)
            {
                var eachAfterSaleReportInfo = new AfterSaleAssistantViewModel();
                // Thông tin Id
                eachAfterSaleReportInfo.Id = viewdAfterSaleInfo.Id;
                // Thông tin username
                eachAfterSaleReportInfo.Username = viewdAfterSaleInfo.UserName;
                // Lấy lịch sử đã giao lấy tài khoản cho after trong khoảng thời gian này
                var lstPostingRetrieveHistoryForASA = lstAssigningAfterSaleHistory.FindAll(o => o.UserId == viewdAfterSaleInfo.Id);
                // Tính ra số khách đã nhận
                eachAfterSaleReportInfo.SumAssistanceCustomer = lstPostingRetrieveHistoryForASA.Select(o => o.CustomerId).Distinct().ToList().Count;
                // Tính ra số tin đã lấy đăng của khách trong khoảng thời gian này
                //var lstPostingAssistanceEachASA = lstAssigningAfterSaleHistory.FindAll(o => o.UserId == viewdAfterSaleInfo.Id);


                var sumActivedAccount = 0;
                // Lấy ra các id khách hàng được giao chăm
                var lstCustomerIdEachASA = lstPostingRetrieveHistoryForASA.Select(o => o.CustomerId).Distinct().ToList();
                foreach (var customerIdItem in lstCustomerIdEachASA)
                {
                    var adminInfo = lstAdminSyncInfo.Find(o => o.CustomerId == customerIdItem);
                    if (adminInfo != null)
                    {
                        var lstProductEachCustomer = lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId);
                        if (lstProductEachCustomer.Count >= 2)
                        {
                            sumActivedAccount++;
                        }
                        else if (lstAdminLoginInTime.Find(o => o.AccountId == adminInfo.AccountId) != null)
                        {
                            sumActivedAccount++;
                        }
                    }
                }
                eachAfterSaleReportInfo.ActiveAccount = sumActivedAccount;

                // Tính tổng số tin đăng đã lấy về của khách hàng
                var sumPosting = 0;
                foreach (var item in lstPostingRetrieveHistoryForASA)
                {
                    // Lấy adminid tương ứng
                    var adminInfo = lstAdminSyncInfo.Find(o=>o.CustomerId == item.CustomerId );

                    if(adminInfo == null)
                        continue;

                    if(item.ReceivedDate < model.StartDate)
                    {
                        if (item.ReturnDate == null || item.ReturnDate == DateTime.MinValue || item.ReturnDate >= model.EndDate)
                        {
                            sumPosting += lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId && o.ApprovedDate >= model.StartDate && o.ApprovedDate <= model.EndDate).Count;
                        }
                        else
                        {
                            sumPosting += lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId && o.ApprovedDate >= model.StartDate && o.ApprovedDate <= item.ReturnDate).Count;
                        }
                    }
                    else
                    {
                        if (item.ReturnDate == null || item.ReturnDate == DateTime.MinValue || item.ReturnDate >= model.EndDate)
                        {
                            sumPosting += lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId && o.ApprovedDate >= item.ReceivedDate && o.ApprovedDate <= model.EndDate).Count;
                        }
                        else
                        {
                            sumPosting += lstProductApprovedHistory.FindAll(o => o.AdminId == adminInfo.AccountId && o.ApprovedDate >= item.ReceivedDate && o.ApprovedDate <= item.ReturnDate).Count;
                        }
                    }
                }
                eachAfterSaleReportInfo.SumPosting = sumPosting;
                var days = (model.EndDate - model.StartDate).Days;
                if(days > 0)
                {
                    eachAfterSaleReportInfo.AverageDailyPosting = (decimal)sumPosting / (decimal)days;
                }

                totalReceived += eachAfterSaleReportInfo.SumAssistanceCustomer;
                totalActive += eachAfterSaleReportInfo.ActiveAccount;
                totalListing += eachAfterSaleReportInfo.SumPosting;
                totalAverage += Math.Round(eachAfterSaleReportInfo.AverageDailyPosting,2);
                reportResult.Add(eachAfterSaleReportInfo);
            }
            ViewBag.TotalReceived = totalReceived;
            ViewBag.TotalActive = totalActive;
            ViewBag.TotalListings = totalListing;
            ViewBag.TotalAverageDailyListing = (decimal)totalAverage;

            return View(reportResult);
        }

        [FilterAuthorize]
        public APICustomerLoginTimeResponse CallAPITraceCustomerLogin(DateTime fromDate, DateTime toDate)
        {
            var data = new
            {
                webId = 7,
                startDate = fromDate.ToString("yyyyMMdd").ToInt(0),
                endDate = toDate.ToString("yyyyMMdd").ToInt(0),
                threshHold = 2
            };

            var url = string.Format("{0}?param={1}"
                                , ConfigurationManager.AppSettings["APITraceCustomerLogin"]
                                , HttpUtility.UrlEncode(AESEncryption.EncryptString(JsonConvert.SerializeObject(data))));
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(new { }), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                var failLogging = new FailureLoggingCommon();
                failLogging.WriteLog(1, "SaleReport.CallAPITraceCustomerLogin", JsonConvert.SerializeObject(new { fromDate = fromDate, toDate = toDate }), UserContext.UserName, "Không thể kết nối đến API hoặc lỗi trong quá trình xử lý API", DateTime.Now, "");
                return null;
            }
            try
            {
                var resCode = JsonConvert.DeserializeObject<APICustomerLoginTimeResponse>(res);
                return resCode;
            }
            catch (Exception ex)
            {
                var failLogging = new FailureLoggingCommon();
                failLogging.WriteLog(1, "SaleReport.CallAPITraceCustomerLogin", JsonConvert.SerializeObject(new { fromDate = fromDate, toDate = toDate }), UserContext.UserName, ex.Message, DateTime.Now, "");
                return null;
            }
        }
    }
}
