﻿using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.BXH.Indo.Controllers
{
    public class CollaboratorReportController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            var searchIndexInfo = new CollaboratorSearchModel();
            // Lấy danh sách các nhóm mà người dùng đăng nhập có quyền
            searchIndexInfo.LstGroup = LoadGroup();
            return View(searchIndexInfo);
        }

        [FilterAuthorize]
        public ActionResult Search(CollaboratorSearchModel model)
        {
            // Lấy danh sách các CTV sale thuộc nhóm
            var userLayer = new UserLayer();
            var userId = 0;
            // Nếu người dùng không có quyền Admin hoặc Manager thì chỉ được phép tìm kiếm thông tin của mình
            if (!UserContext.HasRole(RoleSystem.Admin) && !UserContext.HasRole(RoleSystem.Manager))
            {
                userId = UserContext.Id;
            }
            model.ToDate = new DateTime(model.ToDate.Year, model.ToDate.Month, model.ToDate.Day, 23, 59, 59);

            var lstUserReport = userLayer.GetSaleReport(model.FilterKeyword, model.GroupId,userId);

            if(model.Status != null)
            {
                lstUserReport = lstUserReport.FindAll(o => o.Status == model.Status.Value);
            }
            // Lấy các thông tin về tổng số showroom, số tin đăng
            var customerLayer = new CustomerLayer();
            var syncBcrmNAdminLayer = new SyncBcrmBxhNAdminLayer();
            var adminArticleCategoryLayer = new AdminArticleCategoriesLayer();
            foreach(var userReport in lstUserReport)
            {
                // Lấy ra các showroom mà 
                var lstShowroom = customerLayer.GetShowroomReport("", userReport.UserName, 0, 0);
                if(lstShowroom.Count>0)
                {
                    // Lấy ra các id tương ứng của showroom với bên admin cintamobil
                    var lstSyncId = syncBcrmNAdminLayer.GetByListCustomerId(string.Join(",", lstShowroom.Select(o => o.Id).ToList()));
                    if(lstSyncId.Count > 0)
                    {
                        //var nextMonth = model.FromDate.AddMonths(1);
                        //var firstDayInNextMonth = new DateTime(nextMonth.Year, nextMonth.Month, 1, 0, 0, 0);

                        // Lấy ra các thông tin thống kê đăng tin từ bên showroom
                        var lstAdminShowroomStatistic = adminArticleCategoryLayer.GetShowroomReport(model.FromDate, model.ToDate, lstSyncId.Select(o => o.AccountId).ToList());
                        foreach (var item in lstShowroom)
                        {
                            var eq = lstSyncId.Find(o => o.CustomerId == item.Id);
                            if (eq != null)
                            {
                                var reportInfo = lstAdminShowroomStatistic.Find(o => o.Id == eq.AccountId);
                                if (reportInfo != null)
                                {
                                    item.NumOfPosting = reportInfo.NumOfPosting;
                                    item.JoinDate = reportInfo.JoinDate;
                                    item.NumPostingIn1stMonth = reportInfo.NumPostingIn1stMonth;
                                    item.ShowroomType = reportInfo.ShowroomType;
                                }
                            }
                        }
                    }
                }
                
                lstShowroom = lstShowroom.FindAll(o => o.JoinDate >= model.FromDate && o.JoinDate <= model.ToDate);

                // Tính tổng số các showroom của người dùng
                userReport.TotalShowRoom = lstShowroom.Count;
                // Tính tổng số các posting của người dùng
                userReport.TotalPosting = lstShowroom.Sum(o => o.NumOfPosting);
                // Tính tổng số các posting của showroom loại A
                userReport.NumShowRoomA = lstShowroom.Count(o => o.ShowroomType == 1);
                //Tính tổng số các posting của showroom loại B
                userReport.NumShowRoomB = lstShowroom.Count(o => o.ShowroomType == 2);
                //Tính tổng số các posting của showroom loại C
                userReport.NumShowRoomC = lstShowroom.Count(o => o.ShowroomType == 3);
                //Tính tổng số các posting của showroom loại D
                userReport.NumShowRoomD = lstShowroom.Count(o => o.ShowroomType == 0);
            }

            // Lấy danh sách các CTV sale và báo cáo theo khoảng thời gian 
            var result = lstUserReport;
            return View(result);
        }

        [NonAction]
        public List<Group> LoadGroup()
        {
            var groupLayer = new GroupLayer();
            var permittedGroups = new List<Group>();
            var allGroup = groupLayer.GetAll();
            //Check if this user has permission Admin
            if (UserContext.HasRole(RoleSystem.Admin))
            {
                permittedGroups = allGroup;
            }
            else
            {
                if (UserContext.ListRoleGroup.ContainsKey(RoleSystem.Manager))
                {
                    var lstGroupId = UserContext.ListRoleGroup[RoleSystem.Manager];
                    if (lstGroupId.Contains(-1000))
                    {
                        permittedGroups = allGroup;
                    }
                    else
                    {
                        permittedGroups = allGroup.FindAll(o => lstGroupId.Contains(o.Id));
                    }
                }
                else
                {
                    permittedGroups.Add(allGroup.Find(o => o.Id == UserContext.GroupId));
                }
            }
            return permittedGroups;
        }

    }
}
