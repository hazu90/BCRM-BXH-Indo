﻿using BCRM.BXH.Indo.Common;
using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Language;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using DVS.Algorithm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BCRM.BXH.Indo.Controllers
{
    public class CrawlerCustomerController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            var model = new CrawlerCustomerIndexModel();
            // Lấy ra danh sách mà nhân viên có quyền 
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            var groupids = "";
            if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
            {
                var groupLayer = new GroupLayer();
                var allGroup = groupLayer.GetAll();
                groupids = string.Join(",", allGroup.Select(o => o.Id).ToList());
            }
            else
            {
                groupids = string.Join(",", lstRoledGroup);
            }
            var userLayer = new UserLayer();
            var lstUser = string.IsNullOrEmpty(groupids) ? new List<UserSearchModel>() { new UserSearchModel() { UserName = UserContext.UserName , Id = UserContext.Id} } : userLayer.GetList(groupids);
            // Lấy ra danh sách các saler
            // Lấy ra danh sách các after-saler
            var lstSaler = new List<UserSearchModel>();
            var userRoleGroupLayer = new UserRoleGroupLayer();
            foreach (var item in lstUser)
            {
                // Lấy role của người dùng
                var roles = userRoleGroupLayer.GetRoleGroupByUserId(item.Id);
                if (roles.Find(o => o.RoleId == RoleSystem.Admin.GetHashCode()
                               || o.RoleId == RoleSystem.Manager.GetHashCode()
                               || o.RoleId == RoleSystem.Sale.GetHashCode()) != null)
                {
                    lstSaler.Add(item);
                }
            }

            model.LstUser = lstSaler;
            
            var cityLayer = new CityLayer();
            var regionLayer = new RegionLayer();
            var lstRegion = regionLayer.GetAll();
            ViewBag.ListCity = lstRegion.Count > 0 ? cityLayer.GetByRegionId(lstRegion.FirstOrDefault().Id) : new List<City>();
            ViewBag.ListRegion = lstRegion;
            ViewBag.CurrentUser = UserContext;
            return View(model);
        }
        [FilterAuthorize]
        public ActionResult Search(CrawlerCustomerIndexModel model)
        {
            var isManager = UserContext.HasRole(RoleSystem.Admin.GetHashCode(), RoleSystem.Manager.GetHashCode());
            // Lấy ra danh sách mà nhân viên có quyền 
            var lstRoledGroup = UserContext.GetGroupsByRole(RoleSystem.Admin, RoleSystem.Manager);
            var groupids = "";
            if (lstRoledGroup.Contains(DefaultSystemParameter.AllGroup))
            {
                var groupLayer = new GroupLayer();
                var allGroup = groupLayer.GetAll();
                groupids = string.Join(",", allGroup.Select(o => o.Id).ToList());
            }
            else
            {
                groupids = string.Join(",", lstRoledGroup);
            }
            var userLayer = new UserLayer();
            var lstRoledUser = string.IsNullOrEmpty(groupids) ? new List<UserSearchModel>() { new UserSearchModel() { Id = UserContext.Id, UserName = UserContext.UserName } } : userLayer.GetList(groupids);
            var customerCareHistoryLayer = new CustomerCareHistoryLayer();
            //TH tìm kiếm theo tất cả các khách hàng crawler mà người dùng hiện thời có quyền view
            if (model.AssignTo.Equals("-1"))
            {
                // Lấy ra danh sách các tài khoản mà nhân viên này không được nhìn thấy
                var lstIgnoreUserId = customerCareHistoryLayer.GetIgnoreByListUserId(lstRoledUser.Select(o => o.Id).ToList(), DateTime.Now);
                model.ListIgnoreAdminId = lstIgnoreUserId.Select(o => o.CrawlerAdminId).Distinct().ToList();
            }
            // TH tìm kiếm theo các khách hàng crawler mà chưa được nhận bởi ai
            else if (model.AssignTo.Equals("0"))
            {
                var lstIgnoreUserId = customerCareHistoryLayer.GetIgnoreByListUserId(lstRoledUser.Select(o => o.Id).ToList(), DateTime.Now);
                lstIgnoreUserId.AddRange(customerCareHistoryLayer.GetByListUserId(lstRoledUser.Select(o => o.Id).ToList(), DateTime.Now));
                model.ListIgnoreAdminId = lstIgnoreUserId.Select(o => o.CrawlerAdminId).Distinct().ToList();
            }
            else
            {
                // Kiểm tra xem người dùng có quyền với nhân viên này không
                if (lstRoledUser.Find(o => o.UserName.Equals(model.AssignTo)) == null)
                {
                    ViewBag.Pager = new Pager()
                    {
                        ActionScript = "crawler_customer_index.search"
                       ,CurrentPage = model.PageIndex
                       ,PageSize = model.PageSize
                       ,TotalItem = 0
                    };
                    return View(new List<CrawlerCustomerSearchModel>());
                }
                var userInfo    = userLayer.GetByUserName(model.AssignTo);
                var lstAdminId = customerCareHistoryLayer.GetByListUserId(new List<int>() { userInfo.Id }, DateTime.Now);

                if (lstAdminId.Count == 0)
                {
                    ViewBag.Pager = new Pager()
                    {
                        ActionScript = "crawler_customer_index.search"
                        ,CurrentPage = model.PageIndex
                        ,PageSize    = model.PageSize
                        ,TotalItem   = 0
                    };
                    return View(new List<CrawlerCustomerSearchModel>());
                }

                model.ListFilterAdminId = lstAdminId.Select(o => o.CrawlerAdminId).Distinct().ToList();
            }

            if (model.Status == -1)
            {
                //var lstSuccess = customerCareHistoryLayer.GetLastestCareHistoryByPhoningStatus(PhoningStatus.Success.GetHashCode());
                //model.ListIgnoreAdminId.AddRange(lstSuccess.FindAll(o => !model.ListIgnoreAdminId.Contains(o.CrawlerAdminId)).Select(o => o.CrawlerAdminId).ToList());
            }
            else if (model.Status == 0)
            {
                var lstReject = customerCareHistoryLayer.GetLastestCareHistoryByPhoningStatus(PhoningStatus.Failure.GetHashCode());
                model.ListIgnoreAdminId.AddRange(lstReject.FindAll(o => !model.ListIgnoreAdminId.Contains(o.CrawlerAdminId)).Select(o => o.CrawlerAdminId).ToList());
            }
            else if (model.Status == 2)
            {
                var lstReject = customerCareHistoryLayer.GetLastestCareHistoryByPhoningStatus(PhoningStatus.Failure.GetHashCode());

                if (model.ListFilterAdminId.Count == 0)
                {
                    model.ListFilterAdminId.AddRange(lstReject.FindAll(o => !model.ListFilterAdminId.Contains(o.CrawlerAdminId)).Select(o => o.CrawlerAdminId).ToList());
                }
                else
                {
                    model.ListFilterAdminId = lstReject.FindAll(o => model.ListFilterAdminId.Contains(o.CrawlerAdminId)).Select(o => o.CrawlerAdminId).ToList();

                    if (model.ListFilterAdminId.Count == 0)
                    {
                        ViewBag.Pager = new Pager()
                        {
                            ActionScript = "crawler_customer_index.search"
                            ,CurrentPage = model.PageIndex
                            ,PageSize = model.PageSize
                            ,TotalItem = 0
                        };
                        return View(new List<CrawlerCustomerSearchModel>());
                    }
                }
            }
            var countProductFrom = -1;
            var countProductTo = -1;
            
            if(model.PostingType == PostingType.Personal.GetHashCode())
            {
                countProductFrom = 0;
                countProductTo = 1;
            }
            else if(model.PostingType == PostingType.A.GetHashCode())
            {
                countProductFrom = 2;
                countProductTo =10;
            }
            else if(model.PostingType == PostingType.AA.GetHashCode())
            {
                countProductFrom = 11;
                countProductTo =25;
            }
            else if(model.PostingType == PostingType.AAA.GetHashCode())
            {
                countProductFrom = 26;
                countProductTo = 50;
            }
            else if(model.PostingType == PostingType.AAAA.GetHashCode())
            {
                countProductFrom = 50;
            }


            bool? invisible = false;
            if (model.HiddenStatus == -1)
            {
                if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
                {
                    invisible = null;
                }
            }
            else if (model.HiddenStatus == 0)
            {
                if (UserContext.HasRole(RoleSystem.Admin, RoleSystem.Manager))
                {
                    invisible = true;
                }
            }
            var data = JsonConvert.SerializeObject(new
            {
                Token = Security.CreateKey(),
                model.FilterKeyword,
                Status = -1,
                model.PageIndex,
                model.PageSize,
                model.DateOfCreationFrom,
                model.DateOfCreationTo,
                model.TotalRecord,
                model.ListData,
                model.ListIgnoreAdminId,
                model.ListFilterAdminId,
                CountProductFrom = countProductFrom,
                CountProductTo = countProductTo,
                Invisible  = invisible
            });
            var url = string.Format("{0}/BCRMService.svc/GetListCrawlerCustomer", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, data, "application/json");
            var result = JsonConvert.DeserializeObject<Response>(res);
            if (result.Code != SystemCode.Success)
            {
                Logger.GetInstance().Write(string.Format("Error CrawlerCustomer/Search Data : {0} Message : {1}", data, result.Message));
                ViewBag.Pager = new Pager()
                {
                    ActionScript = "crawler_customer_index.seacrh"
                    ,CurrentPage = model.PageIndex
                    ,PageSize = model.PageSize
                    ,TotalItem = 0
                };
                return View(new List<CrawlerCustomerSearchModel>());
            }

            var resModel = JsonConvert.DeserializeObject<CrawlerCustomerIndexModel>((string)result.Data);

            foreach (var item in resModel.ListData)
            {
                // Kiểm tra khách hàng này đã được nhận bởi sale nào chưa
                var lstCrawlerCarer = customerCareHistoryLayer.GetByCrawlerAdminId(item.AdminId);
                // Kiểm tra xem khách hàng này đã được tạo tài khoản đăng kí bởi sale nào khác chưa
                var successContacted = lstCrawlerCarer.Find(o => o.PhoningStatus == PhoningStatus.Success.GetHashCode());
                if (successContacted != null)
                {
                    item.Status = PhoningStatus.Success.GetHashCode();
                    item.AssignTo = successContacted.ReceivedBy;
                    item.CareHistoryId = 0;
                }
                else
                {
                    // Loại bỏ các sale chăm sóc đã trả lại khách hàng
                    lstCrawlerCarer = lstCrawlerCarer.FindAll(o => !o.IsReturned);

                    if (lstCrawlerCarer.Count == 0)
                    {
                        item.Status = PhoningStatus.NoContact.GetHashCode();
                        item.AssignTo = "";
                        item.CareHistoryId = 0;
                    }
                    else
                    {
                        // Lấy ra thông tin chăm sóc mới nhất của khách hàng này
                        // (không bao gồm nhân viên chăm sóc mà trả lại khách hàng)
                        var lastestCarer = lstCrawlerCarer.OrderByDescending(o => o.ReceivedDate).First();

                        var info = lstCrawlerCarer.Find(o => o.CrawlerAdminId == item.AdminId
                                                  && o.PhoningStatus == PhoningStatus.NoContact.GetHashCode()
                                                  && o.ReceivedDate.AddDays(DefaultSystemParameter.DaysOverContactDeadline) > DateTime.Now);
                        if (info != null)
                        {
                            item.Status = PhoningStatus.NoContact.GetHashCode();
                            item.AssignTo = info.ReceivedBy;
                            item.CareHistoryId = info.Id;
                            item.Note = info.Note;
                        }
                        else
                        {
                            var rejectInfo = lstCrawlerCarer.Find(
                                                         o => o.CrawlerAdminId == item.AdminId
                                                      && o.PhoningStatus == PhoningStatus.Failure.GetHashCode()
                                                      && o.WaitingOverReject > DateTime.Now);
                            if (rejectInfo != null)
                            {
                                item.Status = PhoningStatus.Failure.GetHashCode();
                                item.AssignTo = rejectInfo.ReceivedBy;
                                item.IsWaitingOverRejectDeadline = true;
                                item.CareHistoryId = rejectInfo.Id;
                                item.Note = rejectInfo.Note;
                            }
                            else if (lastestCarer.PhoningStatus == PhoningStatus.Failure.GetHashCode())
                            {
                                item.Status = PhoningStatus.Failure.GetHashCode();
                                item.AssignTo = "";
                                item.IsWaitingOverRejectDeadline = false;
                                item.CareHistoryId = 0;
                                item.Note = "";
                            }
                            else
                            {
                                item.Status = PhoningStatus.NoContact.GetHashCode();
                                item.AssignTo = "";
                                item.CareHistoryId = 0;
                            }
                        }
                    }
                }
            }
            // Kiểm tra các tài khoản và gán nhãn bị ẩn 
            //if(UserContext.HasRole(RoleSystem.Admin,RoleSystem.Manager))
            //{
            CheckHiddenLeadAccount(resModel.ListData);
            //}

            ViewBag.Pager = new Pager()
            {
                ActionScript = "crawler_customer_index.search"
               ,CurrentPage = model.PageIndex
               ,PageSize = model.PageSize
               ,TotalItem = resModel.TotalRecord
            };
            ViewBag.CurrUser = UserContext;
            // Sử dụng dữ liệu test
            return View(resModel.ListData);
        }
        [FilterAuthorize]
        public JsonResult Receive(long crawlerAdminId, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = crawlerAdminId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            // Kiểm tra xem tài khoản này đã có ai nhận chưa
            //var crawlerCustomerLayer = new CrawlerCustomerLayer();
            //var lstExisted = crawlerCustomerLayer.GetByAdminId(crawlerAdminId);
            var customerCareHistoryLayer = new CustomerCareHistoryLayer();
            var lstExisted = customerCareHistoryLayer.GetByCrawlerAdminId(crawlerAdminId);
            // Kiểm tra xem khách hàng này đã được chăm sóc bởi ai chưa
            //Kiểm tra xem khách hàng này đã được tạo tài khoản thực hay chưa
            var realAccount = lstExisted.Find(o => o.PhoningStatus == PhoningStatus.Success.GetHashCode());
            if (realAccount != null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "This customer is cared and create real account !"
                }, JsonRequestBehavior.DenyGet);
            }
            // Kiểm tra xem khách hàng này đã hết hạn liên lạc đối với sale 
            // mà nhận khách này từ trước hay chưa
            var notOverContactDeadline = lstExisted.Find(o => o.PhoningStatus == PhoningStatus.NoContact.GetHashCode()
                                                            && o.ReceivedDate.AddDays(DefaultSystemParameter.DaysOverContactDeadline) > DateTime.Now);
            if (notOverContactDeadline != null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "This customer is received by another saler !"
                }, JsonRequestBehavior.DenyGet);
            }
            // Kiểm tra xem hiện giờ đã quá thời gian mà khách hàng từ chối

            var notEndAfterRejected = lstExisted.Find(o => o.PhoningStatus == PhoningStatus.Failure.GetHashCode()
                                                      && o.WaitingOverReject > DateTime.Now);

            if (notEndAfterRejected != null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = "This customer is rejected before and not yet over this time !"
                }, JsonRequestBehavior.DenyGet);
            }
            // Lưu lại lịch sử nhận khách hàng này
            var careHistoryId = customerCareHistoryLayer.Create(new CustomerCareHistory()
            {
                AdminId = 0,
                CrawlerAdminId = crawlerAdminId,
                ReceivedDate = DateTime.Now,
                ReceivedBy = UserContext.UserName,
                UserId = UserContext.Id,
                Note = ""
            });

            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = new
                {
                    CareHistoryId = careHistoryId,
                    Receiver = UserContext.UserName,
                    CrawlerAdminId = crawlerAdminId
                },
                //Message = "You have received this customer successfully !"
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public ActionResult ConfirmSuccessContact(CrawlerCustomerSuccessContactModel model)
        {
            model.AssignTo = UserContext.UserName;
            var cityLayer = new CityLayer();
            var regionLayer = new RegionLayer();
            var lstRegion = regionLayer.GetAll();
            model.PhoneNumber = model.PhoneNumber.Replace(",", ";");
            ViewBag.LstPhone = Ultility.DetectPhoneNumber(model.PhoneNumber);
            ViewBag.ListCity = lstRegion.Count > 0 ? cityLayer.GetByRegionId(lstRegion.FirstOrDefault().Id) : new List<City>();
            ViewBag.ListRegion = lstRegion;
            ViewBag.CurrentUser = UserContext;
            ViewBag.TokenChecksum = ChecksumHelper.GenToken<CrawlerCustomerSuccessContactModel>(new CrawlerCustomerSuccessContactModel()
            {
                CareHistoryId = model.CareHistoryId,
                CrawlerId = model.CrawlerId
                ,AssignTo = UserContext.UserName
            });
            return View(model);
        }
        [FilterAuthorize]
        [JsonModelBinder(ActionParameterType = typeof(CrawlerCustomerSuccessContactModel), ActionParameterName = "model")]
        public JsonResult ContactSuccess(CrawlerCustomerSuccessContactModel model)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken<CrawlerCustomerSuccessContactModel>
                (new CrawlerCustomerSuccessContactModel
                {
                    CrawlerId = model.CrawlerId,
                    CareHistoryId = model.CareHistoryId,
                    AssignTo = UserContext.UserName
                }, model.TokenChecksum);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            // Kiểm tra xem người dùng có quyền xác nhận đối với khách hàng 
            // này hay không
            var careHistoryLayer = new CustomerCareHistoryLayer();
            var careHistoryInfo = careHistoryLayer.GetById(model.CareHistoryId);
            if (careHistoryInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
            // Kiểm tra xem người dùng đã hết hạn chăm sóc hay chưa
            if (careHistoryInfo.ReceivedDate.AddDays(DefaultSystemParameter.DaysOverContactDeadline) < DateTime.Now)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            // Kiểm tra số điện thoại của khách hàng có trùng với danh sách khách hàng crawler hay không
            var profileLayer = new ProfileLayer();
            var existedProfile = profileLayer.GetByPhoneNumber(model.PhoneNumber);
            if (existedProfile.Id > 0)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotValid,
                    Message = LanguageRes.Msg_Existed_Phonenumber
                }, JsonRequestBehavior.DenyGet);
            }
            var customerLayer = new CustomerLayer();
            // Kiểm tra email của khách hàng có trùng với danh sách khách hàng của hệ thống hay không
            if (!string.IsNullOrEmpty(model.Email) && customerLayer.IsExistedEmail(0, model.Email))
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotValid,
                    Message = LanguageRes.Msg_Existed_Email
                }, JsonRequestBehavior.DenyGet);
            }

            //CallAPIChangeContactStatus(model.CrawlerId, ContactStatus.Accepted.GetHashCode());

            // Gọi API sang bên Admin để cập nhật lại trạng thái liên hệ
            // đồng thời bổ sung tài khoản đăng kí theo tài khoản crawler
            // tự động chuyển tin đã crawl trong vòng 4 tháng
            var adminResponse = MoveCrawlerToRegisterCustmer(model.CrawlerId, model.FullName, model.Email, model.PhoneNumber, model.Address, "");
            if (adminResponse.Code != SystemCode.Success)
            {
                return Json(adminResponse, JsonRequestBehavior.DenyGet);
            }
            var adminId = (int)adminResponse.Data;
            //// Tạo mới file khách mời
            var customerId = customerLayer.Create(new Customer
            {
                FullName = model.FullName,
                PhoneNumber = model.PhoneNumber,
                Address = model.Address,
                PostingType = model.PostingType,
                Email = model.Email,
                Description = model.Description,
                RegionId = model.RegionId,
                CityId = model.CityId,
                DistrictId = model.DistrictId,
                AssignTo = UserContext.UserName,
                CareDate = DateTime.Now,
                ICareDate = DateTime.Now.ToInt32(),
                StartCareDate = DateTime.Now,
                CompanyId = model.CompanyId,
                CreatedBy = UserContext.UserName,
                CreatedDate = DateTime.Now,
                LastModifiedBy = UserContext.UserName,
                LastModifiedDate = DateTime.Now
            });
            customerLayer.UpdateStatus(customerId, ContactStatus.Accepted.GetHashCode());
            profileLayer.Create(new Profile()
            {
                PhoneNumber = model.PhoneNumber
               ,PhoneAlias = model.PhoneNumber
               ,Name = model.PhoneNumber
               ,CustomerId = customerId
               ,CreatedBy = UserContext.UserName
               ,CreatedDate = DateTime.Now
               ,Email = " "
               ,Avatar = " "
            });
            //// Lưu vào bảng syncbcrmbxhnadmin để đồng bộ
            var syncBcrmBxhNAdminLayer = new SyncBcrmBxhNAdminLayer();
            if (adminId > 0)
            {
                syncBcrmBxhNAdminLayer.Create(customerId, adminId, model.Email,model.PhoneNumber);
            }
            // Lưu lịch sử thông tin thao tác khách hàng
            var customerHistoryLayer = new CustomerHistoryLayer();
            customerHistoryLayer.Create(new CustomerHistory()
            {
                Action = "The customer is created",
                OldValue = "",
                NewValue = "",
                Note = "",
                CustomerId = customerId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now
            });
            // Bổ sung thông tin nhận khách của sale để tra cứu thông tin 
            // hoạt động của sale đớ
            careHistoryLayer.UpdateSuccessContact(model.CareHistoryId, PhoningStatus.Success.GetHashCode(), DateTime.Now);
            careHistoryLayer.UpdateIsCared(model.CareHistoryId, true);
            careHistoryLayer.UpdateCustomerIdByCrawlerAdminId(model.CrawlerId, customerId);
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Insert_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult ContactFailure(long crawlerId, int careHistoryId, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = crawlerId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            var careHistoryLayer = new CustomerCareHistoryLayer();

            var careInfo = careHistoryLayer.GetById(careHistoryId);
            if (careInfo == null || !careInfo.ReceivedBy.Equals(UserContext.UserName))
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    //Message = "You don't have a permission to process !"
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
            // Cập nhật lại trạng thái gọi điện là không thành công
            careHistoryLayer.UpdateFailureContact(careHistoryId, PhoningStatus.Failure.GetHashCode(), DateTime.Now.AddDays(DefaultSystemParameter.DaysWaitingAfterRejecting.GetHashCode()));
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult Note(int careHistoryId, string note, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = careHistoryId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            var careHistoryLayer = new CustomerCareHistoryLayer();
            var careCustomerInfo = careHistoryLayer.GetById(careHistoryId);
            //
            if (careCustomerInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "Cannot found carer info for this customer !"
                }, JsonRequestBehavior.DenyGet);
            }
            note = string.Format("{0} : {1}", UserContext.UserName, note);

            careHistoryLayer.UpdateNote(careHistoryId, note);
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult Return(int careHistoryId, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = careHistoryId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            var careHistoryLayer = new CustomerCareHistoryLayer();
            var careHistoryInfo = careHistoryLayer.GetById(careHistoryId);

            if (careHistoryInfo == null)
            {
                return Json(new Response()
                {
                    Code = SystemCode.DataNull,
                    Message = "Cannot found care info for this customer !"
                }, JsonRequestBehavior.DenyGet);
            }

            if (!careHistoryInfo.ReceivedBy.Equals(UserContext.UserName))
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    //Message = "You don't have a permission to excecute this action !"
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
            careHistoryLayer.UpdateReturnStatus(careHistoryId, true);
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = new
                {
                    careHistoryInfo.CrawlerAdminId,
                    CareHistoryId = careHistoryInfo.Id
                },
                //Message = "You returnned this customer successfully"
                Message = LanguageRes.Msg_Updated_Successfully
            }, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public JsonResult Hide(int adminId, short reasonType, string phoneNumber, string note, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = adminId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            var hiddenFromSaleLayer = new HiddenFromSaleLayer();
            // Kiểm tra xem khách hàng lead này đã được ẩn chưa 
            // nếu rồi thì cảnh báo 
            var lstHidden = hiddenFromSaleLayer.GetByAdminId(adminId);
            if (lstHidden.Count > 0)
            {
                return Json(new Response() 
                {
                    Code = SystemCode.NotValid,
                    Message = "This lead has been hidden , you can't do this !"

                }, JsonRequestBehavior.DenyGet);
            }


            // Gọi API sang bên admin để ẩn tài khoản crawler
            var apiResponse = CallAPIHiddenCrawlerAccount(adminId,true);

            if (apiResponse.Code == SystemCode.Success)
            {
                var lstPhone = Ultility.DetectPhoneNumber(phoneNumber);
                foreach (var phoneItem in lstPhone)
                {
                    // Xóa số 0 ở đầu
                    var newPhoneItem = (long)0;
                    if (phoneItem.Substring(0, 1).Equals("0"))
                    {
                        newPhoneItem = phoneItem.Substring(1).ToLong(0);
                    }
                    else
                    {
                        newPhoneItem = phoneItem.ToLong(0);
                    }
                    // Kiểm tra xem thông tin này đã được lưu chưa nếu đã được lưu rồi thì bỏ qua
                    var hiddenInfo = hiddenFromSaleLayer.GetByAdminAndPhonenumber(adminId, newPhoneItem);
                    if (hiddenInfo == null)
                    {
                        hiddenFromSaleLayer.Create(adminId, reasonType, note, UserContext.UserName, DateTime.Now, newPhoneItem);
                    }
                }
                
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Message = "Hiding customer successfully !"
                }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(apiResponse, JsonRequestBehavior.DenyGet);
            }
        }
        [FilterAuthorize]
        public JsonResult UnHide(int adminId, string signatureToken)
        {
            // Xác thực lại giá trị của dữ liệu truyền lên
            var validateToken = ChecksumHelper.ValidateToken(new { Id = adminId, CurrUser = UserContext.UserName }, signatureToken);
            if (!validateToken.IsValid)
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }

            // Chỉ cho phép tài khoản admin , manager được thực thi chức
            // năng này
            if(!UserContext.HasRole(RoleSystem.Admin,RoleSystem.Manager))
            {
                return Json(new Response() 
                {
                    Code = SystemCode.NotPermitted,
                    Message = "You don't have a permission !"
                });
            }

            var hiddenFromSaleLayer = new HiddenFromSaleLayer();
            // Gọi API sang bên admin để mở ẩn tài khoản crawler
            var apiResponse = CallAPIHiddenCrawlerAccount(adminId, false);

            if (apiResponse.Code == SystemCode.Success)
            {
                // Xóa thông tin ẩn tài khoản của sale đó
                hiddenFromSaleLayer.Delete(adminId);
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Message = "Unhiding customer successfully !"
                }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(apiResponse, JsonRequestBehavior.DenyGet);
            }
        }
        [FilterAuthorize]
        public JsonResult GetTokenKeyForOneRequest(int id)
        {
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = ChecksumHelper.GenToken(new { Id = id, CurrUser = UserContext.UserName }, ConfigurationManager.AppSettings["TimeExpiredTokenForConfirmRequest"].ToString().ToInt(1))
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult GetTokenKeyForGeneralView(int id)
        {
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = ChecksumHelper.GenToken(new { Id = id, CurrUser = UserContext.UserName })
            }, JsonRequestBehavior.AllowGet);
        }

        [NonActionAttribute]
        public Response CallAPIChangeContactStatus(long adminId, int contactStatus)
        {
            var url = string.Format("{0}/BCRMService.svc/UpdateCrawlerContactStatus", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = new
            {
                // Token của api
                Token = Security.CreateKey(),
                // id của admin
                ListAdminId = new List<long>() { adminId },
                ContactStatus = contactStatus
            };
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");

            var resCode = JsonConvert.DeserializeObject<Response>(res);
            return resCode;
        }
        [NonActionAttribute]
        public Response MoveCrawlerToRegisterCustmer(long crawlerAdminId, string fullName, string email
                                                    , string phoneNumber, string address, string note)
        {
            var url = string.Format("{0}/BCRMService.svc/ChangeToCus", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = new
            {
                Token = Security.CreateKey(),
                CrawlerId = crawlerAdminId,
                FullName = fullName,
                Email = email,
                PhoneNumber = phoneNumber,
                Address = address,
                Note = note
            };
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");
            var adminResponse = JsonConvert.DeserializeObject<Response>(res);
            if (adminResponse == null)
            {
                return new Response()
                {
                    Code = SystemCode.ErrorConnect,
                    Message = "Cannot to connect to Admin site . Contact IT for detail"
                };
            }
            return adminResponse;
        }
        [NonActionAttribute]
        public Response CallAPIHiddenCrawlerAccount(int adminId,bool hiddenStatus)
        {
            var url = string.Format("{0}/BCRMService.svc/UpdateMembershipCrawlInvisile", ConfigurationManager.AppSettings["AdminBXHAPI"].ToString());
            var data = new
            {
                // Token của api
                Token = Security.CreateKey(),
                // id của admin
                Id = adminId,
                Invisible = hiddenStatus
            };
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(data), "application/json");

            if (string.IsNullOrEmpty(res))
            {
                var failLogging = new FailureLoggingCommon();
                failLogging.WriteLog(1, "CrawlerCustomer.CallAPIHiddenCrawlerAccount", JsonConvert.SerializeObject(new { adminId = adminId, hiddenStatus = hiddenStatus }), UserContext.UserName, "Không thể kết nối đến API hoặc lỗi trong quá trình xử lý API", DateTime.Now, "");

                return new Response()
                {
                    Code = SystemCode.ErrorExist,
                    Message = "Error when call API Admin Cintamobil . Contact IT for detail !"
                };
            }
            else
            {
                try
                {
                    var resCode = JsonConvert.DeserializeObject<Response>(res);
                    return resCode;
                }
                catch (Exception ex)
                {
                    var failLogging = new FailureLoggingCommon();
                    failLogging.WriteLog(1, "CrawlerCustomer.CallAPIHiddenCrawlerAccount", JsonConvert.SerializeObject(new { adminId = adminId, hiddenStatus = hiddenStatus }), UserContext.UserName, ex.Message, DateTime.Now, "");
                    return null;
                }
            }
            
        }
        /// <summary>
        /// Kiểm tra xem danh sách các tài khoản lead có tài khoản nào bị ẩn hay không
        /// nếu có thì gán nhãn cho tài khoản đó
        /// </summary>
        /// <param name="lstLead"></param>
        [NonActionAttribute]
        public void CheckHiddenLeadAccount(List<CrawlerCustomerSearchModel> lstLead)
        {
            var hiddenFromSaleLayer = new HiddenFromSaleLayer();
            foreach (var leadItem in lstLead)
            {
                // Kiểm tra xem tài khoản crawler này có bị ẩn hay không
                // nếu có thì gán nhãn cho tài khoản đó
                // (chỉ hiển thị đối với các tài khoản manager hoặc cao hơn)
                var lstHidden = hiddenFromSaleLayer.GetByAdminId(leadItem.AdminId);
                if (lstHidden.Count > 0)
                {
                    leadItem.IsHidden = true;
                    var reason = "";
                    if (lstHidden[0].ReasonType != HideFromLeadReason.Other.GetHashCode())
                    {
                        reason = string.Format("{0} : {1}", lstHidden[0].CreatedBy, ((int)lstHidden[0].ReasonType).ToHideFromLeadReasonName());
                    }
                    else
                    {
                        reason = string.Format("{0} : {1}", lstHidden[0].CreatedBy, lstHidden[0].Note);
                    }
                    leadItem.HiddenReason = reason; 
                }
            }
        }
    }
}
