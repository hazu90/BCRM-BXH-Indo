﻿using BCRM.BXH.Indo.DAL;
using BCRM.BXH.Indo.Entity;
using BCRM.BXH.Indo.Library;
using BCRM.BXH.Indo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DVS.Algorithm;
using System.Configuration;

namespace BCRM.BXH.Indo.Controllers
{
    public class UserController : BaseController
    {
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public ActionResult Index()
        {
            var userIndexModel = new UserIndexModel();
            var groupLayer = new GroupLayer();
            var allGroup = groupLayer.GetAll();
             //Check if this user has permission Admin
            if(UserContext.HasRole(RoleSystem.Admin))
            {
                userIndexModel.LstGroup = allGroup;
            }
            else
            {
                if (UserContext.ListRoleGroup.ContainsKey(RoleSystem.Manager)) 
                {
                    var lstGroupId = UserContext.ListRoleGroup[RoleSystem.Manager];
                    if(lstGroupId.Contains(-1000))
                    {
                        userIndexModel.LstGroup = allGroup;
                    }
                    else
                    {
                        userIndexModel.LstGroup = allGroup.FindAll(o => lstGroupId.Contains(o.Id)); 
                    }
                }
            }
            ViewBag.CurrUser = UserContext;
            return View(userIndexModel);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult Save(UserForCreateChecksumModel model)
        {
            var validateModel = new UserForCreateUpdateModel(model.Id);
            var checkSumResult = ChecksumHelper.ValidateToken(validateModel, model.SignatureChecksum);
            if (checkSumResult.IsValid)
            {
                validateModel = new UserForCreateUpdateModel(model);
                if (model.Id > 0)
                {
                    return Edit(new UserSaveModel(validateModel));
                }
                else
                {
                    return Create(new UserSaveModel(validateModel));
                }
            }
            else
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = Language.LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }


            //if(model.Id>0)
            //{
            //    return Edit(model);
            //}
            //else
            //{
            //    return Create(model);
            //}
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult Block(int id, string signatureToken)
        {
            // Kiểm tra tính xác thực của yêu cầu
            var validateResponse = ChecksumHelper.ValidateToken(new { Id = id, CurrUser = UserContext.UserName }, signatureToken);

            var userLayer = new UserLayer();
            if (validateResponse.IsValid)
            {
                userLayer.UpdateStatus(id, UserStatus.NotAvaiable.GetHashCode());
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Message = "You blocked user successfully"
                }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = Language.LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
            //var response = new Response();
            //response.Code = SystemCode.Success;
            //response.Message = "You blocked user successfully";
            //return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult Unblock(int id, string signatureToken)
        {
            // Kiểm tra tính xác thực của yêu cầu
            var validateResponse = ChecksumHelper.ValidateToken(new { Id = id, CurrUser = UserContext.UserName }, signatureToken);

            var userLayer = new UserLayer();
            if (validateResponse.IsValid)
            {
                userLayer.UpdateStatus(id, UserStatus.Avaiable.GetHashCode());
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Message = "You unblocked user successfully"
                }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = Language.LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult ChangePassword(NewPasswordModel model)
        {
            var response = new Response();
            var userLayer = new UserLayer();
            try
            {
                if (model.NewPassword.Equals(model.ConfirmPassword))
                {
                    var validateToken = ChecksumHelper.ValidateToken(new { Id = model.UserId, CurrUser = UserContext.UserName }, model.SignatureChecksum);
                    if (validateToken.IsValid)
                    {
                        var rpm = new ResetPasswordModel { Id = model.UserId, NewPassword = model.NewPassword.ToMD5() };
                        userLayer.ResetPassword(rpm);
                        return Json(new Response()
                        {
                            Code = SystemCode.Success,
                            Message = "You have changed password successfully !"
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Code = SystemCode.NotPermitted,
                            Message = Language.LanguageRes.Msg_NotPermitted
                        });
                    }
                }
            }
            catch 
            {
                return Json(new Response()
                {
                    Code = SystemCode.Error,
                    Message = "Have a error in process . Please , contract IT for details !"
                });
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult GetById(int userId)
        {
            var userLayer = new UserLayer();
            var model = new UserForCreateUpdateModel(userId);
            var signatureCheckSum = ChecksumHelper.GenToken<UserForCreateUpdateModel>(model);
            var userInfo = userLayer.GetById(userId);
            var response = new Response()
            {
                Code = SystemCode.Success,
                Data = new UserForCreateChecksumModel(userInfo, signatureCheckSum)
            };
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public ActionResult Search()
        {
            var response = new Response();
            var groupLayer = new GroupLayer();
            var allGroup = groupLayer.GetAll();
            var permissionGroups = new List<Group>(); 
            //Check if this user has permission Admin
            var strGroups = "";
            if (!UserContext.HasRole(RoleSystem.Admin))
            {
                if (UserContext.ListRoleGroup.ContainsKey(RoleSystem.Manager))
                {
                    var lstGroupId = UserContext.ListRoleGroup[RoleSystem.Manager];
                    if (lstGroupId.Contains(-1000))
                    {
                        permissionGroups = allGroup;
                    }
                    else
                    {
                        permissionGroups = allGroup.FindAll(o => lstGroupId.Contains(o.Id));
                    }
                }

                strGroups = string.Join(",", permissionGroups.Select(o => o.Id).ToList());
            }
            else {
                strGroups = "0";
            }
            ViewBag.CurrUser = UserContext;
            var userLayer = new UserLayer();
            if (!string.IsNullOrEmpty(strGroups) )
            {
                return View(userLayer.GetList(strGroups).OrderByDescending(o=>o.Id).ToList());
            }
            else
            {
                return View(new List<UserSearchModel>()); 
            }
        }

        [NonAction]
        public JsonResult Create(UserSaveModel model)
        {
            if (string.IsNullOrEmpty(model.AdminUsername))
            {
                model.AdminUsername = "";
            }
            model.CreatedBy = UserContext.UserName;
            model.CreatedDate = DateTime.Now;
            model.Status = UserStatus.Avaiable.GetHashCode();
            model.Password = model.Password.ToMD5();
            var userLayer = new UserLayer();

            var userInfo = userLayer.GetByUserName(model.UserName);
            if(userInfo !=  null && userInfo.Id>0)
            {
                return Json(new Response() {Code = SystemCode.ErrorExist,Message="This user existed in system !" }, JsonRequestBehavior.AllowGet);
            }

            var userId = userLayer.Create(new Users()
            {
                UserName = model.UserName,
                Password = model.Password,
                AdminUserName = model.AdminUsername,
                CreatedBy = model.CreatedBy,
                CreatedDate = model.CreatedDate,
                DisplayName = string.IsNullOrEmpty(model.DisplayName) ? "" : model.DisplayName,
                Status = model.Status,
                GroupId = model.GroupId,
                DOB = model.DOB.GetValueOrDefault(DateTime.MinValue),
                Email = model.Email
            });
            var response = new Response();
            response.Code = SystemCode.Success;
            response.Message = "You created an new user successfully";
            return Json(response, JsonRequestBehavior.DenyGet);
        }
        [NonAction]
        public JsonResult Edit(UserSaveModel model)
        {
            if (string.IsNullOrEmpty(model.AdminUsername))
            {
                model.AdminUsername = "";
            }
            model.UpdatedBy = UserContext.UserName;
            model.UpdatedDate = DateTime.Now;
            model.Status = UserStatus.Avaiable.GetHashCode();
            var userLayer = new UserLayer();
            userLayer.Update(new Users()
            {
                Id = model.Id,
                UserName = model.UserName,
                AdminUserName = model.AdminUsername,
                UpdatedBy = model.UpdatedBy,
                UpdatedDate = model.UpdatedDate,
                DisplayName = string.IsNullOrEmpty(model.DisplayName) ? "" : model.DisplayName ,
                Status = model.Status,
                GroupId = model.GroupId,
                DOB = model.DOB.GetValueOrDefault(DateTime.MinValue),
                Email = model.Email
            });
            var response = new Response();
            response.Code = SystemCode.Success;
            response.Message = "You editted user successfully";
            return Json(response, JsonRequestBehavior.DenyGet);
        }
        [FilterAuthorize]
        public ActionResult UserProfile()
        {
            var room = "";
            var roomLeader = "";
            var team = "";
            var teamLeader = "";
            var userLayer = new UserLayer();
            var groupLayer = new GroupLayer();
            var user = userLayer.GetByUserName(UserContext.UserName);
            var lstAllGroup = groupLayer.GetAll();
            var group = lstAllGroup.Find(g => g.Id == user.GroupId);
            if (group != null)
            {
                if (group.ParentId > 0)
                {
                    var parent = lstAllGroup.Find(g => g.Id == group.ParentId);
                    if (parent != null)
                    {
                        room = parent.Name;
                        if (parent.Leader > 0)
                        {
                            var leader = userLayer.GetById(parent.Leader);
                            roomLeader = leader != null ? leader.DisplayName : string.Empty;
                        }
                    }
                    team = group.Name;
                    if (group.Leader > 0)
                    {
                        var leader = userLayer.GetById(parent.Leader);
                        teamLeader = leader != null ? leader.DisplayName : string.Empty;
                    }
                }
                else
                {
                    room = group.Name;
                    if (group.Leader > 0)
                    {
                        var leader = userLayer.GetById(group.Leader);
                        roomLeader = leader != null ? leader.DisplayName : string.Empty;
                    }
                }
            }
            ViewBag.currentUser = UserContext;
            ViewBag.Room = room;
            ViewBag.RoomLeader = roomLeader;
            ViewBag.Team = team;
            ViewBag.TeamLeader = teamLeader;

            if (!string.IsNullOrEmpty(user.Avatar))
            {
                ViewBag.url = user.Avatar;
            }
            return View(user);
        }
        [HttpPost]
        [FilterAuthorize]
        public JsonResult ChangeProfile(UserForChangeProfile user)
        {
            var userLayer = new UserLayer();
            var response = new Response();
            try
            {

                var newUser = userLayer.GetById(user.UserId.ToInt(0));
                if (newUser != null)
                {
                    newUser.UpdatedDate = DateTime.Now;
                    newUser.UpdatedBy = UserContext.UserName;
                    newUser.Email = user.Email;
                    newUser.Mobile = user.Mobile;
                    newUser.DisplayName = user.DisplayName;
                    newUser.DOB = user.DOB ?? DateTime.Now;
                    userLayer.UpdateProfile(newUser);

                    response.Code = SystemCode.Success;
                }
                else
                {
                    response.Code = SystemCode.Error;
                    response.Message = "Account not exits.";
                }
            }
            catch (Exception exception)
            {
                response.Code = SystemCode.Error;
                response.Message = exception.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [FilterAuthorize]
        public JsonResult ChangeAvatar(int userId, string avatar)
        {
            var userLayer = new UserLayer();
            var response = new Response();
            try
            {
                var newUser = userLayer.GetById(userId.ToInt(0));
                if (newUser != null)
                {
                    newUser.UpdatedDate = DateTime.Now;
                    newUser.UpdatedBy = UserContext.UserName;
                    var url = avatar;
                    newUser.Avatar = url;
                    userLayer.UpdateAvatar(newUser);
                    response.Data = avatar;
                }
                else
                {
                    response.Code = SystemCode.Error;
                    response.Message = "Tài khoản không tồn tại";
                }
            }
            catch (Exception exception)
            {
                response.Code = SystemCode.Error;
                response.Message = exception.Message;

            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult HideFromReport(int userId, string signatureToken)
        {
            var userLayer = new UserLayer();
            var response = new Response();
            try
            {
                // Kiểm tra tính xác thực của yêu cầu
                var validateResponse = ChecksumHelper.ValidateToken(new { Id = userId, CurrUser = UserContext.UserName }, signatureToken);
                if (validateResponse.IsValid)
                {
                    var newUser = userLayer.GetById(userId);
                    if (newUser != null)
                    {
                        userLayer.UpdateIsHiddenFromReport(userId, true);
                        return Json(new Response()
                        {
                            Code = SystemCode.Success,
                            Message = "Successfully!"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        response.Code = SystemCode.Error;
                        response.Message = "Tài khoản không tồn tại";
                    }
                }
                else
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.NotPermitted,
                        Message = Language.LanguageRes.Msg_NotPermitted
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception exception)
            {
                response.Code = SystemCode.Error;
                response.Message = exception.Message;

            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [FilterAuthorize]
        public JsonResult UnhideFromReport(int userId, string signatureToken)
        {
            var userLayer = new UserLayer();
            var response = new Response();
            try
            {
                var validateResponse = ChecksumHelper.ValidateToken(new { Id = userId, CurrUser = UserContext.UserName }, signatureToken);
                if (validateResponse.IsValid)
                {
                    var newUser = userLayer.GetById(userId);
                    if (newUser != null)
                    {
                        userLayer.UpdateIsHiddenFromReport(userId, false);
                        return Json(new Response()
                        {
                            Code = SystemCode.Success,
                            Message = "Successfully!"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        response.Code = SystemCode.Error;
                        response.Message = "Tài khoản không tồn tại";
                    }
                }
                else
                {
                    return Json(new Response()
                    {
                        Code = SystemCode.NotPermitted,
                        Message = Language.LanguageRes.Msg_NotPermitted
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception exception)
            {
                response.Code = SystemCode.Error;
                response.Message = exception.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult AllocateOTP(int userId, string userName, string signatureToken)
        {
            if (!UserContext.HasRole(RoleSystem.Admin))
            {
                return Json(new Response() 
                {
                    Code = SystemCode.NotPermitted,
                    Message = "You don't have a permission."
                }, JsonRequestBehavior.DenyGet);
            }

            var validateToken = ChecksumHelper.ValidateToken(new { Id = userId, CurrUser = UserContext.UserName }, signatureToken);
            if (validateToken.IsValid)
            {
                // Sinh mã OTP private key trong bảng user
                var randomString = Ultility.RandomString(5);
                // Cập nhật lại OTP private key cho nhân viên này
                var userLayer = new UserLayer();
                userLayer.UpdateOTPPrivateKey(userId, randomString);

                var secretKey = ConfigurationManager.AppSettings["OTPSecretKey"] + randomString;
                //Sinh ra ảnh QR code cho
                var googleOPTAuthenticator = new GoogleTOTP();
                var qRCodeImage = googleOPTAuthenticator.GenerateImage(secretKey, "BCRM.Indo." + userName);

                // Kiểm tra xem đã sinh mã OTP cho người dùng này chưa
                // nếu rồi thì xóa mã OTP cũ đi và sinh mã OTP mới
                var userAuthenticatorLayer = new UserOTPAuthenticatorLayer();
                if (userAuthenticatorLayer.GetByUserId(userId) != null)
                {
                    userAuthenticatorLayer.Delete(userId);
                }

                var link = string.Format("/OTPManager/GenerateQRCode?userName={0}", userName);
                userAuthenticatorLayer.Create(new UserOTPAuthenticator()
                {
                    UserId = userId,
                    UserName = userName,
                    Source = qRCodeImage,
                    Link = link,
                    ExpiredTime = DateTime.Now.AddMinutes(5)
                });
                var fullLink = ConfigurationManager.AppSettings["ServerDomain"].ToString() + link;
                return Json(new Response()
                {
                    Code = SystemCode.Success,
                    Data = fullLink
                }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(new Response()
                {
                    Code = SystemCode.NotPermitted,
                    Message = Language.LanguageRes.Msg_NotPermitted
                }, JsonRequestBehavior.DenyGet);
            }
        }

        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult GetTokenKeyForOneRequest(int id)
        {
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = ChecksumHelper.GenToken(new { Id = id, CurrUser = UserContext.UserName }, ConfigurationManager.AppSettings["TimeExpiredTokenForConfirmRequest"].ToString().ToInt(1))
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult GetTokenKeyForGeneralView(int id)
        {
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = ChecksumHelper.GenToken(new { Id = id, CurrUser = UserContext.UserName }, ConfigurationManager.AppSettings["TimeExpiredTokenForConfirmRequest"].ToString().ToInt(1))
            }, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [FilterRole(RoleSystem.Admin, RoleSystem.Manager)]
        public JsonResult GetTokenKeyForCreate(int id)
        {
            var model = new UserForCreateUpdateModel(id);
            var signatureChecksum = ChecksumHelper.GenToken<UserForCreateUpdateModel>(model);
            return Json(new Response()
            {
                Code = SystemCode.Success,
                Data = signatureChecksum
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
