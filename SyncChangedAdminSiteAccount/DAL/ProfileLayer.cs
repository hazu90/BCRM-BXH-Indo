﻿using BCRM.BXH.Indo.Library;
using Npgsql;
using SyncChangedAdminSiteAccount.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncChangedAdminSiteAccount.DAL
{
    public class ProfileLayer
    {
        public void Create(ProfileModel model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("service_profile_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", model.PhoneNumber));
                    command.Parameters.Add(new NpgsqlParameter("_phonealias", model.PhoneAlias));
                    command.Parameters.Add(new NpgsqlParameter("_name", model.Name));
                    command.Parameters.Add(new NpgsqlParameter("_positiontype", model.PositionType));
                    command.Parameters.Add(new NpgsqlParameter("_customerid", model.CustomerId));
                    command.Parameters.Add(new NpgsqlParameter("_email", model.Email));
                    command.Parameters.Add(new NpgsqlParameter("_avatar", model.Avatar));
                    command.Parameters.Add(new NpgsqlParameter("_createdby", model.CreatedBy));
                    command.Parameters.Add(new NpgsqlParameter("_createddate", model.CreatedDate));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Update(int id,string phoneNumber,string name)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("service_profile_update", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_phonenumber", phoneNumber));
                    command.Parameters.Add(new NpgsqlParameter("_name", name));
                    command.Parameters.Add(new NpgsqlParameter("_id", id));
                    command.ExecuteNonQuery();
                }
            }
        }

        public List<ProfileModel> GetByCustomer(int customerId)
        {
            var ret = new List<ProfileModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("service_profile_getbycustomerid", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var profile = new ProfileModel();
                            EntityBase.SetObjectValue(reader, ref profile);
                            if (profile != null)
                            {
                                ret.Add(profile);
                            }
                        }
                    }
                }
            }
            return ret;
        }
    }
}
