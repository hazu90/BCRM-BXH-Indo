﻿using BCRM.BXH.Indo.Library;
using CalculatingActiveCustomer.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatingActiveCustomer.DAL
{
    public class SyncBcrmBxhNAdminLayer
    {
        public List<SyncBcrmBxhNAdmin> GetByListEmailOrPhone(List<string> lstEmail, List<string> lstPhone)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_getlistemailorphone", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_lstemail", lstEmail));
                    command.Parameters.Add(new NpgsqlParameter("_lstphone", lstPhone));
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<SyncBcrmBxhNAdmin>();
                        while (reader.Read())
                        {
                            var syncModel = new SyncBcrmBxhNAdmin();
                            if (!reader.HasRows)
                            {
                                return new List<SyncBcrmBxhNAdmin>();
                            }

                            EntityBase.SetObjectValue(reader, ref syncModel);
                            ret.Add(syncModel);
                        }
                        return ret;
                    }
                }
            }
        }

        public List<SyncBcrmBxhNAdmin> GetByListAdminId(List<int> lstAdminId)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("syncbcrmbxhnadmin_getbyadminid", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_lstadminid", lstAdminId));
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<SyncBcrmBxhNAdmin>();
                        while (reader.Read())
                        {
                            var syncModel = new SyncBcrmBxhNAdmin();
                            if (!reader.HasRows)
                            {
                                return new List<SyncBcrmBxhNAdmin>();
                            }
                            EntityBase.SetObjectValue(reader, ref syncModel);
                            ret.Add(syncModel);
                        }
                        return ret;
                    }
                }
            }
        }
    }
}
