﻿using BCRM.BXH.Indo.Library;
using CalculatingActiveCustomer.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatingActiveCustomer.DAL
{
    public class CustomerActiveInMonthLayer
    {
        public CustomerActiveInMonth GetByCustomerIdAndMonth(int customerId,int month)
        {
            var ret = new CustomerActiveInMonth();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customeractiveinmonth_getbycustomeridandmonth", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_id", customerId));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            EntityBase.SetObjectValue(reader, ref ret);
                        }
                    }
                }
            }
            return ret;
        }

        public void Create(CustomerActiveInMonth model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customeractiveinmonth_create", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", model.CustomerId));
                    command.Parameters.Add(new NpgsqlParameter("_month", model.Month));
                    command.Parameters.Add(new NpgsqlParameter("_postingcount", model.PostingCount));
                    command.Parameters.Add(new NpgsqlParameter("_logincount", model.LoginCount));
                    command.Parameters.Add(new NpgsqlParameter("_isactive", model.IsActive));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Update(CustomerActiveInMonth model)
        {
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("customeractiveinmonth_update", true))
                {
                    command.Parameters.Add(new NpgsqlParameter("_customerid", model.CustomerId));
                    command.Parameters.Add(new NpgsqlParameter("_month", model.Month));
                    command.Parameters.Add(new NpgsqlParameter("_postingcount", model.PostingCount));
                    command.Parameters.Add(new NpgsqlParameter("_logincount", model.LoginCount));
                    command.Parameters.Add(new NpgsqlParameter("_isactive", model.IsActive));
                    command.Parameters.Add(new NpgsqlParameter("_id", model.IsActive));
                    command.ExecuteNonQuery();
                }
            }
        }

    }
}
