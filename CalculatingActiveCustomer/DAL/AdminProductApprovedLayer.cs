﻿using BCRM.BXH.Indo.Library;
using CalculatingActiveCustomer.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatingActiveCustomer.DAL
{
    public class AdminProductApprovedLayer
    {
        public List<AdminProductApprovedInTimeModel> GetByTime(DateTime startDate, DateTime endDate)
        {
            var ret = new List<AdminProductApprovedInTimeModel>();
            using (var db = new PostgresSQL())
            {
                using (var command = db.CreateCommand("productapproved_getbytime", true))
                {
                    //Truyền tham số cho command
                    command.Parameters.Add(new NpgsqlParameter("_fromdate", startDate));
                    command.Parameters.Add(new NpgsqlParameter("_todate", endDate));
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cust = new AdminProductApprovedInTimeModel();
                            EntityBase.SetObjectValue(reader, ref cust);
                            if (cust != null)
                            {
                                ret.Add(cust);
                            }
                        }
                        return ret;
                    }
                }
            }
        }


    }
}
