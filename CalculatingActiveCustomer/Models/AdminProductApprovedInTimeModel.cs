﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatingActiveCustomer.Models
{
    public class AdminProductApprovedInTimeModel
    {
        public int AdminId { get; set; }
        public int ProductId { get; set; }
        public DateTime ApprovedDate { get; set; }
    }
}
