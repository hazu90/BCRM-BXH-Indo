﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatingActiveCustomer.Models
{
    public class CustomerActiveInMonth
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int Month { get; set; }
        public int PostingCount { get; set; }
        public int LoginCount { get; set; }
        public bool IsActive { get; set; }
    }
}
