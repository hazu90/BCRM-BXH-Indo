﻿using BCRM.BXH.Indo.Library;
using CalculatingActiveCustomer.DAL;
using CalculatingActiveCustomer.Models;
using Newtonsoft.Json;
using SyncChangedAdminSiteAccount;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CalculatingActiveCustomer
{
    public class Processor
    {
        private Processor()
        {

        }
        public static Processor GetInstance()
        {
            return new Processor();
        }

        public void Run()
        {
            Logger.GetInstance().Write("#####################################");
            Logger.GetInstance().Write(string.Format("Start: {0:yyyy/MM/dd HH:mm:ss:FFF}", DateTime.Now));

            try
            {
                var nowTime = DateTime.Now;
                var calculatedDate = nowTime.AddDays(-1);
                var calculatedMonth = nowTime.Month >= 10 ? (string.Format("{0}{1}", nowTime.Year, nowTime.Month)).ToInt(0) : (string.Format("{0}0{1}", nowTime.Year, nowTime.Month)).ToInt(0);

                var firstTimeInDay = new DateTime(calculatedDate.Year, calculatedDate.Month, calculatedDate.Day, 0, 0, 0);
                var lastTimeInDay = new DateTime(calculatedDate.Year, calculatedDate.Month, calculatedDate.Day, 23, 59, 59);
                    
                // Lấy các tài khoản có đăng tin trong tháng 
                var postingLayer = new AdminProductApprovedLayer();
                var lstPosting = postingLayer.GetByTime(firstTimeInDay, lastTimeInDay);
                // Lấy ra các tài khoản có đăng tin trong thời gian đang tìm kiếm
                var adminPosting= lstPosting.GroupBy(o => o.AdminId).Select(o => new { AdminId = o.Key, Count = o.Count() }).ToList();
                // Danh sách adminid
                var lstAdminIdPosting = adminPosting.Select(o => o.AdminId).ToList();
                var syncLayer = new SyncBcrmBxhNAdminLayer();
                var lstSyncPosting = syncLayer.GetByListAdminId(lstAdminIdPosting);

                // Lấy các tài khoản có login trong khoảng thời gian tìm kiếm
                var apiGetLoginData = CallAPITraceCustomerLogin(firstTimeInDay, lastTimeInDay);
                if(apiGetLoginData == null)
                {
                    Logger.GetInstance().Write(string.Format("End: {0:yyyy/MM/dd HH:mm:ss:FFF}", DateTime.Now));
                }
                var lstLogin = apiGetLoginData.Data;
                var lstAdminLoginInTime = new List<SyncBcrmBxhNAdmin>();
                var lstEmail = lstLogin.Where(o => o.PhoneOrEmail.ToLower().ToInt(0) == 0).Select(o => o.PhoneOrEmail).ToList();
                var lstPhone = lstLogin.Where(o => o.PhoneOrEmail.ToLower().ToInt(0) > 0).Select(o => o.PhoneOrEmail).ToList();
                
                var lstSyncLogin = syncLayer.GetByListEmailOrPhone(lstEmail, lstPhone);

                var lstAllSync = (new List<SyncBcrmBxhNAdmin>());
                lstAllSync.AddRange(lstSyncPosting);
                lstAllSync.AddRange(lstSyncLogin);

                var lstAllCustomerIdSync = lstAllSync.Select(o => o.CustomerId).Distinct().ToList();
                
                // Tính ra được tài khoản có login trong khoảng thời gian tìm kiếm hay không
                // và đăng bao nhiêu tin trong khoảng thời gian tìm kiếm 
                var lstCustomerPostingOrLogin = new List<CustomerActiveInMonth>();
                var customerActiveInMonthLayer = new CustomerActiveInMonthLayer();
                foreach (var customerIdItem in lstAllCustomerIdSync)
                {
                    var syncInfo = lstAllSync.Find(o => o.CustomerId == customerIdItem);
                    // Tính số tin đăng
                    var postings = lstPosting.FindAll(o => o.AdminId == syncInfo.AccountId);
                    // Tính số lần login của 
                    var logins = lstLogin.Find(o => (!string.IsNullOrEmpty(syncInfo.PhoneNumber) && syncInfo.PhoneNumber.Substring(1).Equals(o.PhoneOrEmail))
                                                 || (!string.IsNullOrEmpty(syncInfo.Email) && syncInfo.Email.ToLower().Equals(o.PhoneOrEmail.ToLower())));
                    // Nếu đã có thông tin tính toán tính active cũ thì cộng lại với thông tin mới
                    var customerActive = customerActiveInMonthLayer.GetByCustomerIdAndMonth(customerIdItem, calculatedMonth);

                    var postingCount = customerActive == null ? postings.Count : (customerActive.PostingCount + postings.Count);
                    var loginCount = customerActive == null ? 0 : customerActive.LoginCount;
                    loginCount += customerActive == null ? 0 : logins.TotalLoggedInInDay;
                    var isActive = (postingCount >= 2 || loginCount >= 2) ? true : false;
                    var customerActiveInMonth = new CustomerActiveInMonth() 
                    {
                        CustomerId = syncInfo.CustomerId,
                        Month = calculatedMonth,
                        IsActive = isActive,
                        LoginCount = loginCount,
                        PostingCount = postingCount,
                        Id = customerActive == null ? 0 : customerActive.Id
                    };
                    lstCustomerPostingOrLogin.Add(customerActiveInMonth);
                }

                foreach (var item in lstCustomerPostingOrLogin)
                {
                    if(item.Id == 0)
                    {
                        customerActiveInMonthLayer.Create(item);
                    }
                    else
                    {
                        customerActiveInMonthLayer.Update(item);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.GetInstance().Write(ex.Message);
            }
            finally
            {
                Logger.GetInstance().Write(string.Format("End: {0:yyyy/MM/dd HH:mm:ss:FFF}", DateTime.Now));
                MemoryManagement.FlushMemory();
            }

        }


        public APICustomerLoginTimeResponse CallAPITraceCustomerLogin(DateTime fromDate, DateTime toDate)
        {
            var data = new
            {
                webId = 7,
                startDate = fromDate.ToString("yyyyMMdd").ToInt(0),
                endDate = toDate.ToString("yyyyMMdd").ToInt(0),
                threshHold = 1
            };
            var url = string.Format("{0}?param={1}", ConfigurationManager.AppSettings["APITraceCustomerLogin"]
                                                   , HttpUtility.UrlEncode(AESEncryption.EncryptString(JsonConvert.SerializeObject(data))));
            var res = BCRM.BXH.Indo.Library.Common.MakePostRequest(url, JsonConvert.SerializeObject(new { }), "application/json");
            if (string.IsNullOrEmpty(res))
            {
                return null;
            }
            try
            {
                var resCode = JsonConvert.DeserializeObject<APICustomerLoginTimeResponse>(res);
                return resCode;
            }
            catch
            {
                return null;
            }
        }

    }
}
